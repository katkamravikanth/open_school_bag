<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

//$route['default_controller'] = "home";
$route['default_controller'] = "products";
$route['404_override'] = 'products';
//$route['404_override'] = 'home';
$route['u/(:any)'] = "home/index/$1";
$route['logout'] = "home/logout";

$route['about'] = "page/about";
$route['partners'] = "page/partners";
$route['for_buyers'] = "page/buyers";
$route['for_sellers'] = "page/sellers";
$route['privacy_policy'] = "page/privacy_policy";
$route['terms_and_conditions'] = "page/tnc";
$route['faqs'] = "page/faqs";
$route['sitemap'] = "page/sitemap";
$route['contact'] = "page/contact";
$route['captcha'] = "page/captchajax";
$route['captcha_keyword'] = "page/captcha_keyword";

//cms pages
$route['p/(:any)'] = "page/cms_pages/$1";

// Login, Registration, Forgot and Activate

$route['login'] = "users/login";
$route['username'] = "users/username";
$route['email'] = "users/email";
$route['register'] = "users/register";
$route['forgot'] = "users/forgot";
$route['re_acrivate'] = "users/re_acrivate";
$route['activate/(:any)/(:any)'] = "users/activate/$1/$2";

// User profile
$route['dashboard'] = "users/dashboard";

$route['profile'] = "users/profile";
$route['profile/state/(:any)'] = "users/profile/$1";
$route['profile/update'] = "users/profile_update";
$route['profile_upload_file'] = "users/profile_upload_file";

$route['make_seller'] = "users/make_seller";

$route['publish_profile'] = "users/publish_profile";
$route['publish_profile/state/(:any)'] = "users/publish_profile/$1";

$route['cpass'] = "users/cpass";
$route['cpword'] = "users/cpword";

// User links

$route['my_notify'] = "users/notifications";
$route['my_notify/(:num)'] = "users/notifications/$1";

$route['my_favourite'] = "users/my_favourite";
$route['my_favourite/(:num)'] = "users/my_favourite/$1";

$route['my_wishlist'] = "users/my_wishlist";
$route['my_wishlist/(:num)'] = "users/my_wishlist/$1";

$route['my_orders'] = "orders/my_orders";
$route['my_orders/(:num)'] = "orders/my_orders/$1";

$route['my_order/from/(:any)/to/(:any)'] = 'orders/my_orders_p/$1/$2';
$route['my_order/from/(:any)/to/(:any)/(:num)'] = 'orders/my_orders_p/$1/$2/$3';

$route['download/(:any)'] = "products/download/$1";

$route['my_sales'] = "orders/my_sales";
$route['my_sales/(:num)'] = "orders/my_sales/$1";

$route['mysale/(:any)'] = "orders/order_view/$1";

$route['my_sale/from/(:any)/to/(:any)'] = 'orders/my_sales_p/$1/$2';
$route['my_sale/from/(:any)/to/(:any)/(:num)'] = 'orders/my_sales_p/$1/$2/$3';

$route['change_status'] = "orders/order_status";

$route['ship_address'] = "orders/ship_address";

// Seller

$route['seller/(:any)'] = "users/seller/$1";
$route['reports/(:any)/from/(:any)/to/(:any)'] = "users/seller/$1/$2/$3/$4/$5";
$route['reports/(:any)/from/(:any)/to/(:any)/(:num)'] = "users/seller/$1/$2/$3/$4/$5/$6";

//$route['seller/(:any)/(:num)'] = "users/seller/$1/$2";

// Products

$route['products'] = "products";
$route['products/(:num)'] = "products/index/$1";

$route['my_products'] = "products/my_products";
$route['my_products/state/(:any)'] = "products/my_products/$1";
$route['my_products/s/(:any)'] = "products/my_products/$1";
$route['my_products/(:num)'] = "products/my_products/$1";

$route['product_upload_file'] = "products/upload_file";
$route['preview_file'] = "products/preview_file";

$route['products/add'] = "products/add_product";
$route['products/edit/(:any)'] = "products/add_product/$1";

$route['additproduct'] = "products/addit_product";

$route['products/delete/(:any)'] = "products/del_product/$1";

$route['level_subjects'] = "products/level_subjects";
$route['subjects'] = "products/subjects";
$route['subject'] = "products/subject";

// Product View
$route['product/(:any)'] = "products/product_view/$1";
$route['product/(:any)/no'] = "products/product_view/$1";
$route['star'] = "products/star";

$route['add_review'] = "products/add_review";
$route['delete_review'] = "products/delete_review";

$route['add_qna'] = "products/add_qna";
$route['add_qna_reply'] = "products/add_qna_reply";
$route['delete_qna'] = "products/delete_qna";

// Reports

$route['reports'] = "reports";
$route['reports/(:any)/from/(:any)/to/(:any)'] = 'reports/index/$1/$2/$3/$4/$5';
$route['reports/(:any)/from/(:any)/to/(:any)/(:num)'] = 'reports/index/$1/$2/$3/$4/$5/$6';
//$route['reports/(:any)/(:num)'] = "reports/$1";

// Search

$route['search'] = "products/search";
$route['search/(:num)'] = "products/search/$1";
$route['products/(:any)'] = "products/search/$1";
$route['products/(:any)/(:num)'] = "products/search/$1/$2";

// Cart

$route['cart'] = "products/cucart";
$route['add_cart'] = "products/add_cart";
$route['cart_cp'] = "products/cart_cp";
$route['remove_cart'] = "products/remove_cart";
$route['clear_cart'] = "products/clear_cart";

//using promo code
$route['redeem_it'] = "orders/redeem_it";
$route['redeem_user'] = "orders/redeem_user";

$route['shop'] = "shop";
$route['shop/place/(:any)'] = "shop/index/$1";
$route['place_order'] = "orders/place_order";
$route['ship'] = "orders/ship";














// Admin Panel

$route['admin'] = "admin/admin";

$route['admin/forgot'] = "admin/admin/forgot";

$route['admin/dashboard'] = "admin/admin";

$route['admin/settings/site'] = "admin/admin/settings";
$route['admin/settings/profile'] = 'admin/admin/profile';
$route['admin/settings/config'] = 'admin/admin/config';

$route['admin/settings/cpass'] = 'admin/admin/cpass';
$route['admin/settings/cpword'] = 'admin/admin/cpword';
$route['admin/logout'] = 'admin/admin/logout';

$route['admin/notifications'] = 'admin/admin/notifications';
$route['admin/notifications/(:num)'] = 'admin/admin/notifications/$1';

$route['admin/login'] = 'admin/admin';
$route['admin/validate_credentials'] = 'admin/admin/validate_credentials';

//promotions
$route['admin/promotions'] = 'admin/admin_promo';
$route['admin/promotions/ss/(:any)'] = 'admin/admin_promo/index/$1';
$route['admin/promotions/ss/(:any)/(:num)'] = 'admin/admin_promo/index/$1/$2';
$route['admin/promotions/ord/(:any)'] = 'admin/admin_promo/index/$1';
$route['admin/promotions/ord/(:any)/(:num)'] = 'admin/admin_promo/index/$1/$2';
$route['admin/promotions/ss/(:any)/ord/(:any)'] = 'admin/admin_promo/index/$1/$2';
$route['admin/promotions/ss/(:any)/ord/(:any)/(:num)'] = 'admin/admin_promo/index/$1/$2/$3';

$route['admin/promotions/add'] = 'admin/admin_promo/add';
$route['admin/promotions/update/(:any)'] = 'admin/admin_promo/update/$1';
$route['admin/promotions/delete/(:any)'] = 'admin/admin_promo/delete/$1';
$route['admin/promotions/(:any)'] = 'admin/admin_promo/index/$1'; //$1 = page number

//products
$route['admin/products'] = 'admin/admin_products';
$route['admin/products/ss/(:any)'] = 'admin/admin_products/index/$1';
$route['admin/products/ss/(:any)/(:num)'] = 'admin/admin_products/index/$1/$2';
$route['admin/products/ord/(:any)'] = 'admin/admin_products/index/$1';
$route['admin/products/ord/(:any)/(:num)'] = 'admin/admin_products/index/$1/$2';
$route['admin/products/ss/(:any)/ord/(:any)'] = 'admin/admin_products/index/$1/$2';
$route['admin/products/ss/(:any)/ord/(:any)/(:num)'] = 'admin/admin_products/index/$1/$2/$3';

$route['admin/products/add'] = 'admin/admin_products/add';
$route['admin/products/update/(:any)'] = 'admin/admin_products/add/$1';
$route['admin/products/delete/(:any)'] = 'admin/admin_products/delete/$1';
$route['admin/products/state/(:any)'] = 'admin/admin_products/index/$1'; //$1 = page number
$route['admin/products/(:num)'] = 'admin/admin_products/index/$1'; //$1 = page number

$route['admin/products/rqna/(:any)'] = 'admin/admin_products/r_q_n/$1';

//users
$route['admin/users'] = 'admin/admin_users';
$route['admin/users/ss/(:any)'] = 'admin/admin_users/index/$1';
$route['admin/users/ss/(:any)/(:num)'] = 'admin/admin_users/index/$1/$2';
$route['admin/users/ord/(:any)'] = 'admin/admin_users/index/$1';
$route['admin/users/ord/(:any)/(:num)'] = 'admin/admin_users/index/$1/$2';
$route['admin/users/ss/(:any)/ord/(:any)'] = 'admin/admin_users/index/$1/$2';
$route['admin/users/ss/(:any)/ord/(:any)/(:num)'] = 'admin/admin_users/index/$1/$2/$3';

$route['admin/users/add'] = 'admin/admin_users/add';
$route['admin/users/user_upload_file'] = 'admin/admin_users/user_upload_file';
$route['admin/users/update/(:num)'] = 'admin/admin_users/update/$1';
$route['admin/users/delete/(:num)'] = 'admin/admin_users/delete/$1';
$route['admin/users/(:any)'] = 'admin/admin_users/index/$1'; //$1 = page number

//orders
$route['admin/orders'] = 'admin/admin_orders';
$route['admin/orders/(:num)'] = 'admin/admin_orders/index/$1';

$route['admin/orders/search'] = 'admin/admin_orders/search';
$route['admin/orders/search/from/(:any)/to/(:any)'] = 'admin/admin_orders/search/$1/$2';
$route['admin/orders/search/from/(:any)/to/(:any)/(:num)'] = 'admin/admin_orders/search/$1/$2/$3';

$route['admin/orders/pname'] = 'admin/admin_orders/pname';
$route['admin/orders/pname/(:any)'] = 'admin/admin_orders/pname/$1';
$route['admin/orders/pname/(:any)/(:num)'] = 'admin/admin_orders/pname/$1/$2';

$route['admin/order/(:any)'] = 'admin/admin_orders/view/$1';

$route['admin/order_report'] = 'admin/admin_orders/orders_report';

//reports
$route['admin/reports'] = 'admin/admin_reports';
$route['admin/reports/orders_report'] = 'admin/admin_reports/orders_report';

$route['admin/reports/sales_report'] = 'admin/admin_reports/sales_report';
$route['admin/reports/sales_report_by'] = 'admin/admin_reports/sales_report_by';
$route['admin/reports/saller_report'] = 'admin/admin_reports/saller_report';
$route['admin/reports/seller_report_by'] = 'admin/admin_reports/seller_report_by';

$route['admin/reports/sales/from/(:any)/to/(:any)/by/(:any)'] = 'admin/admin_reports/index/$1/$2/$3';
$route['admin/reports/sales/from/(:any)/to/(:any)/by/(:any)/(:any)'] = 'admin/admin_reports/index/$1/$2/$3/$4';
$route['admin/reports/sales/from/(:any)/to/(:any)/by/(:any)/(:any)/(:num)'] = 'admin/admin_reports/index/$1/$2/$3/$4/$5';

$route['admin/reports/downloads_report'] = 'admin/admin_reports/downloads_report';

$route['admin/reports/downloads/from/(:any)/to/(:any)/by/(:any)/(:any)'] = 'admin/admin_reports/index/$1/$2/$3/$4';
$route['admin/reports/downloads/from/(:any)/to/(:any)/by/(:any)/(:any)/(:num)'] = 'admin/admin_reports/index/$1/$2/$3/$4/$5';

$route['admin/reports/products_report'] = 'admin/admin_reports/products_report';
$route['admin/reports/products_reports'] = 'admin/admin_reports/products_reports';

$route['admin/reports/products/from/(:any)/to/(:any)/by/(:any)'] = 'admin/admin_reports/index/$1/$2/$3';
$route['admin/reports/products/from/(:any)/to/(:any)/by/(:any)/(:num)'] = 'admin/admin_reports/index/$1/$2/$3/$4';

$route['admin/reports/products/from/(:any)/to/(:any)/by/(:any)/tag/(:any)'] = 'admin/admin_reports/index/$1/$2/$3/$4';
$route['admin/reports/products/from/(:any)/to/(:any)/by/(:any)/tag/(:any)/(:num)'] = 'admin/admin_reports/index/$1/$2/$3/$4/$5';

$route['admin/reports/promotions/from/(:any)/to/(:any)'] = 'admin/admin_reports/index/$1/$2';
$route['admin/reports/promotions/from/(:any)/to/(:any)/(:num)'] = 'admin/admin_reports/index/$1/$2/$3';

$route['admin/reports/promotions_report'] = 'admin/admin_reports/promotions_report';

$route['admin/reports/users_report'] = 'admin/admin_reports/users_report';


//Images
$route['admin/images'] = 'admin/admin_images';
$route['admin/images/add'] = 'admin/admin_images/add';
$route['admin/images/update'] = 'admin/admin_images/update';
$route['admin/images/delete/(:any)'] = 'admin/admin_images/delete/$1';
$route['admin/images/(:num)'] = 'admin/admin_images/index/$1'; //$1 = page number

//Static pages
$route['admin/page/about'] = 'admin/admin_pages/about';
$route['admin/page/partners'] = 'admin/admin_pages/partners';
$route['admin/page/faqs'] = 'admin/admin_pages/faqs';
$route['admin/page/privacy_policy'] = 'admin/admin_pages/privacy_policy';
$route['admin/page/tnc'] = 'admin/admin_pages/tnc';

$route['admin/partner_upload_file'] = 'admin/admin_pages/partner_upload_file';

//CMS
$route['admin/cms/pages'] = 'admin/admin_cms';
$route['admin/cms/pages/state/(:any)'] = 'admin/admin_cms/index/$1';
$route['admin/page_slug'] = 'admin/admin_cms/page_slug';
$route['admin/cms/add'] = 'admin/admin_cms/add';
$route['admin/cms/update/(:any)'] = 'admin/admin_cms/update/$1';
$route['admin/cms/delete/(:any)'] = 'admin/admin_cms/delete/$1';
$route['admin/cms/pages/(:num)'] = 'admin/admin_cms/index/$1'; //$1 = page number

//levels
$route['admin/categories/levels'] = "admin/admin_categories/levels";
$route['admin/categories/levels/(:num)'] = "admin/admin_categories/levels/$1";
$route['admin/categories/s_level'] = "admin/admin_categories/s_level";
$route['admin/categories/e_level'] = "admin/admin_categories/e_level";
$route['admin/categories/d_level/(:num)'] = "admin/admin_categories/d_level/$1";

//subjects
$route['admin/categories/subjects'] = "admin/admin_categories/subjects";
$route['admin/categories/subjects/(:num)'] = "admin/admin_categories/subjects/$1";

$route['admin/categories/s_subject'] = "admin/admin_categories/subjects_add";
$route['admin/categories/e_subject/(:num)'] = "admin/admin_categories/subjects_edit/$1";

$route['admin/categories/d_subject/(:num)'] = "admin/admin_categories/d_subjects/$1";

//resources
$route['admin/categories/resources'] = "admin/admin_categories/resources";
$route['admin/categories/resources/(:any)'] = "admin/admin_categories/resources/$1";
$route['admin/categories/resources/(:num)'] = "admin/admin_categories/resources/$1";

$route['admin/categories/s_resource'] = "admin/admin_categories/resources_add";
$route['admin/categories/e_resource/(:num)'] = "admin/admin_categories/resources_edit/$1";

$route['admin/categories/d_resource/(:num)'] = "admin/admin_categories/d_resources/$1";

/* End of file routes.php */
/* Location: ./application/config/routes.php */