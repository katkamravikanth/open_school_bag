<link href="files/css/home.css" rel="stylesheet" type="text/css">

<article class="content">
  <?php if(count($top_products)){?>
  <h1 class="tops">Top Sellers</h1>
  <section class="top_sellers">
    <?php foreach($top_products as $prod){?>
		<?php if($prod['pro']->product_pic!=''){$pic = 'files/'.$prod['pro']->product_pic;}else{
            if($prod['pro']->product_resource == 'Assessment Book'){$pic = 'files/images/files/assessment-books.png';
            }else if($prod['pro']->product_resource == 'Guidebooks'){$pic = 'files/images/files/guide-books.png';
            }else if($prod['pro']->product_resource == 'Storybooks'){$pic = 'files/images/files/story-books1.png';
            }else if($prod['pro']->product_resource == 'Ten Year Series'){$pic = 'files/images/files/10-year-series.png';
            }else if($prod['pro']->product_resource == 'Enrichment'){$pic = 'files/images/files/enrichment.png';
            }else if($prod['pro']->product_resource == 'Educational Kits and Games'){$pic = 'files/images/files/story-books2.png';
            }else if($prod['pro']->product_resource == 'Audio'){$pic = 'files/images/files/audio.png';
            }else if($prod['pro']->product_resource == 'Video'){$pic = 'files/images/files/video.png';  
          	}else{$pic = 'files/images/files/folder.png';}
          }?>
    <div class="item">
	  <a href="product/<?php echo $prod['pro']->product_unique;?>">
     	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="190">
                <tr>
                  <td valign="middle"> <img src="<?php echo $pic;?>" alt="<?php echo $prod['pro']->product_name;?>" title="<?php echo $prod['pro']->product_name;?>" style="max-height:206px; max-width:187px;" /></td>
                </tr>
        </table>
      	<div class="details"><?php echo stripslashes(substr($prod['pro']->product_name, 0, 10));?> <span style="line-height:20px;"><?php if($prod['disc'] != 0){?><div style="height:5px;">&nbsp;</div><del style="color:#000;">S$<?php echo number_format($prod['pro']->product_price, 2);?></del><br>S$ <?php echo number_format($prod['pro']->product_price - $prod['disc'], 2);}else{?><div style="height:15px;">&nbsp;</div>S$ <?php echo number_format($prod['pro']->product_price, 2);}?></span></div>
      </a> </div>
    <?php }?>
  </section>
  <?php }if(count($promos)){?>
  <h1 class="prom">Promotions</h1>
  <section class="promotions">
  	<?php $j=0; $i=1; foreach($promos as $promo){?>
    <div class="promo<?php echo $i;?>"> <?php if($promobg[$j]->photo == ''){?><img src="files/<?php echo $promobg[0]->photo;?>" alt="" /><?php }else{?><img src="files/<?php echo $promobg[$j]->photo;?>" alt="" /><?php }?>
      <div class="details">
        <div class="ellipse <?php if($i==1){echo 'bg_red';}else{echo 'bg_green';}?>">
          <div class="l_ellipse">
            <div class="ll_ellipse"><?php if($promo->promo_percent_type == '%'){echo $promo->promo_percent.' '.$promo->promo_percent_type;}else{echo $promo->promo_percent_type.' '.$promo->promo_percent;}?><br />
              off</div>
          </div>
        </div>
        <!--<div class="title"><?php if($promo->product_title !=''){echo 'On Product : <br>'.$promo->product_title;}else{echo 'On Order of :<br> S$ '.$promo->promo_min_amount;}?></div>-->
        <div class="sub_title">&nbsp;</div>
      </div>
    </div>
    <?php $j++; $i++;if($i==3){$i=1;}}?>
  </section>
  <?php }?>
</article>