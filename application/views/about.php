<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="files/css/sliderman.css" />
<script type="text/javascript" src="files/js/sliderman.1.3.7.js"></script>
<article class="content">
  <h1 class="para-nav"><a href="<?php echo base_url();?>">Home</a> &raquo; <?php echo $title;?></h1>
  <!-------------- start main-content ----------->
  <div id="main-content">
    <h1 id="page_title"><?php echo $title;?></h1>
    <!-------------- start banner ----------->
    <div id="banner">
      <div id="SliderName"> 
      	<?php foreach($slider as $slide){?>
        <a href="#1"> <img src="files/<?php echo $slide->photo;?>" title="About Us" /> </a>
        <?php }?>
        <div id="SliderNameNavigation"></div>
        <script type="text/javascript">
			// we created new effect and called it 'demo01'. We use this name later.
			Sliderman.effect({name: 'about', cols: 10, rows: 5, delay: 10, fade: true, order: 'straight_stairs'});
			var demoSlider = Sliderman.slider({container: 'SliderName', width: 322, height: 227, effects: 'about',
			display: {
				pause: true, // slider pauses on mouseover
				autoplay: 3000, // 3 seconds slideshow
				always_show_loading: 200, // testing loading mode
				description: {background: 'none', opacity: 0.5, height: 50, position: 'bottom'}, // image description box settings
				loading: {background: '#000000', opacity: 0.2, image: 'files/images/loading.gif'}, // loading box settings
				buttons: {opacity: 1, prev: {className: 'SliderNamePrev', label: ''}, next: {className: 'SliderNameNext', label: ''}}, // Next/Prev buttons settings
				navigation: {container: 'SliderNameNavigation', label: '&nbsp;'} // navigation (pages) settings
			}});
		  </script>
        <div class="c"></div>
      </div>
      <div class="c"></div>
    </div>
    <!-------------- end banner ----------->
    <div id="content-div">
      <h1>Our Story</h1>
      <?php echo $page[0]->body;?> </div>
    <div style="clear:both"></div>
    <div id="content-div1" style="min-height:160px"> <?php echo $page1[0]->body;?><br> <?php echo $page2[0]->body;?></div>
    <div id="content-div" class="filetype" style="width:100% ">
      <h1>Our Products</h1>
      <ul>
      <?php foreach($icons as $icon){?>
        <li><img src="files/<?php echo $icon->photo;?>" alt="<?php echo $icon->title;?>" style="max-height:99px;" /><h1><?php echo $icon->title;?></h1></li>
      <?php }?>
      </ul>
    </div>
    <div style="clear:both"></div>
    <div id="sub_section">
      <h1 align="left" style="color:#e16b68; font-size:32px;">Who we care about and how can we help?</h1>
      <center>
      <?php foreach($who as $whow){?>
        <a href="#1"> <img src="files/<?php echo $whow->photo;?>" title="Who we care about and how can we help?" /> </a>
        <?php }?>
      </center>
    </div>
  </div>
  <!-------------- start main-content -----------> 
</article>