<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="files/js/jquery_ravikanth_contact.js"></script>
<style type="text/css">
#captchaimg img {
	width:230px !important;
	height:40px !important;
}
</style>
<article class="content">
  <h1> <a href="<?php echo base_url();?>">Home</a> &raquo; <?php echo $title;?></h1>
  <!-------------- start main-content ----------->
  <div id="main-content"> 
    <!----------- start contact page ---------------------->
    <div id="contact-page">
      <h1><?php echo $title;?></h1>
      <div>Do use our <a href="<?php echo base_url();?>faqs" style="text-decoration:underline;">FAQs</a> to answer your queries and frequently asked questions to save time.<br />
        <br />
        Can't find an answer from our <a href="<?php echo base_url();?>faqs" style="text-decoration:underline;">FAQs</a>? Interested in partnering with us? We welcome you to contact us! Please give us up to 3 working days to respond to you.</div>
      <!----------- start form -------------->
      <div class="form">
        <div class="cont_mess" style="display:none; margin-left:15px; margin-top:15px;"></div>
        <?php
		  //flash messages
		  if(isset($flash_message)){
			if($flash_message == TRUE)
			{
			  echo '<div class="success" style="color:#090; margin-top:20px; font-size:16px; font-weight:bold;">
					Submitted successfully! Thanks for contacting us we will get back to you soon</div>
					<script type="text/javascript">$(function(){$("input[type=\'text\'], input[type=\'email\'], textarea").val("");$("textarea").val("");});</script>';
			}else{
			  echo '<div class="error" style="margin-top:20px; color:#f30; font-size:16px; font-weight:bold;">× Oh snap! change a few things up and try submitting again.</div>';
			}
		  }
		  echo '<div class="error" style="margin-top:20px; color:#f30; font-size:16px; font-weight:bold;">';
		  echo validation_errors();
		  echo '</div>';
		  ?>
        <form action="contact" name="contact_form" id="contact_form" method="post">
          <table border="0" cellspacing="20">
            <tbody>
              <tr>
                <td valign="top" class="font">Name *</td>
                <td valign="top"><input type="text" class="text-box" name="name" id="name" size="30" value="<?php if($this->session->userdata('fullname')){echo $this->session->userdata('fullname');}else{echo set_value('name');}?>">
                  <div class="name_mess" style="display:none; padding-left:20px;"></div></td>
              </tr>
              <tr>
                <td valign="top" class="font">Email *</td>
                <td valign="top"><input type="email" class="text-box" name="email" id="email" size="30" value="<?php if($this->session->userdata('email')){echo $this->session->userdata('email');}else{echo set_value('email');}?>">
                  <div class="email_mess" style="display:none; padding-left:20px;"></div></td>
              </tr>
              <tr>
                <td valign="top" class="font">Contact *</td>
                <td valign="top"><input type="text" class="text-box" name="phone" id="phone" size="30" value="<?php if($this->session->userdata('phone')){echo $this->session->userdata('phone');}else{echo set_value('phone');}?>">
                  <div class="phone_mess" style="display:none; padding-left:20px;"></div></td>
              </tr>
              <tr>
                <td valign="top" class="font">Company</td>
                <td valign="top"><input type="text" class="text-box" name="company" id="company" size="30" value="<?php echo set_value('company');?>"></td>
              </tr>
              <tr>
                <td valign="top" class="font">Comment Type *</td>
                <td valign="top"><select class="text-box" name="comment_type" id="comment_type" style="width:90% !important">
                    <option value="">Select One</option>
                    <option <?php if(set_value('comment_type') == 'Login/Registration'){echo 'selected';}?>>Login/Registration</option>
                    <option <?php if(set_value('comment_type') == 'Product Information'){echo 'selected';}?>>Product Information</option>
                    <option <?php if(set_value('comment_type') == 'Payment'){echo 'selected';}?>>Payment</option>
                    <option <?php if(set_value('comment_type') == 'Suggestion'){echo 'selected';}?>>Suggestion</option>
                    <option <?php if(set_value('comment_type') == 'Feedback'){echo 'selected';}?>>Feedback</option>
                    <option <?php if(set_value('comment_type') == 'Others'){echo 'selected';}?>>Others</option>
                  </select>
                  <div class="comments_mess" style="display:none; padding-left:20px;"></div></td>
              </tr>
              <tr>
                <td valign="top" class="font">Comment *</td>
                <td valign="top"><textarea class="text-box1" name="comment" id="comment"><?php echo set_value('comment');?></textarea>
                  <div class="comment_mess" style="display:none; padding-left:20px;"></div></td>
              </tr>
              <tr>
                <td valign="top"><b>Security Check</b></td>
                <td valign="top"><p style="margin-left:25px; float:left;" id="captchaimg"><?php print_r($captcha);?></p>
               	  <img src="files/images/icons/update.jpg" width="16" alt="Rrefresh" id="refresh" style="float:left; margin-left:20px; margin-top:7px; cursor:pointer;" />
                  <input type="hidden" name="precap" id="precap" value="<?php echo $this->session->userdata('word');?>" /><br />
                  <div class="precap_mess" style="display:none; padding-left:20px; float:left; width:100%;"></div></td>
              </tr>
              <tr>
                <td valign="top" class="font">Text in the box:</td>
                <td valign="top"><input type="text" class="text-box" name="captcha" id="captcha"></td>
              </tr>
              <tr>
                <td valign="top" class="font">&nbsp;</td>
                <td valign="top"><input type="submit" id="ans-sub" name="contact_submit" value="SUBMIT" /></td>
              </tr>
            </tbody>
          </table>
        </form>
      </div>
      <!----------- end form --------------> 
    </div>
    <!----------- end contact page ----------------------> 
    
    <!------------ start right ------------->
    <div class="right">
      <h4><b>TELEPHONE</b></h4>
      <p style="margin:20px 0 0 20px; font-size:14px;"><?php echo $site->phone;?></p>
      <p style="margin:20px 0 0 20px; font-size:14px;">&nbsp;</p>
      <h4><b>EMAIL</b></h4>
      <P style="margin:0px 0 40px 20px; font-size:14px;"><a style="color:#ef4036 !important;" href="mailto:<?php echo $site->email;?>"><?php echo $site->email;?></a></p>
    </div>
    <!------------ end right -------------> 
  </div>
  <!-------------- start main-content -----------> 
</article>
