<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<link href="files/css/users/style.css" type="text/css" rel="stylesheet" />
<link href="files/css/users/style3.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="files/css/themes/alertify.core.css" />
<link rel="stylesheet" href="files/css/themes/alertify.default.css" id="toggleCSS" />
<script type="text/javascript" src="files/js/customSelect.jquery.min.js"></script>
<script src="files/js/lib/alertify.min.js"></script>
<script type="text/javascript">
$(function(){
	$(".custom_select select").customSelect();
	
	function reset () {
		$("#toggleCSS").attr("href", "files/css/themes/alertify.default.css");
		alertify.set({
			labels : {
				ok     : "OK",
				cancel : "Cancel"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}
	$('.status_update').click(function(e) {
		id = $(this).attr('name');
		status = $('#change_status_'+id).val();
		$.post('change_status', {order_id:id, status:status}, function(data){
			if(data == 'Status updated'){
				alertify.success(data);
			}else{
				alertify.error(data);
			}
		});
    });
});
</script>
<link rel="stylesheet" type="text/css" href="files/css/dropdown2.css" />

<article class="content">
  <h1> <a href="<?php echo base_url();?>">Home</a> &raquo; <?php echo $title;?></h1>
  <div style="background: url(files/images/users/bg1.gif) repeat; margin:0 auto;"> <?php echo $this->load->view('templates/navigation');?>
    <div class="containerinner">
      <p class="heading"><?php echo $title;?></p>
      <div class="purchases">
        <h3>Order date: <?php echo $orders[0]['ord']->created;?></h3>
        <?php foreach($orders as $ords){?>
        <?php if($ords['ord']->order_ptype == 'Physical'){?>
        <h1>
          <table width="70%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>Order #</td>
              <td class="order_no"><?php echo $orders[0]['ord']->order_no;?></td>
              <td>&nbsp;-&nbsp;</td>
              <td>
              <?php if($this->session->userdata('level') == 2 || $this->session->userdata('level') == 3 || $ords['ord']->order_status != 'Delivered'){?>
              <div class="input4" style="margin-bottom:-20px;">
                  <div class="custom_select">
                    <select name="change_status" id="change_status_<?php echo $ords['ord']->order_item_id;?>">
                      <option <?php if($ords['ord']->order_status == 'Pending'){?>selected<?php }?>>Pending</option>
                      <option <?php if($ords['ord']->order_status == 'Shipped'){?>selected<?php }?>>Shipped</option>
                      <!--<option <?php if($ords['ord']->order_status == 'Delivered'){?>selected<?php }?>>Delivered</option>-->
                    </select>
                  </div>
                </div>
                <?php }else{
					echo $ords['ord']->order_status;
				}?></td>
              <td><input type="button" class="status_update" name="<?php echo $ords['ord']->order_item_id;?>" value="Update" style="border:none; color:#fff; font-size:14px; cursor:pointer; background: #0b9444; border-radius: 3px; width: 83px; height: 25px;" /></td>
            </tr>
          </table>
        </h1>
        <p style="line-height:20px; font-size:16px; font-weight:bold; margin-bottom:0; margin-top:5px;">Product Name: <?php echo $ords['ord']->order_pname;?></p>
        <div class="purchases-inner">
          <h1>About This  Order: Order Information Invoices</h1>
          <div class="purchases-inner1">
            <div class="purchases-inner-left">
              <ul>
                <li><a class="first">Shipping Address</a></li>
                <li><a><?php echo $ords['ship'][0]->shipping_fname;?>,</a></li>
                <li><a><?php echo $ords['ship'][0]->shipping_address;?>,</a></li>
                <li><a><?php echo $ords['ship'][0]->shipping_city;?>.</a></li>
                <li><a>Tel: <?php echo $ords['ord']->user_phone;?></a></li>
              </ul>
            </div>
            <div class="purchases-inner-left1">
              <ul>
                <li><a class="first">Shipping Method</a></li>
                <li><a>Shipping Charge - Fixed</a></li>
              </ul>
            </div>
          </div>
        </div>
        <br />
        <?php }
		}?>
        <h1><b>Invoice #<?php echo $orders[0]['ord']->order_no;?></b></h1>
        <h2>Items Invoiced</h2>
        <div class="right3">
          <table width="100%" border="0" align="center" cellpadding="5" style="margin:0px 0 0px 0px; text-align:center;" cellspacing="0">
            <tr style="background:#f0eeea;">
              <th scope="col" align="center" class="td">Product Name</th>
              <th scope="col" align="center" class="td">Product Type</th>
              <th scope="col" align="center" class="td">Price</th>
              <th scope="col" align="center" class="td">Qty Invoiced</th>
              <th scope="col" align="center" class="td">Discount</th>
              <th scope="col" align="center" class="td">Subtotal</th>
            </tr>
            <?php $total = 0;foreach($orders as $ord){?>
            <tr>
              <td class="td" style="width:180px;"><?php echo $ord['ord']->order_pname;?></td>
              <td class="td" style="width:70px;"><?php echo $ord['ord']->order_ptype;?></td>
              <td class="td" style="width:70px;">S$ <?php echo number_format($ord['ord']->order_pprice, 2);?></td>
              <td class="td">1</td>
              <td class="td" style="width:70px;">S$ <?php if($ord['ord']->order_promo == ''){echo number_format($ord['ord']->order_discount, 2);}else{echo '0.00';}?></td>
              <td class="td">S$ <?php if($ord['ord']->order_promo == ''){echo number_format($ord['ord']->order_pprice - $ord['ord']->order_discount, 2);}else{echo number_format($ord['ord']->order_pprice, 2);}?></td>
            </tr>
            <?php }?>
          </table>
          <!--<h1 style="font-size:12px; float:right; background:none;">Subtotal: S$ <?php echo number_format($orders[0]['ord']->final_amount, 2);?><br />
          Transaction Fee: S$ <?php echo number_format($orders[0]['ord']->trans_fee, 2);?>
          </h1>
          <br />
          <br />
          <h3>Grand Total: S$ <?php echo number_format($orders[0]['ord']->final_amount + $orders[0]['ord']->trans_fee, 2);?></h3>-->
        </div>
      </div>
      <div class="clear"></div>
      <div class="clear"></div>
      <div class="clear"></div>
    </div>
  </div>
</article>