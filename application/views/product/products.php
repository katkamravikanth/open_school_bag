<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="files/css/themes/alertify.core.css" />
<link rel="stylesheet" href="files/css/themes/alertify.default.css" id="toggleCSS" />
<script src="files/js/lib/alertify.min.js"></script>
<script type="text/javascript" src="files/js/jquery_ravikanth_products.js"></script>
<style type="text/css">
input[type="text"], select, textarea {
	color:#000 !important;
	font-style:normal !important;
}
.submit {
	width: 120px;
	height: 35px;
	line-height:35px;
	display:block;
	background: #067936;
	border: none;
	text-align: center;
	color: #FFF !important;
	border-radius: 4px;
	font-size: 12px;
	font-weight: bold;
}
</style>
<article class="content">
  <div id="left-product">
    <h1><a href="<?php echo base_url();?>">Home</a> <?php if($title != ''){?>&raquo; <?php echo $title; }?>
      <?php if($this->uri->segment(2)!=''){?>
      &raquo; <?php echo urldecode($this->uri->segment(2));}?></h1>
    <!-------------- start main-content ----------->
    <div id="main-content">
      <div id="left-indiv">
        <h2><?php echo $title; ?></h2>
        <?php if(count($products)){
        foreach($products as $prod){?>
        <div class="product-1">
        <?php if($prod['avg'] != 0){?>
          <div class="porduct-star" align="right">
            <?php for($i=1;$i<=$prod['avg'];$i++){echo '<a class="icon star">&nbsp;</a> ';}?>
          </div>
          <?php }?>
          <a href="product/<?php echo $prod['pro']->product_unique;?>" class="<?php echo $prod['pro']->product_unique;?>">
          <div class="product-2">
            <?php if($prod['pro']->product_pic!=''){$pic = 'files/'.$prod['pro']->product_pic;
				foreach($resources as $sourc){
				  if($prod['pro']->product_resource == $sourc->resources){$pic1 = 'files/'.$sourc->icon;}
				}
			}else{
				foreach($resources as $source){
				  if($prod['pro']->product_resource == $source->resources){$pic = 'files/'.$source->icon; $pic1 = 'files/'.$source->icon;}
				}?>
            <?php }?>
            <center><img src="<?php echo $pic;?>" alt="<?php echo $prod['pro']->product_name;?>" style="max-height:109px; max-width:85%;" /></center>
            <h1><?php echo stripslashes(substr(str_replace('\n', ' ', $prod['pro']->product_name), 0, 19));?></h1>
            <p><?php echo stripslashes(substr(str_replace('\n', ' ', $prod['pro']->product_desc), 0, 45));?></p>
          </div>
          </a>
          <div class="footer <?php echo $prod['pro']->product_unique;?>">
            <?php if($prod['disc'] != 0){?>
                <input type="hidden" name="disc" id="disc_<?php echo $prod['pro']->product_unique;?>" value="<?php echo $prod['disc'];?>" />
                <h2 style="line-height:17px;"><del style="color:#f30">S$<?php echo number_format($prod['pro']->product_price, 2);?></del><br>
                S$<?php echo number_format($prod['pro']->product_price - $prod['disc'], 2);?></h2>
            <?php }else{?>
                <input type="hidden" name="disc" id="disc_<?php echo $prod['pro']->product_unique;?>" value="0" />
                <h2>S$<?php echo number_format($prod['pro']->product_price, 2);?></h2>
            <?php }
			
			if($prod['pro']->product_shipping != '' && $prod['pro']->product_shipping != 0){?>
                <input type="hidden" name="sprice" id="sprice_<?php echo $prod['pro']->product_unique;?>" value="<?php echo number_format($prod['pro']->product_shipping, 2);?>" />
            <?php }else{?>
                <input type="hidden" name="sprice" id="sprice_<?php echo $prod['pro']->product_unique;?>" value="0" />
            <?php }?>
            
            <img src="<?php echo $pic1;?>" class="file_type" style="max-width:24px; max-height:24px;" />
            <?php if($title != 'My Products' && $prod['pro']->user_id != $user_id){?>
                <a href="javascript:;" name="<?php echo $prod['pro']->product_unique;?>" class="add_cart"><h3>ADD</h3></a>
            
            <?php }if($this->session->userdata('level') == 2 && $prod['pro']->user_id == $user_id && $title == 'My Products'){?>
                    <a href="products/edit/<?php echo $prod['pro']->product_unique;?>" style="float:right; margin-top:10px;" title="Edit"><img src="files/images/icons/edit.png" title="Edit" /></a>
            <?php }?>
          </div>
        </div>
        <?php }?>
        <div id="cont"> <?php echo $pagination; ?> </div>
        <?php }else{?>
			<div style="font-size:16px;" align="center">No product posted<br><br>
            <?php if($title == 'My Products'){?>
                <a href="products/add" class="submit">Add Product</a>
            <?php }?></div>
		<?php }?>
      </div>
      <div id="right-indiv">
        <h1>My Cart</h1>
        <div id="cucart">&nbsp;</div>
        <h1>Categories</h1>
        <ol class="tree">
          <li>
            <label for="levels">Levels</label>
            <input type="checkbox" <?php foreach($levels as $leve){if(urldecode($this->uri->segment(2)) == $leve->level || $this->uri->segment(2) == ''){echo 'checked';}}?> id="levels" />
            <ol>
              <?php foreach($levels as $lev){?>
              <li class="categ" <?php if(urldecode($this->uri->segment(2)) == $lev->level){echo 'style="font-weight:bold;"';}?>><a href="products/<?php echo $lev->level;?>"><?php echo $lev->level;?></a></li>
              <?php }?>
            </ol>
          </li>
          <li>
            <label for="subjects">Subjects</label>
            <input type="checkbox" <?php foreach($subjects as $subj){if(urldecode($this->uri->segment(2)) == $subj->subjects){echo 'checked';}}?> id="subjects" />
            <ol>
              <?php foreach($subjects as $sub){?>
              <li class="categ" <?php if(urldecode($this->uri->segment(2)) == $sub->subjects){echo 'style="font-weight:bold;"';}?>><a href="products/<?php echo $sub->subjects;?>"><?php echo $sub->subjects;?></a></li>
              <?php }?>
            </ol>
          </li>
          <li>
            <label for="resource">Resource Type</label>
            <input type="checkbox" <?php foreach($resources as $sorce){if(urldecode($this->uri->segment(2)) == $sorce->resources){echo 'checked';}}?> id="resource" />
            <ol>
              <?php foreach($resources as $source){?>
              <li class="categ" <?php if(urldecode($this->uri->segment(2)) == $source->resources){echo 'style="font-weight:bold;"';}?>><a href="products/<?php echo $source->resources;?>"><?php echo $source->resources;?></a></li>
              <?php }?>
            </ol>
          </li>
        </ol>
      </div>
      <!----- end left-indiv ----------> 
    </div>
    <!----- end left-produc -----------> 
  </div>
  <!-------------- start main-content -----------> 
</article>