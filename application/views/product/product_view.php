<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<link href="files/css/layout.css" rel="stylesheet" type="text/css">
<link href="files/css/tab.css" rel="stylesheet" type="text/css">
<link href="files/css/products.css" rel="stylesheet" type="text/css">
<link href="files/css/jquery.rating.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="files/css/themes/alertify.core.css" />
<link rel="stylesheet" href="files/css/themes/alertify.default.css" id="toggleCSS" />
<script src="files/js/lib/alertify.min.js"></script>
<script type="text/javascript" src="files/js/jquery.rating.js"></script>
<script type="text/javascript" src="files/js/widgetkit.js"></script>
<script type="text/javascript" src="files/js/jquery.visualize.js"></script>
<script type="text/javascript" src="files/js/jquery_ravikanth_product_view.js"></script>
<?php if($this->uri->segment(3) == 'no'){?>
<script type="text/javascript">
$(function(){
	function reset () {
		$("#toggleCSS").attr("href", "files/css/themes/alertify.default.css");
		alertify.set({
			labels : {
				ok     : "OK",
				cancel : "Cancel"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}
	alertify.error("This product contain some unfinished orders");
});
</script>
<?php }?>
<style type="text/css">
#captchaimg img {
	width:230px !important;
	height:40px !important;
}
</style>

<?php if(!$this->session->userdata('logged_in')){$user_id=0;}?>

<div id="success" class="post_product" style="border-radius: 4px; background: rgba(255,255,255,0.8); padding: 30px; color:#000; max-width:650px !important">
  <div class="close"></div>
  <!-- close button of the register form -->
  <div id="message_post" align="center" style="padding-top:50px;">
  	<img src="" class="preview_image" alt="" style="max-width:600px; max-height:700px;" />
  </div>
</div>
<!-- END OF SUCCESS MESSAGE -->

<div id="background-on-popup" class="mask_post"></div>
<article class="content">
  <h1> <a href="<?php echo base_url();?>">Home</a> &raquo; <?php echo $title;?></h1>
  <div id="left-product"> 
    <!-------------- start main-content ----------->
    <div id="main-content"> 
      <!---------- start left-question page ---------->
      <?php if($product['pro']->product_pic!=''){$pic = 'files/'.$product['pro']->product_pic;
			foreach($resources as $sourc){
			  if($product['pro']->product_resource == $sourc->resources){$pic1 = 'files/'.$sourc->icon;}
			}
		}else{
			foreach($resources as $source){
			  if($product['pro']->product_resource == $source->resources){$pic = 'files/'.$source->icon; $pic1 = 'files/'.$source->icon;}
			}?>
      <?php }?>
      <div id="left-question"> <span class="doc" style="text-align:center"> <img src="<?php echo $pic1;?>" width="106" height="101" alt="documents" />
        <h1><?php echo $product['pro']->product_resource;?></h1>
        </span>
        <input type="hidden" name="hidden_id" id="hidden_id" value="<?php echo $user_id;?>"/>
        <input type="hidden" name="uhidden_id" id="uhidden_id" value="<?php echo $product['pro']->user_id;?>"/>
        <input type="hidden" name="hidden_product_unique" id="hidden_product_unique" value="<?php echo $product['pro']->product_unique;?>"/>
        <h2><?php echo $title;?></h2>
        <h3><?php echo stripslashes($product['pro']->product_name);?></h3>
        <div class="containerr">
          <ul class="tabs">
            <li>
              <div  class="tabs1_1">
                <div class="tabs1_2"> <a href="#tab1">Details</a> </div>
              </div>
            </li>
            <li>
              <div  class="tabs1_1">
                <div class="tabs1_2"> <a href="#tab2">Figures</a> </div>
              </div>
            </li>
            <li>
              <div  class="tabs1_1">
                <div class="tabs1_2"> <a href="#tab3">Reviews</a> </div>
              </div>
            </li>
            <li>
              <div  class="tabs1_1">
                <div class="tabs1_2"> <a href="#tab4">Ask a Question</a> </div>
              </div>
            </li>
          </ul>
          <div class="tab_container">
            <div id="tab1" class="tab_content">
              <div id="left-indiv">
                <div class="product-2">
                  <?php if(count($rate)){?>
                  <div class="porduct-star">
                    <?php $t=0; foreach($rate as $rat ){$t=$t+$rat->rate;}
					for($i = 1;$i <= 5;$i++){$s='';
		  			if ($i > round(count($rate)/$t)){$s = 'un';}?>
                    <img src="files/images/icons/<?php echo $s;?>star.png" style="border:0; margin:0; padding:0;" />
                    <?php }?>
                  </div>
                  <?php }?>
                  <table width="163" border="0" cellspacing="0" cellpadding="0" height="190">
                    <tr>
                      <td valign="middle" align="center"><img src="<?php echo $pic;?>" alt="<?php echo $product['pro']->product_name;?>" title="<?php echo $product['pro']->product_name;?>" style="max-height:150px; max-width:128px; float:none !important;" class="pic" /></td>
                    </tr>
                  </table>
                </div>
              </div>
              <!------------ Start tab1 (Details) -------------->
              <div class="Details">
                <h4><span class="pname"><?php echo stripslashes($product['pro']->product_name);?></span></h4>
                <?php $subject = explode(',', $product['pro']->product_subject);
				$subjects = ''; foreach($subject as $subj){$subjects .= $subj.', ';}?>
                
                <p class="desc"><strong>Level : </strong><?php echo $product['pro']->product_level;?><br />
                <strong>Subjects : </strong><?php echo rtrim($subjects, ', ');?></p>
                
				<?php if($product['pro']->product_shipping != '' && $product['pro']->product_shipping != 0){
					echo '<p><strong>Shipping charges : </strong><span style="font-size:18px; color:#090">S$ '.number_format($product['pro']->product_shipping, 2).'</span></p>';?>
                    <input type="hidden" name="sprice" id="sprice" value="<?php echo number_format($product['pro']->product_shipping, 2);?>" />
				<?php }else{ ?>
                <input type="hidden" name="sprice" id="sprice" value="0" />
                <?php }?>
                
                <p class="desc"><?php echo stripslashes(str_replace('\n', '<br />', $product['pro']->product_desc));?></p>
                <?php if($product['disc'] != 0){?>
	                <input type="hidden" name="disc" id="disc" value="<?php echo $product['disc'];?>" />
    	            <h5 style="line-height:25px;"><del style="color:#f30; font-size:15px;">S$ <?php echo number_format($product['pro']->product_price, 2);?></del><br>
        	        S$ <?php echo number_format($product['pro']->product_price - $product['disc'], 2);?></h5>
                <?php }else{?>
            	    <input type="hidden" name="disc" id="disc" value="0" />
                	<h5>S$ <?php echo number_format($product['pro']->product_price, 2);?></h5>
                <?php }?>
                
                <?php if($product['pro']->user_id != $user_id){?>
                	<input type="button" value="ADD" id="add_cart" title="Add to Cart" />
                <?php }
				if(($this->session->userdata('level') == 2 || $this->session->userdata('level') == 3) && $product['pro']->user_id == $user_id){?>
	                <img src="files/images/icons/edit.png" onclick="window.location='products/edit/<?php echo $product['pro']->product_unique;?>'" value="Edit Product" style="cursor:pointer" />
    	            <img src="files/images/icons/delete.png" id="delete" name="<?php echo $product['pro']->product_unique;?>" value="Delete Product" style="cursor:pointer" />
                <?php }
				echo '<div style="width:100%; float:left">&nbsp;</div>';
				if($product['pro']->product_preview != ''){
					$preview = explode(",", $product['pro']->product_preview);
					if(count($preview)){
						echo '<ul>';
						foreach($preview as $pview){
							echo '<li style="float:left; display:inline-block;"><img src="files/'.$pview.'" alt="" style="cursor:pointer; max-width:100px; max-height:100px;" onclick="popup(\''.$pview.'\')" /></li>';
						}
						echo '</ul>';
					}
				}
				?>
              </div>
            </div>
            <!------------ end tab1 (Details) --------------> 
            <!------------ Start tab2 (Figures) -------------->
            <div id="tab2" class="tab_content">
              <?php if($product['pro']->product_type == 'Digital'){?>
                  <ul>
                    <li>Product Type: <?php echo $product['pro']->product_type;?></li>
                    <li>File Format:
                      <?php $info = new SplFileInfo($product['pro']->product_file);
                    echo $info->getExtension();?>
                    </li>
                    <li>File Size:
                  <?php if($product['pro']->product_file != ''){
					echo round((filesize('files/'.$product['pro']->product_file)/1024)/1024, 3).' MB';
				}else{echo '0 bytes';};?>
                    </li>
                    <li>Pages: <?php echo $product['pro']->product_pages;?></li>
                  </ul>
              <?php }else if($product['pro']->product_type == 'Physical'){?>
                  <ul>
                    <li>Product Type: <?php echo $product['pro']->product_type;?></li>
                    <li>Resource Type: <?php echo $product['pro']->product_resource;?></li>
                    <li>Product Weight: <?php echo stripslashes($product['pro']->product_weight);?> grams</li>
                  </ul>
              <?php }?>
            </div>
            <!------------ end tab2 (Figures) --------------> 
            
            <!------------ start tab3 (Reviews) --------------> 
            <!-- Review section -->
            <div id="tab3" class="tab_content">
              <?php if(count($re_show)){?>
              <p id="msg_qna"></p>
              <form action="" name="" id="" method="post">
                <table border="0" width="100%">
                  <tbody>
                    <tr>
                      <td valign="top"><b>Rating  <a href="#" class="tooltip"><img src="files/images/icons/tool-tip.gif" style="border:0; padding:0; margin:0; float:none;"><span>Rate by selecting the number of stars the product deserves.</span></a> :</b></td>
                      <td><p style="padding-left:80px;">
                          <?php for($i = 1;$i <= 5;$i++){?>
                          <input class="auto-submit-star" type="radio" name="rating1" value="<?php echo $i;?>"/>
                          <?php }?>
                        </p></td>
                    </tr>
                    <tr>
                      <td valign="top"><b>Comment :</b></td>
                      <td valign="top"><textarea name="comment" id="comment" class="content-text"></textarea></td>
                    </tr>
                    <tr>
                      <td valign="top"><b>Security Check</b></td>
                      <td valign="top"><div style="margin-left:75px; float:left;" id="captchaimg"><?php print_r($captcha);?></div>
                      	<img src="files/images/icons/update.jpg" width="16" alt="Rrefresh" id="refresh" style="float:left; margin-left:20px; margin-top:7px; cursor:pointer;" />
                        <input type="hidden" name="precap" id="precap" value="<?php echo $this->session->userdata('word');?>" />
                      </td>
                    </tr>
                    <tr>
                      <td valign="top"><h5 style="font-size:10px; margin:10px 0 0 0px; font-weight:normal; padding:0; color:#999;">Text in the box:</h5></td>
                      <td valign="top"><div style="margin-left:60px;">
                          <input type="text" name="captcha" id="captcha" class="searc" size="26" value="">
                        </div></td>
                      <td valign="top"><input type="button" class="ans-sub" style="float:right;" name="submit" id="submit" value="Submit" /></td>
                    </tr>
                  </tbody>
                </table>
              </form>
              <?php }?>
              <div id="reviews-main"> 
                <!-- add the class .pagination to dynamically create a working pagination! The rel-attribute will tell how many items there are per page -->
                <?php if(count($review)){?>
                <table class="pagination" rel="5" width="100%">
                  <tbody>
                    <?php $j=2; foreach($review as $reviews){?>
                    <tr>
                      <td><div class="reviews-main1" style="position:relative; padding-top:8px; padding-bottom:8px;">
                      	<!--<img src="files/images/icons/delete.png" width="16" class="delete" name="<?php echo $reviews->review_id;?>" alt="Delete" style="float:right; margin-left:20px; cursor:pointer;" />-->
                          <div class="porduct-star" style="float:right; color:#000;">
                            <?php for($i = 1;$i <= 5;$i++){$s='';
				  			if ($i > $reviews->rate){$s = 'un';}?>
                            <img src="files/images/icons/<?php echo $s;?>star.png" style="border:0; margin:0; padding:0;" />
                            <?php }?>
                          </div>
                          <h4 style="margin:0; padding:3px;"><?php echo $reviews->user_username;?></h4>
                          <p style="margin-left:10px;"><?php echo $reviews->review;?></p>
                        </div></td>
                    </tr>
                    <?php $j++;}?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </tfoot>
                </table>
                <?php }else{
					echo '<div class="reviews-main1"><p style="margin-left:10px; margin-top:10px;">No reviews yet</p></div>';
				}?>
              </div>
            </div>
            <!------------ end tab3 (Reviews) --------------> 
            
            <!------------ start tab4 (Questions and Answers) --------------> 
            <div id="tab4" class="tab_content">
              <?php if($this->session->userdata('logged_in')){?>
              <p id="msg_qna"></p>
              <form action="" name="" id="" method="post">
                <table border="0">
                  <tbody>
                    <tr>
                      <td valign="top"><b>Question :</b></td>
                      <td valign="top"><textarea class="content-text" name="question" id="question"></textarea></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td valign="top"><input type="button" class="submit" name="add_submit" id="add_submit" value="submit"></td>
                    </tr>
                  </tbody>
                </table>
              </form>
              <?php }?>
              <div style="clear: both"></div>
              <div id="reviews-main">
                <?php if(count($qna)!=0){
					print_r($this->threaded->arrange($qna));
				}else{
					echo '<div align="center">Be the first to post a question</div>';
				}?>
              </div>
            </div>
            <!------------ end tab4 (Questions and Answers) --------------> 
          </div>
        </div>
        <div style="clear:both"></div>
        <div class="seller">
          <h3>About the Seller</h3>
          <div class="name">
            <div class="img">
              <?php if($selrate != 0){?>
              <div class="porduct-star" style="position:absolute; top:0; right:0; background:rgba(255,255,255,0.5)">
                <?php for($i = 1;$i <= 5;$i++){$s='';
		  			if ($i > round($selrate)){$s = 'un';}?>
                <img src="files/images/icons/<?php echo $s;?>star.png" style="border:0; margin:0; padding:0;" />
                <?php }?>
              </div>
              <?php }?>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" height="190">
                <tr>
                  <td valign="middle"><img src="<?php if(count($profile)){if($profile->profile_pic!=''){echo 'files/'.$profile->profile_pic;}else{echo 'files/profile/blank.png';}}else{echo 'files/profile/blank.png';}?>" alt="<?php echo $user->user_username;?>" title="<?php echo $user->user_username;?>" style="max-width:130px; max-height:150px" /></td>
                </tr>
              </table>
              </div>
            <h5 style="text-transform:capitalize;"><?php echo $user->user_username;?></h5>
          </div>
          <div class="right" style="min-height:170px;">
            <h4 style="text-transform:capitalize;"><?php echo $user->user_username;?></h4>
            <p style="padding-left:25px;padding-right:25px;">
              <?php if(count($profile)){
				if($profile->profile_qualifications!=''){
					echo '<div style="padding-left:10px;"><strong>Qualifications</strong> : '.$profile->profile_qualifications.'</div><br /><br />';
				}else{
					echo '<div style="padding-left:10px;"><strong>Qualifications</strong> : No details yet</div><br /><br />';
				}if($profile->profile_total_exp!=''){
					echo '<div style="padding-left:10px;"><strong>Total Experience</strong> : '.$profile->profile_total_exp.'</div><br /><br />';
				}else{
					echo '<div style="padding-left:10px;"><strong>Total Experience</strong> : No details yet</div><br /><br />';
				}if($profile->profile_special!=''){
					echo '<div style="padding-left:10px;"><strong>Specialization</strong> : '.$profile->profile_special.'</div><br /><br />';
				}else{
					echo '<div style="padding-left:10px;"><strong>Specialization</strong> : No details yet</div><br /><br />';
				}
			}else{echo 'No details yet';}?>
            </p>
            <a href="seller/<?php echo $user->user_username;?>">
            <h5>Go to Seller's profile page</h5>
            </a> </div>
        </div>
      </div>
      <!---------- end left-question page ---------->
      <div id="right-indiv">
        <h1>My Cart</h1>
        <div id="cucart">&nbsp;</div>
        <h1>Categories</h1>
        <ol class="tree">
          <li>
            <label for="levels">Levels</label>
            <input type="checkbox" <?php foreach($levels as $leve){if(urldecode($this->uri->segment(2)) == $leve->level || $this->uri->segment(2) == ''){echo 'checked';}}?> id="levels" />
            <ol>
              <?php foreach($levels as $lev){?>
              <li class="categ" <?php if(urldecode($this->uri->segment(2)) == $lev->level){echo 'style="font-weight:bold;"';}?>><a href="products/<?php echo $lev->level;?>"><?php echo $lev->level;?></a></li>
              <?php }?>
            </ol>
          </li>
          <li>
            <label for="subjects">Subjects</label>
            <input type="checkbox" <?php foreach($subjects as $subj){if(urldecode($this->uri->segment(2)) == $subj->subjects){echo 'checked';}}?> id="subjects" />
            <ol>
              <?php foreach($subjects as $sub){?>
              <li class="categ" <?php if(urldecode($this->uri->segment(2)) == $sub->subjects){echo 'style="font-weight:bold;"';}?>><a href="products/<?php echo $sub->subjects;?>"><?php echo $sub->subjects;?></a></li>
              <?php }?>
            </ol>
          </li>
          <li>
            <label for="resource">Resource Type</label>
            <input type="checkbox" <?php foreach($resources as $sorce){if(urldecode($this->uri->segment(2)) == $sorce->resources){echo 'checked';}}?> id="resource" />
            <ol>
              <?php foreach($resources as $source){?>
              <li class="categ" <?php if(urldecode($this->uri->segment(2)) == $source->resources){echo 'style="font-weight:bold;"';}?>><a href="products/<?php echo $source->resources;?>"><?php echo $source->resources;?></a></li>
              <?php }?>
            </ol>
          </li>
        </ol>
      </div>
      <!----- end left-indiv ----------> 
    </div>
    <!----- end left-produc -----------> 
  </div>
  <!-------------- start main-content -----------> 
</article>