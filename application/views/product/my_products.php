<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<link href="files/css/users/style.css" type="text/css" rel="stylesheet" />
<link href="files/css/users/style3.css" type="text/css" rel="stylesheet" />
<link href="files/css/my_products.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="files/css/themes/alertify.core.css" />
<link rel="stylesheet" href="files/css/themes/alertify.default.css" id="toggleCSS" />
<script src="files/js/lib/alertify.min.js"></script>
<script src="files/js/jquery.tipsy.js" type="text/javascript"></script>
<?php if($this->uri->segment(2) == 'state' && $this->uri->segment(3) == 'yes'){?>
<script type="text/javascript">
$(function(){
	function reset () {
		$("#toggleCSS").attr("href", "files/css/themes/alertify.default.css");
		alertify.set({
			labels : {
				ok     : "OK",
				cancel : "Cancel"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}
	alertify.success("This product has been deleted");
});
</script>
<?php }?>

<article class="content">
  <h1><a href="<?php echo base_url();?>">Home</a>
    <?php if($title != ''){?>
    &raquo; <?php echo $title; }?>
    <?php if($this->uri->segment(2)!=''){?>
    &raquo; <?php echo urldecode($this->uri->segment(2));}?></h1>
  <div style="background: url(files/images/users/bg1.gif) repeat; margin:0 auto;"> <?php echo $this->load->view('templates/navigation');?>
    <div class="containerinner" id="profile" style="height:900px;">
      <p class="heading"><?php echo $title;?></p>
      
      <?php if(count($products)){
        foreach($products as $prod){?>
      <div class="product-1" style="margin-left:20px;">
        <?php if($prod['avg'] != 0){?>
        <div class="porduct-star" align="right">
          <?php for($i=1;$i<=$prod['avg'];$i++){echo '<a class="icon star">&nbsp;</a> ';}?>
        </div>
        <?php }?>
        <a href="product/<?php echo $prod['pro']->product_unique;?>" class="<?php echo $prod['pro']->product_unique;?>">
        <div class="product-2">
          <?php if($prod['pro']->product_pic!=''){$pic = 'files/'.$prod['pro']->product_pic;
				foreach($resources as $sourc){
				  if($prod['pro']->product_resource == $sourc->resources){$pic1 = 'files/'.$sourc->icon;}
				}
			}else{
				foreach($resources as $source){
				  if($prod['pro']->product_resource == $source->resources){$pic = 'files/'.$source->icon; $pic1 = 'files/'.$source->icon;}
				}?>
          <?php }?>
          <center>
            <img src="<?php echo $pic;?>" alt="<?php echo $prod['pro']->product_name;?>" style="max-height:109px; max-width:85%;" class="pic" />
          </center>
          <h1><?php echo stripslashes(substr(str_replace('\n', ' ', $prod['pro']->product_name), 0, 19));?></h1>
          <span class="pname" style="display:none"><?php echo stripslashes($prod['pro']->product_name);?></span>
          <p><?php echo stripslashes(substr(str_replace('\n', ' ', $prod['pro']->product_desc), 0, 50));?></p>
        </div>
        </a>
        <div class="footer <?php echo $prod['pro']->product_unique;?>">
          <?php if($prod['disc'] != 0){?>
          <input type="hidden" name="disc" id="disc" value="<?php echo $prod['disc'];?>" />
          <h2 style="line-height:17px;"><del style="color:#f30">S$<?php echo number_format($prod['pro']->product_price, 2);?></del><br>
            S$<span class="price"><?php echo number_format($prod['pro']->product_price - $prod['disc'], 2);?></span></h2>
          <?php }else{?>
          <input type="hidden" name="disc" id="disc" value="0" />
          <h2>S$<span class="price"><?php echo number_format($prod['pro']->product_price, 2);?></span></h2>
          <?php }?>
          <img src="<?php echo $pic1;?>" class="file_type" style="max-width:24px; max-height:24px;" />
          <?php if($title != 'My Products' && $prod['pro']->user_id != $user_id){?>
          <a href="javascript:;" name="<?php echo $prod['pro']->product_unique;?>" class="add_cart">
          <h3>ADD</h3>
          </a>
          <?php }if($this->session->userdata('level') == 2 && $prod['pro']->user_id == $user_id && $title == 'My Products'){?>
          <a href="products/edit/<?php echo $prod['pro']->product_unique;?>" style="float:right; margin-top:10px;" title="Edit"><img src="files/images/icons/edit.png" title="Edit" /></a>
          <?php }?>
        </div>
      </div>
      <?php }?>
      <div id="cont"> <?php echo $pagination; ?> </div>
      <?php }else{?>
      <div style="font-size:16px;" align="center">No product posted<br>
        <br>
        <?php if($title == 'My Products'){?>
        <a href="products/add" class="submit">Add Product</a></div>
      <?php }?>
      <?php }?>
    </div>
  </div>
</article>