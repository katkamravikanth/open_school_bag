<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<link href="files/css/users/style.css" type="text/css" rel="stylesheet" />
<link href="files/css/users/style2.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="files/css/users/front.css" type="text/css" media="screen"/>
<script src="files/js/jquery.tipsy.js" type="text/javascript"></script>
<script type="text/javascript" src="files/js/jquery_ravikanth_users.js"></script>
<style type="text/css">
footer .col1 a, footer .col2 a, footer .col3 a, footer .col4 a {
	color: #333;
}
footer .col1 a:hover, footer .col2 a:hover, footer .col3 a:hover, footer .col4 a:hover {
	text-decoration: none;
}
.styled-select select {
	background: transparent;
	width: 100%;
	padding: 5px;
	font-size: 12px;
	color: #333333;
	line-height: 12px;
	border: 0;
	border-radius: 0;
	height: 25px;
	-webkit-appearance: none;
}
.styled-select {
	width: 85%;
	height: 25px;
	overflow: hidden;
	background: url(files/images/icons/select.gif) no-repeat right #fff;
	border: 1px solid #ccc;
}
input[type="text"] {
	width:85%;
}
@-moz-document url-prefix() {
	select {
		overflow:hidden;
		width: 112% !important;
		-moz-appearance: none !important;
	}
}
</style>
<article class="content">
  <h1><a href="<?php echo base_url();?>">Home</a> &raquo; <?php echo $title;?></h1>
  <div style="background: url(files/images/users/bg1.gif) repeat; margin:0 auto;"> <?php echo $this->load->view('templates/navigation');?>
    <div class="containerinner">
      <p class="heading"><?php echo $title;?></p>
      <div class="main">
        <div class="product1" style="background:none;">
          <div class="information">
            <div class="table">
              <fieldset style="border:0;">
                <br>
                <div class="mess" style="display:none; font-size:16px; font-weight:bold;"><img src="files/images/ajax-loader.gif" /></div>
                <form method="post" id="make_seller" action="make_seller">
                <input type="hidden" name="uid" id="uid" value="<?php echo $user->user_id;?>" />
                  <table>
                    <tr class="signintr">
                      <td class="signintd"> Bank Name : </td>
                      <td class="signininput"><div class="styled-select">
                          <select name="bank_name" id="bank_name" style="width:100%;">
                            <option value="">Select One</option>
                            <option <?php if(set_value('bank_name') == 'ANZ'){ echo 'selected';}?>>ANZ</option>
                            <option <?php if(set_value('bank_name') == 'Bank of China'){ echo 'selected';}?>>Bank of China</option>
                            <option <?php if(set_value('bank_name') == 'Citibank'){ echo 'selected';}?>>Citibank</option>
                            <option <?php if(set_value('bank_name') == 'DBS Bank'){ echo 'selected';}?>>DBS Bank</option>
                            <option <?php if(set_value('bank_name') == 'HSBC'){ echo 'selected';}?>>HSBC</option>
                            <option <?php if(set_value('bank_name') == 'Maybank'){ echo 'selected';}?>>Maybank</option>
                            <option <?php if(set_value('bank_name') == 'OCBC'){ echo 'selected';}?>>OCBC</option>
                            <option <?php if(set_value('bank_name') == 'POSB'){ echo 'selected';}?>>POSB</option>
                            <option <?php if(set_value('bank_name') == 'Standard Chartered Bank'){ echo 'selected';}?>>Standard Chartered Bank</option>
                            <option <?php if(set_value('bank_name') == 'UOB'){ echo 'selected';}?>>UOB</option>
                            <option <?php if(set_value('bank_name') == 'Others'){ echo 'selected';}?>>Others</option>
                          </select>
                        </div></td>
                    </tr>
                    <tr class="signintr bank_other" style="display:none">
                      <td class="signintd"> Other Bank Name : </td>
                      <td class="signininput"><input type="text" name="bank_other" id="bank_other" class="popupinput" /></td>
                    </tr>
                    <tr class="signintr">
                      <td class="signintd"> Bank Code : </td>
                      <td class="signininput"><input type="text" name="bank_code" id="bank_code" class="popupinput" /></td>
                    </tr>
                    <tr class="signintr">
                      <td class="signintd"> Bank Branch Code : </td>
                      <td class="signininput"><input type="text" name="bank_branch_code" id="bank_branch_code" class="popupinput" /></td>
                    </tr>
                    <tr class="signintr">
                      <td class="signintd"> Account Type <a href="#" class="tooltip"><img src="files/images/icons/tool-tip.gif" alt="" /><span>Savings Account, Current Account</span></a> : </td>
                      <td class="signininput"><input type="text" name="account_type" id="account_type" class="popupinput" /></td>
                    </tr>
                    <tr class="signintr">
                      <td class="signintd"> Account Number : </td>
                      <td class="signininput"><input type="text" name="account_no" id="account_no" class="popupinput" /></td>
                    </tr>
                    <tr class="signintr">
                      <td class="signintd"></td>
                      <td class="signininput"><input type="button" name="bank_save" id="bank_save" value="Save" class="save" style="color:#fff; cursor:pointer;" /></td>
                    </tr>
                  </table>
                </form>
              </fieldset>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</article>