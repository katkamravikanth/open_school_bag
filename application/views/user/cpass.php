<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<link href="files/css/users/style.css" type="text/css" rel="stylesheet" />
<link href="files/css/users/style3.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="files/admin/css/jquery.fileupload-ui.css" />
<script src="files/js/jquery.tipsy.js" type="text/javascript"></script>
<script type="text/javascript" src="files/js/jquery_ravikanth_users.js"></script>

<article class="content">
  <h1><a href="<?php echo base_url();?>">Home</a> &raquo; <?php echo $title;?></h1>
  <div style="background: url(files/images/users/bg1.gif) repeat; margin:0 auto;"> <?php echo $this->load->view('templates/navigation');?>
    <div class="containerinner" id="profile">
      <p class="heading"><?php echo $title;?></p>
      <br />
      <div class="mess" style="display:none; font-size:16px; font-weight:bold;">&nbsp;</div>
      <table width="100%" border="0" cellspacing="20" cellpadding="0" align="center">
        <tr>
          <th valign="top" class="fonts">Current Password :</th>
          <td valign="top"><input type="password" name="current" id="current" class="box" value="<?php echo set_value('name'); ?>" /></td>
        </tr>
        <tr>
          <th valign="top" class="fonts">New Password :</th>
          <td valign="top"><input type="password" name="new" id="new" class="box" value="<?php echo set_value('email'); ?>" /></td>
        </tr>
        <tr>
          <th valign="top" class="fonts">Confirm New Password :</th>
          <td valign="top"><input type="password" name="re_new" id="re_new" class="box" /></td>
        </tr>
        <tr>
          <th colspan="3" align="center"> <div class="right-botton">
              <div class="right-botton1">
                <div class="right-botton2">
                  <input type="submit" name="cpass" id="cpass" value="Change" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
                </div>
              </div>
            </div></th>
        </tr>
      </table>
    </div>
  </div>
</article>
