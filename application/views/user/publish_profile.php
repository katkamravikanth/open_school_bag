<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<link href="files/css/users/style.css" type="text/css" rel="stylesheet" />
<link href="files/css/users/style2.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="files/js/ajaxfileupload.js"></script>
<script src="files/js/jquery.tipsy.js" type="text/javascript"></script>
<script type="text/javascript" src="files/js/jquery_ravikanth_users.js"></script>
<script type="text/javascript" src="files/admin/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
$(function(){
	tinymce.init({selector:'textarea'});
	
	<?php if($this->uri->segment(2) == 'state' && $this->uri->segment(3) != ''){?>
		popup();
	<?php }
	if(isset($flash_message)){
		if($flash_message == TRUE){?>
			popup();
		<?php }
	}?>
	
	$("div.close, a.close, .mask_post").click(function() {
		disablePopup();  // function to close pop-up forms
	});
	function popup(){
		$(".success_product").fadeIn(300);
		$(".success_product").css('top', '15%');
		$(".mask_post").css("opacity", "0.7");
		$(".mask_post").fadeIn(300);
	}
	function disablePopup() {
		$(".success_product").fadeOut("normal");
		$(".mask_post").fadeOut("normal");
	}
})
</script>
<style type="text/css">
.profile_link {
	border: none;
	color: #fff !important;
	font-weight: bold;
	font-size: 14px;
	cursor: pointer;
	background: #0b9444;
	border-radius: 3px;
	width: 153px;
	height: 39px;
	display: inline-block;
	line-height: 39px;
	text-align: center;
}
.profile_link:hover {
	text-decoration: none;
}
</style>

<?php if(($this->uri->segment(2) == 'state' && $this->uri->segment(3) != '') || (isset($flash_message) && $flash_message == TRUE)){?>
<!--Success message popup-->
<div id="success" class="success_product" style="border-radius: 4px; background: rgba(255,255,255,0.8); padding: 30px; color:#000;">
  <div class="close" style="margin-top:-30px; margin-right:-30px;"></div>
  <!-- close button of the register form -->
  <div id="message_post" align="center" style="color:#090; margin-bottom:10px; text-align:left; font-size:16px; font-weight:bold;">
  <?php //flash messages
	if($this->uri->segment(2) == 'state' && $this->uri->segment(3) == 'yes'){
		echo '<div style="padding-bottom:20px; color:#090; font-size:16px; font-weight:bold;">';
		echo '<strong>Well done!</strong> Profile Picture successfully uploaded.<br /><br />';
		echo '<a href="seller/'.$user->user_username.'" target="_blank" class="profile_link" style="margin-left:120px;">Preview Profile</a>';
		echo '<a href="javascript:;" style="margin-left:20px;" class="close profile_link">Close</a>';
		echo '</div>';
	}else if($this->uri->segment(2) == 'state' && $this->uri->segment(2) == 'no'){
		echo '<div style="padding-bottom:20px; color:#f30; font-size:16px; font-weight:bold;">';
		echo '<a class="close" data-dismiss="alert">×</a>';
		echo '<strong>Oh snap!</strong> Please try later.';
		echo '</div>';          
	}
	if($this->uri->segment(2) == 'state' && $this->uri->segment(2) == 'nopic'){
		echo '<div style="padding-bottom:20px; color:#f30; font-size:16px; font-weight:bold;">';
		echo '<a class="close" data-dismiss="alert">×</a>';
		echo '<strong>Oh snap!</strong> please select picture';
		echo '</div>';
	}
	if(isset($flash_message)){
		if($flash_message == TRUE){
			echo '<div style="padding-bottom:20px; color:#090; font-size:16px; font-weight:bold;">';
			echo '<strong>Well done!</strong> You have successfully published your profile.<br /><br />';
			echo '<a href="seller/'.$user->user_username.'" target="_blank" class="profile_link" style="margin-left:90px;">Preview Profile</a>';
			echo '<a href="javascript:;" style="margin-left:20px;" class="close profile_link">Close</a>';
			echo '</div>';       
		}
	}?>
  </div>
</div>
<!-- END OF SUCCESS MESSAGE -->
<div id="background-on-popup" class="mask_post"></div>
<?php }?>

<article class="content">
  <h1><a href="<?php echo base_url();?>">Home</a> &raquo; <?php echo $title;?> <a href="seller/<?php echo $user->user_username;?>" target="_blank" class="profile_link" style="margin-top:43px; margin-right:10px; text-align:center; float:right;">Preview Profile</a></h1>
  <div style="background: url(files/images/users/bg1.gif) repeat; margin:0 auto;"> <?php echo $this->load->view('templates/navigation');?>
    <div class="containerinner" style="height:1250px;">
      <p class="heading"><?php echo $title;?></p>
      <div class="main">
        <div class="product1" style="width:100%; background:none;">
          <div class="profile_img">
            <div class="profile_img1" style="background:url(<?php if(count($profile)){if($profile->profile_pic){echo 'files/'.$profile->profile_pic;}else{echo 'files/profile/blank.png';}}else{echo 'files/profile/blank.png';}?>) center center no-repeat; background-size: 100% Auto;">
              <div class="profile_img2"> </div>
            </div>
          </div>
          <div align="left" style="margin-top:210px; padding-bottom:20px; padding-left:20px;">
            <a href="javascript:;" id="change_pic" class="profile_link" style="margin-left:0px;">Change Profile Pic</a><br>
            <form action="profile_upload_file" method="post" enctype="multipart/form-data" id="profile_pic" style="display:none; width:350px; margin:0;">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td colspan="3"><div class="mess" style="display:none;"><img src="files/images/ajax-loader.gif" /></div></td>
                </tr>
                <tr>
                  <td><input type="file" name="profile_pic" id="profile_pics" /><br /><small>Allowed extensions - .gif, .jpg, .jpeg, .png</small></td>
                  <td>&nbsp;</td>
                  <td><input type="submit" name="publish" id="publish" value="Update" style="border:none; color:#fff; font-size:14px; cursor:pointer; background: #0b9444; border-radius: 3px; width: 83px; height: 29px; display:none" /></td>
                </tr>
              </table>
            </form>
          </div>
          <form name="publish_form" id="publish_form" method="post">
            <input type="hidden" name="uid" id="uid" value="<?php echo $user->user_id;?>" />
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr height="40">
                <td colspan="2" valign="top"><a style="color:#c00; font-size:16px;">"*" are compulsory fields.</a>
                  <?php //flash messages
				  if(isset($flash_message)){
					if($flash_message == FALSE){
					  echo '<div style="padding-bottom:20px; color:#f30; font-size:16px; font-weight:bold;">';
						echo '<a class="close" data-dismiss="alert">×</a>';
						echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
					  echo '</div>';          
					}
				  }
				  echo '<div style="color:#f30; font-size:16px; font-weight:bold;">';
				  echo validation_errors('');
				  echo '</div>';
				  if(count($profile)){
					$qul = $profile->profile_qualifications;
					$texp = $profile->profile_total_exp;
					$spec = $profile->profile_special;
				  }else{
					$qul = '';
					$texp = '';
					$spec = '';
				  }
				  ?></td>
              </tr>
              <tr>
                <td valign="top">Qualifications <a style="color:#c00; font-size:16px;">*</a></td>
                <td><textarea name="qualific" id="qualific" cols="45"><?php echo $qul;?></textarea></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td valign="top">Total Experience <a style="color:#c00; font-size:16px;">*</a></td>
                <td><textarea name="total_exp" id="total_exp" cols="45"><?php echo $texp;?></textarea></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td valign="top">Specialization <a style="color:#c00; font-size:16px;">*</a></td>
                <td><textarea name="special" id="special" cols="45"><?php echo $spec;?></textarea></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td></td>
                <td><input type="submit" name="publish" id="publish" value="Publish" class="profile_link" /></td>
              </tr>
            </table>
          </form>
        </div>
      </div>
    </div>
  </div>
</article>
