<link rel="stylesheet" href="files/css/inner-pages.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="files/css/seller.css" type="text/css" media="screen"/>
<link rel="stylesheet" type="text/css" href="files/css/styles-1.css" />
<link rel="stylesheet" href="files/css/themes/alertify.core.css" />
<link rel="stylesheet" href="files/css/themes/alertify.default.css" id="toggleCSS" />
<!--[if IE 8 ]>
<style type="text/css">
.seller-profile-righttdiv #left-indiv .product-2 h1 {
	margin: 0 0 0 12px;
	color: #ef4036;
	padding: 0;
	letter-spacing: 1px;
	font-size: 11px !important;
}
</style>
<![endif]-->
<script src="files/js/lib/alertify.min.js"></script>
<script type="text/javascript">
$(function() {
	function reset () {
		$("#toggleCSS").attr("href", "files/css/themes/alertify.default.css");
		alertify.set({
			labels : {
				ok     : "OK",
				cancel : "Cancel"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}
	//add to cart
	$('#left-indiv .product-1 .footer .add_cart').click(function(e) {
		id = $(this).attr('name');
		sprce = $('.'+id+' #sprice').val();
		discacr = $('.'+id+' #disc').val();
		
		$.post('add_cart', {unique:id, disc:discacr, sprice:sprce}, function(data){
			if(data){
				$('#quick_links .login_cart .cart').load('cart_cp');
				alertify.success('Successfully added to cart');
			}else{
				alertify.error('Not added to cart');
			}
		});
    });
});
</script>
<article class="content">
  <h1><a href="<?php echo base_url();?>">Home</a> &raquo; <?php echo $title;?></h1>
  <div id="left-product"> 
    <!-------------- start main-content ----------->
    <div id="main-content"> 
      <!---------- start left-question page ---------->
      <div id="left-question">
        <div style="clear:both"></div>
        <div id="seller" style="width:800px;">
          <h2>Seller Profile</h2>
          <div class="name">
            <div class="img">
              <h5 style="border:none; margin:-24px 0 5px -28px; padding:0;"><?php echo $title;?></h5>
              <?php if($selrate != 0){?>
              <div class="porduct-star" style="margin:0 0 0 40px !important; width: 120px !important; position: absolute;">
                <?php for($i = 1;$i <= 5;$i++){$s='';
                if ($i > round($selrate)){$s = 'un';}?>
                <img src="files/images/icons/<?php echo $s;?>star.png" style="border:0; margin:0; padding:0;" />
                <?php }?>
              </div>
              <?php }?>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" height="190">
                <tr>
                  <td valign="middle"><img src="<?php if(count($profile) && $profile->profile_pic != ''){echo 'files/'.$profile->profile_pic;}else{echo 'files/profile/blank.png';}?>" alt="<?php echo $user->user_username;?>" title="<?php echo $user->user_username;?>" style="max-height:190px; max-width:135px;" /></td>
                </tr>
              </table>
            </div>
            <!---------------------- start seller-profile css ---------------------> 
          </div>
          <div class="seller-profile-right" style="margin:20px 20px;">
            <h4>Qualifications</h4>
            <div style="margin:0 14px; text-align:left;">
              <?php if(count($profile) && $profile->profile_qualifications != ''){echo $profile->profile_qualifications;}else{echo 'No details yet';}?>
            </div>
            <h5>Experience</h5>
            <div style="margin:0 14px; text-align:left;">
              <?php if(count($profile) && $profile->profile_total_exp != ''){echo $profile->profile_total_exp;}else{echo 'No details yet';}?>
            </div>
            <h4>Specialization</h4>
            <div style="margin:0 14px; text-align:left;">
              <?php if(count($profile) && $profile->profile_special != ''){echo $profile->profile_special;}else{echo 'No details yet';}?>
            </div>
          </div>
        </div>
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
            <!---------------------- start seller-profile-leftdiv-main css --------------------->
            <div class="seller-profile-leftdiv-main"> 
              
              <!---------------------- start seller-profile-leftdiv css --------------------->
              <div class="seller-profile-leftdiv">
                <h4>Comments</h4>
                <div id="main">
                  <ul id="holder">
                    <?php if(count($reviews)){?>
                    <!------------ start com1 --------->
                    <?php foreach($reviews as $review){?>
                    <li><?php echo $review->review; ?> <br />
                      <u style="text-align:right;">- <?php echo $user->user_username;?></u> </li>
                    <?php }?>
                    <!------------ end com1 --------->
                    <?php }else{echo '<li><div class="col10"><p style="margin:0px 14px; font-size:11px;">No comments posted</p></div></li>';}?>
                  </ul>
                </div>
              </div>
              
              <!---------------------- end seller-profile-leftdiv css ---------------------> 
              
              <!---------------------- start seller-profile-rightdiv css --------------------->
              
              <div class="seller-profile-righttdiv">
                <div id="left-indiv">
                  <h3><?php echo $title;?> Products</h3>
                  <?php if(count($products)){
                foreach($products as $prod){?>
                  <div class="product-1">
                    <?php if($prod['avg'] != 0){?>
                    <div class="porduct-star" align="right">
                      <?php for($i=1;$i<=$prod['avg'];$i++){echo '<a class="icon star">&nbsp;</a> ';}?>
                    </div>
                    <?php }?>
                    <a href="product/<?php echo $prod['pro']->product_unique;?>" class="<?php echo $prod['pro']->product_unique;?>">
                    <div class="product-2">
                      <?php if($prod['pro']->product_pic!=''){$pic = 'files/'.$prod['pro']->product_pic;
                        foreach($resources as $sourc){
                          if($prod['pro']->product_resource == $sourc->resources){$pic1 = 'files/'.$sourc->icon;}
                        }
                      }else{
                        foreach($resources as $source){
                          if($prod['pro']->product_resource == $source->resources){$pic = 'files/'.$source->icon; $pic1 = 'files/'.$source->icon;}
                        }?>
                      <?php }?>
                      <center>
                        <img src="<?php echo $pic;?>" alt="<?php echo $prod['pro']->product_name;?>" style="max-height:62px; max-width:62px;" class="pic" />
                      </center>
                      <h1><?php echo stripslashes(substr(str_replace('\n', ' ', $prod['pro']->product_name), 0, 16));?></h1>
                      <span class="pname" style="display:none"><?php echo stripslashes($prod['pro']->product_name);?></span>
                      <p style="font-size:11px !important;"><?php echo stripslashes(substr(str_replace('\n', ' ', $prod['pro']->product_desc), 0, 35));?></p>
                    </div>
                    </a>
                    <div class="footer <?php echo $prod['pro']->product_unique;?>">
                      <?php if($prod['disc'] != 0){?>
                      <input type="hidden" name="disc" id="disc" value="<?php echo $prod['disc'];?>" />
                      <h2 style="line-height:12px; margin-top:1px;"><del style="color:#f30">S$<?php echo number_format($prod['pro']->product_price, 2);?></del><br>
                        S$<?php echo number_format($prod['pro']->product_price - $prod['disc'], 2);?></h2>
                      <?php }else{?>
                      <input type="hidden" name="disc" id="disc" value="0" />
                      <h2>S$<?php echo number_format($prod['pro']->product_price, 2);?></h2>
                      <?php }
                      
                      if($prod['pro']->product_shipping != '' && $prod['pro']->product_shipping != 0){?>
                      <input type="hidden" name="sprice" id="sprice" value="<?php echo number_format($prod['pro']->product_shipping, 2);?>" />
                      <?php }else{?>
                      <input type="hidden" name="sprice" id="sprice" value="0" />
                      <?php }?>
                      
                      <img src="<?php echo $pic1;?>" class="file_type" style="max-width:15px; margin-left:-3px; margin-bottom:-3px;" />
                      <?php if($user_id == 0 || $user_id == '' || $prod['pro']->user_id != $user_id){?>
                      <a href="javascript:;" name="<?php echo $prod['pro']->product_unique;?>" class="add_cart">
                      <h3>ADD</h3>
                      </a>
                      <?php }?>
                    </div>
                  </div>
                  <?php }?>
                  <div id="cont"> <?php echo $pagination; ?> </div>
                  <?php }else{echo 'No product posted';}?>
                </div>
                <!-------------- end left-indiv ----------->
              </div>
              <!---------------------- start seller-profile-rightdiv css ---------------------> 
            </div>
            <!---------------------- end seller-profile-leftdiv-main css ---------------------> 
            </td>
          </tr>
        </table>
      </div>
      <!---------- end left-question page ----------> 
      <!----- end left-indiv ----------> 
    </div>
    <!----- end left-produc -----------> 
  </div>
  <!-------------- start main-content -----------> 
</article>