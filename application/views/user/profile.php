<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<link href="files/css/users/style.css" type="text/css" rel="stylesheet" />
<link href="files/css/users/style2.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="files/css/users/front.css" type="text/css" media="screen"/>
<script src="files/js/jquery.tipsy.js" type="text/javascript"></script>
<script type="text/javascript" src="files/js/jquery_ravikanth_users.js"></script>
<script type="text/javascript">
$(function(){
	<?php if($this->uri->segment(2) == 'state' && $this->uri->segment(3) != ''){?>
		popup();
	<?php }?>
	
	$("div.close, a.close, .mask_post").click(function() {
		disablePopup();  // function to close pop-up forms
	});
	function popup(){
		$(".success_product").fadeIn(300);
		$(".success_product").css('top', '15%');
		$(".mask_post").css("opacity", "0.7");
		$(".mask_post").fadeIn(300);
	}
	function disablePopup() {
		$(".success_product").fadeOut("normal");
		$(".mask_post").fadeOut("normal");
	}
})
</script>
<style type="text/css">
.profile_link {
	border:none;
	color:#fff !important;
	font-weight:bold;
	font-size:14px;
	cursor:pointer;
	background: #0b9444;
	border-radius: 3px;
	width: 153px;
	height: 39px;
	display:inline-block;
	line-height:39px;
	text-align:center;
}
.profile_link:hover {
	text-decoration:none;
}
footer .col1 a, footer .col2 a, footer .col3 a, footer .col4 a {
	color: #333;
}
footer .col1 a:hover, footer .col2 a:hover, footer .col3 a:hover, footer .col4 a:hover {
	text-decoration:none;
}
</style>

<?php if($this->uri->segment(2) == 'state' && $this->uri->segment(3) != ''){?>
<!--Success message popup-->
<div id="success" class="success_product" style="border-radius: 4px; background: rgba(255,255,255,0.8); padding: 30px; color:#000;">
  <div class="close" style="margin-top:-30px; margin-right:-30px;"></div>
  <!-- close button of the register form -->
  <div id="message_post" align="center" style="color:#090; margin-bottom:10px; text-align:left; font-size:16px; font-weight:bold;">
  <?php //flash messages
	if($this->uri->segment(2) == 'state' && $this->uri->segment(3) == 'yes'){
		echo '<div style="padding-bottom:20px; color:#090; font-size:16px; font-weight:bold;">';
		echo '<strong>Well done!</strong> Profile Picture successfully uploaded.';
		echo '</div>';
	}else if($this->uri->segment(2) == 'state' && $this->uri->segment(2) == 'no'){
		echo '<div style="padding-bottom:20px; color:#f30; font-size:16px; font-weight:bold;">';
		echo '<a class="close" data-dismiss="alert">×</a>';
		echo '<strong>Oh snap!</strong> Please try later.';
		echo '</div>';          
	}
	if($this->uri->segment(2) == 'state' && $this->uri->segment(2) == 'nopic'){
		echo '<div style="padding-bottom:20px; color:#f30; font-size:16px; font-weight:bold;">';
		echo '<a class="close" data-dismiss="alert">×</a>';
		echo '<strong>Oh snap!</strong> please select picture';
		echo '</div>';
	}?>
  </div>
</div>
<!-- END OF SUCCESS MESSAGE -->
<div id="background-on-popup" class="mask_post"></div>
<?php }?>

<article class="content">
  <h1><a href="<?php echo base_url();?>">Home</a> &raquo; <?php echo $title;?></h1>
  <div style="background: url(files/images/users/bg1.gif) repeat; margin:0 auto;"> <?php echo $this->load->view('templates/navigation');?>
    <div class="containerinner" style="height:800px;">
      <p class="heading"><?php echo $title;?></p>
      <div class="main">
        <div class="product1">
          <div class="profile_img">
            <div class="profile_img1" style="background:url(<?php if(count($profile)){if($profile->profile_pic){echo 'files/'.$profile->profile_pic;}else{echo 'files/profile/blank.png';}}else{echo 'files/profile/blank.png';}?>) center center no-repeat; background-size: 100% Auto;">
              <div class="profile_img2">&nbsp;</div>
            </div>
          </div>
          <input type="hidden" name="uid" id="uid" value="<?php echo $user->user_id;?>" />
          <div class="information">
            <h1>Information</h1>
            <div class="table">
              <table class="inform">
                <tr>
                  <td class="fi1"> Full Name : </td>
                  <td class="fi2" id="pro_fullname"><?php echo $user->user_fname;?></td>
                  <td class="fi3"></td>
                </tr>
                <tr>
                  <td class="fi1"> Email : </td>
                  <td class="fi2" id="pro_email"><?php echo $user->user_email;?></td>
                  <td class="fi3"></td>
                </tr>
                <tr>
                  <td class="fi1"> Contact # : </td>
                  <td class="fi2" id="pro_phone"><?php echo $user->user_phone;?></td>
                  <td class="fi3"></td>
                </tr>
                <tr>
                  <td class="fi1"> Username : </td>
                  <td class="fi2"><?php echo $user->user_username;?></td>
                  <td class="fi3"></td>
                </tr>
                <tr>
                  <td class="fi1"> Account type : </td>
                  <td class="fi2"><?php if($user->user_level == 3){?>
                    Publisher
                    <?php }if($user->user_level == 2){?>
                    Seller
                    <?php }if($user->user_level == 1){?>
                    Buyer
                    <?php }?></td>
                  <td class="fi3"><div id="topnav" class="topnav"><a href="" class="signin"><span>EDIT</span></a> </div>
                    <div class="field">
                      <fieldset id="signin_menu">
                      <div class="mess" style="display:none; font-size:16px; font-weight:bold;"><img src="files/images/ajax-loader.gif" /></div>
                        <table>
                          <form method="post" id="signin" action="">
                            <tr class="signintr">
                              <td class="signintd"> Full Name <span style="color:#ef4036;">*</span> : </td>
                              <td class="signininput"><input type="text" name="full_name" id="full_name" value="<?php echo $user->user_fname;?>" class="popupinput" /></td>
                            </tr>
                            <tr class="signintr">
                              <td class="signintd"> Email Address <span style="color:#ef4036;">*</span> : </td>
                              <td class="signininput"><input type="email" name="email" id="email" value="<?php echo $user->user_email;?>" class="popupinput" /></td>
                            </tr>
                            <tr class="signintr">
                              <td class="signintd"> Contact number <span style="color:#ef4036;">*</span> : </td>
                              <td class="signininput"><input type="text" name="telephone" id="telephone" value="<?php echo $user->user_phone;?> " class="popupinput" /></td>
                            </tr>
                            <tr class="signintr">
                              <td class="signintd"></td>
                              <td class="signininput"><input type="button" name="update" id="update" value="Save" class="save" /></td>
                            </tr>
                          </form>
                        </table>
                      </fieldset>
                    </div></td>
                </tr>
              </table>
            </div>
          </div>
          <a href="javascript:;" id="change_pic" class="profile_link">Change Profile Pic</a><br>
          <form action="profile_upload_file" method="post" enctype="multipart/form-data" id="profile_pic" style="display:none;">
          <div class="mess" style="display:none;"><img src="files/images/ajax-loader.gif" /></div>
          
          <input type="file" name="profile_pic" id="profile_pics" /><br />
          <small>Allowed extensions - .gif, .jpg, .jpeg, .png</small><br />
          <input type="submit" name="profile" id="profile" value="Update" style="border:none; color:#fff; font-size:14px; cursor:pointer; background: #0b9444; border-radius: 3px; width: 83px; height: 29px; display:none" />
          </form>
        </div>
        <div class="bank_details" <?php if($user->user_level == 1){?> style="display:none;"<?php }?>>
          <h1>Payment Account</h1>
          <br>
          <?php //if(count($bank)){?>
          <div class="bank_table">
            <div class="table1">
              <div class="left_details">
                <p>Bank</p>
              </div>
              <div class="right_details">
                <p class="bank_name"><?php if(count($bank)){if($bank->bank_name != 'Others'){echo $bank->bank_name;}else{echo $bank->bank_other;}}?></p>
              </div>
            </div>
            <div class="table1">
              <div class="left_details">
                <p>Bank Code :</p>
              </div>
              <div class="right_details">
                <p class="bank_code"><?php if(count($bank)){echo $bank->bank_code;}?></p>
              </div>
            </div>
            <div class="table1">
              <div class="left_details">
                <p>Branch Code :</p>
              </div>
              <div class="right_details">
                <p class="branch_code"><?php if(count($bank)){echo $bank->bank_branch_code;}?></p>
              </div>
            </div>
            <div class="table1">
              <div class="left_details">
                <p>Account Type :</p>
              </div>
              <div class="right_details">
                <p class="account_type"><?php if(count($bank)){echo $bank->bank_account_type;}?></p>
              </div>
            </div>
            <div class="table1">
              <div class="left_details">
                <p>Account Number :</p>
              </div>
              <div class="right_details">
                <p class="account_number"><?php if(count($bank)){echo $bank->bank_account_number;}?></p>
              </div>
            </div>
          </div>
          <?php //}?>
        </div>
        <div class="edit" <?php if($user->user_level == 1){?> style="display:none;"<?php }?>>
          <div id="topnav" class="topnav">
            <a href="" class="payment"><span>EDIT</span></a>
          </div>
          <div class="field">
            <fieldset id="payment_menu">
            <div class="mess" style="display:none; font-size:16px; font-weight:bold;"><img src="files/images/ajax-loader.gif" /></div>
              <table>
                <form method="post" id="payment" action="">
                <input type="hidden" name="bank_id" id="bank_id" value="<?php if(count($bank)){echo $bank->bank_id;}?>" />
                  <tr class="signintr">
                    <td class="signintd"> Bank : </td>
                    <td><div class="styled-select">
                        <select name="bank_name" id="bank_name" style="width:100%;">
                          <option value="">Select One</option>
                            <option <?php if(count($bank)){if($bank->bank_name == 'ANZ'){ echo 'selected';}}?>>ANZ</option>
                            <option <?php if(count($bank)){if($bank->bank_name == 'Bank of China'){ echo 'selected';}}?>>Bank of China</option>
                            <option <?php if(count($bank)){if($bank->bank_name == 'Citibank'){ echo 'selected';}}?>>Citibank</option>
                            <option <?php if(count($bank)){if($bank->bank_name == 'DBS Bank'){ echo 'selected';}}?>>DBS Bank</option>
                            <option <?php if(count($bank)){if($bank->bank_name == 'HSBC'){ echo 'selected';}}?>>HSBC</option>
                            <option <?php if(count($bank)){if($bank->bank_name == 'Maybank'){ echo 'selected';}}?>>Maybank</option>
                            <option <?php if(count($bank)){if($bank->bank_name == 'OCBC'){ echo 'selected';}}?>>OCBC</option>
                            <option <?php if(count($bank)){if($bank->bank_name == 'POSB'){ echo 'selected';}}?>>POSB</option>
                            <option <?php if(count($bank)){if($bank->bank_name == 'Standard Chartered Bank'){ echo 'selected';}}?>>Standard Chartered Bank</option>
                            <option <?php if(count($bank)){if($bank->bank_name == 'UOB'){ echo 'selected';}}?>>UOB</option>
                            <option <?php if(count($bank)){if($bank->bank_name == 'Others'){ echo 'selected';}}?>>Others</option>
                        </select>
                    </div></td>
                  </tr>
                  <tr class="signintr bank_other" <?php if(count($bank)){if($bank->bank_other == ''){ echo 'style="display:none"';}}else{ echo 'style="display:none"';}?>>
                    <td class="signintd"> Other Bank Name : </td>
                    <td class="signininput"><input type="text" name="bank_other" id="bank_other" value="<?php if(count($bank)){echo $bank->bank_other;}?>" class="popupinput" /></td>
                  </tr>
                  <tr class="signintr">
                    <td class="signintd"> Bank Code : </td>
                    <td class="signininput"><input type="text" name="bank_code" id="bank_code" value="<?php if(count($bank)){echo $bank->bank_code;}?>" class="popupinput" /></td>
                  </tr>
                  <tr class="signintr">
                    <td class="signintd"> Bank Branch Code : </td>
                    <td class="signininput"><input type="text" name="bank_branch_code" id="bank_branch_code" value="<?php if(count($bank)){echo $bank->bank_branch_code;}?>" class="popupinput" /></td>
                  </tr>
                  <tr class="signintr">
                    <td class="signintd"> Account Type <a href="#" class="tooltip"><img src="files/images/icons/tool-tip.gif" alt="" /><span>Savings Account, Current Account</span></a> : </td>
                    <td class="signininput"><input type="text" name="bank_account_type" id="bank_account_type" value="<?php if(count($bank)){echo $bank->bank_account_type;}?>" class="popupinput" /></td>
                  </tr>
                  <tr class="signintr">
                    <td class="signintd"> Account Number : </td>
                    <td class="signininput"><input type="text" name="bank_account_number" id="bank_account_number" value="<?php if(count($bank)){echo $bank->bank_account_number;}?>" class="popupinput" /></td>
                  </tr>
                  <tr class="signintr">
                    <td class="signintd"></td>
                    <td class="signininput"><input type="button" name="payment_bank_save" id="payment_bank_save" value="Save" class="save" style="color:#fff" /></td>
                  </tr>
                </form>
              </table>
            </fieldset>
          </div>
        </div>
      </div>
    </div>
  </div>
</article>