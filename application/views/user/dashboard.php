<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<link href="files/css/users/style.css" type="text/css" rel="stylesheet" />
<link href="files/css/users/style3.css" type="text/css" rel="stylesheet" />

<article class="content">
  <h1><a href="<?php echo base_url();?>">Home</a> &raquo; <?php echo $title;?></h1>
  <div style="background: url(files/images/users/bg1.gif) repeat; margin:0 auto;"> <?php echo $this->load->view('templates/navigation');?>
    <div class="containerinner">
      <p class="heading"><?php echo $title;?></p>
      <!------- start main ------------>
      <div class="main">
        <h4>General</h4>
        <!------ start left --------->
        <div class="left"> 
          <!--------- start col1 ----->
          <div class="col1"> <a href="my_notify">
            <h1 style="margin: 5px 0;">Notifications</h1>
            <img src="files/images/users/col-img2.png" />
            <h2 style="margin: 10px 0; line-height: 15px;">You have <b><?php echo $note;?></b> notification(s)</h2>
            </a> </div>
          <div class="col1"> <a href="faqs"><img src="files/images/users/col-faqs-img.png" style="margin:15px 0 0 46px;" width="60" />
            <h2 style="margin: 5px 0; line-height: 15px;">Frequently Asked Questions</h2>
            </a> </div>
          <div class="col1"> <a href="contact"><img src="files/images/users/col-mail-img.png" style="margin:20px 0 0 46px;" />
            <h1 style="margin:5px 10px 0 0;">Enquiry</h1>
            </a> </div>
          <!--------- start col1 -----> 
        </div>
        <!------ start left ---------> 
        
        <!------ start right --------->
        <div class="right" align="center">
          <h1><?php echo $this->session->userdata('fullname');?></h1>
          <img src="<?php if(count($profile)){if($profile->profile_pic){echo 'files/'.$profile->profile_pic;}else{echo 'files/profile/blank.png';}}else{echo 'files/profile/blank.png';}?>" style="max-height:110px;" /> <a href="profile">
          <div class="span"> Update my information </div>
          </a> </div>
        <!------ start right ---------> 
      </div>
      <!------------ end main --------->
      <?php if($this->session->userdata('level') == 2 || $this->session->userdata('level') == 3){?>
      <!---------- start main2 page ----------->
      <div class="main2">
        <h3>Selling</h3>
        
        <!------ start left --------->
        <div class="left"> 
          <!--------- start col1 ----->
          <div class="col1"> <a href="publish_profile"><img src="files/images/users/main-2-img.png" style="margin:20px 0 0 46px;" />
            <h1 style="margin:5px 10px 0 0;">Publish my Profile</h1>
            </a> </div>
          <div class="col1"> <a href="my_products">
            <h1 style="margin:30px 10px 0 0; float:right; text-align:left;">Manage <br />
              My<br />
              Products</h1>
            <img src="files/images/users/main-2-img1.png" style="margin:20px 0 0 16px; float:left;" /> </a></div>
          <div class="col1"> <a href="products/add"><img src="files/images/users/main-2-img3.png" style="margin:20px 0 0 46px;" />
            <h1 style="margin:5px 10px 0 0;">Post a product</h1>
            </a> </div>
          <div class="col1"> <a href="my_sales"><img src="files/images/users/main-2-img2.png" style="margin:20px 0 0 46px;" />
            <h1 style="margin:5px 10px 0 0;">View Sales</h1>
            </a> </div>
          <!--------- start col1 -----> 
        </div>
        <!------ start left ---------> 
      </div>
      <div style="clear:both"></div>
      <?php } ?>
      <!---------- start main2 page ----------->
      <div class="main3">
        <h3>Buying</h3>
        
        <!------ start left --------->
        <div class="left"> 
          <!--------- start col1 ----->
          <div class="col1"> <a href="my_orders"><img src="files/images/users/main-2-img4.png" style="margin:20px 0 0 46px;" />
            <h1 style="margin:5px 10px 0 0;">View purchases</h1>
            </a> </div>
          <!--------- start col1 -----> 
        </div>
        <!------ start left ---------> 
      </div>
      <!---------- start main2 page -----------> 
    </div>
  </div>
</article>