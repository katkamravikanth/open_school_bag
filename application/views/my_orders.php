<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<link href="files/css/users/style.css" type="text/css" rel="stylesheet" />
<link href="files/css/users/style3.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="files/css/jquery.datepick.css" rel="stylesheet">
<script type="text/javascript" src="files/js/jquery.datepick.js"></script>
<script type="text/javascript">
$(function(){
	$('#from').datepick({dateFormat: 'yyyy-mm-dd'});
	$('#to').datepick({dateFormat: 'yyyy-mm-dd'});
});
</script>
<style type="text/css">
@-moz-document url-prefix() {
	.datepick-month-header select:first-child {
		overflow:hidden;
		width: 55% !important;
		-moz-appearance: none !important;
		float:left;
	}
	.datepick-month-header select:nth-child(2) {
		overflow:hidden;
		width: 35% !important;
		-moz-appearance: none !important;
		float:right
	}
}
</style>

<article class="content">
  <h1>
    <a href="<?php echo base_url();?>">Home</a>
    &raquo;
    <?php echo $title;?></h1>
  <div style="background: url(files/images/users/bg1.gif) repeat; margin:0 auto;">
    <?php echo $this->load->view('templates/navigation');?>
    <div class="containerinner" style="width:640px; padding:30px 20px;">
      <p class="heading"><?php echo $title;?></p>
      <!------- start Q and A page ------------>
      
      <div class="view-page">
        <form action="my_orders" method="get" name="orders_search" id="orders_search">
          <h3>From</h3>
          <input type="text" name="from" id="from" class="box">
          <h4>To</h4>
          <input type="text" name="to" id="to" class="box">
          <input type="submit" value="Submit" name="get_orders" id="get_orders" class="submit"  />
        </form>
        <?php $totals=0; foreach($orders as $carts){ $totals = $totals + $carts->order_pprice - $carts->order_discount;}?>
        <?php $total_s=0; foreach($total_sales as $total_sale){ $total_s = $total_s + $total_sale->trans_fee;}?>
        <h1>Total Orders: <b>S$<?php echo number_format($total_s + $totals, 2);?></b></h1>
      </div>
      <div class="containerinner2" style="width:95%;">
      <?php if(count($orders)){?>
        <p>Please help us to rate and review the product that you have purchased by clicking on the link to the product page.</p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr class="headerlist" style="color:#fff;">
            <th style="padding:3px;">Order No</th>
            <th style="padding:3px;" width="200">Product Name</th>
            <th style="padding:3px;">Type</th>
            <th style="padding:3px;" colspan="2">Price</th>
            <th style="padding:3px;">Status</th>
            <th style="padding:3px;">Expiry Date</th>
          </tr>
        <?php $i = 0;
		foreach($orders as $carts){
			if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
          <tr style="height:35px;">
            <td align="center" style="padding:3px; border:1px solid #ccc;"><?php echo $carts->order_no;?></td>
            <td style="padding:3px; border:1px solid #ccc;"><a href="product/<?php echo $carts->order_punique;?>" style="text-decoration:underline; color:#666;" target="_blank"><?php echo $carts->order_pname;?></a></td>
            <td align="center" style="padding:3px; border:1px solid #ccc;"><?php echo $carts->order_ptype;?></td>
            <td align="left" style="padding:3px; border-top:1px solid #ccc; border-bottom:1px solid #ccc; border-left:1px solid #ccc;">S$</td>
            <td align="left" style="padding:3px; border-top:1px solid #ccc; border-bottom:1px solid #ccc; border-right:1px solid #ccc;">
			<?php echo number_format($carts->order_pprice - $carts->order_discount, 2);?></td>
            <td align="center" style="padding:3px; border:1px solid #ccc;">
			<?php if($carts->order_ptype == 'Physical'){
					echo '<span style="color:#ef4036;">'.$carts->order_status.'</span>';
				}else{
					if($carts->order_d_expir != ''){if($carts->order_d_expir > date('Y-m-d H:i:s')){
						echo ' <a href="download/'.$carts->order_punique.'" target="_blank" style="color:#0b9444; text-decoration:underline">Download</a>';}else{
						echo 'Expired';
					}}
				}?></td>
             <td align="center" style="padding:3px; border:1px solid #ccc;"><?php if($carts->order_d_expir != ''){echo date('Y-m-d',strtotime($carts->order_d_expir));}?></td>
          </tr>
        <?php $i++;
		}?>
        </table>
        <div id="cont" style="float:right; margin-top:20px;"><?php echo $pagination; ?></div>
        <!---------- start generate-report ----------->
        <div class="generate-report" style="margin:0 !important;"><?php echo $link;?></div>
        <!---------- end generate-report ----------->
        <?php }else{echo '<p style=" font-size:16px; font-weight:bold;">No record found.</p>';}?>
      </div>
      <div class="clear"></div>
      
      <!---------- end Q and A page ----------->
      
      <div class="clear"></div>
      <div class="clear"></div>
    </div>
  </div>
</article>