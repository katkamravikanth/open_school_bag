<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<article class="content">
  <h1><a href="<?php echo base_url();?>">Home</a> &raquo; <?php echo $title;?></h1>
  <!-------------- start main-content ----------->
  <div id="main-content">
    <h1><?php echo $title;?></h1>
    <h2>Meet our partners</h2>
    <?php echo $page[0]->body;?> 
    
    <!---------------- start div-left ---------->
    <div id="d" style="margin:0 auto;">
      <?php if(count($partners)){
        foreach($partners as $part){?>
      <div id="div-left">
        <div class="green_b_ellipse">
          <?php if($part->partner_logo!=''){$pic = 'files/'.$part->partner_logo;}else{$pic = 'files/partners/home-icon.png';}?>
          <div class="green_t_ellipse" style="background-image:url(<?php echo $pic;?>); background-size:100% auto; background-repeat:no-repeat; background-position:center center;">&nbsp;</div>
        </div>
        <h1 style="height:100px;"><?php echo stripslashes(str_replace('\n', ' ', $part->partner_title));//if(strlen($part->partner_title)>15){echo '...';} ?></h1>
        <p style="text-align:left; height:70px; color:#000;"><?php echo stripslashes(str_replace('\n', ' ', $part->partner_slogan));//if(strlen($part->partner_slogan)>25){echo '...';}?>adsf</p>
		<!--<span>
        <p style="text-align:left; height:50px;"><?php echo stripslashes(substr(str_replace('\n', ' ', $part->partner_body), 0, 50));if(strlen($part->partner_body)>50){echo '...';}?></p>
        </span>-->
        <div id="div-ico"> <a href="<?php echo $part->partner_fb;?>" target="_blank" class="icon f">&nbsp;</a> 
        <a href="<?php echo $part->partner_tw;?>" target="_blank" class="icon t">&nbsp;</a> 
        <a href="mailto:<?php echo $part->user_email;?>" target="_blank" class="icon m">&nbsp;</a> </div>
      </div>
      <?php } }else{echo 'Partners not found';}?>
    </div>
    <!---------------- start div-left ---------->
  </div>
  <!-------------- start main-content -----------> 
</article>