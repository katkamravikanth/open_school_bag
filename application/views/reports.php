<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<link href="files/css/users/style.css" type="text/css" rel="stylesheet" />
<link href="files/css/users/style3.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="files/css/jquery.datepick.css" rel="stylesheet">
<script type="text/javascript" src="files/js/jquery.datepick.js"></script>
<script type="text/javascript">
$(function(){
	$('#from').datepick({dateFormat: 'yyyy-mm-dd'});
	$('#to').datepick({dateFormat: 'yyyy-mm-dd'});
});
</script>
<style type="text/css">
@-moz-document url-prefix() {
	.datepick-month-header select:first-child {
		overflow:hidden;
		width: 55% !important;
		-moz-appearance: none !important;
		float:left;
	}
	.datepick-month-header select:nth-child(2) {
		overflow:hidden;
		width: 35% !important;
		-moz-appearance: none !important;
		float:right
	}
}
</style>

<article class="content">
  <h1> <a href="<?php echo base_url();?>">Home</a> &raquo; <?php echo $title;?></h1>
  <div style="background: url(files/images/users/bg1.gif) repeat; margin:0 auto;"> <?php echo $this->load->view('templates/navigation');?>
    <div class="containerinner">
      <p class="heading"><?php echo $title;?></p>
      <!------- start Q and A page ------------>
      <div id="report" style="height:130px;">
        <form name="report_form" id="report_form" method="get">
          <div style="width:600px !important; margin:0;">
            <h3 style="float:left">From</h3>
            <input type="text" name="from" id="from" class="box" style="float:left">
            <h3 style="float:left">To</h3>
            <input type="text" name="to" id="to" class="box" style="margin-right:20px; float:left">
          </div>
          <br />
          <br />
          <div style="width:370px !important; margin-top:20px; margin-left:40px;">
            <div class="styled-select">
              <select name="report_by" id="report_by">
                <option>Downloads</option>
                <option>Sales</option>
                <option>Products</option>
              </select>
            </div>
            <div class="bottom-button">
              <div class="bottom-button1">
                <div class="bottom-button2">
                  <input type="submit" name="generate" id="generate" value="Generate" />
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <?php if(isset($prd)){?>
      <div class="containerinner2" style="width:100%;"> <strong><?php echo $prd;?></strong>
        <?php if(count($orders)){
			  if($prd == 'Products'){?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr class="headerlist" style="color:#fff;">
				<th style="padding:3px;" width="200">Product Name</th>
				<th style="padding:3px;">Product Type</th>
				<th style="padding:3px;">Sales / Downloads</th>
				<th style="padding:3px;">Price</th>
			  </tr>
			  <?php $i = 0;
			foreach($orders as $carts){
				if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
			  <tr style="height:35px; <?php echo $bg;?>">
				<td style="padding:3px; border:1px solid #ccc;"><?php echo $carts->product_name;?></td>
				<td style="padding:3px; border:1px solid #ccc;" align="center"><?php echo $carts->product_type;?></td>
				<td style="padding:3px; border:1px solid #ccc;" align="center"><?php echo $carts->order_count.' / '.$carts->down_count;?></td>
				<td style="padding:5px; border:1px solid #ccc;" align="right">S$ <?php echo $carts->product_price;?></td>
			  </tr>
			  <?php $i++;
			}?>
			</table>
			<div id="cont" style="float:right; margin-top:20px;"><?php echo $pagination; ?></div>
			<!---------- start generate-report ----------->
			<div class="generate-report" style="margin:0 !important;"><?php echo $link;?></div>
			<?php }else if($prd == 'Downloads'){?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr class="headerlist" style="color:#fff;">
				<th style="padding:3px;">Product Name</th>
				<th style="padding:3px;">Product Type</th>
				<th style="padding:3px;">Product Category</th>
				<th style="padding:3px;">No of Downloads</th>
			  </tr>
			  <?php $i = 0;
			foreach($orders as $carts){
				if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
			  <tr style="height:35px; <?php echo $bg;?>">
				<td style="padding:3px; border:1px solid #ccc;"><?php echo $carts->order_pname;?></td>
				<td style="padding:3px; border:1px solid #ccc;" align="center"><?php echo $carts->order_ptype;?></td>
                <?php $cat = explode(':', $carts->order_cat);?>
				<td style="padding:3px; border:1px solid #ccc;" align="center"><?php $categ=''; foreach($cat as $cate){$categ .= $cate.', ';}echo trim($categ, ', ');?></td>
				<td style="padding:3px; border:1px solid #ccc;" align="center"><?php echo $carts->down_count;?></td>
			  </tr>
			  <?php $i++;
			}?>
			</table>
			<div id="cont" style="float:right; margin-top:20px;"><?php echo $pagination; ?></div>
			<!---------- start generate-report ----------->
			<div class="generate-report" style="margin:0 !important;"><?php echo $link;?></div>
			<?php }else if($prd == 'Sales'){?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr class="headerlist" style="color:#fff;">
				<th style="padding:3px;">Product Name</th>
				<th style="padding:3px;" width="100">Ordered Date</th>
				<th style="padding:3px;">Buyer</th>
				<th style="padding:3px;">Type</th>
				<th style="padding:3px;">Actions</th>
			  </tr>
			  <?php $i = 0;
			foreach($orders as $carts){
				if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
			  <tr style="height:35px; padding-bottom:10px; <?php echo $bg;?>">
				<td style="padding:3px; border:1px solid #ccc;"><?php echo $carts->order_pname;?></td>
				<td style="padding:3px; border:1px solid #ccc;" align="center"><?php echo date('Y-m-d',strtotime($carts->created));?></td>
				<td style="padding:3px; border:1px solid #ccc;" align="center"><?php echo $carts->user_id;?></td>
				<td style="padding:3px; border:1px solid #ccc;" align="center"><?php echo $carts->order_ptype;?></td>
				<td style="padding:3px; border:1px solid #ccc;" align="center"><a href="mysale/<?php echo $carts->order_no;?>" class="physical" style="text-decoration:underline">View</a></td>
			  </tr>
			  <?php $i++;
			}?>
			</table>
			<div id="cont" style="float:right; margin-top:20px;"><?php echo $pagination; ?></div>
			<!---------- start generate-report ----------->
			<div class="generate-report" style="margin:0 !important;"><?php echo $link;?></div>
			<?php }
        }else{echo '<div style=" font-size:16px; font-weight:bold;">No records found.</div>';}?>
      </div>
      <?php }?>
      <div class="clear"></div>
      <div class="clear"></div>
      <div class="clear"></div>
    </div>
  </div>
</article>