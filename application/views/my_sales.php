<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<link href="files/css/users/style.css" type="text/css" rel="stylesheet" />
<link href="files/css/users/style3.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="files/css/jquery.datepick.css" rel="stylesheet">
<script type="text/javascript" src="files/js/jquery.datepick.js"></script>
<script type="text/javascript">
$(function(){
	$('#from').datepick({dateFormat: 'yyyy-mm-dd'});
	$('#to').datepick({dateFormat: 'yyyy-mm-dd'});
});
</script>
<style>
@-moz-document url-prefix() {
 .datepick-month-header select:first-child {
	 overflow:hidden;
	 width: 55% !important;
	 -moz-appearance: none !important;
	 float:left;
	}
	 .datepick-month-header select:nth-child(2) {
	 overflow:hidden;
	 width: 35% !important;
	 -moz-appearance: none !important;
	 float:right
	}
}
</style>

<article class="content">
  <h1> <a href="<?php echo base_url();?>">Home</a> &raquo; <?php echo $title;?></h1>
  <div style="background: url(files/images/users/bg1.gif) repeat; margin:0 auto;"> <?php echo $this->load->view('templates/navigation');?>
    <div class="containerinner" style="width:640px; padding:30px 20px;">
      <p class="heading"><?php echo $title;?></p>
      <!------- start Q and A page ------------>
      
      <div class="view-page">
        <form action="my_sales" method="get" name="sales_search" id="sales_search">
          <h3>From</h3>
          <input type="text" name="from" id="from" class="box">
          <h4>To</h4>
          <input type="text" name="to" id="to" class="box">
          <input type="submit" value="Submit" name="get_sales" id="get_sales" class="submit"  />
        </form>
        <h1>Total Sales: <b>S$ 
          <?php $total=0; foreach($orders as $carts){
		if($carts->order_promo == ''){$total = $total + ($carts->order_pprice - $carts->order_discount);
		}else{$total = $total + $carts->order_pprice;}
          }echo number_format($total, 2);
          ?>
          </b></h1>
      </div>
      <div class="containerinner2" style="margin:0; width:100%;">
        <?php if(count($orders)){?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr class="headerlist" style="color:#fff;">
            <th style="padding:3px;">Ordered Date</th>
            <th style="padding:3px;" width="200">Product Name</th>
            <th style="padding:3px;">Buyer</th>
            <th style="padding:3px;">Type</th>
            <th style="padding:3px;" colspan="2">Price</th>
            <th style="padding:3px;">Status</th>
            <th style="padding:3px;">Actions</th>
          </tr>
          <?php $i = 0;
		foreach($orders as $carts){
			if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
          <tr style="height:35px; padding-bottom:10px;">
            <td style="padding:3px; border:1px solid #ccc;" align="center"><?php echo date('Y-m-d',strtotime($carts->created));?></td>
            <td style="padding:3px; border:1px solid #ccc;"><?php echo $carts->order_pname;?></td>
            <td style="padding:3px; border:1px solid #ccc;" align="center"><?php echo $carts->user_id;?></td>
            <td style="padding:3px; border:1px solid #ccc;" align="center"><?php echo $carts->order_ptype;?></td>
            <td style="padding:3px; border-top:1px solid #ccc; border-bottom:1px solid #ccc; border-left:1px solid #ccc;" align="right">S$</td>
            <td style="padding:3px; border-top:1px solid #ccc; border-bottom:1px solid #ccc; border-right:1px solid #ccc;" align="right">
			<?php if($carts->order_promo == ''){echo number_format($carts->order_pprice - $carts->order_discount, 2);}else{echo number_format($carts->order_pprice, 2);}?></td>
            <td style="padding:3px; border:1px solid #ccc;" align="center"><?php echo $carts->order_status;?></td>
            <td style="padding:3px; border:1px solid #ccc;" align="center"><a href="mysale/<?php echo $carts->order_no;?>" class="physical" style="text-decoration:underline">View</a></td>
          </tr>
          <?php $i++;
		}?>
        </table>
        <div id="cont" style="float:right; margin-top:20px;"><?php echo $pagination; ?></div>
        <!---------- start generate-report ----------->
        <div class="generate-report" style="margin:0 !important;"><?php echo $link;?></div>
        <!---------- end generate-report ----------->
        <?php }else{echo '<p style=" font-size:16px; font-weight:bold;">No record found.</p>';}?>
      </div>
      <div class="clear"></div>
      
      <!---------- end Q and A page ----------->
      
      <div class="clear"></div>
      <div class="clear"></div>
    </div>
  </div>
</article>