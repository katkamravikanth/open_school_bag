<link rel="stylesheet" href="files/admin/css/jquery.fileupload-ui.css" />
<link type="text/css" href="files/css/jquery.datepick.css" rel="stylesheet">
<script type="text/javascript" src="files/js/jquery.datepick.js"></script>
<script type="text/javascript">
$(function(){
	$('#from, #to').datepick({dateFormat: 'yyyy-mm-dd'});
});
</script>
<style type="text/css">
input[type="text"], select, textarea {
	color: #000;
}
a {color: #666}
a:hover {color: #000}
h3 {
	margin-left: 20px;
	margin-top: 20px;
}
form {
	border-bottom: 1px solid #060;
}
form:last-child {
	border-bottom: 0;
}
#cont {
	width: 100%;
	height: 30px;
	float: right;
	background: none;
}
#cont li a[href="#"], #cont li a:hover {
	background: #0b9444;
	padding: 3px 8px;
	color: #fff;
	margin: 0;
}
#cont ul {
	float:right;
	margin: 0;
	margin-right:20px;
	padding: 0;
}
#cont li {
	list-style: none;
	float: left;
	padding: 5px 5px;
}
#cont a {
	padding: 3px 8px;
	font-size: 12pxpx;
	font-weight: bold;
	color: #666;
	margin: 0 0px 0 0;
}
@-moz-document url-prefix() {
 .datepick-month-header select:first-child {
 overflow:hidden;
 width: 55% !important;
 -moz-appearance: none !important;
 float:left;
}
 .datepick-month-header select:nth-child(2) {
 overflow:hidden;
 width: 35% !important;
 -moz-appearance: none !important;
 float:right
}
}
</style>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p><?php echo $title;?></p>
        </li>
      </ul>
    </div>
    <div class="export">
      <p><?php echo $link;?></p>
    </div>
  </div>
  <!------- start product  page ------------>
  <div class="view-page" style="margin:0; margin-top:80px; margin-bottom:20px; height:150px;">
  	<h2>Search by Date</h2><br>
    <form action="admin/orders/search" method="get" name="orders_search" id="orders_search" style="padding-bottom:15px;">
      <h4>From</h4>
      <input type="text" name="from" id="from" placeholder="From date (yyyy-mm-dd)" style="height:25px !important;" class="box" />
      <h4>To</h4>
      <input type="text" name="to" id="to" placeholder="To date (yyyy-mm-dd)" class="box">
      <input type="submit" value="Submit" name="get_orders" id="get_orders" class="submit"  />
    </form>
    <br>
    <h2>Search by Product name</h2><br>
    <form action="admin/orders/pname" method="get" name="orders_prod_search" id="orders_prod_search">
      <h4>Enter Name</h4>
      <input type="text" name="pname" id="pname" placeholder="Product name/Keyword" class="box">
      <input type="submit" value="Submit" name="get_orders" id="get_orders" class="submit"  />
    </form>
  </div>
  <div class="containerinner2" style="margin:0; width:100%;">
    <?php if(count($orders)){?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
      <tr class="headerlist" style="color:#fff;">
        <th style="padding:2px;">Ordered Date</th>
        <th style="padding:2px;" width="200">Product Name</th>
        <th style="padding:2px;">Buyer</th>
        <th style="padding:2px;">Seller</th>
        <th style="padding:2px;">Product Type</th>
        <th style="padding:2px;">Actions</th>
      </tr>
      <?php $i = 0;
		foreach($orders as $carts){
		if($carts['usr'] != '' ){
			if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
      <tr style="height:35px;">
        <td style="padding:2px; border:1px solid #CCC" align="center"><?php echo $carts['ord']->created;?></td>
        <td style="padding:2px; border:1px solid #CCC"><?php echo $carts['ord']->order_pname;?></td>
        <td style="padding:2px; border:1px solid #CCC" align="center"><?php echo $carts['ord']->user_username;?></td>
        <td style="padding:2px; border:1px solid #CCC" align="center"><?php echo $carts['usr'];?></td>
        <td style="padding:2px; border:1px solid #CCC" align="center"><?php echo $carts['ord']->order_ptype;?></td>
        <td style="padding:2px; border:1px solid #CCC" align="center"><a href="admin/order/<?php echo $carts['ord']->order_no;?>" style="text-decoration:underline;">View</a></td>
      </tr>
      <?php $i++;
		}}?>
    </table>
    <div id="cont" style="float:right; margin-top:20px;"><?php echo $pagination; ?></div>
    <!---------- start generate-report ----------->
    <?php }else{echo '<p style=" font-size:16px; font-weight:bold;">Orders not found</p>';}?>
  </div>
  <div class="clear"></div>
  <!---------- end product page ----------->
  <div class="clear"></div>
  <div class="clear"></div>
</div>