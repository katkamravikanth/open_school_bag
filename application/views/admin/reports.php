<link rel="stylesheet" href="files/admin/css/jquery.fileupload-ui.css" />
<link type="text/css" href="files/css/jquery.datepick.css" rel="stylesheet">
<link rel="stylesheet" href="files/css/themes/alertify.core.css" />
<link rel="stylesheet" href="files/css/themes/alertify.default.css" id="toggleCSS" />
<script src="files/js/lib/alertify.min.js"></script>
<script type="text/javascript" src="files/js/jquery.datepick.js"></script>
<script type="text/javascript">
$(function(){
	function reset () {
		$("#toggleCSS").attr("href", "files/css/themes/alertify.default.css");
	}
	$('.box').datepick({dateFormat: 'yyyy-mm-dd'});
	product_type
	$('form#sales #by').change(function(e) {
        if($(this).val() == 'by_seller'){
			$('form#sales #product_type').hide();
			$('form#sales #product').hide();
			$('form#sales #level').hide();
			$('form#sales #subject').hide();
			$('form#sales #seller').show();
		}else if($(this).val() == 'by_product'){
			$('form#sales #seller').hide();
			$('form#sales #product_type').hide();
			$('form#sales #level').hide();
			$('form#sales #subject').hide();
			$('form#sales #product').show();
		}else if($(this).val() == 'product_type'){
			$('form#sales #seller').hide();
			$('form#sales #product').hide();
			$('form#sales #level').hide();
			$('form#sales #subject').hide();
			$('form#sales #product_type').show();
		}else if($(this).val() == 'product_level'){
			$('form#sales #seller').hide();
			$('form#sales #product').hide();
			$('form#sales #subject').hide();
			$('form#sales #product_type').hide();
			$('form#sales #level').show();
		}else if($(this).val() == 'product_subject'){
			$('form#sales #seller').hide();
			$('form#sales #product').hide();
			$('form#sales #level').hide();
			$('form#sales #product_type').hide();
			$('form#sales #subject').show();
		}else{
			$('form#sales #seller').hide();
			$('form#sales #product').hide();
			$('form#sales #level').hide();
			$('form#sales #subject').hide();
			$('form#sales #product_type').hide();
		}
    });
	
	$("form#sales").submit(function(e) {
		
        if($("form#sales #from").val() != '' && $("form#sales #to").val() != '' && $('form#sales #by').val() != ''){
			return true;
		}else{
			alertify.error('Please select report by, from and to dates');
			return false;
		}
    });
	
	$('form#downloads #by').change(function(e) {
        if($(this).val() == 'by_seller'){
			$('form#downloads #product').hide();
			$('form#downloads #level').hide();
			$('form#downloads #subject').hide();
			$('form#downloads #seller').show();
		}else if($(this).val() == 'by_product'){
			$('form#downloads #seller').hide();
			$('form#downloads #level').hide();
			$('form#downloads #subject').hide();
			$('form#downloads #product').show();
		}else if($(this).val() == 'product_level'){
			$('form#downloads #seller').hide();
			$('form#downloads #product').hide();
			$('form#downloads #subject').hide();
			$('form#downloads #level').show();
		}else if($(this).val() == 'product_level'){
			$('form#downloads #seller').hide();
			$('form#downloads #product').hide();
			$('form#downloads #subject').hide();
			$('form#downloads #level').show();
		}else if($(this).val() == 'product_subject'){
			$('form#downloads #seller').hide();
			$('form#downloads #product').hide();
			$('form#downloads #level').hide();
			$('form#downloads #subject').show();
		}else{
			$('form#downloads #seller').hide();
			$('form#downloads #product').hide();
			$('form#downloads #level').hide();
			$('form#downloads #subject').hide();
		}
    });
	
	$("form#downloads").submit(function(e) {
        if($("form#downloads #from").val() != '' && $("form#downloads #to").val() != '' && $('form#downloads #by').val() != ''){
			return true;
		}else{
			alertify.error('Please select report by, from and to dates');
			return false;
		}
    });
	
	$('form#products #by').change(function(e) {
        if($(this).val() == 'by_seller'){
			$('form#products #level').hide();
			$('form#products #subject').hide();
			$('form#products #product_type').hide();
			$('form#products #seller').show();
		}else if($(this).val() == 'product_type'){
			$('form#products #seller').hide();
			$('form#products #level').hide();
			$('form#products #subject').hide();
			$('form#products #product_type').show();
		}else if($(this).val() == 'product_level'){
			$('form#products #seller').hide();
			$('form#products #subject').hide();
			$('form#products #product_type').hide();
			$('form#products #level').show();
		}else if($(this).val() == 'product_subject'){
			$('form#products #seller').hide();
			$('form#products #level').hide();
			$('form#products #product_type').hide();
			$('form#products #subject').show();
		}else{
			$('form#products #seller').hide();
			$('form#products #level').hide();
			$('form#products #subject').hide();
			$('form#products #product_type').hide();
		}
    });
	
	$("form#products").submit(function(e) {
        if($("form#products #from").val() != '' && $("form#products #to").val() != '' && $('form#products #by').val() != ''){
			return true;
		}else{
			alertify.error('Please select report by, from and to dates');
			return false;
		}
    });
});
</script>
<style type="text/css">
input[type="text"] {
	width: 170px !important;
}
input[type="text"], select, textarea {
	color: #000;
}
h3 {
	margin-left: 20px;
	margin-top: 20px;
}
form {
	border-bottom: 1px solid #060;
}
form:last-child {
	border-bottom: 0;
}
#cont {
	width: 100%;
	height: 30px;
	float: right;
	background: none;
}
#cont li a[href="#"], #cont li a:hover {
	background: #0b9444;
	padding: 3px 8px;
	color: #fff;
	margin: 0;
}
#cont ul {
	float:right;
	margin: 0;
	margin-right:20px;
	padding: 0;
}
#cont li {
	list-style: none;
	float: left;
	padding: 5px 5px;
}
#cont a {
	padding: 3px 8px;
	font-size: 12pxpx;
	font-weight: bold;
	color: #666;
	margin: 0 0px 0 0;
}
@-moz-document url-prefix() {
	.datepick-month-header select:first-child {
		overflow:hidden;
		width: 55% !important;
		-moz-appearance: none !important;
		float:left;
	}
	.datepick-month-header select:nth-child(2) {
		overflow:hidden;
		width: 35% !important;
		-moz-appearance: none !important;
		float:right
	}
}
</style>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p><?php echo $title;?></p>
        </li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start product  page ------------>
  <div class="product" style="width:100%; margin-left:0; margin-bottom:30px;">
    <?php
      if(isset($flash_message)){
        if($flash_message != TRUE){
          echo '<div class="alert alert-error" style="margin-top:20px; color:#f30; font-size:16px; font-weight:bold;">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> there are no list to generate';
          echo '</div>';          
        }
      }
      ?>
    <h3>Sales</h3>
    <form action="admin/reports" method="get" name="sales" id="sales">
      <table width="90%" border="0" cellspacing="20" cellpadding="0" align="center">
        <tr>
          <th valign="top"><select name="by" id="by" style="width:100px;">
              <option value="">Select One</option>
              <option value="summery">By Summary</option>
              <option value="by_product">By Products</option>
              <option value="product_type">By Products Type</option>
              <option value="product_level">By Products Level</option>
              <option value="product_subject">By Products Subject</option>
              <option value="by_seller">By Sellers</option>
            </select>
            <select name="product" id="product" style="width:100px; display:none; margin-top:10px;">
              <option>All</option>
              	<?php foreach($products as $product){?>
                <option><?php echo $product->product_name;?></option>
                <?php }?>
            </select>
            <select name="seller" id="seller" style="width:100px; display:none; margin-top:10px;">
              <option>All</option>
              	<?php foreach($users as $user){?>
                <option><?php echo $user->user_username;?></option>
                <?php }?>
            </select>
            <select name="level" id="level" style="width:100px; display:none; margin-top:10px;">
              <option>All</option>
              	<?php foreach($levels as $level){?>
                <option><?php echo $level->level;?></option>
                <?php }?>
            </select>
            <select name="subject" id="subject" style="width:100px; display:none; margin-top:10px;">
              <option>All</option>
              	<?php foreach($subjects as $subject){?>
                <option><?php echo $subject->subjects;?></option>
                <?php }?>
            </select>
            <select name="product_type" id="product_type" style="width:100px; display:none; margin-top:10px;">
              <option>All</option>
                <option>Digital</option>
                <option>Physical</option>
            </select></th>
          <th valign="top"><input type="text" name="from" id="from" class="box" placeholder="From date (yyyy-mm-dd)" /></th>
          <th valign="top"><input type="text" name="to" id="to" class="box" placeholder="To date (yyyy-mm-dd)" /></th>
          <th valign="top"><div class="right-botton">
              <div class="right-botton1">
                <div class="right-botton2">
                  <input type="submit" name="sales_but" id="sales_but" value="Generate" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
                </div>
              </div>
            </div></th>
        </tr>
      </table>
    </form>
    <h3>Downloads</h3>
    <form action="admin/reports" method="get" name="downloads" id="downloads">
      <table width="90%" border="0" cellspacing="20" cellpadding="0" align="center">
        <tr>
          <th valign="top"><select name="by" id="by" style="width:100px;">
              <option value="">Select One</option>
              <option value="by_product">By Products</option>
              <option value="by_seller">By Sellers</option>
              <option value="product_level">By Products Level</option>
              <option value="product_subject">By Products Subject</option>
            </select>
            <select name="product" id="product" style="width:100px; display:none; margin-top:10px;">
              <option>All</option>
              	<?php foreach($products as $product){?>
                <option><?php echo $product->product_name;?></option>
                <?php }?>
            </select>
            <select name="seller" id="seller" style="width:100px; display:none; margin-top:10px;">
              <option>All</option>
              	<?php foreach($users as $user){?>
                <option><?php echo $user->user_username;?></option>
                <?php }?>
            </select>
            <select name="level" id="level" style="width:100px; display:none; margin-top:10px;">
              <option>All</option>
              	<?php foreach($levels as $level){?>
                <option><?php echo $level->level;?></option>
                <?php }?>
            </select>
            <select name="subject" id="subject" style="width:100px; display:none; margin-top:10px;">
              <option>All</option>
              	<?php foreach($subjects as $subject){?>
                <option><?php echo $subject->subjects;?></option>
                <?php }?>
            </select></th>
          <th valign="top"><input type="text" name="from" id="from" class="box" placeholder="From date (yyyy-mm-dd)" /></th>
          <th valign="top"><input type="text" name="to" id="to" class="box" placeholder="To date (yyyy-mm-dd)" /></th>
          <th valign="top"><div class="right-botton">
              <div class="right-botton1">
                <div class="right-botton2">
                  <input type="submit" name="downloads_but" id="downloads_but" value="Generate" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
                </div>
              </div>
            </div></th>
        </tr>
      </table>
    </form>
    <h3>Products</h3>
    <form action="admin/reports" method="get" name="products" id="products">
      <table width="90%" border="0" cellspacing="20" cellpadding="0" align="center">
        <tr>
          <th valign="top"><select name="by" id="by" style="width:100px;">
              <option value="">Select One</option>
              <option value="by_seller">By Sellers</option>
              <option value="product_level">By Products Level</option>
              <option value="product_subject">By Products Subject</option>
              <option value="product_type">By Products Type</option>
            </select>
            <select name="level" id="level" style="width:100px; display:none; margin-top:10px;">
              <option>All</option>
              	<?php foreach($levels as $level){?>
                <option><?php echo $level->level;?></option>
                <?php }?>
            </select>
            <select name="subject" id="subject" style="width:100px; display:none; margin-top:10px;">
              <option>All</option>
              	<?php foreach($subjects as $subject){?>
                <option><?php echo $subject->subjects;?></option>
                <?php }?>
            </select>
            <select name="seller" id="seller" style="width:100px; display:none; margin-top:10px;">
              <option>All</option>
              <?php foreach($users as $user){?>
              <option><?php echo $user->user_username;?></option>
              <?php }?>
            </select>
            <select name="product_type" id="product_type" style="width:100px; display:none; margin-top:10px;">
              <option>All</option>
              <option>Digital</option>
              <option>Physical</option>
            </select></th>
          <th valign="top"><input type="text" name="from" id="from" class="box" placeholder="From date (yyyy-mm-dd)" /></th>
          <th valign="top"><input type="text" name="to" id="to" class="box" placeholder="To date (yyyy-mm-dd)" /></th>
          <th valign="top"><div class="right-botton">
              <div class="right-botton1">
                <div class="right-botton2">
                  <input type="submit" name="products_but" id="products_but" value="Generate" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
                </div>
              </div>
            </div></th>
        </tr>
      </table>
    </form>
    <h3>Promotions</h3>
    <form action="admin/reports" method="get" name="promotions" id="promotions">
      <table width="90%" border="0" cellspacing="20" cellpadding="0" align="center">
        <tr>
          <th valign="top">&nbsp;</th>
          <th valign="top"><input type="text" name="from" id="from" class="box" placeholder="From date (yyyy-mm-dd)" /></th>
          <th valign="top"><input type="text" name="to" id="to" class="box" placeholder="To date (yyyy-mm-dd)" /></th>
          <th valign="top"><div class="right-botton">
              <div class="right-botton1">
                <div class="right-botton2">
                  <input type="submit" name="promotions_but" id="promotions_but" value="Generate" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
                </div>
              </div>
            </div></th>
        </tr>
      </table>
    </form>
  </div>
  <?php if(isset($prd)){ //echo 'Total: '.$total;?>
  <div class="containerinner2" style="margin:0; width:100%;"> <h2><?php echo $prd;?></h2>
    <?php if(count($orders)){
		  if($prd == 'Sales'){
			  if($this->uri->segment(9) == 'summery'){?>
              	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr class="headerlist" style="color:#fff;">
                    <th style="padding:3px;">Username</th>
                    <th style="padding:3px;">Total Sales</th>
                    <th style="padding:3px;">Total Sales<br>Amount </th>
                    <th style="padding:3px;">Net Sales<br>Amount </th>
                    <th style="padding:3px; text-transform:uppercase">Bank Details</th>
                  </tr>
                  <?php $i = 0;
                    foreach($orders as $carts){
                        if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
                  <tr height="40" style=" <?php echo $bg;?>">
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts['sale']->productOwner;?></td>
                    <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts['sale']->totalOrders;?></td>
                    <?php $tots = $carts['sale']->totlaSale - $carts['disc'];?>
                    <td style="border:1px solid #CCC; padding:2px;" align="center">S$ <?php echo number_format($tots, 2)?></td>
                    <td style="border:1px solid #CCC; padding:2px;" align="center">S$ <?php echo number_format($tots - ($tots * $site->margin/100), 2);?></td>
                    <td style="border:1px solid #CCC; padding:2px;" align="left"><?php if($carts['sale']->bank_name == 'Others'){
						echo 'Bank Name: <strong>'.$carts['sale']->bank_other.'</strong>';
						}else{echo 'Bank Name: <strong>'.$carts['sale']->bank_name.'</strong>';}
						echo '<br />Bank Code: <strong>'.$carts['sale']->bank_code.'</strong>,<br />Branch Code: <strong>'.
						$carts['sale']->bank_branch_code.'</strong>,<br />Account Type: <strong>'.$carts['sale']->bank_account_type.'</strong>,<br />
						Account Number: <strong>'.$carts['sale']->bank_account_number.'</strong>'?></td>
                  </tr>
                  <?php $i++;
                    }?>
                </table>
              <?php }else if($this->uri->segment(9) == 'seller'){?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr class="headerlist" style="color:#fff;">
                    <th style="padding:3px;">Seller Name</th>
                    <th style="padding:3px;">Product Name</th>
                    <th style="padding:3px;">Product Price</th>
                    <th style="padding:3px;">Purchase Date</th>
                    <th style="padding:3px;">Buyer Name</th>
                  </tr>
                  <?php $i = 0;
                    foreach($orders as $carts){
                        if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
                  <tr height="40" style=" <?php echo $bg;?>">
                    <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->puser_id;?></td>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->order_pname);?></td>
                    <td style="border:1px solid #CCC; padding:2px;">S$ <?php echo number_format($carts->order_pprice, 2);?></td>
                    <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo date('Y-m-d', strtotime($carts->created));?></td>
                    <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->user_id;?></td>
                  </tr>
                  <?php $i++;
                    }?>
                </table>
             <?php }else if($this->uri->segment(9) == 'product'){?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr class="headerlist" style="color:#fff;">
                    <th style="padding:3px;">Product Name</th>
                    <th style="padding:3px;">Product Type</th>
                    <th style="padding:3px;">Purchase Date</th>
                    <th style="padding:3px;">Seller Name</th>
                    <th style="padding:3px;">Product Price</th>
                    <th style="padding:3px;">Total Sales</th>
                    <th style="padding:3px;">Total Sales Amount</th>
                  </tr>
                  <?php $i = 0;
                    foreach($orders as $carts){
                        if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
                  <tr height="40" style=" <?php echo $bg;?>">
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts['sale']->order_pname);?></td>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts['sale']->order_ptype;?></td>
                    <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo date('Y-m-d', strtotime($carts['sale']->created));?></td>
                    <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts['sale']->puser_id;?></td>
                    <td style="border:1px solid #CCC; padding:2px;" align="right">S$ <?php echo number_format($carts['sale']->order_pprice, 2);?></td>
                    <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts['sale']->TotalSales;?></td>
                    <td style="border:1px solid #CCC; padding:2px; padding-right:10px;" align="right">S$ <?php echo number_format($carts['sale']->TotalAmount - $carts['disc'], 2);?></td>
                  </tr>
                  <?php $i++;
                    }?>
                </table>
             <?php }else if($this->uri->segment(9) == 'product_type'){?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr class="headerlist" style="color:#fff;">
                    <th style="padding:3px;">Product Type</th>
                    <th style="padding:3px;">Product Level</th>
                    <th style="padding:3px;">Product Subject</th>
                    <th style="padding:3px;">Product Name</th>
                    <th style="padding:3px;">Product Price</th>
                    <th style="padding:3px;">Purchase Date</th>
                    <th style="padding:3px;">Seller Name</th>
                    <th style="padding:3px;">Buyer Name</th>
                  </tr>
                  <?php $i = 0;
                    foreach($orders as $carts){
                        if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
                  <tr height="40" style=" <?php echo $bg;?>"><?php $cat = explode(':',$carts->order_cat);?>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->order_ptype;?></td>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo $cat[0];?></td>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo $cat[1];?></td>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->order_pname);?></td>
                    <?php if($carts->order_promo == ''){$discount = $carts->order_discount;}else{$discount = 0;} ?>
                    <td style="border:1px solid #CCC; padding:2px;">S$ <?php echo number_format($carts->order_pprice - $discount, 2);?></td>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo date('Y-m-d',strtotime($carts->created));?></td>
                    <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->puser_id;?></td>
                    <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->user_id;?></td>
                  </tr>
                  <?php $i++;
                    }?>
                </table>
             <?php }else if($this->uri->segment(9) == 'level'){?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr class="headerlist" style="color:#fff;">
                    <th style="padding:3px;">Product Level</th>
                    <th style="padding:3px;">Product Type</th>
                    <th style="padding:3px;">Product Subject</th>
                    <th style="padding:3px;">Product Name</th>
                    <th style="padding:3px;">Product Price</th>
                    <th style="padding:3px;">Purchase Date</th>
                    <th style="padding:3px;">Seller Name</th>
                    <th style="padding:3px;">Buyer Name</th>
                  </tr>
                  <?php $i = 0;
                    foreach($orders as $carts){
                        if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
                  <tr height="40" style=" <?php echo $bg;?>"><?php $cat = explode(':',$carts->order_cat);?>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo $cat[0];?></td>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->order_ptype;?></td>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo $cat[1];?></td>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->order_pname);?></td>
                    <?php if($carts->order_promo == ''){$discount = $carts->order_discount;}else{$discount = 0;} ?>
                    <td style="border:1px solid #CCC; padding:2px;">S$ <?php echo number_format($carts->order_pprice - $discount, 2);?></td>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo date('Y-m-d',strtotime($carts->created));?></td>
                    <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->puser_id;?></td>
                    <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->user_id;?></td>
                  </tr>
                  <?php $i++;
                    }?>
                </table>
             <?php }else if($this->uri->segment(9) == 'subject'){?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr class="headerlist" style="color:#fff;">
                    <th style="padding:3px;">Product Subject</th>
                    <th style="padding:3px;">Product Type</th>
                    <th style="padding:3px;">Product Level</th>
                    <th style="padding:3px;">Product Name</th>
                    <th style="padding:3px;">Product Price</th>
                    <th style="padding:3px;">Purchase Date</th>
                    <th style="padding:3px;">Seller Name</th>
                    <th style="padding:3px;">Buyer Name</th>
                  </tr>
                  <?php $i = 0;
                    foreach($orders as $carts){
                        if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
                  <tr height="40" style=" <?php echo $bg;?>"><?php $cat = explode(':',$carts->order_cat);?>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo $cat[1];?></td>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->order_ptype;?></td>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo $cat[0];?></td>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->order_pname);?></td>
                    <?php if($carts->order_promo == ''){$discount = $carts->order_discount;}else{$discount = 0;} ?>
                    <td style="border:1px solid #CCC; padding:2px;">S$ <?php echo number_format($carts->order_pprice - $discount, 2);?></td>
                    <td style="border:1px solid #CCC; padding:2px;"><?php echo date('Y-m-d',strtotime($carts->created));?></td>
                    <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->puser_id;?></td>
                    <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->user_id;?></td>
                  </tr>
                  <?php $i++;
                    }?>
                </table>
             <?php }
			 
			 
		   }else if($prd == 'Downloads'){
			   if($this->uri->segment(9) == 'product'){?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr class="headerlist" style="color:#fff;">
                <th style="padding:3px;">Product Name</th>
                <th style="padding:3px;">Product Level</th>
                <th style="padding:3px;">Product Subject</th>
                <th style="padding:3px;">Seller Name</th>
                <th style="padding:3px;">Product Price</th>
                <th style="padding:3px;">Download Count</th>
              </tr>
              <?php $i = 0;
                foreach($orders as $carts){
                    if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
              <tr height="40" style=" <?php echo $bg;?>">
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->product_name);?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->product_level;?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->product_subject;?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->user_username);?></td>
                <td style="border:1px solid #CCC; padding:2px;">S$ <?php echo number_format($carts->product_price, 2);?></td>
                <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->down_count;?></td>
              </tr>
              <?php $i++;
                }?>
            </table>
		  <?php }else if($this->uri->segment(9) == 'seller'){?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr class="headerlist" style="color:#fff;">
                <th style="padding:3px;">Seller Name</th>
                <th style="padding:3px;">Product Name</th>
                <th style="padding:3px;">Product Level</th>
                <th style="padding:3px;">Product Subject</th>
                <th style="padding:3px;">Product Price</th>
                <th style="padding:3px;">Download Count</th>
              </tr>
              <?php $i = 0;
                foreach($orders as $carts){
                    if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
              <tr height="40" style=" <?php echo $bg;?>">
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->user_username);?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->product_name);?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->product_level;?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->product_subject;?></td>
                <td style="border:1px solid #CCC; padding:2px;">S$ <?php echo number_format($carts->product_price, 2);?></td>
                <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->down_count;?></td>
              </tr>
              <?php $i++;
                }?>
            </table>
		  <?php }else if($this->uri->segment(9) == 'subject'){?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr class="headerlist" style="color:#fff;">
                <th style="padding:3px;">Product Subject</th>
                <th style="padding:3px;">Product Name</th>
                <th style="padding:3px;">Product Level</th>
                <th style="padding:3px;">Seller Name</th>
                <th style="padding:3px;">Product Price</th>
                <th style="padding:3px;">Download Count</th>
              </tr>
              <?php $i = 0;
                foreach($orders as $carts){
                    if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
              <tr height="40" style=" <?php echo $bg;?>">
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->product_subject;?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->product_name);?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->product_level;?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->user_username);?></td>
                <td style="border:1px solid #CCC; padding:2px;">S$ <?php echo number_format($carts->product_price, 2);?></td>
                <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->down_count;?></td>
              </tr>
              <?php $i++;
                }?>
            </table>
		  <?php }else if($this->uri->segment(9) == 'level'){?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr class="headerlist" style="color:#fff;">
                <th style="padding:3px;">Product Level</th>
                <th style="padding:3px;">Product Name</th>
                <th style="padding:3px;">Product Subject</th>
                <th style="padding:3px;">Seller Name</th>
                <th style="padding:3px;">Product Price</th>
                <th style="padding:3px;">Download Count</th>
              </tr>
              <?php $i = 0;
                foreach($orders as $carts){
                    if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
              <tr height="40" style=" <?php echo $bg;?>">
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->product_level;?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->product_name);?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->product_subject;?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->user_username);?></td>
                <td style="border:1px solid #CCC; padding:2px;">S$ <?php echo number_format($carts->product_price, 2);?></td>
                <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->down_count;?></td>
              </tr>
              <?php $i++;
                }?>
            </table>
		  <?php }
		  }else if($prd == 'Products'){
			  if($this->uri->segment(9) == 'seller'){?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr class="headerlist" style="color:#fff;">
                <th style="padding:3px;">Seller Name</th>
                <th style="padding:3px;">Product Name</th>
                <th style="padding:3px;">Product Level</th>
                <th style="padding:3px;">Product Subject</th>
                <th style="padding:3px;">Product Price</th>
                <th style="padding:3px;">Product Type</th>
              </tr>
              <?php $i = 0;
                foreach($orders as $carts){
                    if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
              <tr height="40" style=" <?php echo $bg;?>">
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->user_username);?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->product_name);?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->product_level;?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->product_subject;?></td>
                <td style="border:1px solid #CCC; padding:2px;">S$ <?php echo number_format($carts->product_price, 2);?></td>
                <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->product_type;?></td>
              </tr>
              <?php $i++;
                }?>
            </table>
		  <?php }else if($this->uri->segment(9) == 'level'){?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr class="headerlist" style="color:#fff;">
                <th style="padding:3px;">Product Level</th>
                <th style="padding:3px;">Product Name</th>
                <th style="padding:3px;">Product Subject</th>
                <th style="padding:3px;">Product Price</th>
                <th style="padding:3px;">Product Type</th>
                <th style="padding:3px;">Seller Name</th>
              </tr>
              <?php $i = 0;
                foreach($orders as $carts){
                    if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
              <tr height="40" style=" <?php echo $bg;?>">
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->product_level;?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->product_name);?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->product_subject;?></td>
                <td style="border:1px solid #CCC; padding:2px;">S$ <?php echo number_format($carts->product_price, 2);?></td>
                <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->product_type;?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->user_username);?></td>
              </tr>
              <?php $i++;
                }?>
            </table>
		  <?php }else if($this->uri->segment(9) == 'subject'){?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr class="headerlist" style="color:#fff;">
                <th style="padding:3px;">Product Subject</th>
                <th style="padding:3px;">Product Name</th>
                <th style="padding:3px;">Product Level</th>
                <th style="padding:3px;">Product Price</th>
                <th style="padding:3px;">Product Type</th>
                <th style="padding:3px;">Seller Name</th>
              </tr>
              <?php $i = 0;
                foreach($orders as $carts){
                    if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
              <tr height="40" style=" <?php echo $bg;?>">
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->product_subject;?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->product_name);?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->product_level;?></td>
                <td style="border:1px solid #CCC; padding:2px;">S$ <?php echo number_format($carts->product_price, 2);?></td>
                <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->product_type;?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->user_username);?></td>
              </tr>
              <?php $i++;
                }?>
            </table>
		  <?php }else if($this->uri->segment(9) == 'product_type'){?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr class="headerlist" style="color:#fff;">
                <th style="padding:3px;">Product Type</th>
                <th style="padding:3px;">Product Name</th>
                <th style="padding:3px;">Product Level</th>
                <th style="padding:3px;">Product Subject</th>
                <th style="padding:3px;">Product Price</th>
                <th style="padding:3px;">Seller Name</th>
              </tr>
              <?php $i = 0;
                foreach($orders as $carts){
                    if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
              <tr height="40" style=" <?php echo $bg;?>">
                <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->product_type;?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->product_name);?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->product_level;?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->product_subject;?></td>
                <td style="border:1px solid #CCC; padding:2px;">S$ <?php echo number_format($carts->product_price, 2);?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->user_username);?></td>
              </tr>
              <?php $i++;
                }?>
            </table>
		  <?php }
		  }else if($prd == 'Promotions'){?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr class="headerlist" style="color:#fff;">
                <th style="padding:3px;">Start Date</th>
                <th style="padding:3px;">End Date</th>
                <th style="padding:3px;">Discount Value</th>
                <th style="padding:3px;">Promo Code</th>
                <th style="padding:3px;">Promo For</th>
                <th style="padding:3px;">User Name</th>
                <th style="padding:3px;">Date Used</th>	
                <th style="padding:3px;">Purchased Amount</th>
              </tr>
              <?php $i = 0;
                foreach($orders as $carts){
                    if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}?>
              <tr height="40" style=" <?php echo $bg;?>">
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->start_date);?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo stripslashes($carts->end_date);?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->discount_values;?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->promo_code;?></td>
                <td style="border:1px solid #CCC; padding:2px;"><?php echo $carts->promo_for;?></td>
                <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->username;?></td>
                <td style="border:1px solid #CCC; padding:2px;" align="center"><?php echo $carts->date_used;?></td>
                <td style="border:1px solid #CCC; padding:2px;">S$ <?php echo number_format($carts->purchased_amount, 2);?></td>
              </tr>
              <?php $i++;
                }?>
            </table>
		  <?php 
		  }?>
        <div id="cont" style="float:right; margin-top:20px;"><?php echo $pagination; ?></div>
        <!---------- start generate-report ----------->
        <div class="generate-report" style="margin:0 !important;margin-top:40px !important;"><?php echo $link;?></div>
        <!---------- end generate-report ----------->
        <?php }else{echo '<p style=" font-size:16px; font-weight:bold;">No records found.</p>';}?>
      </div>
  <?php }?>
  <div class="clear"></div>
  <!---------- end product page ----------->
  <div class="clear"></div>
  <div class="clear"></div>
</div>