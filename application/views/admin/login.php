<!DOCTYPE html>
<html lang="en-US">
<head>
<title>OpenSchoolbag Admin Panel</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<base href="<?php echo base_url();?>" />
<link rel="shortcut icon" href="files/images/favicon.jpg" type="image/jpg" />
<link href="files/admin/css/global.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="files/js/jquery-1.10.2.min.js"></script>
<script src="files/admin/js/admin.js"></script>
</head>

<body>
<center><img src="files/images/logo.png" width="216" height="137"></center>
<div class="container login" id="login">
  <form id="sign-in" action="admin/validate_credentials" method="post" class="form-signin">
    <h2 class="form-signin-heading">Login</h2>
    <?php if(isset($message_error) && $message_error){
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> Change a few things up and try submitting again.';
          echo '</div>';             
      	}?>
    <input name="user_name" id="user_name" pattern="[a-zA-Z0-9]{4,}" type="text" tabindex="1" placeholder="Username" required autofocus />
    <input name="password" id="password" pattern=".{6,}" type="password" tabindex="2" placeholder="Password" required />
    <input name="submit" type="submit" id="submit" class="btn btn-large btn-primary" value="Sign In" /><br><br>
    <span style="font-weight:bold; letter-spacing:1px;"><a href="javascript:;" onClick="document.getElementById('forgot').style.display='block'; document.getElementById('login').style.display='none';">Forgot password?</a></span>
  </form>
</div>
<div class="container login" id="forgot" style="display:none;">
  <form id="forgot-form" action="" method="post" class="form-signin">
    <h2 class="form-signin-heading">Forgot Password?</h2>
    <div class="forg_mess" style="color:#C30; display:none;"><img src="files/images/ajax-loader.gif" /></div>
    <span>Email*</span>
    <input type="email" name="forgot_email" id="forgot_email" tabindex="1" required placeholder="Email Address" />
    <input name="forgot-submit" type="button" id="forgot-submit" class="btn btn-large btn-primary" value="Send Password" style="width:130px;" /><br><br><br>
    <span style="font-weight:bold; letter-spacing:1px;"><a href="javascript:;" onClick="document.getElementById('login').style.display='block'; document.getElementById('forgot').style.display='none';">LOGIN</a></span>
</form>
</div>
</body>
</html>