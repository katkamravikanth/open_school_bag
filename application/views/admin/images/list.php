<link href="files/admin/css/style1.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="files/admin/css/dropdown.css" />
<link rel="stylesheet" href="files/css/themes/alertify.core.css" />
<link rel="stylesheet" href="files/css/themes/alertify.default.css" id="toggleCSS" />
<script src="files/js/lib/alertify.min.js"></script>
<script type="text/javascript">
$(function(){
	function reset () {
		$("#toggleCSS").attr("href", "files/css/themes/alertify.default.css");
		alertify.set({
			labels : {
				ok     : "OK",
				cancel : "Cancel"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}
	// delete product
	$(".delete").on( 'click', function () {
		id = $(this).attr('name');
		reset();
		alertify.confirm("Are you sure do you want to delete this image?", function (e) {
			if(e){
				window.location='admin/images/delete/'+id;
			}
		});
		return false;
	});
	
	$('.save_order').click(function(e) {
        id = $(this).attr('name');
		order = $('#change_order'+id).val();
		$.post('admin/images/update', {id:id, order:order}, function(data){
			if(data != true){
				$('.banner_mess').addClass('alert-success');
				$('.banner_mess').html('Order changed');
				$('.banner_mess').show();
			}else{
				$('.banner_mess').addClass('alert-error');
				$('.banner_mess').html('Order not changed');
				$('.banner_mess').show();
			}
		})
    });
});
</script>
<style>
input[type="text"], select, textarea {
	color: #000;
}
a.add_banner {
	color: #6E6C64;
}
a.add_banner:hover {
	color: #ef4f45;
}
</style>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p><?php echo $title;?></p>
        </li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start main ------------>
  <div style="float: right; font-size:16px; font-weight:bold;"><br />
    <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>/add" class="add_banner">Add a new</a></div>
  <div class="main" style="padding-top:10px; width:100%">
    <div class="product1">
      <div class="banner_mess" style="display:none; padding-bottom:10px; color:#090; font-size:16px; font-weight:bold;">&nbsp;</div><br />
      <?php
      if(count($banner)){?>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="width:670px;">
        <tr style="background-color:#0b9444; color:#fff; height:40px; font-size:16px; margin-bottom:10px;">
          <th>Image</th>
          <th>Path</th>
          <th>Actions</th>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <?php foreach($banner as $row){?>
        <tr style="padding-bottom:4px;">
          <td style="border-bottom:1px solid #666; padding-top:5px; padding-bottom:5px"><img src="files/<?php echo $row->photo;?>" style="max-width:500px; max-height:100px;" /></td>
          <td style="border-bottom:1px solid #666;"><?php echo $row->path;?></td>
          <td style="border-bottom:1px solid #666;"><div class="update">
          <ul>
            <li style="border-bottom:1px solid #666; margin-bottom:10px; padding-bottom:10px">Order
              <select name="change_order<?php echo $row->image_id;?>" id="change_order<?php echo $row->image_id;?>" style="border:1px solid #666">
                <?php for($i=1; $i<=5;$i++){?>
                <option <?php if($row->order == $i){echo 'selected="selected"';} echo 'value="'.$i.'"'?>><?php echo $i;?></option>
                <?php }?>
              </select>
            </li>
            <li style="border-right:1px solid #333"><a name="<?php echo $row->image_id;?>" style="cursor:pointer;" class="delete">Delete</a></li>
            <li><a href="javascript:;" name="<?php echo $row->image_id;?>" class="save_order">Save</a></li>
          </ul>
        </div></td>
        </tr>
        <?php }?>
      </table><br /><br />
      <div id="cont"><?php echo $this->pagination->create_links();?></div>
      <?php }else{?>
      No images found
      <?php }?>
    </div>
  </div>
</div>