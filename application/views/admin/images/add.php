<link rel="stylesheet" href="files/admin/css/jquery.fileupload-ui.css" />
<script type="text/javascript" src="files/js/customSelect.jquery.min.js"></script>
<script type="text/javascript">
$(function () {
	$(".custom_select select").customSelect();
	
	$("#path").change(function() {
        if($(this).val() == 'product-icon'){
			$('#size').html('W 180px x H 160px');
			$(".title").slideDown();
		}else if($(this).val() == 'about-slider'){
			$('#size').html('W 322px x H 215px');
			$(".title").slideUp();
		}else if($(this).val() == 'who-we-care' || $(this).val() == 'seller' || $(this).val() == 'buyer'){
			$('#size').html('Max W 885px');
			$(".title").slideUp();
		}else if($(this).val() == 'promo-bg'){
			$('#size').html('W 415px x H 165px');
			$(".title").slideUp();
		}else if($(this).val() == 'home'){
			$('#size').html('W 875px x H 370px');
			$(".title").slideUp();
		}else{
			$('#size').html('');
			$(".title").slideUp();
		}
    });
});
</script>
<link rel="stylesheet" type="text/css" href="files/css/dropdown2.css" />
<style type="text/css">
input[type="text"], select, textarea {
	color: #000;
}
</style>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" /><a href="<?php echo site_url("admin").'/'.$this->uri->segment(2);?>">
          <p><?php echo ucfirst($this->uri->segment(2));?></p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p><?php echo $title;?></p>
        </li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start product  page ------------>
  <div class="product">
    <div class="product-inner">
      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div style="padding-left:20px; padding-top:20px; color:#090; font-size:16px; font-weight:bold;">';
            echo '<strong>Well done!</strong> You have added a new image.';
          echo '</div><script type="text/javascript">$("input[type=\'text\'], select, textarea").val("");</script>';       
        }else{
          echo '<div style="padding-left:20px; padding-top:20px; color:#f30; font-size:16px; font-weight:bold;">';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
	  echo '<div style="padding-left:20px; padding-top:20px; color:#f30; font-size:16px; font-weight:bold;">';
      echo validation_errors();
	  echo '</div>';?>
      <form action="admin/images/add" method="post" enctype="multipart/form-data" class="form-horizontal" id="add_user">
      <div class="banner_mess" style="display:none; padding-left:20px; padding-top:20px; font-size:16px; font-weight:bold;">&nbsp;</div>
      <table width="90%" border="0" cellspacing="20" cellpadding="0" align="center">
        <tr>
          <th valign="top" class="fonts">Path * :</th>
          <td valign="top"><div class="input2">
              <div class="custom_select">
                <select name="path" id="path">
                  <option value="home">Home Banner</option>
                  <option value="about-slider">About Slider</option>
                  <option value="who-we-care">Who we care about and how can we help?</option>
                  <option value="seller">For Seller</option>
                  <option value="buyer">For Buyer</option>
                  <option value="promo-bg">Promotions BG</option>
                  <option value="product-icon">Product Icon</option>
                </select>
              </div>
            </div><span id="size" style="font-weight:bold;">W 875px x H 370px</span></td>
        </tr>
        <tr>
          <th valign="top" class="fonts">Photo * :</th>
          <td valign="top"><input type="file" id="photo" name="photo" class="box" /></td>
        </tr>
        <tr class="title" style="display:none">
          <th valign="top" class="fonts">Icon Title * :</th>
          <td valign="top"><input type="text" id="title" name="title" class="box" /></td>
        </tr>
        <tr>
          <th valign="top" class="fonts">Order * :<br><small>(Image position)</small></th>
          <td valign="top"><div class="input2">
              <div class="custom_select">
                <select name="order" id="order">
                  <?php for($i=1; $i<=5;$i++){?>
                  <option value="<?php echo $i;?>"><?php echo $i;?></option>
                  <?php }?>
                </select>
              </div>
            </div></td>
        </tr>
        <tr>
          <th valign="top" class="fonts" colspan="2"><div class="right-botton" style="width:117px">
              <div class="right-botton1" style="width:111px">
                <div class="right-botton2" style="width:107px">
                  <input type="submit" name="save" id="save" value="Add" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
                </div>
              </div>
            </div>
            </td>
        </tr>
      </table>
      </form>
    </div>
  </div>
  <div class="clear"></div>
  <!---------- end product page ----------->
  <div class="clear"></div>
  <div class="clear"></div>
</div>