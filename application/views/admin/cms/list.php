<style>
input[type="text"], select, textarea {
	color: #000;
}
a.add_page {
	color: #6E6C64;
}
a.add_page:hover {
	color: #ef4f45;
}
</style>
<link href="files/admin/css/style1.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="files/admin/css/dropdown.css" />
<link rel="stylesheet" href="files/css/themes/alertify.core.css" />
<link rel="stylesheet" href="files/css/themes/alertify.default.css" id="toggleCSS" />
<script src="files/js/lib/alertify.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	function reset () {
		$("#toggleCSS").attr("href", "files/css/themes/alertify.default.css");
		alertify.set({
			labels : {
				ok     : "OK",
				cancel : "Cancel"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}
	// delete page
	$(".delete").on( 'click', function () {
		id = $(this).attr('name');
		reset();
		alertify.confirm("Are you sure do you want to delete this page?", function (e) {
			if(e){
				window.location='admin/cms/delete/'+id;
			}
		});
		return false;
	});
	<?php if($this->uri->segment(4) == 'state' && $this->uri->segment(5) == 'yes'){?>
		alertify.success('This page has been deleted');
	<?php }else if($this->uri->segment(4) == 'state' && $this->uri->segment(5) == 'no'){?>
		alertify.error('Unable to delete this page. Please try later');
	<?php }?>
});
</script>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p>CMS</p>
        </li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p><?php echo $title;?></p>
        </li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start main ------------>
  <div style="float: right; font-size:16px; font-weight:bold; color:#f30"><br />
    <a href="admin/cms/add" class="add_page">Add new</a></div>
  <div class="main" style="padding-top:10px;">
    <div class="product1" style="min-height:300px;">
      <?php if(count($pages)){
	  foreach($pages as $row){?>
      <div class="products_list" style="float:left;">
        <div class="details" style="width:100%;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="width:650px !important;">
            <tr class="first" style="color:#fff;">
              <th style="padding:2px;">Title</th>
              <th style="padding:2px;">URL</th>
              <th style="padding:2px;">Order</th>
            </tr>
            <tr height="40">
              <td style="padding:2px; border:1px solid #CCC;"><?php echo $row['title'];?></td>
              <td style="padding:2px; border:1px solid #CCC;"><?php echo $row['slug'];?></td>
              <td align="center" style="padding:2px; border:1px solid #CCC;"><?php echo $row['order'];?></td>
            </tr>
          </table>
        </div>
        <div class="update">
          <ul>
            <li><a href="javascript:;" name="<?php echo $row['cms_id'];?>" class="delete" style="border-right:1px solid #000;">Delete</a></li>
            <li><a href="admin/cms/update/<?php echo $row['cms_id'];?>">Update</a></li>
          </ul>
        </div>
      </div>
      <?php }?>
      <br />
      <br />
      <div id="cont"><?php echo $this->pagination->create_links();?></div>
      <?php }else{?>
      No pages added
      <?php }?>
    </div>
  </div>
</div>