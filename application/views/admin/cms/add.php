<link rel="stylesheet" href="files/admin/css/jquery.fileupload-ui.css" />
<script type="text/javascript" src="files/admin/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
$(function(){
	tinymce.init({selector:'textarea'});
	
	$("input#slug").blur(function(){
		if($(this).val() != ''){
			$("#cms_error").show();
			
			$.post('admin/page_slug',{slug:$(this).val()}, function (data){
				if(data){
					$("#cms_error").html("Slug already exists");
					return false;
				}else{
					$("#cms_error").hide();
					$("#cms_error").html('<img src="files/images/ajax-loader.gif" />')
				}
			});
		}
	});
	$("input#slug").keydown(function (e) {
		if (e.keyCode == 32) { 
			$(this).val($(this).val() + "_"); // append '_' to input
			return false; // return false to prevent space from being added
		}
	});
	$('#add_cms_page').submit(function(e) {
        if($('#cms_error').html() == "Slug already exists"){
			return false;
		}else{
			return true;
		}
    });
})
</script>
<style type="text/css">
input[type="text"], select, textarea {
	color: #000;
}
</style>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" /><p>Home</p></a></li>
        <li><img src="files/admin/images/navinnerarrow.png" /><a href="<?php echo site_url("admin").'/'.$this->uri->segment(2);?>/pages"><p>CMS</p></a></li>
        <li><img src="files/admin/images/navinnerarrow.png" /><p><?php echo $title;?></p></li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start product  page ------------>
  <div class="product" style="width:100%; padding-bottom:60px; margin-left:0; margin-right:0;">
    <div class="product-inner" style="width:90%; margin-left:0; margin-right:0; float:none; padding-left:20px;">
      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div style="padding-top:20px; color:#090; font-size:16px; font-weight:bold;">';
            echo '<strong>Well done!</strong> new page created with success.';
          echo '</div><script type="text/javascript">$("input, textarea, select").val("");</script>';
        }else{
          echo '<div style="padding-top:20px; color:#f30; font-size:16px; font-weight:bold;">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => 'add_cms_page');

      //form validation
      echo '<div style="padding-top:20px; color:#f30; font-size:16px; font-weight:bold;">';
      echo validation_errors();
	  echo '</div>';
      
      echo form_open('admin/cms/add', $attributes);
      ?>
      <div id="cms_error" style="display:none; padding-top:20px; color:#f30; font-size:16px; font-weight:bold;"><img src="files/images/ajax-loader.gif" /></div>
      <br />
      <br />
      <label style="font-size:16px; font-weight:bold;">Title</label>
      <br />
      <br />
      <input type="text" id="title" name="title" class="box" value="<?php echo set_value('name'); ?>" style="width:100%" />
      <br />
      <br />
      <label style="font-size:16px; font-weight:bold;">Body</label>
      <br />
      <br />
      <textarea id="body" name="body" rows="10" style="width:100%;"><?php echo set_value('body'); ?></textarea>
      <br />
      <br />
      <label style="font-size:16px; font-weight:bold;">URL</label>
      <br />
      <br />
      <input type="text" id="slug" name="slug" class="box" value="<?php echo set_value('slug'); ?>" style="width:100%" />
      <br />
      <br />
      <label style="font-size:16px; font-weight:bold;">Order <small style="font-size:10px">(This will make order in navigation)</small></label>
      <br />
      <br />
      <select id="order" name="order">
        <?php for($i=1;$i<=5;$i++){?>
        <option <?php if(set_value('order') == $i){echo 'selected="selected" ';} echo 'value="'.$i.'"';?>><?php echo $i;?></option>
        <?php }?>
      </select>
      <br />
      <br />
      <div style="display:block">
        <div class="right-botton">
          <div class="right-botton1">
            <div class="right-botton2">
              <input type="reset" name="reset" id="reset" value="Cancel" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
            </div>
          </div>
        </div>
        <div class="right-botton" style="width:117px">
          <div class="right-botton1" style="width:111px">
            <div class="right-botton2" style="width:107px">
              <input type="submit" name="save" id="save" value="Save Changes" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
            </div>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?> </div>
  </div>
  <div class="clear"></div>
  <!---------- end product page ----------->
  <div class="clear"></div>
  <div class="clear"></div>
</div>
