<style type="text/css">
input[type="text"], select, textarea {
	color: #000;
}
a.add_prod {
	color: #6E6C64;
}
a.add_prod:hover {
	color: #ef4f45;
}
</style>
<link href="files/admin/css/style1.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="files/css/dropdown.css" />
<link rel="stylesheet" href="files/css/themes/alertify.core.css" />
<link rel="stylesheet" href="files/css/themes/alertify.default.css" id="toggleCSS" />
<script src="files/js/lib/alertify.min.js"></script>
<script type="text/javascript" src="files/js/customSelect.jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	function reset () {
		$("#toggleCSS").attr("href", "files/css/themes/alertify.default.css");
		alertify.set({
			labels : {
				ok     : "OK",
				cancel : "Cancel"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}
	
	$(".custom_select select").customSelect();
	
	// delete product
	$(".delete").on( 'click', function () {
		id = $(this).attr('name');
		reset();
		alertify.confirm("Are you sure do you want to delete this product?", function (e) {
			if(e){
				window.location='admin/products/delete/'+id;
			}
		});
		return false;
	});
	<?php if($this->uri->segment(3) == 'state' && $this->uri->segment(4) == 'yes'){?>
		alertify.success('This product has been deleted');
	<?php }else if($this->uri->segment(3) == 'state' && $this->uri->segment(4) == 'no'){?>
		alertify.error('This product contain unfinished orders');
	<?php }?>
});
</script>

<div class="containerinner">
  <p class="heading">Manage Products</p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p>Selling</p>
        </li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p>Manage Products</p>
        </li>
      </ul>
    </div>
    <div class="export"><p><?php echo $link;?></p></div>
  </div>
  <!------- start main ------------>
  <div style="float: right; font-size:16px; font-weight:bold; color:#f30"><br />
    <a href="<?php echo 'admin/'.$this->uri->segment(2); ?>/add" class="add_prod">Add a new</a></div>
  <div class="main" style="padding-top:10px;">
    <form action="" method="get" style="border-bottom:1px solid #999; padding-bottom:5px; padding-top:80px; height:70px;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td width="15%">Search by </td>
          <td width="40%"><input type="text" name="search_string" id="search_string" placeholder="Enter Product Name/Keyword" style="height:25px; border: 1px solid #c7c7c7; width:100%" /></td>
          <td width="10%" align="center"><strong>( OR )</strong></td>
          <td width="10%">Filter by </td>
          <td width="10%"><div class="input">
              <div class="custom_select">
                <select name="order" id="order">
                  <option value="">Sort By</option>
                  <option>Digital</option>
                  <option>Physical</option>
                </select>
              </div>
            </div></td>
          <td width="15%" align="right"><div class="right-bottons" style="height:27px; width:100px; margin-bottom:-20px; margin-right:-50px;">
              <div class="right-bottons1" style="height:21px; width:94px;">
                <div class="right-bottons2" style="height:19px; width:90px;">
                  <input type="submit" name="search_prod" id="search_prod" value="Search" style="background:none; border:none; color:#fff; font-size:14px; cursor:pointer;" />
                </div>
              </div>
            </div></td>
        </tr>
      </table>
    </form>
    <div class="product1">
    	<div style="font-size:16px; font-weight:bold; color:#f30">Total: <?php echo $total;?></div>
      <?php if(count($products)){
	 foreach($products as $row){?>
      <div class="products_list">
        <?php if($row['pro']->product_pic!=''){$pic = 'files/'.$row['pro']->product_pic;}else{
				foreach($resources as $source){
				  if($row['pro']->product_resource == $source->resources){$pic = 'files/'.$source->icon;}
				}?>
        <?php }?>
        <div class="images"> <img src="<?php echo $pic;?>" width="99" height="103" /></div>
        <div class="details">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr class="first" style="font-size:16px; color:#fff;">
              <th style="padding:3px;" width="220">Title of Product</th>
              <th style="padding:3px;" width="90">Price</th>
              <th style="padding:3px;" width="100">Type</th>
              <th style="padding:3px;">Rating</th>
              <th style="padding:3px;">Reviews / Q&amp;A</th>
            </tr>
            <tr height="50" style="font-size:14px;">
              <td style="padding:3px; border:1px solid #CCC;"><?php echo $row['pro']->product_name;?></td>
              <td align="center" style="padding:3px; border:1px solid #CCC;">S$ <?php echo $row['pro']->product_price;?></td>
              <td align="center" style="padding:3px; border:1px solid #CCC;"><?php echo $row['pro']->product_type;?></td>
              <td align="center" style="padding:3px; border:1px solid #CCC;"><?php if($row['avg'] != 0){?>
                <div class="porduct-star">
                  <?php for($i=1;$i<=$row['avg'];$i++){echo '<a class="icon star">&nbsp;</a> ';}?>
                </div>
                <?php }else{echo 0;}?></td>
              <td align="center" style="padding:3px; border:1px solid #CCC;"><a href="admin/products/rqna/<?php echo $row['pro']->product_unique;?>" style="color:#333; text-decoration:underline">Reviews / Q&amp;A</a></td>
            </tr>
          </table>
        </div>
        <div class="update" <?php if($row['pro']->product_type == 'Digital'){?>style="width:44%; float:right;"<?php }?> align="right">
          <ul>
          <?php if($row['pro']->product_type == 'Digital'){?>
            <li><a href="download/<?php echo $row['pro']->product_unique;?>" style="border-right:1px solid #000; cursor:pointer;">Download product file</a></li>
          <?php }?>
            <li><a class="delete" name="<?php echo $row['pro']->product_id;?>" style="border-right:1px solid #000; cursor:pointer;">Delete</a></li>
            <li><a href="admin/products/update/<?php echo $row['pro']->product_unique;?>">Update</a></li>
          </ul>
        </div>
      </div>
      <?php }?>
      <br />
      <br />
      <div id="cont"><?php echo $this->pagination->create_links();?></div>
      <?php }else{?>
      No product posted
      <?php }?>
    </div>
  </div>
</div>
