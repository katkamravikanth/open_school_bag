<?php
if($title == 'Edit Product'){
	$pub = $product->user_id;
	$ptype = $product->product_type;
	$pname = $product->product_name;
	$plevel = $product->product_level;
	//$plevel = explode(",", $product->product_level);
	$psubject = $product->product_subject;
	//$psubject = explode(",", $product->product_subject);
	$presource = $product->product_resource;
	//$presource = explode(",", $product->product_resource);
	$pdesc = $product->product_desc;
	$pprice = $product->product_price;
	$pdownd = $product->product_down_days;
	$ppages = $product->product_pages;
	$pweight = $product->product_weight;
	$ppic = $product->product_pic;
	$ppreview = explode(",", $product->product_preview);
	$ppview = $product->product_preview;
	$pfile = $product->product_file;
	$pdisc = $product->product_desclaimer;
	$sprice = $product->product_shipping;
	$punique = $product->product_unique;
	$uslevel = $ulevel->user_level;
}else{
	$pub = ''; $ptype = ''; $pname = ''; $plevel = ''; $psubject = ''; $presource = ''; $pdesc = ''; $pprice = ''; $pdownd = ''; $ppages = ''; $pweight = ''; $ppic = ''; $ppreview = array(); $ppview = ''; $pfile = ''; $pdisc = ''; $sprice = ''; $punique = ''; $uslevel = '';
}
?>
<script type="text/javascript" src="files/js/customSelect.jquery.min.js"></script>
<script type="text/javascript">
$(function () {
	$(".custom_select select").customSelect();
});
</script>
<link rel="stylesheet" type="text/css" href="files/css/dropdown2.css" />
<link rel="stylesheet" href="files/admin/css/jquery.fileupload-ui.css" />
<script type="text/javascript" src="files/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="files/js/jquery_ravikanth_products.js"></script>
<style type="text/css">
/* Tool tip */ 
a.tooltip {
	outline: none;
}
a.tooltip strong {
	line-height: 30px;
}
a.tooltip:hover {
	text-decoration: none;
}
a.tooltip span {
	z-index: 10;
	display: none;
	padding: 14px 20px;
	margin-top: -30px;
	margin-left: 28px;
	width: 220px;
	line-height: 16px;
}
a.tooltip:hover span {
	display: inline;
	position: absolute;
	color: #fff;
	border: 1px solid #DCA;
	background: #666;
	background: rgba(0,0,0,0.7);
}
.callout {
	z-index: 20;
	position: absolute;
	top: 30px;
	border: 0;
	left: -12px;
}
/*CSS3 extras*/
a.tooltip span {
	border-radius: 4px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
}
input[type="text"], select, textarea {
	color: #000;
}
.submit {
	width: 100px;
	height: 22px;
	background: #067936;
	border: none;
	text-align: center;
	color: #FFF;
	border-radius: 4px;
	font-size: 12px;
	font-weight: normal;
}
/* === the dark full screen background, which appears on form pop-up === */
#background-on-popup {
    z-index: 9990;
    position: fixed;
    display: none;
    height: 100%;
    width: 100%;
    background: #000000;
    top: 0px;
    left: 0px;
}

/* === The X-mark from the top-right corner of the pop-up form, from which you can close the box === */
div.close {
    background: url("files/images/login/closebox.png") no-repeat scroll 0 0 transparent;
    cursor: pointer;
    float: right;
    height: 16px;
    top: 10px;
    right: 10px;
    position: relative;
    width: 16px;
}
#success {
	max-width: 480px;
	min-width: 280px;
	width: 100%;
	margin-top: 20px;
	margin-bottom: 20px;
	position: absolute;
	display: none;
	left: 50%;
    top: 20%;
	margin-left: -240px;
    border-radius: 4px;
    z-index: 9999;
}
</style>

<div id="success" class="success_product" style="border-radius: 4px; background: rgba(255,255,255,0.8); padding: 30px; color:#000;">
  <div class="close"></div>
  <!-- close button of the register form -->
  <div id="message_post" align="center">&nbsp;</div>
</div>
<!-- END OF SUCCESS MESSAGE -->

<div id="background-on-popup" class="mask_post"></div>

<div class="containerinner">
  <p class="heading">Post a Product</p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p>Products</p>
        </li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p>Post a Product</p>
        </li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <div class="product">
    <div class="product-inner" style="width:100%;">
		<span style="color:#C00; padding-left:10px;">" * " are compulsory fields</span>
      <form action="" name="padd_form" id="padd_form" method="post">
        <div class="mess" style="color:#C30; display:none; padding-top:20px; padding-left:20px; font-size:16px; font-weight:bold;"><img src="files/images/ajax-loader.gif" /></div>
        <input type="hidden" name="punique" id="punique" value="<?php echo $punique;?>" />
        <input type="hidden" name="pbject" id="pbject" value="<?php echo $psubject;?>" />
        <input type="hidden" name="ulevel" id="ulevel" value="<?php echo $uslevel;?>" />
        <table width="100%" border="0" cellspacing="20" cellpadding="0" align="center">
          <?php if($uslevel == '' || $uslevel == 3){?>
          <tr>
            <th valign="top" width="30%" class="fonts">Publisher <span style="color:#ef4036;">*</span> :</th>
            <td><div class="input2">
                <div class="custom_select">
                  <select name="publishers" id="publishers" autofocus>
                    <option value="">Select</option>
                    <?php foreach($publishers as $publis){?>
                    <option value="<?php echo $publis->user_id;?>" <?php if($pub == $publis->user_id){echo 'selected="selected"';}?>><?php echo $publis->user_username;?></option>
                    <?php }?>
                  </select>
                </div>
              </div></td>
          </tr>
          <?php }?>
          <tr>
            <th valign="top" width="35%" class="fonts">Product Type <a href="#" class="tooltip"><img src="files/images/icons/tool-tip.gif"><span>Digital products refer to downloadable files such as pdf, word documents and media files. For Physical products, you would need to arrange for delivery to your buyers.</span></a> <span style="color:#ef4036;">*</span> :</th>
            <td><div class="input2">
                <div class="custom_select">
                  <select name="ptype" id="ptype" autofocus>
                    <option value="">Select</option>
                    <option <?php if($ptype == 'Digital'){echo 'selected="selected"';}?>>Digital</option>
                    <option <?php if($ptype == 'Physical'){echo 'selected="selected"';}?>>Physical</option>
                  </select>
                </div>
              </div></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Product Name <span style="color:#ef4036;">*</span> :</th>
            <td valign="top"><input type="text" name="pname" id="pname" class="box" value="<?php echo $pname;?>" /></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Level <a href="#" class="tooltip"><img src="files/images/icons/tool-tip.gif"><span>You should select the level which your product is suitable for. Only 1 option is allowed.</span></a> <span style="color:#ef4036;">*</span> :</th>
            <td valign="top"><div class="input2"><div class="custom_select">
              <select name="plevel" id="plevel">
                <option value="">Select</option>
                <?php foreach($levels as $lev){?>
                <option <?php if($plevel == $lev->level){echo 'selected="selected"';}?>><?php echo $lev->level;?></option>
                <?php }?>
              </select>
              </div></div>
            </td>
          </tr>
          <tr>
            <th valign="top" class="fonts"><img src="files/images/ajax-loader.gif" id="loader" /> Subject <a href="#" class="tooltip"><img src="files/images/icons/tool-tip.gif"><span>Press ‘Ctrl’ for multiple selections. Please be selective in your choices in order to reach out to your targeted buyers.</span></a> <span style="color:#ef4036;">*</span> :</th>
            <td valign="top" id="subject_drop"><select name="psubject" id="psubject"><option value="">Select</option></select></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Resource Type <span style="color:#ef4036;">*</span> :</th>
            <td valign="top"><div class="input2">
                <div class="custom_select">
                  <select name="presource" id="presource">
                  	<option value="">Select</option>
                    <?php foreach($resources as $source){?>
                    <option <?php if($presource == $source->resources){echo 'selected="selected"';}?>><?php echo $source->resources;?></option>
                    <?php }?>
                  </select>
                </div>
              </div></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Description <a href="#" class="tooltip"><img src="files/images/icons/tool-tip.gif"><span>This should contain keywords of the topics/highlights of your product to facilitate keyword searches and boost the marketability of your product. For physical products, please include type of delivery (e.g. courier, registered mail or normal mail) and estimated waiting time.</span></a> <span style="color:#ef4036;">*</span> :</th>
            <td valign="top"><textarea name="pdesc" id="pdesc" rows="7" class="text-box1"><?php echo stripslashes($pdesc);?></textarea></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Currency :</th>
            <td valign="top"><strong>SGD</strong></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Price <a href="#" class="tooltip"><img src="files/images/icons/tool-tip.gif"><span>You should include all costs you would incur such as service charge and delivery costs.</span></a> <span style="color:#ef4036;">*</span> :</th>
            <td valign="top"><input type="text" name="pprice" id="pprice" class="box" value="<?php echo $pprice;?>" /></td>
          </tr>
          <tr <?php //class="physical" if($ptype == 'Digital' || $ptype == ''){?>style="display:none;"<?php //}?>>
            <th valign="top" class="fonts">Shipping Charges <span style="color:#ef4036;">*</span> :</th>
            <td valign="top"><input type="text" name="sprice" id="sprice" class="box" value="<?php echo $sprice;?>" /></td>
          </tr>
          <tr class="digital" <?php if($ptype == 'Physical' || $ptype == ''){?>style="display:none;"<?php }?>>
            <th valign="top" class="fonts">Downloadable days <a href="#" class="tooltip"><img src="files/images/icons/tool-tip.gif"><span>This is the number of days from the date of purchase where buyers would be able to download your product for unlimited times. For e.g. 7 or 30 days.</span></a> <span style="color:#ef4036;">*</span> :</th>
            <td valign="top"><input type="text" name="pdownd" id="pdownd" class="box" value="<?php echo $pdownd;?>" />
              <small>(days)</small></td>
          </tr>
          <tr class="digital" <?php if($ptype == 'Physical' || $ptype == ''){?>style="display:none;"<?php }?>>
            <th valign="top" class="fonts">Pages <span style="color:#ef4036;">*</span> :</th>
            <td valign="top"><input type="text" name="ppages" id="ppages" class="box" value="<?php echo $ppages;?>" /></td>
          </tr>
          <tr class="physical" <?php if($ptype == 'Digital' || $ptype == ''){?>style="display:none;"<?php }?>>
            <th valign="top" class="fonts">Weight <span style="color:#ef4036;">*</span> :</th>
            <td valign="top"><input type="text" name="pweight" id="pweight" class="box" value="<?php echo $pweight;?>" /><small>(grams)</small></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Image :</th>
            <td valign="top"><div class="pic_mess" style="display:none;"><img src="files/images/ajax-loader.gif" /></div>
              <?php if($ppic != ''){?>
              <img src="files/<?php echo $ppic;?>" alt="<?php echo $pname;?>" title="<?php echo $pname;?>" style="max-height:100px;" /> <a href="javascript:;" onclick="$('#ppic, .ppic').show();" style="color:#f30">change</a>
              <input type="hidden" name="cppic" id="cppic" value="<?php echo $ppic;?>" />
              <?php }else{?>
              <input type="hidden" name="cppic" id="cppic" />
              <?php }?>
              <input type="file" name="ppic" id="ppic" <?php if($ppic != ''){?>style="display:none;"<?php }?> />
              <input type="button" name="ppic_but" id="ppic_but" value="Upload" style="display:none;" class="submit" />
              <br />
              <small class="ppic" <?php if($ppic != ''){?>style="display:none;"<?php }?>>Allowed extensions - .gif, .jpg, .jpeg, .png</small></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Preview :</th>
            <td valign="top"><?php $i=0; if(count($ppreview)){foreach($ppreview as $pview){if($pview != ''){?>
              <img src="files/<?php echo $pview;?>" alt="" style="cursor:pointer; max-width:100px; max-height:100px;" />
              <?php $i++;}}}?>
              <a href="javascript:;" onclick="$('#ppview').toggle(); $('#ppview5').toggle(); $(this).html($(this).html() == 'change' ? 'hide' : 'channge'); $(this).html($(this).html() == 'hide' ? 'change' : 'hide');" style="color:#f30; <?php if(count($ppreview)){if($ppreview[0] == ''){?>display:none;<?php }}else{?>display:none;<?php }?>">change</a>
              <input type="hidden" name="ppviews" id="ppviews" value="<?php echo $ppview;?>" />
              
              <div class="ppreview_mess" style="display:none;"><img src="files/images/ajax-loader.gif" /></div>
              <input type="file" name="ppview" id="ppview" <?php if(count($ppreview)){if($ppreview[0] != ''){?>style="display:none"<?php }}?> />
              <input type="button" name="ppreview_but" id="ppreview_but" value="Upload" style="display:none;" class="submit" />
              
              <div class="ppreview_mess1" style="display:none;"><img src="files/images/ajax-loader.gif" /></div>
              <input type="file" name="ppview1" id="ppview1" style="display:none;" />
              <input type="button" name="ppreview_but" id="ppreview_but1" value="Upload" style="display:none;" class="submit" />
              
              <div class="ppreview_mess2" style="display:none;"><img src="files/images/ajax-loader.gif" /></div>
              <input type="file" name="ppview2" id="ppview2" style="display:none;" />
              <input type="button" name="ppreview_but" id="ppreview_but2" value="Upload" style="display:none;" class="submit" />
              
              <div class="ppreview_mess3" style="display:none;"><img src="files/images/ajax-loader.gif" /></div>
              <input type="file" name="ppview3" id="ppview3" style="display:none;" />
              <input type="button" name="ppreview_but" id="ppreview_but3" value="Upload" style="display:none;" class="submit" />
              
              <div class="ppreview_mess4" style="display:none;"><img src="files/images/ajax-loader.gif" /></div>
              <input type="file" name="ppview4" id="ppview4" style="display:none;" />
              <input type="button" name="ppreview_but" id="ppreview_but4" value="Upload" style="display:none;" class="submit" /><br />
              <small class="ppview5" <?php if($ppview != ''){?>style="display:none;"<?php }?>>Allowed extensions - .gif, .jpg, .jpeg, .png</small></td>
          </tr>
          <?php if($pfile == '' || $title != 'Edit Product'){?>
          <tr class="digital">
            <th valign="top" class="fonts">Product File :</th>
            <td valign="top"><div class="file_mess" style="display:none;"><img src="files/images/ajax-loader.gif" /></div>
              <input type="file" name="pfile" id="pfile" />
              <input type="button" name="pfile_but" id="pfile_but" value="Upload" style="display:none;" class="submit" />
              <br />
              <small class="pfile">Allowed extensions - .gif, .jpg, .jpeg, .png, .doc, .docx, .rtf, .odt, .pdf, .ppt, .pptx, .pps, .ppsx, .xls, .xlsx, .mp3, .m4a, .ogg, .wav, .mp4, .m4v, .mov, .wmv, .avi, .mpg, .ogv, .3gp, .3g2</small></td>
          </tr>
          <?php }?>
          <tr>
            <th colspan="3" align="center"><div class="right-botton" style="width:117px">
                <div class="right-botton1" style="width:111px">
                  <div class="right-botton2" style="width:107px">
                    <input type="button" name="save" id="save" value="<?php if($title == 'Edit Product'){echo 'Update';}else{echo 'Add';}?>" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
                  </div>
                </div>
              </div></th>
          </tr>
        </table>
        <input type="hidden" name="title" id="title" value="<?php echo $title;?>" /><input type="hidden" name="user" id="user" value="Admin" />
      </form>
    </div>
  </div>
</div>