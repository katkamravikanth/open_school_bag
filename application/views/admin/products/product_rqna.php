<link href="files/admin/css/style1.css" type="text/css" rel="stylesheet" />
<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<link href="files/css/tab.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="files/js/widgetkit.js"></script>
<script type="text/javascript" src="files/js/jquery.visualize.js"></script>
<script type="text/javascript" src="files/admin/js/jquery_ravikanth_rqna.js"></script>

<div class="containerinner" style="min-height:600px; height:auto;">
  <p class="heading">Manage Products</p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p>Selling</p>
        </li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p>Product Reviews / Q&amp;A</p>
        </li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start main ------------>
  <div class="main" style="padding-top:10px; float:left;">
    <div class="containerr" style="margin-top:10px; float:left; display:block">
      <ul class="tabs">
        <li>
          <div  class="tabs1_1">
            <div class="tabs1_2"> <a href="#tab1">Reviews</a> </div>
          </div>
        </li>
        <li>
          <div  class="tabs1_1">
            <div class="tabs1_2"> <a href="#tab2">Questions and Answers</a> </div>
          </div>
        </li>
      </ul>
      <div class="tab_container"> 
        <!------------ start tab3 (Reviews) --------------> 
        <!-- Review section -->
        <div id="tab1" class="tab_content">
          <div id="reviews-main"> 
            <!-- add the class .pagination to dynamically create a working pagination! The rel-attribute will tell how many items there are per page -->
            <?php if(count($review)){?>
            <table class="pagination" rel="10" width="100%">
              <tbody>
                <?php $j=2; foreach($review as $reviews){?>
                <tr>
                  <td><div class="reviews-main1" style="position:relative; padding-top:8px; padding-bottom:8px;">
			 		<img src="files/images/icons/delete.png" width="16" class="delete" name="<?php echo $reviews->review_id;?>" alt="Delete" style="float:right; margin-left:20px;" />
                      <div class="porduct-star" style="float:right; color:#000;">
                        <?php for($i = 1;$i <= 5;$i++){$s='';
				  			if ($i > $reviews->rate){$s = 'un';}?>
                        <img src="files/images/icons/<?php echo $s;?>star.png" style="border:0; margin:0; padding:0;" />
                        <?php }?>
                      </div>
                      <h4 style="margin:0; padding:3px;"><?php echo $reviews->user_username;?></h4>
                      <p style="margin-left:10px;"><?php echo $reviews->review;?></p>
                    </div></td>
                </tr>
                <?php $j++;}?>
              </tbody>
              <tfoot>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </tfoot>
            </table>
            <?php }else{
					echo '<div class="reviews-main1" style="padding:8px;"><p style="margin-left:10px;">No reviews posted</p></div>';
				}?>
          </div>
        </div>
        <!------------ end tab3 (Reviews) --------------> 
        
        <!------------ start tab4 (Questions and Answers) -------------->
        <div id="tab2" class="tab_content">
          <div style="clear: both"></div>
          <div id="reviews-main">
            <?php if(count($qna)!=0){
					print_r($this->threaded->arrange($qna));
				}else{
					echo '<div align="center">No questions posted</div>';
				}?>
          </div>
        </div>
        <!------------ end tab4 (Questions and Answers) --------------> 
      </div>
    </div>
  </div>
</div>
