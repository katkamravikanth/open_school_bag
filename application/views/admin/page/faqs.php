<link rel="stylesheet" href="files/admin/css/jquery.fileupload-ui.css" />
<script type="text/javascript" src="files/admin/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
$(function(){
	tinymce.init({selector:'textarea'});
})
</script>
<style type="text/css">
input[type="text"], select, textarea {
	color: #000;
}
</style>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p><?php echo ucfirst($this->uri->segment(2));?></p>
          </li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p><?php echo $title;?></p>
        </li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start product  page ------------>
  <div class="product" style="width:100%; padding-bottom:60px; margin-left:0; margin-right:0;">
    <div class="product-inner" style="width:90%; margin-left:0; margin-right:0; float:none; padding-left:20px;">
      <?php
      //flash messages
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == 'updated'){
          echo '<div style="padding-top:20px; color:#090; font-size:16px; font-weight:bold;">';
            echo '<strong>Well done!</strong> '.$title.' page updated with success.';
          echo '</div>';       
        }else{
          echo '<div style="padding-top:20px; color:#f30; font-size:16px; font-weight:bold;">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
      //form validation
      echo '<div style="padding-top:20px; color:#f30; font-size:16px; font-weight:bold;">';
      echo validation_errors();
	  echo '</div>';

      echo form_open('admin/page/faqs', $attributes);
      ?>
      <br />
      <div class="alert mess alert-error" style="display:none; padding-top:20px; font-size:16px; font-weight:bold;">&nbsp;</div>
      <input type="hidden" id="page_id" name="page_id" value="<?php echo $page[0]->page_id; ?>">
      <label style="font-size:18px; font-weight:bold;">Body</label>
      <br />
      <br />
      <textarea id="body" name="body" rows="12" style="width:100%;"><?php echo $page[0]->body; ?></textarea>
      <br />
      <br />
      <div style="display:block;">
        <div class="right-botton">
          <div class="right-botton1">
            <div class="right-botton2">
              <input type="submit" value="Save Change" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
            </div>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?> </div>
  </div>
  <div class="clear"></div>
  <!---------- end product page ----------->
  <div class="clear"></div>
  <div class="clear"></div>
</div>