<link href="files/admin/css/style1.css" type="text/css" rel="stylesheet" />
<style type="text/css">
input[type="text"], select, textarea {
	color: #000;
}
</style>

<div class="containerinner">
  <p class="heading">Notifications</p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" /><p>Home</p></a></li>
        <li><img src="files/admin/images/navinnerarrow.png" /><p>General</p></li>
        <li><img src="files/admin/images/navinnerarrow.png" /><p>Notifications</p></li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  
  <!------- start notification page ------------>
  
  <div class="notification">
    <!--<h1>Today</h1>-->
    <!------- start inner page ------------>
    <?php if(count($note)){
	foreach($note as $notes){?>
    <div class="inner">
      <p><?php echo stripslashes($notes->note_message);?></p>
      <?php if($notes->note_linkname!=''){?><a href="<?php echo $notes->note_link;?>" target="_blank" style="color:#060;float:right"><?php echo $notes->note_linkname;?></a><?php }?>
      <h2><?php echo $notes->created;?></h2>
    </div>
    <?php }?>
    <div id="cont"><?php echo $this->pagination->create_links();?></div>
    <?php }else{echo 'You do not have any notification';}?>
    <!------- end inne inner page ------------> 
  </div>
  <!---------- end notification page ----------->
  
  <div class="clear"></div>
  <div class="clear"></div>
</div>