<style>
a.add_promo {
	color: #6E6C64;
}
a.add_promo:hover {
	color: #ef4f45;
}
input[type="text"], select, textarea {
	color: #000;
}
</style>
<link href="files/admin/css/style1.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="files/css/dropdown.css" />
<link rel="stylesheet" href="files/css/themes/alertify.core.css" />
<link rel="stylesheet" href="files/css/themes/alertify.default.css" id="toggleCSS" />
<script src="files/js/lib/alertify.min.js"></script>
<script type="text/javascript" src="files/js/customSelect.jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	function reset () {
		$("#toggleCSS").attr("href", "files/css/themes/alertify.default.css");
		alertify.set({
			labels : {
				ok     : "OK",
				cancel : "Cancel"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}
	
	$(".custom_select select").customSelect();
	
	// delete product
	$(".delete").on( 'click', function () {
		id = $(this).attr('name');
		reset();
		alertify.confirm("Are you sure do you want to delete this promotion?", function (e) {
			if(e){
				window.location='admin/promotions/delete/'+id;
			}
		});
		return false;
	});
});
</script>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p><?php echo $title;?></p>
        </li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start main ------------>
  <div style="float: right; font-size:16px; font-weight:bold; color:#f30"><br />
    <a href="<?php echo 'admin/'.$this->uri->segment(2); ?>/add" class="add_promo">Add a new</a></div>
  <div class="main" style="padding-top:10px;">
    <form action="" method="get" style="border-bottom:1px solid #999; padding-bottom:5px; padding-top:80px; height:70px;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td width="15%" style="font-size:12px !important;">Search by </td>
          <td width="40%"><input type="text" name="search_string" id="search_string" placeholder="Enter Price/Percentage/Promo Code" style="height:25px; border: 1px solid #c7c7c7; width:100%" /></td>
          <td width="10%" align="center" style="font-size:12px !important;"><strong>( OR )</strong></td>
          <td width="10%" style="font-size:12px !important;">Filter by </td>
          <td width="10%"><div class="input">
              <div class="custom_select">
                <select name="order" id="order">
                  <option value="">Sort By</option>
                  <option>Active</option>
                  <option>Expired</option>
                  <option value="On Product Amount">On Product</option>
                  <option value="On Cart Amount">On Cart</option>
                </select>
              </div>
            </div></td>
          <td width="15%" align="right"><div class="right-bottons" style="height:27px; width:100px; margin-bottom:-20px; margin-right:-50px;">
              <div class="right-bottons1" style="height:21px; width:94px;">
                <div class="right-bottons2" style="height:19px; width:90px;">
                  <input type="submit" name="search_user" id="search_user" value="Search" style="background:none; border:none; color:#fff; font-size:14px; cursor:pointer;" />
                </div>
              </div>
            </div></td>
        </tr>
      </table>
    </form>
    <div class="product1">
      <?php if(count($promotions)){foreach($promotions as $row){?>
      <div class="products_list">
        <div class="details" style="width:100%;">
          <div class="box">
            <table border="0" cellpadding="0" cellspacing="0" style="width:100% !important;">
              <tr class="first" style="color:#fff; font-size:14px;">
                <td align="center" width="150">Discount Value</td>
                <td align="center" width="150">Promo Code</td>
                <td align="center" width="200">Promo for</td>
                <td align="center" width="150">Users Can use</td>
                <td align="center" width="150">Used</td>
                <td align="center" width="150">Status</td>
              </tr>
              <tr class="second" style="height:35px">
                <td align="center"><?php if($row->promo_percent_type == '%'){echo $row->promo_percent.' '.$row->promo_percent_type;}else{echo $row->promo_percent_type.' '.$row->promo_percent;}?></td>
                <td align="center"><?php echo $row->promo_code;?></td>
                <td align="center"><?php if($row->promo_type == 'On Product Amount'){echo $row->product_title.' '; echo $row->promo_category;}else{echo '<strong>'.$row->promo_type.'</strong>';}?></td>
                <td align="center"><?php echo $row->promo_users;?></td>
                <td align="center"><?php echo $row->promo_used_count;?></td>
                <td align="center"><?php if($row->promo_users == $row->promo_used_count){echo 'Expired';}else{echo $row->promo_status;}?></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="update">
          <ul>
            <li><a href="javascript:;" name="<?php echo $row->promo_id;?>" class="delete" style="border-right:1px solid #000;">Delete</a></li>
            <li><a href="admin/promotions/update/<?php echo $row->promo_id;?>">Update</a></li>
          </ul>
        </div>
      </div>
      <?php }?><br /><br />
      <div id="cont"><?php echo $this->pagination->create_links();?></div>
      <?php }else{echo 'Promotions not found';}?>
    </div>
  </div>
</div>