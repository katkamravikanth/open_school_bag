<link rel="stylesheet" href="files/admin/css/jquery.fileupload-ui.css" />
<link type="text/css" href="files/css/jquery.datepick.css" rel="stylesheet">
<script type="text/javascript" src="files/js/jquery.datepick.js"></script>
<script type="text/javascript">
$(function(){
	$('.date_box').datepick({dateFormat: 'yyyy-mm-dd'});
	$('#ptype').change(function() {
        if($(this).val() == 'On Cart Amount'){
			$('.product_amount').hide();
			$('.shipping_amount').show();
		}else if($(this).val() == 'On Product Amount'){
			$('.shipping_amount').hide();
			$('.product_amount').show();
		}else{
			$('.shipping_amount').hide();
			$('.product_amount').hide();
		}
    });
	$('#ptitle').change(function(e) {
        $('#plevel').prop('selectedIndex',0);
    });
	$('#plevel').change(function(e) {
        $('#ptitle').prop('selectedIndex',0);
    });
});
</script>

<style type="text/css">
input[type="text"] {
	padding-left:5px;
	padding-right:5px;
}
input[type="text"], select, textarea {
	color: #000;
}
</style>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>"><img src="files/admin/images/navinnerarrow.png" />
          <p>Promotions</p></a>
        </li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p><?php echo $title;?></p>
        </li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start product  page ------------>
  <div class="product">
    <div class="product-inner" style="width:100% !important">
      <?php
      //error messages
      if(isset($error_message)){
        if($error_message == FALSE){
          echo '<div style="padding-left:10px; padding-top:20px; color:#f30; font-size:16px; font-weight:bold;">';
            echo '<strong>Oh snap!</strong> Please select product or category';
          echo '</div>';          
        }
      }
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div style="padding-left:10px; padding-top:20px; color:#090; font-size:16px; font-weight:bold;">';
            echo '<strong>Well done!</strong> promo code updated successfully.';
          echo '</div>';       
        }else{
          echo '<div style="padding-left:10px; padding-top:20px; color:#f30; font-size:16px; font-weight:bold;">';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
	  echo '<div style="padding-left:10px; padding-top:20px; color:#f30; font-size:16px; font-weight:bold;">';
      echo validation_errors('');
	  echo '</div>';?>
      <form action="" name="padd_form" id="padd_form" method="post">
        <table width="100%" border="0" cellspacing="20" cellpadding="0" align="center">
          <tr>
            <th valign="top" width="30%" class="fonts">Promo Type * :</th>
            <td valign="top"><select name="ptype" id="ptype" style="width:80% !important;">
                <option value="">Select Type</option>
                <option <?php if($promo->promo_type == 'On Product Amount'){echo 'selected';}?>>On Product Amount</option>
                <option <?php if($promo->promo_type == 'On Cart Amount'){echo 'selected';}?>>On Cart Amount</option>
              </select></td>
          </tr>
          <tr class="shipping_amount"  style=" <?php if($promo->promo_type == 'On Cart Amount'){echo '';}else{echo 'display:none';}?>">
            <th valign="top" width="30%" class="fonts">Minimum Amount * :</th>
            <td valign="top"><input type="text" name="pmin" id="pmin" class="box" value="<?php echo $promo->promo_min_amount; ?>" /></td>
          </tr>
          <tr class="product_amount" style=" <?php if($promo->promo_type == 'On Product Amount'){echo '';}else{echo 'display:none';}?>">
            <th valign="top" class="fonts">Product :</th>
            <td valign="top"><select name="ptitle" id="ptitle" style="width:80% !important;">
                <option value="">Select</option>
                <?php foreach($products as $prod){?>
                <option <?php if(stripslashes($promo->product_title) == stripslashes($prod->product_name)){echo 'selected';}?>><?php echo stripslashes($prod->product_name);?></option>
                <?php }?>
              </select></td>
          </tr>
          <tr class="product_amount" style=" <?php if($promo->promo_type == 'On Product Amount'){echo '';}else{echo 'display:none';}?>">
            <th valign="top" colspan="2">(OR)</th>
          </tr>
          <tr class="product_amount" style=" <?php if($promo->promo_type == 'On Product Amount'){echo '';}else{echo 'display:none';}?>">
            <th valign="top" class="fonts">Product Category :</th>
            <td valign="top"><select name="plevel" id="plevel" style="width:80% !important;">
                <option value="">Select</option>
                <?php foreach($levels as $lev){?>
                <option <?php if($promo->promo_category == $lev->level){echo 'selected';}?>><?php echo $lev->level;?></option>
                <?php }?>
                <?php foreach($subjects as $sub){?>
                <option <?php if($promo->promo_category == $sub->subjects){echo 'selected';}?>><?php echo $sub->subjects;?></option>
                <?php }?>
                <?php foreach($resources as $source){?>
                <option <?php if($promo->promo_category == $source->resources){echo 'selected';}?>><?php echo $source->resources;?></option>
                <?php }?>
              </select></td>
          </tr>
          <tr class="shipping_amount" style=" <?php if($promo->promo_type == 'On Cart Amount'){echo '';}else{echo 'display:none';}?>">
            <th valign="top" class="fonts">Discount Type * :</th>
            <td valign="top"><select name="pertype" id="pertype" style="width:80% !important;">
                <option value="">Select</option>
                <option <?php if($promo->promo_percent_type == '%'){echo 'selected';}?>>%</option>
                <option <?php if($promo->promo_percent_type == 'S$'){echo 'selected';}?>>S$</option>
              </select></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Discount Value * :</th>
            <td valign="top"><input type="text" name="ppercent" id="ppercent" class="box" value="<?php echo $promo->promo_percent; ?>" />
            <span class="product_amount" style=" <?php if(set_value('ptype') == 'On Product Amount'){echo '';}else{echo 'display:none';}?>">%</span></td>
          </tr>
          <tr class="shipping_amount" style=" <?php if($promo->promo_type == 'On Cart Amount'){echo '';}else{echo 'display:none';}?>">
            <th valign="top" class="fonts">Promotion Code * :</th>
            <td valign="top"><input type="text" name="pcode" id="pcode" class="box" value="<?php echo $promo->promo_code; ?>" /></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Start Date * :</th>
            <td valign="top"><input type="text" name="pstart" id="pstart" class="box date_box" value="<?php echo $promo->promo_start; ?>" /></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">End Date * :</th>
            <td valign="top"><input type="text" name="pend" id="pend" class="box date_box" value="<?php echo $promo->promo_end; ?>" /></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">No of users * :</th>
            <td valign="top"><input type="text" name="no_users" id="no_users" class="box" value="<?php echo $promo->promo_users; ?>" /></td>
          </tr>
          <tr>
            <th colspan="3" align="center"><div class="right-botton">
                <div class="right-botton1">
                  <div class="right-botton2">
                    <input type="submit" name="save" id="save" value="Edit Promo" tabindex="15" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:16px; cursor:pointer;" />
                  </div>
                </div>
              </div></th>
          </tr>
        </table>
      </form>
    </div>
  </div>
  <div class="clear"></div>
  <!---------- end product page ----------->
  <div class="clear"></div>
  <div class="clear"></div>
</div>