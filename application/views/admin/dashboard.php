<style type="text/css">
input[type="text"], select, textarea {
	color: #000;
}
</style>

<div class="containerinner">
  <p class="heading">Dashboard</p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p>Dashboard</p></li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  
  <!------- start main ------------>
  <div class="main">
    <h4>General</h4>
    <!------ start left --------->
    <div class="left"> 
      <!--------- start col1 ----->
      <div class="col1"> <a href="admin/notifications">
        <h1 style="margin: 15px 0;">Notifications</h1>
        <img src="files/admin/images/col-img2.png" />
        <h2 style="margin: 15px 0; line-height: 15px;">You have <b><?php echo $notif;?></b> new notification(s)</h2>
        </a> </div>
      <div class="col1"><a href="admin/page/faqs"> <img src="files/admin/images/col-faqs-img.png" style="margin:20px 0 0 46px;" />
        <h2 style="margin: 5px 0; line-height: 15px;">Frequently Asked Questions</h2>
        </a> </div>
      <!--------- start col1 -----> 
    </div>
    <!------ start left ---------> 
    
    <!------ start right --------->
    <div class="right">
      <h1 style="text-transform:capitalize"><?php echo $this->session->userdata('user_name');?></h1>
      <img src="<?php if(count($profile)){if($profile->profile_pic){echo 'files/'.$profile->profile_pic;}else{echo 'files/profile/blank.png';}}else{echo 'files/profile/blank.png';}?>" style="max-height:119px;" /> <a href="admin/settings/profile">
      <div class="span"> Update my information </div>
      </a> </div>
    <!------ start right ---------> 
  </div>
  <!------------ end main ---------> 
  
  <!---------- start main2 page ----------->
  <div class="main2">
    <h3>Selling</h3>
    
    <!------ start left --------->
    <div class="left"> 
      <!--------- start col1 ----->
      <div class="col1"> <a href="admin/products">
        <h1 style="margin:30px 10px 0 0; float:right; text-align:left;">Manage <br />
          Products</h1>
        <img src="files/admin/images/main-2-img1.png" style="margin:20px 0 0 16px; float:left;" /> </a></div>
      <div class="col1"> <a href="admin/products/add"><img src="files/admin/images/main-2-img3.png" style="margin:20px 0 0 46px;" />
        <h1 style="margin:5px 10px 0 0;">Post a product</h1>
        </a> </div>
      <div class="col1"> <a href="admin/orders"><img src="files/admin/images/main-2-img2.png" style="margin:20px 0 0 46px;" />
        <h1 style="margin:5px 10px 0 0;">View Sales</h1>
        </a> </div>
      <!--------- start col1 -----> 
    </div>
    <!------ start left --------->
    
    <div style="clear:both"></div>
    
    <!---------- start main2 page ----------->
    <div class="main3">
      <h3>Reports</h3>
      
      <!------ start left --------->
      <div class="left"> 
        <!--------- start col1 ----->
      	<div class="col1"> <a href="admin/reports"><img src="files/admin/images/main-2-img2.png" style="margin:20px 0 0 46px;" />
        	<h1 style="margin:5px 10px 0 0;">View Reports</h1>
        </a></div>
        <!--------- start col1 -----> 
      </div>
      <!------ start left ---------> 
      
    </div>
  </div>
  <!---------- start main2 page ----------->
  
  <div class="clear"></div>
  <div class="clear"></div>
</div>