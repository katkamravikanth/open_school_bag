<div class="container top">
<ul class="breadcrumb">
  <li><a href="<?php echo site_url("admin"); ?>"><?php echo ucfirst($this->uri->segment(1));?></a> <span class="divider">/</span> </li>
  <li class="active"><?php echo ucfirst($this->uri->segment(2));?></li>
</ul>
<div class="page-header users-header">
  <h2><?php echo ucfirst($this->uri->segment(2));?> <a  href="<?php echo 'admin/'.$this->uri->segment(2); ?>/add" class="btn btn-success">Add a new</a> </h2>
</div>
<div class="row">
  <div class="span12 columns">
    <div class="well">
      <?php
            $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');
           
            $options_user = array(0 => "all");
            foreach ($users as $row){
              $options_user[$row->user_id] = $row->user_fname;
            }
            //save the columns names in a array that we will use as filter         
            $options_products = array();    
            foreach ($products as $array) {
              foreach ($array as $key => $value) {
                $options_products[$key] = $key;
              }
              break;
            }

            echo form_open('admin/products', $attributes);
     
              echo form_label('Search:', 'search_string');
              echo form_input('search_string', $search_string_selected, 'style="width: 170px;
height: 26px;"');

              echo form_label('Filter by user:', 'user_id');
              echo form_dropdown('user_id', $options_user, $user_selected, 'class="span2"');

              echo form_label('Order by:', 'order');
              echo form_dropdown('order', $options_products, $order, 'class="span2"');

              $data_submit = array('name' => 'mysubmit', 'class' => 'btn btn-primary', 'value' => 'Go');

              $options_order_type = array('Asc' => 'Asc', 'Desc' => 'Desc');
              echo form_dropdown('order_type', $options_order_type, $order_type_selected, 'class="span1"');

              echo form_submit($data_submit);

            echo form_close();
            ?>
    </div>
    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
          <th class="header">#</th>
          <th class="yellow header headerSortDown">Name</th>
          <th class="green header">Resource Type</th>
          <th class="red header">Price</th>
          <th class="red header">User</th>
          <th class="red header">Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php
              foreach($products as $row)
              {
                echo '<tr>';
                echo '<td>'.$row['product_id'].'</td>';
                echo '<td>'.$row['product_name'].'</td>';
                echo '<td>'.$row['product_resource'].'</td>';
                echo '<td>'.$row['product_price'].'</td>';
                echo '<td>'.$row['user_fname'].'</td>';
                echo '<td class="crud-actions">
                  <a href="admin/products/update/'.$row['product_unique'].'" class="btn btn-info">view & edit</a>  
                  <a href="admin/products/delete/'.$row['product_unique'].'" class="btn btn-danger">delete</a>
                </td>';
                echo '</tr>';
              }
              ?>
      </tbody>
    </table>
    <?php echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?> </div>
</div>