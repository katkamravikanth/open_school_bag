<style type="text/css">
input[type="text"] {
	padding-left:5px;
	padding-right:5px;
}
input[type="text"], select, textarea {
	color: #000;
}
</style>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>"><img src="files/admin/images/navinnerarrow.png" />
          <p>Settings</p></a>
        </li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p><?php echo $title;?></p>
        </li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start product  page ------------>
  <div class="product">
    <div class="product-inner">
      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success" style="padding-top:20px; color:#090; font-size:16px; font-weight:bold;">';
            echo '<strong>Well done!</strong> updated successfully.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error" style="padding-top:20px; color:#f30; font-size:16px; font-weight:bold;">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      echo validation_errors('');?>
      <form action="" name="site_form" id="site_form" method="post">
        <table width="100%" border="0" cellspacing="20" cellpadding="0" align="center">
          <tr>
            <th valign="top" width="30%" class="fonts">Email Address * :</th>
            <td valign="top"><input type="email" name="email" id="email" class="box" value="<?php echo $site->email; ?>" /></td>
          </tr>
          <tr>
            <th valign="top" width="30%" class="fonts">Phone Number * :</th>
            <td valign="top"><input type="text" name="phone" id="phone" class="box" value="<?php echo $site->phone; ?>" /></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Margin * :</th>
            <td valign="top"><input type="text" name="margin" id="margin" class="box" value="<?php echo $site->margin; ?>" /></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Transaction Fee * :</th>
            <td valign="top"><input type="text" name="trans_fee" id="trans_fee" class="box" value="<?php echo $site->trans_fee; ?>" /></td>
          </tr>
          <tr>
            <th colspan="3" align="center"><div class="right-bottons">
                <div class="right-bottons1">
                  <div class="right-bottons2">
                    <input type="submit" name="save" id="save" value="Save" tabindex="15" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:16px; cursor:pointer;" />
                  </div>
                </div>
              </div></th>
          </tr>
        </table>
      </form>
    </div>
  </div>
  <div class="clear"></div>
  <!---------- end product page ----------->
  <div class="clear"></div>
  <div class="clear"></div>
</div>