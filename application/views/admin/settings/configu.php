<link rel="stylesheet" href="files/admin/css/jquery.fileupload-ui.css" />
<style type="text/css">
input[type="text"], select, textarea {
	color: #000;
}
</style>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" /><p>Home</p></a></li>
        <li><img src="files/admin/images/navinnerarrow.png" /><a href="<?php echo site_url("admin").'/'.$this->uri->segment(2);?>"><p><?php echo ucfirst($this->uri->segment(2));?></p></a></li>
        <li><img src="files/admin/images/navinnerarrow.png" /><p><?php echo $title;?></p></li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start product  page ------------>
  <div class="product" style="height:300px;">
    <div class="product-inner" style="height:550px;">
      <?php $fname = explode(' ', $user->user_username);
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div style="color:#090; padding-top:20px; font-size:16px; font-weight:bold;">';
            echo '<strong>Well done!</strong> Profile updated successully.';
          echo '</div>';       
        }else{
          echo '<div style="color:#f30; padding-top:20px; font-size:16px; font-weight:bold;">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => 'add_user');

      //form validation
	  echo '<div style="padding-top:20px; color:#f30; font-size:16px; font-weight:bold;">';
      echo validation_errors();
	  echo '</div>';
      
      echo form_open('admin/settings/config', $attributes);
      ?>
      <input type="hidden" name="uid" id="uid" value="<?php echo $user->user_id;?>" />
      <div class="mess" style="display:none; padding-top:20px; font-size:16px; font-weight:bold;">&nbsp;</div>
      <table width="100%" border="0" cellspacing="20" cellpadding="0" align="center">
        <tr>
          <th valign="top" class="fonts">First Name * :</th>
          <td valign="top"><input type="text" name="fname" id="fname" class="box" value="<?php echo $fname[0];?>" autofocus /></td>
        </tr>
        <tr>
          <th valign="top" class="fonts">Last Name * :</th>
          <td valign="top"><input type="text" name="lname" id="lname" class="box" value="<?php echo $fname[1];?>" /></td>
        </tr>
        <tr>
          <th valign="top" class="fonts">Email * :</th>
          <td valign="top"><input type="text" name="email" id="email" class="box" value="<?php echo $user->user_email;?>" /></td>
        </tr>
        <tr>
          <th valign="top" class="fonts">Phone No. * :</th>
          <td valign="top"><input type="text" name="telephone" id="telephone" class="box" value="<?php echo $user->user_phone;?>" /></td>
        </tr>
        <tr>
          <th colspan="3" align="center"> <div class="right-botton">
              <div class="right-botton1">
                <div class="right-botton2">
                  <input type="submit" value="Update" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
                </div>
              </div>
            </div></th>
        </tr>
      </table>
      <?php echo form_close(); ?>
    </div>
  </div>
  <div class="clear"></div>
  <!---------- end product page ----------->
  <div class="clear"></div>
  <div class="clear"></div>
</div>