<link rel="stylesheet" href="files/admin/css/jquery.fileupload-ui.css" />
<style type="text/css">
input[type="text"], select, textarea {
	color: #000;
}
</style>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" /><p>Home</p></a></li>
        <li><img src="files/admin/images/navinnerarrow.png" /><a href="<?php echo site_url("admin").'/'.$this->uri->segment(2);?>"><p><?php echo ucfirst($this->uri->segment(2));?></p></a></li>
        <li><img src="files/admin/images/navinnerarrow.png" /><p><?php echo $title;?></p></li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start product  page ------------>
  <div class="product">
    <div class="product-inner" id="profile">
      <div class="mess" style="display:none; padding-top:20px; font-size:16px; font-weight:bold;">&nbsp;</div>
      <table width="100%" border="0" cellspacing="20" cellpadding="0" align="center">
        <tr>
          <th valign="top" class="fonts">Current Password :</th>
          <td valign="top"><input type="password" name="current" id="current" class="box" /></td>
        </tr>
        <tr>
          <th valign="top" class="fonts">New Password :</th>
          <td valign="top"><input type="password" name="new" id="new" class="box" /></td>
        </tr>
        <tr>
          <th valign="top" class="fonts">Confirm New Password :</th>
          <td valign="top"><input type="password" name="re_new" id="re_new" class="box" /></td>
        </tr>
        <tr>
          <th colspan="3" align="center"> <div class="right-botton">
              <div class="right-botton1">
                <div class="right-botton2">
                  <input type="submit" name="cpass" id="cpass" value="Change" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
                </div>
              </div>
            </div></th>
        </tr>
      </table>
    </div>
  </div>
  <div class="clear"></div>
  <!---------- end product page ----------->
  <div class="clear"></div>
  <div class="clear"></div>
</div>