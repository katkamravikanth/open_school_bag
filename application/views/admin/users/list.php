<style type="text/css">
input[type="text"], select, textarea {
	color: #000;
}
a.add_user {
	color: #6E6C64;
}
a.add_user:hover {
	color: #ef4f45;
}
.alert-error {
	color: #f30 !important;
	line-height: 20px;
}
.alertify-log-success {
	margin-top:100px !important;
}
</style>
<link href="files/admin/css/style1.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="files/css/dropdown.css" />
<link rel="stylesheet" href="files/css/themes/alertify.core.css" />
<link rel="stylesheet" href="files/css/themes/alertify.default.css" id="toggleCSS" />
<script src="files/js/lib/alertify.min.js"></script>
<script type="text/javascript" src="files/js/customSelect.jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	function reset () {
		$("#toggleCSS").attr("href", "files/css/themes/alertify.default.css");
		alertify.set({
			labels : {
				ok     : "OK",
				cancel : "Cancel"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}
	
	$(".custom_select select").customSelect();
	
	// delete product
	$(".delete").on( 'click', function () {
		id = $(this).attr('name');
		reset();
		alertify.confirm("Are you sure do you want to delete this user?", function (e) {
			if(e){
				window.location='admin/users/delete/'+id;
			}
		});
		return false;
	});
	<?php if($this->uri->segment(3) == 'yes'){?>
		alertify.success('User has been deleted');
	<?php }else if($this->uri->segment(3) == 'no'){?>
		alertify.error('This user contain unfinished orders');
	<?php }?>
});
</script>

<div class="containerinner">
  <p class="heading">Manage <?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p>Users</p>
        </li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p>Manage <?php echo $title;?></p>
        </li>
      </ul>
    </div>
    <div class="export"><p><?php echo $link;?></p></div>
  </div>
  <!------- start main ------------>
  <div style="float: right; font-size:16px; font-weight:bold;"><br />
    <a href="<?php echo 'admin/'.$this->uri->segment(2); ?>/add" class="add_user">Add User</a></div>
  <div class="main" style="padding-top:10px;">
    <form action="" method="get" style="border-bottom:1px solid #999; padding-bottom:5px; padding-top:80px; height:70px;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td width="15%">Search by </td>
          <td width="40%"><input type="text" name="search_string" id="search_string" placeholder="Enter Username/Full Name/Email" style="height:25px; border: 1px solid #c7c7c7; width:100%" /></td>
          <td width="10%" align="center"><strong>( OR )</strong></td>
          <td width="10%">Filter by </td>
          <td width="10%"><div class="input">
              <div class="custom_select">
                <select name="order" id="order">
                  <option value="">Sort By</option>
                  <option value="2">Seller</option>
                  <option value="1">Buyer</option>
                  <option value="3">Publisher</option>
                </select>
              </div>
            </div></td>
          <td width="15%" align="right"><div class="right-bottons" style="height:27px; width:100px; margin-bottom:-20px; margin-right:-50px;">
              <div class="right-bottons1" style="height:21px; width:94px;">
                <div class="right-bottons2" style="height:19px; width:90px;">
                  <input type="submit" name="search_user" id="search_user" value="Search" style="background:none; border:none; color:#fff; font-size:14px; cursor:pointer;" />
                </div>
              </div>
            </div></td>
        </tr>
      </table>
    </form>
    <div class="product1">
      <?php foreach($users as $row){?>
      <div class="products_list">
        <?php if($row['pic']!=''){$pic = 'files/'.$row['pic'];}else{$pic = 'files/profile/blank.png';}?>
        <div class="images"> <img src="<?php echo $pic;?>" width="99" height="103" /></div>
        <div class="details">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr class="first" style="color:#fff">
              <th width="100">Name</th>
              <th width="100">Username</th>
              <th width="180">Email</th>
              <th>Phone</th>
              <th>User Type</th>
            </tr>
            <tr height="50">
              <td align="center" style="border:1px solid #CCC; padding:3px;"><?php echo $row['user']->user_fname;?></td>
              <td align="center" style="border:1px solid #CCC; padding:3px;"><?php echo $row['user']->user_username;?></td>
              <td align="center" style="border:1px solid #CCC; padding:3px;"><?php echo $row['user']->user_email;?></td>
              <td align="center" style="border:1px solid #CCC; padding:3px;"><?php echo $row['user']->user_phone;?></td>
              <td align="center" style="border:1px solid #CCC; padding:3px;"><?php if($row['user']->user_level == 9){$lev = 'Admin';}else if($row['user']->user_level == 3){$lev = 'Publisher';}else if($row['user']->user_level == 2){$lev = 'Seller';}else if($row['user']->user_level == 1){$lev = 'Buyer';} echo $lev;?></td>
            </tr>
          </table>
        </div>
        <div class="update">
          <ul>
            <li><a name="<?php echo $row['user']->user_id;?>" class="delete" style="cursor:pointer; <?php if($row['user']->user_level == 3){?>border-right:1px solid #000;<?php }?>">Delete</a></li>
            <?php if($row['user']->user_level == 3){?>
            <li><a href="admin/users/update/<?php echo $row['user']->user_id;?>">Update</a></li>
            <?php }?>
          </ul>
        </div>
      </div>
      <?php }?><br /><br />
      <div id="cont"><?php echo $this->pagination->create_links();?></div>
    </div>
  </div>
</div>
