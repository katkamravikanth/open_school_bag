<link rel="stylesheet" href="files/admin/css/jquery.fileupload-ui.css" />
<style type="text/css">
input[type="text"], select, textarea {
	color: #000;
	font-style:normal !important;
}
.alert-error {
	color: #f30 !important;
	line-height: 20px;
}
</style>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" /><a href="<?php echo site_url("admin").'/'.$this->uri->segment(2);?>">
          <p><?php echo ucfirst($this->uri->segment(2));?></p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p><?php echo $title;?></p>
        </li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start product  page ------------>
  <div class="product">
    <div class="product-inner">
      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div style="padding-top:20px; padding-left:20px; color:#090; font-size:16px; font-weight:bold;">';
            echo '<strong>Well done!</strong> new user created with success.';
          echo '</div><script type="text/javascript">$(function(){$("input[type=\'text\'], input[type=\'email\'], select, textarea").val("");})</script>';
        }else{
          echo '<div style="padding-top:20px; padding-left:20px; color:#f30; font-size:16px; font-weight:bold;">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
	  echo '<div style="padding-top:20px; padding-left:20px; color:#f30; font-size:16px; font-weight:bold;">';
      echo validation_errors('');
	  echo '</div>';?>
      <form action="admin/users/add" method="post" enctype="multipart/form-data" class="form-horizontal" id="add_user">
        <div class="alert mess alert-error" style="display:none; padding-top:20px; font-size:16px; font-weight:bold;">&nbsp;</div>
        <table width="90%" border="0" cellspacing="20" cellpadding="0" align="center">
          <tr>
            <th valign="top" class="fonts">Name * :</th>
            <td valign="top"><input type="text" name="name" id="name" class="box" value="<?php echo set_value('name'); ?>" /></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Email * :</th>
            <td valign="top"><input type="email" name="email" id="email" class="box" value="<?php echo set_value('email'); ?>" /></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Username * :</th>
            <td valign="top"><input type="text" name="username" id="username" class="box" value="<?php echo set_value('username'); ?>" /></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Password * :</th>
            <td valign="top"><input type="password" name="password" id="password" class="box" /></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">User Level * :</th>
            <td valign="top"><select id="level" name="level" class="box1">
                <option value="">Select one</option>
                <option value="9" <?php if(set_value('level') == 9){echo 'selected="selected"';}?>>Admin</option>
                <option value="3" <?php if(set_value('level') == 3){echo 'selected="selected"';}?>>Publisher</option>
                <option value="2" <?php if(set_value('level') == 2){echo 'selected="selected"';}?>>Seller</option>
                <option value="1" <?php if(set_value('level') == 1){echo 'selected="selected"';}?>>Buyer</option>
              </select></td>
          </tr>
        </table>
        <div id="publisher" <?php if(set_value('level') != 3){?>style="display:none;"<?php }?>>
          <table width="90%" border="0" cellspacing="20" cellpadding="0" align="center">
            <tr>
              <th valign="top" class="fonts">Publisher Title * :</th>
              <td valign="top"><input type="text" name="partner_title" id="partner_title" class="box" value="<?php echo set_value('partner_title'); ?>" /></td>
            </tr>
            <tr>
              <th valign="top" class="fonts">Publisher Slogan :</th>
              <td valign="top"><input type="text" name="partner_slogan" id="partner_slogan" class="box" value="<?php echo set_value('partner_slogan'); ?>" /></td>
            </tr>
            <tr>
              <th valign="top" class="fonts">Publisher Logo :</th>
              <td valign="top"><input type="file" name="partner_logo" id="partner_logo" /></td>
            </tr>
            <tr>
              <th valign="top" class="fonts">Publisher Body * :</th>
              <td valign="top"><textarea cols="30" rows="5" id="partner_body" name="partner_body"><?php echo set_value('partner_body'); ?></textarea></td>
            </tr>
            <tr>
              <th valign="top" class="fonts">Facebook Profile :</th>
              <td valign="top"><input type="text" name="partner_fb" id="partner_fb" class="box" value="<?php echo set_value('partner_fb'); ?>" /></td>
            </tr>
            <tr>
              <th valign="top" class="fonts">Twitter Profile :</th>
              <td valign="top"><input type="text" name="partner_tw" id="partner_tw" class="box" value="<?php echo set_value('partner_tw'); ?>" /></td>
            </tr>
          </table>
        </div>
        <div id="seller" <?php if(set_value('level') != 2){?>style="display:none;"<?php }?>>
          <table width="90%" border="0" cellspacing="20" cellpadding="0" align="center">
            <tr>
              <th valign="top" class="fonts">Bank Name * :</th>
              <td valign="top"><div class="styled-select">
                  <select name="bank_name" id="bank_name" style="width:100%;" class="popupinput">
                    <option value="">Select One</option>
                    <option <?php if(set_value('bank_name') == 'ANZ'){ echo 'selected';}?>>ANZ</option>
                    <option <?php if(set_value('bank_name') == 'Bank of China'){ echo 'selected';}?>>Bank of China</option>
                    <option <?php if(set_value('bank_name') == 'Citibank'){ echo 'selected';}?>>Citibank</option>
                    <option <?php if(set_value('bank_name') == 'DBS Bank'){ echo 'selected';}?>>DBS Bank</option>
                    <option <?php if(set_value('bank_name') == 'HSBC'){ echo 'selected';}?>>HSBC</option>
                    <option <?php if(set_value('bank_name') == 'Maybank'){ echo 'selected';}?>>Maybank</option>
                    <option <?php if(set_value('bank_name') == 'OCBC'){ echo 'selected';}?>>OCBC</option>
                    <option <?php if(set_value('bank_name') == 'POSB'){ echo 'selected';}?>>POSB</option>
                    <option <?php if(set_value('bank_name') == 'Standard Chartered Bank'){ echo 'selected';}?>>Standard Chartered Bank</option>
                    <option <?php if(set_value('bank_name') == 'UOB'){ echo 'selected';}?>>UOB</option>
                    <option <?php if(set_value('bank_name') == 'Others'){ echo 'selected';}?>>Others</option>
                  </select>
                </div></td>
            </tr>
            <tr class="bank_other" style="display:none">
              <td valign="top" class="fonts"> Other Bank Name : </td>
              <td valign="top"><input type="text" name="bank_other" id="bank_other" class="box" value="<?php echo set_value('bank_other'); ?>" /></td>
            </tr>
            <tr>
              <th valign="top" class="fonts">Bank Code * :</th>
              <td valign="top"><input type="text" name="bank_code" id="bank_code" class="box" value="<?php echo set_value('bank_code'); ?>" /></td>
            </tr>
            <tr>
              <th valign="top" class="fonts">Branch Code * :</th>
              <td valign="top"><input type="text" name="bank_branch_code" id="bank_branch_code" class="box" value="<?php echo set_value('bank_branch_code'); ?>" /></td>
            </tr>
            <tr>
              <th valign="top" class="fonts">Account Type * :</th>
              <td valign="top"><input type="text" name="account_type" id="account_type" class="box" value="<?php echo set_value('account_number'); ?>" /></td>
            </tr>
            <tr>
              <th valign="top" class="fonts">Account Number * :</th>
              <td valign="top"><input type="text" name="account_number" id="account_number" class="box" value="<?php echo set_value('account_number'); ?>" /></td>
            </tr>
          </table>
        </div>
        <table width="90%" border="0" cellspacing="20" cellpadding="0" align="center">
          <tr>
            <th colspan="3" align="center"> <div class="right-botton">
                <div class="right-botton1">
                  <div class="right-botton2">
                    <input type="reset" name="reset" id="reset" value="Cancel" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
                  </div>
                </div>
              </div>
              <div class="right-botton" style="width:117px">
                <div class="right-botton1" style="width:111px">
                  <div class="right-botton2" style="width:107px">
                    <input type="submit" name="save" id="save" value="Add User" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
                  </div>
                </div>
              </div></th>
          </tr>
        </table>
      </form>
      <?php //echo form_close(); ?>
    </div>
  </div>
  <div class="clear"></div>
  <!---------- end product page ----------->
  <div class="clear"></div>
  <div class="clear"></div>
</div>
