<link rel="stylesheet" href="files/admin/css/jquery.fileupload-ui.css" />
<style type="text/css">
input[type="text"], select, textarea {
	color: #000;
}
.alert-error {
	color: #f30 !important;
	line-height: 20px;
}
</style>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" /><a href="<?php echo site_url("admin").'/'.$this->uri->segment(2);?>">
          <p><?php echo ucfirst($this->uri->segment(2));?></p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p><?php echo $title;?></p>
        </li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start product  page ------------>
  <div class="product">
    <div class="product-inner" style="width:100%;">
      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div style="padding-top:20px; padding-left:20px; color:#090; font-size:16px; font-weight:bold;">';
            echo '<strong>Well done!</strong> new user created with success.';
          echo '</div>';       
        }else{
          echo '<div style="padding-top:20px; padding-left:20px; color:#f30; font-size:16px; font-weight:bold;">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
	  echo '<div style="padding-top:20px;; color:#f30; font-size:16px; font-weight:bold;">';
      echo validation_errors('');
	  echo '</div>';?>
      <form action="admin/users/update/<?php echo $this->uri->segment(4); ?>" method="post" enctype="multipart/form-data" class="form-horizontal" id="add_user">
      <input type="hidden" name="level" id="level" class="box" value="<?php echo $user->user_level; ?>" />
        <div class="alert mess alert-error" style="display:none; padding-top:20px; font-size:16px; font-weight:bold;">&nbsp;</div>
        <table width="90%" border="0" cellspacing="20" cellpadding="0" align="center">
          <tr>
            <th valign="top" class="fonts">Publisher Title * :</th>
            <td valign="top"><input type="text" name="partner_title" id="partner_title" class="box" value="<?php echo $partner->partner_title; ?>" /></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Publisher Slogan :</th>
            <td valign="top"><input type="text" name="partner_slogan" id="partner_slogan" class="box" value="<?php echo $partner->partner_slogan; ?>" /></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Publisher Logo :</th>
            <td valign="top"><img src="files/<?php echo $partner->partner_logo;?>" style="max-width:100px; max-height:120px;" alt="" /><br>
            <input type="file" name="partner_logo" id="partner_logo" /></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Publisher Body * :</th>
            <td valign="top"><textarea cols="30" rows="5" id="partner_body" name="partner_body"><?php echo $partner->partner_body; ?></textarea></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Facebook Profile :</th>
            <td valign="top"><input type="text" name="partner_fb" id="partner_fb" class="box" value="<?php echo $partner->partner_fb; ?>" /></td>
          </tr>
          <tr>
            <th valign="top" class="fonts">Twitter Profile :</th>
            <td valign="top"><input type="text" name="partner_tw" id="partner_tw" class="box" value="<?php echo $partner->partner_tw; ?>" /></td>
          </tr>
          <tr>
            <th colspan="3" align="center"> <div class="right-botton">
                <div class="right-botton1">
                  <div class="right-botton2">
                    <input type="reset" name="reset" id="reset" value="Cancel" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
                  </div>
                </div>
              </div>
              <div class="right-botton" style="width:117px">
                <div class="right-botton1" style="width:111px">
                  <div class="right-botton2" style="width:107px">
                    <input type="submit" name="save" id="save" value="Save Changes" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
                  </div>
                </div>
              </div></th>
          </tr>
        </table>
      </form>
      <?php //echo form_close(); ?>
    </div>
  </div>
  <div class="clear"></div>
  <!---------- end product page ----------->
  <div class="clear"></div>
  <div class="clear"></div>
</div>