<script type="text/javascript">
$(function(){
	<?php if($this->uri->segment(2) == 'users' || $this->uri->segment(2) == 'notifications' || $this->uri->segment(2) == 'settings'){?>
	$('#dashboard').removeClass('active');
	$('#general').addClass('active');
	$('.banner_content .wk-accordion-default #general').next(".content-wrapper").show();
	$(".banner_content .wk-accordion-default #general").next(".content-wrapper").height(140);
	<?php }?>
	<?php if($this->uri->segment(2) == 'products' || $this->uri->segment(2) == 'orders' || $this->uri->segment(2) == 'reports'){?>
	$('#dashboard').removeClass('active');
	$('#products').addClass('active');
	$('.banner_content .wk-accordion-default #products').next(".content-wrapper").show();
	$(".banner_content .wk-accordion-default #products").next(".content-wrapper").height(100);
	<?php }?>
	<?php if($this->uri->segment(2) == 'promotions'){?>
	$('#dashboard').removeClass('active');
	$('#promotions').addClass('active');
	<?php }?>
	<?php if($this->uri->segment(2) == 'page'){?>
	$('#dashboard').removeClass('active');
	$('#main_pages').addClass('active');
	$('.banner_content .wk-accordion-default #main_pages').next(".content-wrapper").show();
	$(".banner_content .wk-accordion-default #main_pages").next(".content-wrapper").height(130);
	<?php }?>
	<?php if($this->uri->segment(2) == 'cms'){?>
	$('#dashboard').removeClass('active');
	$('#cms').addClass('active');
	$('.banner_content .wk-accordion-default #cms').next(".content-wrapper").show();
	$(".banner_content .wk-accordion-default #cms").next(".content-wrapper").height(50);
	<?php }?>
	<?php if($this->uri->segment(2) == 'categories'){?>
	$('#dashboard').removeClass('active');
	$('#categories').addClass('active');
	$('.banner_content .wk-accordion-default #categories').next(".content-wrapper").show();
	$(".banner_content .wk-accordion-default #categories").next(".content-wrapper").height(80);
	<?php }?>
	<?php if($this->uri->segment(2) == 'images'){?>
	$('#dashboard').removeClass('active');
	$('#images').addClass('active');
	<?php }?>
});
</script>
<div class="nav">
  <div id="home_banner_content">
    <div class="banner_content"> 
      <!-- START: Modules Anywhere -->
      <div class="wk-accordion wk-accordion-default " data-widgetkit="accordion" style="display:block">
      	<a href="admin"><h5 class="toggler active" id="dashboard"> <div class="homeicon"></div><b>Dashboard</b></h5></a>
        
        <h5 class="toggler" id="general"><div class="homeicon2"></div><b>General</b></h5>
        <div style="overflow: hidden;" class="content-wrapper">
          <a href="admin/users"><h1 <?php if($this->uri->segment(2) == 'users'){echo 'class="firs"';}?>>Users</h1></a>
          <a href="admin/notifications"><h1 <?php if($this->uri->segment(2) == 'notifications'){echo 'class="firs"';}?>>Notifications</h1></a>
          <a href="admin/settings/site"><h1 <?php if($this->uri->segment(3) == 'site'){echo 'class="firs"';}?>>Site Settings</h1></a>
          <a href="admin/settings/profile"><h1 <?php if($this->uri->segment(3) == 'profile' || $this->uri->segment(3) == 'config'){echo 'class="firs"';}?>>Update Profile</h1></a>
          <a href="admin/settings/cpass"><h1 <?php if($this->uri->segment(3) == 'cpass'){echo 'class="firs"';}?>>Change Password</h1></a>
          <a href="admin/logout"><h1>Logout</h1></a>
        </div>
        
        <h5 class="toggler" id="products"><div class="homeicon3"></div><b>Products</b></h5>
        <div style="overflow: hidden;" class="content-wrapper">
          <a href="admin/products"><h1 <?php if($this->uri->segment(2) == 'products' && !$this->uri->segment(3)){echo 'class="firs"';}?>>Manage Products</h1></a>
          <a href="admin/products/add"><h1 <?php if($this->uri->segment(2) == 'products' && $this->uri->segment(3) == 'add'){echo 'class="firs"';}?>>Post a Product</h1></a>
          <a href="admin/orders"><h1 <?php if($this->uri->segment(2) == 'orders'){echo 'class="firs"';}?>>Orders</h1></a>
          <a href="admin/reports"><h1 <?php if($this->uri->segment(2) == 'reports'){echo 'class="firs"';}?>>Reports</h1></a>
        </div>
        
        <a href="admin/promotions"><h5 class="toggler" id="promotions"> <div class="homeicon3"></div><b>Promotions</b></h5></a>
        
        <h5 class="toggler" id="main_pages"><div class="homeicon2"></div><b>Main Pages</b></h5>
        <div style="overflow: hidden;" class="content-wrapper">
            <a href="admin/page/about"><h1 <?php if($this->uri->segment(3) == 'about'){echo 'class="firs"';}?>>About Us</h1></a>
            <a href="admin/page/partners"><h1 <?php if($this->uri->segment(3) == 'partners'){echo 'class="firs"';}?>>Partners</h1></a>
            <a href="admin/page/privacy_policy"><h1 <?php if($this->uri->segment(3) == 'privacy_policy'){echo 'class="firs"';}?>>Privacy Policy</h1></a>
            <a href="admin/page/tnc"><h1 <?php if($this->uri->segment(3) == 'tnc'){echo 'class="firs"';}?>>T &amp; C</h1></a>
            <a href="admin/page/faqs"><h1 <?php if($this->uri->segment(3) == 'faqs' || $this->uri->segment(3) == 'faq_add' || $this->uri->segment(3) == 'faq_update'){echo 'class="firs"';}?>>FAQ's</h1></a>
        </div>
        
        <h5 class="toggler" id="cms"><div class="homeicon2"></div><b>CMS</b></h5>
        <div style="overflow: hidden;" class="content-wrapper">
            <a href="admin/cms/pages"><h1 <?php if($this->uri->segment(3) == 'pages'){echo 'class="firs"';}?>>Manage Pages</h1></a>
            <a href="admin/cms/add"><h1 <?php if($this->uri->segment(3) == 'add'){echo 'class="firs"';}?>>Add Page</h1></a>
        </div>
        
        <h5 class="toggler" id="categories"><div class="homeicon2"></div><b>Categories</b></h5>
        <div style="overflow: hidden;" class="content-wrapper">
            <a href="admin/categories/levels"><h1 <?php if($this->uri->segment(3) == 'levels'){echo 'class="firs"';}?>>Levels</h1></a>
            <a href="admin/categories/subjects"><h1 <?php if($this->uri->segment(3) == 'subjects'){echo 'class="firs"';}?>>Subjects</h1></a>
            <a href="admin/categories/resources"><h1 <?php if($this->uri->segment(3) == 'resources'){echo 'class="firs"';}?>>Resources</h1></a>
        </div>
        
        <a href="admin/images"><h5 class="toggler" id="images"> <div class="homeicon2"></div><b>Images</b></h5></a>
      </div>
    </div>
  </div>
  <div class="clear"></div>
</div>