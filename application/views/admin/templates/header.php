<!DOCTYPE html>
<html lang="en-US">
<head>
<title><?php echo $title;?>:: Admin Panel</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<base href="<?php echo base_url();?>" />
<link rel="icon" href="files/images/favicon.icon" type="image/x-icon" />
<link rel="shortcut icon" href="files/images/favicon.icon" type="image/x-icon" />

<link href="files/admin/css/style.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="files/admin/css/layout.css" type="text/css" media="screen"/>

<script type="text/javascript" src="files/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="files/js/jquery-migrate-1.2.1.min.js"></script>
<script src="files/js/widgetkit.js" type="text/javascript"></script>
<script src="files/admin/js/admin.js"></script>
</head>

<body>
<div class="wapper">
  <div class="header">
    <div class="headerinner">
      <div class="logo"> <a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/logo.png" alt="OpenSchoolbag" title="OpenSchoolbag" /></a> </div>
      <div class="searchbg" style="display:none">
        <input type="text" value="Search.." />
        <img src="files/admin/images/searchimg.png" /> </div>
      <div class="settings">
        <div class="alert"> <a href="admin/notifications"><img src="files/admin/images/alert.png" />
          <div class="alertnum"> <?php echo $notif;?> </div></a> </div>
        <div class="settingsinner"> <a href="admin/settings/profile"><img src="files/admin/images/profileimg.png" class="settinginnerimg" /></a> </div>
        <div class="profileimg" style="margin-left:10px;"> <a href="admin/logout" title="Logout"><img src="files/admin/images/logout.png" class="settinginnerimg" title="Logout" /></a> </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  <div class="container">
    <div style="width:964px; background: url(files/admin/images/bg1.gif) repeat; margin:0 auto;">