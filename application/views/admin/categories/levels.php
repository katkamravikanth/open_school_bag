<style>
input[type="text"], select, textarea {
	color: #000;
}
a {
	color: #6E6C64;
}
a:hover {
	color: #ef4f45;
}
.alert-error {
	color: #f30 !important;
	line-height: 20px;
}
/* === the dark full screen background, which appears on form pop-up === */
#background-on-popup {
    z-index: 9990;
    position: fixed;
    display: none;
    height: 100%;
    width: 100%;
    background: #000000;
    top: 0px;
    left: 0px;
}

/* === The X-mark from the top-right corner of the pop-up form, from which you can close the box === */
div.close {
    background: url("files/images/login/closebox.png") no-repeat scroll 0 0 transparent;
    cursor: pointer;
    float: right;
    height: 16px;
    top: 10px;
    right: 10px;
    position: relative;
    width: 16px;
}
#success {
	max-width: 480px;
	min-width: 280px;
	width: 100%;
	margin-top: 20px;
	margin-bottom: 20px;
	position: absolute;
	display: none;
	left: 50%;
    top: 20%;
	margin-left: -240px;
    border-radius: 4px;
    z-index: 9999;
}
.form-section {
	border-radius: 4px;
	background: rgba(255,255,255,0.8); /* here you can change the background color of the form */
	padding: 30px;
}
#cont {
	width: 100%;
	height: 30px;
	float: right;
	background: none;
}
#cont li a[href="#"], #cont li a:hover {
	background: #0b9444;
	padding: 3px 8px;
	color: #fff;
	margin: 0;
}
#cont ul {
	float:right;
	margin: 0;
	margin-right:20px;
	padding: 0;
}
#cont li {
	list-style: none;
	float: left;
	padding: 5px 5px;
}
#cont a {
	padding: 3px 8px;
	font-size: 12pxpx;
	font-weight: bold;
	color: #666;
	margin: 0 0px 0 0;
}
#save, #update {
	border:none;
	color:#fff;
	font-size:14px;
	cursor:pointer;
	background: #0b9444;
	border-radius: 3px;
	width: 83px;
	height: 29px;
}
#save:hover, #update:hover {
	background: #ef4036;
}
@media \0screen {
	.form-section {
		background: #CCC;
	}
}
</style>
<link rel="stylesheet" href="files/css/themes/alertify.core.css" />
<link rel="stylesheet" href="files/css/themes/alertify.default.css" id="toggleCSS" />
<script src="files/js/lib/alertify.min.js"></script>
<script type="text/javascript">
$(function(){
	function reset () {
		$("#toggleCSS").attr("href", "files/css/themes/alertify.default.css");
		alertify.set({
			labels : {
				ok     : "OK",
				cancel : "Cancel"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}
	
	$('.add_level').click(function(e) {
		loadAdd();
    });
	
	$('#save').click(function(e) {
		level = $('#s_level').val();
		if(level == ''){$("#pop_smess").css('color', '#f30');$("#pop_smess").html("Please enter a level name");return false;}
		$.post("admin/categories/s_level",{level:level}, function(data){
			if(data){
				$("#pop_smess").css('color', '#f30');
				$("#pop_smess").html("!Failed Please try later");
			}else{
				$("#pop_smess").css('color', '#090');
				$("#pop_smess").html("Successfully added");
			}
		});
    });
	
	$('td a.update').click(function(e) {
		console.log($(this).attr('name'));
        id = $(this).attr('name');
		level = $('.level_'+id).html();
		$('#level_id').val(id);
		$('#level').val(level);
		loadEdit();
    });
	
	$('#update').click(function(e) {
        id = $('#level_id').val();
		level = $('#level').val();
		if(level == ''){$("#pop_mess").css('color', '#f30');$("#pop_mess").html("Please enter a level name");return false;}
		$.post("admin/categories/e_level",{id:id, level:level}, function(data){
			if(data){
				$("#pop_mess").css('color', '#f30');
				$("#pop_mess").html("!Failed Please try later");
			}else{
				$("#pop_mess").css('color', '#090');
				$("#pop_mess").html("Successfully updated");
			}
		});
    });
	
	$("div.close, .mask_post").click(function() {
		disablePopup();  // function to close pop-up forms
	});
	
	function loadAdd() {
		$("#pop_smess").hide();
		$(".post_cat").fadeIn(300);
		$(".post_cat").css('top', '25%');
		$(".mask_post").css("opacity", "0.7");
		$(".mask_post").fadeIn(300);
	}
	
	function loadEdit() {
		$("#pop_mess").hide();
		$(".post_product").fadeIn(300);
		$(".post_product").css('top', '25%');
		$(".mask_post").css("opacity", "0.7");
		$(".mask_post").fadeIn(300);
	}
	function disablePopup() {
		$(".post_cat").fadeOut("normal");
		$(".post_product").fadeOut("normal");
		$(".mask_post").fadeOut("normal");
		location.reload();
	}
	
	// delete product
	$(".delete").on( 'click', function () {
		id = $(this).attr('name');
		reset();
		alertify.confirm("Are you sure do you want to delete this level?", function (e) {
			if(e){
				window.location='admin/categories/d_level/'+id;
			}
		});
		return false;
	});
});
</script>

<div id="success" class="post_cat" style="border-radius: 4px; background: rgba(255,255,255,0.8); padding: 30px; color:#000;">
  <div class="close"></div>
  <!-- close button of the register form -->
  <div id="message_post" class="form-section" align="center">
  	<p style="font-weight:bold; text-align:left;">Add Level</p>
  	<div id="pop_smess" style="color:#f30; margin-bottom:10px; text-align:left; font-size:16px; font-weight:bold;">&nbsp;</div>
    
    Level Name : <input type="text" name="s_level" id="s_level" /><br><br>
    <input type="button" name="save" id="save" value="Add" />
  </div>
</div>
<!-- END OF SUCCESS MESSAGE -->

<div id="success" class="post_product" style="border-radius: 4px; background: rgba(255,255,255,0.8); padding: 30px; color:#000;">
  <div class="close"></div>
  <!-- close button of the register form -->
  <div id="message_post" class="form-section" align="center">
  	<p style="font-weight:bold; text-align:left;">Edit Level</p>
  	<div id="pop_mess" style="color:#f30; margin-bottom:10px; text-align:left; font-size:16px; font-weight:bold;">&nbsp;</div>
    
    <input type="hidden" name="level_id" id="level_id" />
    Level Name : <input type="text" name="level" id="level" /><br><br>
    <input type="button" name="update" id="update" value="Update" />
  </div>
</div>
<!-- END OF SUCCESS MESSAGE -->

<div id="background-on-popup" class="mask_post"></div>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p>Categories</p>
        </li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p><?php echo $title;?></p>
        </li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start main ------------>
  <div style="float: right; font-size:16px; font-weight:bold; color:#f30"><br />
    <a href="javascript:;" class="add_level">Add a new</a></div>
  <div class="main" style="padding-top:10px;">
    <div class="product1"><br />
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr style="background:#090; color:#fff; height:40px; font-size:16px">
          <th>S.No</th>
          <th>Level</th>
          <th colspan="2">Action</th>
        </tr>
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr>
        <?php if(count($levels)){if(!$this->uri->segment(4)){$i=1;}else{$i=$this->uri->segment(4)+1;}foreach($levels as $level){
			if($i%2){$bg = '#eee';}else{$bg='#ddd';}?>
        <tr style="background:<?php echo $bg;?>; color:#000; height:30px;" valign="middle" align="center">
          <td><?php echo $i; ?></td>
          <td class="level_<?php echo $level->level_id;?>"><?php echo $level->level; ?></td>
          <td><a style="cursor:pointer;" name="<?php echo $level->level_id;?>" class="update"><img src="files/images/icons/edit.png" alt="Update" width="16" /></a></td>
          <td><a style="cursor:pointer;" name="<?php echo $level->level_id;?>" class="delete"><img src="files/images/icons/delete.png" alt="Delete" width="16" /></a></td>
        </tr>
        <?php $i++;}}else{?>
        <tr>
          <td colspan="4" align="center">No levels found</td>
        </tr>
        <?php }?>
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="4"><div id="cont"><?php echo $this->pagination->create_links();?></div></td>
        </tr>
      </table>
    </div>
  </div>
</div>