<link rel="stylesheet" href="files/admin/css/jquery.fileupload-ui.css" />
<style type="text/css">
input[type="text"], select, textarea {
	color: #000;
}
</style>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" /><a href="<?php echo site_url("admin").'/'.$this->uri->segment(2);?>">
          <p><?php echo ucfirst($this->uri->segment(2));?></p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p><?php echo $title;?></p>
        </li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start product  page ------------>
  <div class="product" style="height:300px;">
    <div class="product-inner">
      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div style="padding-top:20px; color:#090; font-size:16px; font-weight:bold;">';
            echo '<strong>Well done!</strong> You have added a new resource.';
          echo '</div><script type="text/javascript">$("input[type=\'text\'], select, textarea").val("");</script>';       
        }else{
          echo '<div style="padding-top:20px; color:#f30; font-size:16px; font-weight:bold;">';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
	  echo validation_errors('');?>
      <form action="" method="post" enctype="multipart/form-data" class="form-horizontal" id="add_user">
      <div class="banner_mess" style="display:none; padding-top:20px; font-size:16px; font-weight:bold;">&nbsp;</div>
      <table width="90%" border="0" cellspacing="20" cellpadding="0" align="center">
        <tr>
          <th valign="top" class="fonts">Icon * :</th>
          <td valign="top"><input type="file" id="icon" name="icon" class="box" /></td>
        </tr>
        <tr>
          <th valign="top" class="fonts">Resource Title * :</th>
          <td valign="top"><input type="text" id="resource" name="resource" class="box" /></td>
        </tr>
        <tr>
          <th valign="top" class="fonts" colspan="2"><div class="right-botton" style="width:117px">
              <div class="right-botton1" style="width:111px">
                <div class="right-botton2" style="width:107px">
                  <input type="submit" name="save" id="save" value="Add" style="background:none; border:none; margin:5px 8px; color:#fff; font-size:14px; cursor:pointer;" />
                </div>
              </div>
            </div>
            </td>
        </tr>
      </table>
      </form>
    </div>
  </div>
  <div class="clear"></div>
  <!---------- end product page ----------->
  <div class="clear"></div>
  <div class="clear"></div>
</div>