<style>
input[type="text"], select, textarea {
	color: #000;
}
a {
	color: #6E6C64;
}
a:hover {
	color: #ef4f45;
}
.alert-error {
	color: #f30 !important;
	line-height: 20px;
}
#cont {
	width: 100%;
	height: 30px;
	float: right;
	background: none;
}
#cont li a[href="#"], #cont li a:hover {
	background: #0b9444;
	padding: 3px 8px;
	color: #fff;
	margin: 0;
}
#cont ul {
	float:right;
	margin: 0;
	margin-right:20px;
	padding: 0;
}
#cont li {
	list-style: none;
	float: left;
	padding: 5px 5px;
}
#cont a {
	padding: 3px 8px;
	font-size: 12pxpx;
	font-weight: bold;
	color: #666;
	margin: 0 0px 0 0;
}
@media \0screen {
	.form-section {
		background: #CCC;
	}
}
</style>
<link rel="stylesheet" href="files/css/themes/alertify.core.css" />
<link rel="stylesheet" href="files/css/themes/alertify.default.css" id="toggleCSS" />
<script src="files/js/lib/alertify.min.js"></script>
<script type="text/javascript">
$(function(){
	function reset () {
		$("#toggleCSS").attr("href", "files/css/themes/alertify.default.css");
		alertify.set({
			labels : {
				ok     : "OK",
				cancel : "Cancel"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}
	
	// delete product
	$(".delete").on( 'click', function () {
		id = $(this).attr('name');
		reset();
		alertify.confirm("Are you sure do you want to delete this resource?", function (e) {
			if(e){
				window.location='admin/categories/d_resource/'+id;
			}
		});
		return false;
	});
});
</script>

<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p>Categories</p>
        </li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p><?php echo $title;?></p>
        </li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start main ------------>
  <div style="float: right; font-size:16px; font-weight:bold; color:#f30"><br />
    <a href="admin/categories/s_resource" class="add_level">Add a new</a></div>
  <div class="main" style="padding-top:10px;min-height:550px">
    <div class="product1">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr style="background:#090; color:#fff; height:40px; font-size:16px">
          <th>S.No</th>
          <th>Resource</th>
          <th>Resource Icon</th>
          <th colspan="2">Action</th>
        </tr>
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr>
        <?php if(count($resources)){if(!$this->uri->segment(4)){$i=1;}else{$i=$this->uri->segment(4)+1;}foreach($resources as $resource){
			if($i%2){$bg = '#eee';}else{$bg='#ddd';}?>
        <tr style="background:<?php echo $bg;?>; color:#000; height:30px;" valign="middle" align="center">
          <td><?php echo $i; ?></td>
          <td><?php echo $resource->resources; ?></td>
          <td style="padding-top:10px;"><img src="files/<?php echo $resource->icon;?>" alt="" style="max-width:100px; max-height:100px;" /></td>
          <td><a style="cursor:pointer;" href="admin/categories/e_resource/<?php echo $resource->resource_id;?>"><img src="files/images/icons/edit.png" alt="Update" width="16" /></a></td>
          <td><a style="cursor:pointer;" name="<?php echo $resource->resource_id;?>" class="delete"><img src="files/images/icons/delete.png" alt="Delete" width="16" /></a></td>
        </tr>
        <?php $i++;}}else{?>
        <tr>
          <td colspan="5" align="center">No resources found</td>
        </tr>
        <?php }?>
        <tr>
          <td colspan="5">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="5"><div id="cont"><?php echo $this->pagination->create_links();?></div></td>
        </tr>
      </table>
    </div>
  </div>
</div>