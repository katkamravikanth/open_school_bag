<style type="text/css">
input[type="text"], select, textarea {
	color: #000;
}
</style>
<div class="containerinner">
  <p class="heading"><?php echo $title;?></p>
  <div class="navinner">
    <div class="menu">
      <ul>
        <li><a href="<?php echo site_url("admin"); ?>"><img src="files/admin/images/navinnerhome.png" />
          <p>Home</p>
          </a></li>
        <li><img src="files/admin/images/navinnerarrow.png" />
          <p><?php echo $title;?></p>
        </li>
      </ul>
    </div>
    <div class="date">
      <p><?php echo date("M d Y");?></p>
    </div>
  </div>
  <!------- start purchases page ------------>
  <div class="purchases">
    <h1>Order #<?php echo $orders[0]['ord']->order_no;?></h1>
    <h3>Order date: <?php echo $orders[0]['ord']->created; $i=0;?></h3>
    <?php foreach($orders as $ords){?>
		<?php if($ords['ord']->order_ptype == 'Physical'){?>
        <p style="line-height:20px; font-size:16px; font-weight:bold;">Product Name: <?php echo $ords['ord']->order_pname;?><br /><br />
        Physical Product Status: <?php echo $ords['ord']->order_status;?></p>
        <div class="purchases-inner">
          <h1>About This  Order: Order Information Invoices</h1>
          <div class="purchases-inner1">
            <div class="purchases-inner-left">
              <ul>
                <li><a class="first">Shipping Address</a></li>
                <li><a><?php echo $ords['ship'][0]->shipping_fname;?>,</a></li>
                <li><a><?php echo $ords['ship'][0]->shipping_address;?>,</a></li>
                <li><a><?php echo $ords['ship'][0]->shipping_city;?>.</a></li>
                <li><a>Tel:<?php echo $ords['ord']->user_phone;?></a></li>
              </ul>
            </div>
            <div class="purchases-inner-left1">
              <ul>
                <li><a class="first">Shipping Method</a></li>
                <li><a>Shipping Charge - Fixed</a></li>
              </ul>
            </div>
          </div>
        </div>
        <br />
        <?php }$i++;
	}?>
    <h1><b>Invoice #<?php echo $orders[0]['ord']->order_no;?></b></h1>
    <h2 style="font-size:16px; color:#ef4f45; margin:10px 0px;">Items Invoiced</h2>
    <div class="right3">
      <table width="100%" border="0" align="center" cellpadding="5" style="margin:0px 0 0px 0px; text-align:center;" cellspacing="0">
        <tr style="background:#f0eeea;">
          <th scope="col" align="center" class="td">Product Name</th>
          <th scope="col" align="center" class="td">Product Type</th>
          <th scope="col" align="center" class="td">Price</th>
          <th scope="col" align="center" class="td">Qty Invoiced</th>
          <th scope="col" align="center" class="td">Discount</th>
          <th scope="col" align="center" class="td">Subtotal</th>
        </tr>
        <?php $total = 0;foreach($orders as $ord){?>
        <tr>
          <td class="td" style="width:180px;"><?php echo $ord['ord']->order_pname;?></td>
          <td class="td" style="width:100px;"><?php echo $ord['ord']->order_ptype;?></td>
          <td class="td" style="width:100px;">S$ <?php echo number_format($ord['ord']->order_pprice, 2);?></td>
          <td class="td">1</td>
          <?php if($ord['ord']->order_promo == ''){$discounts = $ord['ord']->order_discount;}else{$discounts = 0;}?>
          <td class="td" style="width:100px;">S$ <?php echo number_format($discounts, 2);?></td>
          <td class="td">S$ <?php echo number_format($ord['ord']->order_pprice - $discounts, 2);?></td>
        </tr>
        <?php }?>
      </table>
      <!--<h1 style="font-size:12px; float:right;">Subtotal: S$ <?php echo number_format($orders[0]['ord']->final_amount, 2);?> <br />
      	Transaction Fee: S$ <?php echo number_format($orders[0]['ord']->trans_fee, 2);?>
        <!--Shipping &amp; Handling: S$ 0.00-></h1>
      <br />
      <br />
      <h3>Grand Total: S$ <?php echo number_format($orders[0]['ord']->final_amount + $orders[0]['ord']->trans_fee, 2);?></h3>-->
    </div>
  </div>
  <div class="clear"></div>
  <!---------- end purchases page ----------->
  <div class="clear"></div>
  <div class="clear"></div>
</div>