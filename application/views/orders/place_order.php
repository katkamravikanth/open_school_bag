<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="files/css/themes/alertify.core.css" />
<link rel="stylesheet" href="files/css/themes/alertify.default.css" id="toggleCSS" />
<script src="files/js/lib/alertify.min.js"></script>
<script type="text/javascript" src="files/js/jquery_ravikanth_order.js"></script>

<div id="success" class="post_product" style="border-radius: 4px; background: rgba(255,255,255,0.8); padding: 30px; color:#000;">
  <div class="close"></div>
  <!-- close button of the register form -->
  <div id="message_post" align="center">
    <p style="font-weight:bold; text-align:left;">Disclaimer * </p>
    <div id="pop_mess" style="color:#f30; margin-bottom:10px; text-align:left; font-size:16px; font-weight:bold;">&nbsp;</div>
    <input type="checkbox" name="pdisc" id="pdisc" />
    I hereby declare that my purchase is solely for my own use and I will not misuse the content purchased nor distribute through unauthorised means.<br>
    <br>
    <br>
    <p style="color:#C30"><strong>Please note that your transaction is only completed when you are redirected back to this site from Paypal.</strong></p>
    <br>
    <input type="button" name="add_it" id="add_it" value="Checkout" style="border:none; color:#fff; font-size:14px; cursor:pointer; background: #0b9444; border-radius: 3px; width: 83px; height: 29px;" />
  </div>
</div>
<!-- END OF SUCCESS MESSAGE -->

<div id="background-on-popup" class="mask_post"></div>
<article class="content">
  <h1><a href="<?php echo base_url();?>">Home</a> &raquo; Shopping cart</h1>
  <!-------------- start main-content ----------->
  <div id="main-content">
    <h1>Shopping Cart</h1>
    <!------------- start shopping cart page ---------->
    <?php if($this->cart->total_items() != 0){?>
    <div align="right" style="margin-right:55px;"> <a href="javascript:;" id="clear_cart" class="clear_cart">Clear Cart</a> </div>
    <div class="shopping-cart" style="display:block !important; width:90% !important;">
      <div class="title">
        <h4>Items (<?php echo $this->cart->total_items();?>)</h4>
        <h4>Name of Product</h4>
        <f>Price</f>
      </div>
      <?php $type = 0; $disc = 0; $sprice = 0;
	  foreach ($this->cart->contents() as $items){
		  if($items['options']['type'] == 'Physical'){
			  $type = $type+1;
		  }
		  if($items['options']['disc'] != '0' && $items['options']['disc'] != ''){
			  $disc = $disc+1;
			  $prod_unq = $items['options']['punique'].',';
		  }?>
      <div class="shopping-cart1" style="width:100% !important;"> <img src="files/<?php echo $items['options']['pic'];?>" alt="<?php echo $items['name'];?>" class="pro_pic" />
        <div class="content_part" style="width:550px !important; margin-left:100px !important;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="40%"><h3><?php echo $items['name'];?></h3><b>Type: <?php echo $items['options']['type'];?></b><h5 name="<?php echo $items['rowid'];?>" class="remove_cart">Remove</h5></td>
                <td>&nbsp;</td>
                <td width="40%"><h6>S$ <?php echo number_format($items['price'], 2);?></h6></td>
              </tr>
            </table>
        </div>
      </div>
      <?php $sprice = $sprice + $items['options']['sprice'];
	  }?>
    </div>
    <div style="display:block !important; width:100% !important;">&nbsp;</div>
    <!------------- end shopping cart page ---------->
    <div id="promo" style="width:500px; margin:0 auto; <?php if($disc != 0 || !$this->session->userdata('logged_in')){echo 'display:none';}?>">
      <div id="promo_mess" style="color:#f30">&nbsp;</div>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td width="20%">Redeem your Promo code</td>
          <td><input type="text" name="promo_code" id="promo_code" style="height:30px; padding:5px; font-size:16px; border:2px solid #666" /></td>
          <td><div class="shopping-cart-button">
              <div class="shopping-cart-button1">
                <input type="button" name="promo_but" id="promo_but" value="Redeem it" class="shopping-cart-button2" />
              </div>
            </div></td>
        </tr>
      </table>
    </div>
    <div class="error" style=" font-size:16px; font-weight:bold;"><?php echo validation_errors();?></div>
    <div class="shopping-cart-bottom">
      <?php if($type==0){
		$page = 'shop';
	  }else{
		$page = 'place_order';
	  }?>
      <form action="<?php echo $page;?>" method="post" name="shop" id="shop">
        <input type="hidden" name="tamount" id="tamount" value="<?php echo number_format($this->cart->total() + $site->trans_fee + $sprice, 2);?>" />
        <input type="hidden" name="otype" id="otype" value="<?php echo $type;?>" />
        <input type="hidden" name="promocode" id="promocode" />
        <!------------- start sh page ---------->
        <div class="shopping-contact" <?php if($type==0 || !$this->session->userdata('logged_in')){echo 'style="display:none;"';}?>>
          <h3>Shipping Address <a href="#" class="tooltip"><img src="files/images/icons/tool-tip.gif"><span style="text-transform:none !important;">Please provide the address you wish to have your <u>physical product(s)</u> delivered to." </span></a></h3>
          <div class="form">
            <table border="0" cellspacing="20">
              <tbody>
              	<tr>
                  <td colspan="2" style="color:#C00;">For delivery addresses in Singapore only.<br>" * " are compulsory fields</td>
                </tr>
                <tr>
                  <td valign="top" class="font">Full Name <a style="color:#c00; font-size:16px;">*</a></td>
                  <td valign="top"><input type="text" name="fname" id="fname" class="text-box" size="30" value="<?php echo set_value('fname');?>">
                    <div id="fname_mess" style="display:none; color:#f30; font-size:16px; font-weight:bold; padding-left:20px;"></div></td>
                </tr>
                <tr>
                  <td valign="top" class="font">Address <a style="color:#c00; font-size:16px;">*</a></td>
                  <td valign="top"><input type="text" name="address" id="address" class="text-box" size="30" value="<?php echo set_value('address');?>">
                    <div id="add_mess" style="display:none; color:#f30; font-size:16px; font-weight:bold; padding-left:20px;"></div></td>
                </tr>
                <tr>
                  <td valign="top" class="font">Postal Code <a style="color:#c00; font-size:16px;">*</a></td>
                  <td valign="top"><input type="text" name="pin_code" id="pin_code" class="text-box" size="30" value="<?php echo set_value('pic_code');?>">
                    <div id="pin_mess" style="display:none; color:#f30; font-size:16px; font-weight:bold; padding-left:20px;"></div></td>
                </tr>
                <tr>
                  <td valign="top" class="font">City <a style="color:#c00; font-size:16px;">*</a></td>
                  <td valign="top"><input type="text" name="city" id="city" class="text-box" size="30" value="<?php echo set_value('city');?>">
                    <div id="city_mess" style="display:none; color:#f30; font-size:16px; font-weight:bold; padding-left:20px;"></div></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!------------- end shopping-contact page ----------> 
        
        <!------------- start sho page ---------->
        <div class="sho" <?php if($type==0 || !$this->session->userdata('logged_in')){echo 'style="margin:0 auto; float:none; display:block"';}?>>
          <div class="shopping-cart-right">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr height="30">
                <td>Subtotal</td>
                <td width="15">:</td>
                <td>S$</td>
                <td align="right" id="stamount"><?php echo number_format($this->cart->total(), 2);?></td>
              </tr>
              <tr height="30" id="promoValueDisc" style="display:none;">
                <td>Discount value</td>
                <td width="15">:</td>
                <td>S$</td>
                <td align="right" id="userPromoValue">0.00</td>
              </tr>
              <?php if($sprice != 0){?>
              <tr height="30">
                <td>Shipping Charges</td>
                <td width="15">:</td>
                <td>S$</td>
                <td align="right" id="sprice"><?php echo number_format($sprice, 2);?></td>
              </tr>
              <?php }?>
              <tr height="30">
                <td>Transaction Fee <a href="#" class="tooltip"><img src="files/images/icons/tool-tip.gif"><span>This is a flat fee for each Checkout. You may wish to consolidate your purchases into a single payment to avoid incurring multiple transaction fee.</span></a></td>
                <td>:</td>
                <td>S$</td>
                <td align="right" id="trans_fee"><?php echo number_format($site->trans_fee, 2);?></td>
              </tr>
              <tr height="30" style="font-size:18px; font-weight:bold;">
                <td>Grand Total</td>
                <td>:</td>
                <td>S$</td>
                <td align="right" id="gtotal"><?php echo number_format($this->cart->total()+$site->trans_fee+$sprice, 2);?></td>
              </tr>
            </table>
            <?php if($this->session->userdata('logged_in')){?>
            <div class="shopping-cart-button" style="margin:30px 0px 0 18px;">
              <div class="shopping-cart-button1">
                <input type="button" name="place_order" id="place_order" value="Checkout" class="shopping-cart-button2" />
              </div>
            </div>
            <?php }else{?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-left:-100px;">
              <tr>
                <td><div class="shopping-cart-button" style="margin:10px 0px 0 18px;">
                    <div class="shopping-cart-button1">
                      <input type="button" name="login_checkout" id="login_checkout" value="Login to Checkout" class="shopping-cart-button2" />
                    </div>
                  </div></td>
                <td>&nbsp;</td>
                <td><div class="shopping-cart-button" style="margin:10px 0px 0 18px;">
                    <div class="shopping-cart-button1">
                      <input type="button" name="register_checkout" id="register_checkout" value="Register to Checkout" class="shopping-cart-button2" />
                    </div>
                  </div></td>
              </tr>
              <tr height="30">
                <td colspan="3"></td>
              </tr>
              <tr>
                <td colspan="3" style="color:#ef4036; font-size:15px;">Please login if you are an existing user or register with us in order to proceed with the purchase.</td>
              </tr>
            </table>
            <?php }?>
            <br />
            <div align="center" class="shopping-cart-button" style="margin:10px 0px 0 18px; line-height:40px; width:220px; height:40px;"><a href="products" style="color:#fff; font-weight:bold; font-size:16px;">
              &lt;&lt; Continue shopping
            </a> </div>
          </div>
        </div>
        <!------------- start shopping-contact page ---------->
      </form>
    </div>
    <?php }else{echo 'No items in your cart';}?>
    <!-------------- end main-content -----------> 
  </div>
  <div style="clear:both"></div>
  <!-------------- end main-content -----------> 
</article>