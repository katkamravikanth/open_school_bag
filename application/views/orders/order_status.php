<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<style type="text/css">
input[type="text"], select, textarea {
	color:#000 !important;
	font-style:normal !important;
}
.submit {
	width: 160px;
	height: 35px;
	line-height:35px;
	margin-left:15px;
	display:inline-block;
	background: #067936;
	border: none;
	text-align: center;
	color: #FFF !important;
	border-radius: 4px;
	font-size: 12px;
	font-weight: bold;
}
</style>
<article class="content">
  <h1><a href="<?php echo base_url();?>">Home</a> &raquo; Purchase Status</h1>
  <!-------------- start main-content ----------->
  <div id="main-content">
    <h1>Purchase Status: <?php echo $title;?></h1>
    <div id="orders" style="margin-top:10px; height:200px;">
      <div align="left"><?php echo $message;?></div><br><br>
      <div align="center"><a href="my_orders" class="submit">Back to Order History</a> <a href="products" class="submit">Back to Products page</a></div>
    </div>
  </div>
  <!-------------- start main-content -----------> 
</article>