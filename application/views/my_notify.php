<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<link href="files/css/users/style.css" type="text/css" rel="stylesheet" />
<link href="files/css/users/style3.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">
$(function() {
	$(".banner_content .wk-accordion-default .toggler.active").next(".content-wrapper").show();
	$(".banner_content .wk-accordion-default .toggler.active").next(".content-wrapper").height(120);
});
</script>

<article class="content">
  <h1><a href="<?php echo base_url();?>">Home</a> &raquo; <?php echo $title;?></h1>
  <div style="background: url(files/images/users/bg1.gif) repeat; margin:0 auto;"> <?php echo $this->load->view('templates/navigation');?>
    <div class="containerinner">
      <p class="heading">Notifications</p>
      <!------- start notification page ------------>
      <div class="notification"> 
        <!--<h1>Today</h1>--> 
        <!------- start inner page ------------>
        <?php if(count($note)){
		foreach($note as $notes){?>
        <div class="inner">
          <p align="left"><?php echo stripslashes($notes->note_message);?></p>
          <?php if($notes->note_linkname != '' && $notes->note_linkname != NULL){?>
          <a href="<?php echo $notes->note_link;?>" target="_blank" style="color:#060;float:right"><?php echo $notes->note_linkname;?></a>
          <?php }?>
          <h2><?php echo $notes->created;?></h2>
        </div>
        <?php }?>
        <div class="pagination"><?php echo $this->pagination->create_links();?></div>
        <?php }else{echo 'You do not have any notification';}?>
        <!------- end inne inner page ------------> 
      </div>
      <!---------- end notification page ----------->
      
      <div class="clear"></div>
      <div class="clear"></div>
    </div>
  </div>
</article>