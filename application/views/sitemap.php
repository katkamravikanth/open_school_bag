<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">

<article class="content">
  <h1><a href="<?php echo base_url();?>">Home</a> &raquo; <?php echo $title;?></h1>
  <!-------------- start main-content ----------->
  <div id="main-content">
    <h1><?php echo $title;?></h1>
    <div class="site-map">
      <div class="site-map1">
        <div class="site-map-left">
          <div class="home"><a href="."><h3>Home</h3></a></div>
          <ul>
            <li><a href="about"><img src="files/images/sitemap/sit-3.png">About us</a></li>
            <li><a href="partners"><img src="files/images/sitemap/sit-3.png">Our Partners</a></li>
            <li><a href="products"><img src="files/images/sitemap/sit-4.png">Our Products</a></li>
            <li><a href="sitemap"><img src="files/images/sitemap/sit-5.png">Site Map</a></li>
            <li><a href="for_buyers"><img src="files/images/sitemap/sit-6.png">Buyers</a></li>
            <li><a href="for_sellers"><img src="files/images/sitemap/sit-6.png">Sellers</a></li>
            <li><a href="faqs"><img src="files/images/sitemap/sit-5.png">FAQs</a></li>
            <li><a href="contact"><img src="files/images/sitemap/sit-7.png">Contact Us</a></li>
            <li><a href="privacy_policy"><img src="files/images/sitemap/sit-8.png">Privacy Terms</a></li>
            <li><a href="terms_and_conditions"><img src="files/images/sitemap/sit-8.png">Terms and Conditions</a></li>
            <?php if(count($page)){?>
            <li style="color:#000" class="cms"><img src="files/images/sitemap/sit-8.png">CMS
            	<ul>
                <?php foreach($page as $pag){?>
                	<li><a href="p/<?php echo $pag->slug;?>"><img src="files/images/sitemap/sit-8.png"><?php echo $pag->title;?></a></li>
                <?php }?>
                </ul>
            </li>
            <?php }?>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-------------- start main-content -----------> 
</article>