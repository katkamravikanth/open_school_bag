<link rel="stylesheet" href="files/css/users/layout.css" type="text/css" media="screen"/>
<script type="text/javascript">
$(function(){
	
	<?php if($this->uri->segment(1) == 'profile' || $this->uri->segment(1) == 'make_seller' || $this->uri->segment(1) == 'cpass' || $this->uri->segment(1) == 'my_notify'){?>
	$('#dashboard').removeClass('active');
	$('#general').addClass('active');
	$('.banner_content .wk-accordion-default #general').next(".content-wrapper").show();
	$(".banner_content .wk-accordion-default #general").next(".content-wrapper").height(120);
	<?php }?>
	<?php if($this->uri->segment(1) == 'publish_profile' || $this->uri->segment(1) == 'my_products' || $this->uri->segment(2) == 'add' || $this->uri->segment(1) == 'my_sales' || $this->uri->segment(1) == 'reports'){?>
	$('#dashboard').removeClass('active');
	$('#selling').addClass('active');
	$('.banner_content .wk-accordion-default #selling').next(".content-wrapper").show();
	$(".banner_content .wk-accordion-default #selling").next(".content-wrapper").height(145);
	<?php }?>
	<?php if($this->uri->segment(1) == 'my_orders'){?>
	$('#dashboard').removeClass('active');
	$('#buying').addClass('active');
	$('.banner_content .wk-accordion-default #buying').next(".content-wrapper").show();
	$(".banner_content .wk-accordion-default #buying").next(".content-wrapper").height(60);
	<?php }?>
});
</script>

<div class="nav">
  <div id="home_banner_content">
    <div class="banner_content"> 
      <!-- START: Modules Anywhere -->
      <div class="wk-accordion wk-accordion-default" data-widgetkit="accordion" style="display:block"> <a href="dashboard">
        <h5 class="toggler" id="dashboard">
          <div class="homeicon"></div>
          <b>Dashboard</b></h5>
        </a>
        
        <h5 class="toggler" id="general">
          <div class="homeicon2"></div>
          <b>General</b></h5>
        <div style="overflow: hidden;" class="content-wrapper general"> <a href="my_notify">
          <h1 <?php if($this->uri->segment(1) == 'my_notify'){?>style="font-weight:bold;"<?php }?>>Notifications</h1>
          </a><!-- <a href="my_wishlist">
          <h1 <?php if($this->uri->segment(1) == 'my_wishlist'){?>style="font-weight:bold;"<?php }?>>Wishlist</h1>
          </a>--> <?php if($this->session->userdata('level') == 1){?>
           <a href="make_seller">
          <h1 <?php if($this->uri->segment(1) == 'make_seller'){?>style="font-weight:bold;"<?php }?>>Make me a Seller</h1>
          </a><?php }?>
           <a href="profile">
          <h1 <?php if($this->uri->segment(1) == 'profile'){?>style="font-weight:bold;"<?php }?>>Update Profile</h1>
          </a> <a href="cpass">
          <h1 <?php if($this->uri->segment(1) == 'cpass'){?>style="font-weight:bold;"<?php }?>>Change Password</h1>
          </a></div>
        <?php if($this->session->userdata('level') == 2 || $this->session->userdata('level') == 3){?>
        <h5 class="toggler" id="selling">
          <div class="homeicon3"></div>
          <b>Selling</b></h5>
        <div style="overflow: hidden;" class="content-wrapper"> <a href="for_sellers">
          <h1 <?php if($this->uri->segment(1) == 'for_sellers'){?>style="font-weight:bold;"<?php }?>>Seller's Guide</h1>
          </a> <a href="publish_profile">
          <h1 <?php if($this->uri->segment(1) == 'publish_profile'){?>style="font-weight:bold;"<?php }?>>Publish Profile</h1>
          </a> <a href="my_products">
          <h1 <?php if($this->uri->segment(1) == 'my_products'){?>style="font-weight:bold;"<?php }?>>Manage Products</h1>
          </a> <a href="products/add">
          <h1 <?php if($this->uri->segment(2) == 'add'){?>style="font-weight:bold;"<?php }?>>Post a Product</h1>
          </a> <a href="my_sales">
          <h1 <?php if($this->uri->segment(1) == 'my_sales'){?>style="font-weight:bold;"<?php }?>>View Sales</h1>
          </a> <a href="reports">
          <h1 <?php if($this->uri->segment(1) == 'reports'){?>style="font-weight:bold;"<?php }?>>Reports</h1>
          </a> </div>
        <?php }?>
        <h5 class="toggler" id="buying">
          <div class="homeicon4"></div>
          <b>Buying</b></h5>
        <div style="overflow: hidden;" class="content-wrapper"> <a href="for_buyers">
          <h1 <?php if($this->uri->segment(1) == 'for_buyers'){?>style="font-weight:bold;"<?php }?>>Buyer’s Guide</h1>
          </a> <a href="my_orders">
          <h1 <?php if($this->uri->segment(1) == 'my_orders'){?>style="font-weight:bold;"<?php }?>>View Purchases</h1>
          </a> </div>
      </div>
    </div>
  </div>
  <div class="clear"></div>
</div>