<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title><?php echo $title;?>:: OpenSchoolbag</title>
<base href="<?php echo base_url()?>" />
<link rel="icon" href="files/images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="files/images/favicon.ico" type="image/x-icon" />
<link href="files/css/styles.css" rel="stylesheet" type="text/css">
<link href="files/css/user_nav.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="files/css/login.css" media="screen, projection">
<!--[if lte IE 7]><script src="files/css/ie/warning.js"></script><script>window.onload=function(){e("files/css/ie/")}</script><![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 8 ]>
<link rel="stylesheet" href="files/css/ie-8.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="files/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="files/js/jquery-migrate-1.2.1.min.js"></script>
<!--[if (gte IE 6)&(lte IE 8)]>
<script type="text/javascript" src="files/js/selectivizr.js"></script>
<![endif]-->
<script src="files/js/widgetkit.js" type="text/javascript"></script>
<script type="text/javascript" src="files/js/jquery_ravikanth_script.js"></script>
</head>

<body>
<!-- BEGIN SIGN IN FORM -->
<form id="sign-in-form" action="" method="post">
  <div class="close"></div>
  <!-- close button of the sign in form -->
  <ul id="form-section">
    <p><span class="register-numbering-text">Login</span></p>
    <li class="mess" style="color:#C30; display:none;"><img src="files/images/ajax-loader.gif" /></li>
    <li>
      <label> <span>Username</span>
        <input placeholder="Please enter your username" name="log_username" id="log_username" pattern="[a-zA-Z0-9]{6,}" type="text" title="It must contain the username that you have chosen at registration" required autofocus />
      </label>
    </li>
    <li>
      <label> <span>Password</span>
        <input placeholder="Please enter your password" name="log_password" id="log_password" pattern=".{6,}" type="password" title="It must contain the password that you have chosen at registration" required />
      </label>
    </li>
    <li> <span style="float:left; margin-top:20px; font-weight:bold; letter-spacing:1px;"><a href="javascript:;" id="register_form" class="profile_link" style="width:110px;">REGISTER</a><br />
      <br /><a href="javascript:;" id="forgot_form" class="profile_link" style="width:200px;">FORGOT PASSWORD?</a></span>
      <button name="sign-in-submit" type="button" id="sign-in-submit" style="float:right">
      <div class="bold">
        <div class="thin">Sign In</div>
      </div>
      </button>
    </li>
    <li>&nbsp;</li>
    <div style="clear: both;"></div>
  </ul>
</form>
<!-- END OF SIGN IN FORM -->

<!-- BEGIN REGISTER FORM -->
<form id="register-form" action="" method="post">
  <div class="close"></div>
  <!-- close button of the register form -->
  <ul id="form-section">
    <li class="reg_mess" style="color:#C30; display:none;"><img src="files/images/ajax-loader.gif" /></li>
    <li>
      <label> <span style="float:right;"><a href="javascript:;" id="login_form" class="profile_link" style="width:90px;"><strong>LOGIN</strong></a></span> </label>
    </li>
    <li>
      <label style="color:#000;">To start buying and selling products on OpenSchoolbag, please register with us by filling in this simple registration form.</label>
    </li>
    <li style="padding-top:10px;">
      <label style="color:#C00;">" * " are compulsory fields</label>
    </li>
    <p><span class="register-numbering">1</span><span class="register-numbering-text">Basic information</span></p>
    <li>
      <label class="left-column"> <span>First Name <a style="color:#c00; font-size:16px;">*</a></span>
        <input type="text" name="fname" id="fname" pattern="[a-zA-Z ]{2,}"  placeholder="Please enter your first name" required autofocus title="It must contain only letters and a length of minimum 2 characters!" />
      </label>
      <label class="right-column"> <span>Last Name <a style="color:#c00; font-size:16px;">*</a></span>
        <input type="text" name="lname" id="lname" pattern="[a-zA-Z ]{2,}" placeholder="Please enter your last name" title="It must contain only letters and a length of minimum 2 characters!" required />
      </label>
    </li>
    <div style="clear: both;"></div>
    <li>
      <label> <span>Email <a style="color:#c00; font-size:16px;">*</a></span>
        <input type="email" name="email" id="email" placeholder="Please enter a valid email address" title="It must contain a valid email address e.g. 'someone@provider.com' !" required />
      </label>
    </li>
    <li>
      <label> <span>Contact Number <a style="color:#c00; font-size:16px;">*</a></span>
        <input type="tel" name="telephone" id="telephone" pattern="(\+?\d[- .]*){7,13}" placeholder="Please enter your phone number" title="It must contain a valid phone number formed only by numerical values and a length between 7 and 13 characters !" required />
      </label>
    </li>
    <div style="clear: both;"></div>
    <p><span class="register-numbering">2</span><span class="register-numbering-text">Account credentials</span></p>
    <li class="user_mess" style="color:#C30; display:none;"><img src="files/images/ajax-loader.gif" /></li>
    <li>
      <label> <span>Username <a href="#" class="tooltip"><img src="files/images/icons/tool-tip.gif" alt="" /><b>You will use this username for future logins and all sales transactions. Your first and last name will not be reflected.</b></a> <a style="color:#c00; font-size:16px;">*</a></span>
        <input type="text" name="reg_username" id="reg_username" pattern="[a-zA-Z0-9]{6,}"  placeholder="Please enter your username" title="It must contain alphanumeric characters and a length of minimum 6 characters !" required />
      </label>
    </li>
    <li>
      <label class="left-column"> <span>Password <a style="color:#c00; font-size:16px;">*</a></span>
        <input type="password" name="reg_password" id="reg_password" pattern=".{6,}"  placeholder="Please enter your password" title="Minimum 6 characters!" required />
      </label>
      <label class="right-column"> <span>Confirm Password  <a style="color:#c00; font-size:16px;">*</a></span>
        <input type="password" name="re_password" id="re_password" pattern=".{6,}"  placeholder="Please enter your password" title="Minimum 6 characters!" required />
      </label>
    </li>
    <div style="clear: both;"></div>
    <div class="radio">
        <li style="padding-top:10px;"><input name="user_seller" type="radio" id="user_buyer" checked /> 
            <label class="radio_lab" for="user_buyer">Yes, I want to be a buyer</label></li>
            
        <li style="padding-top:10px;"><input name="user_seller" type="radio" id="user_seller" /> 
            <label class="radio_lab" for="user_seller">Yes, I want to be a buyer and seller</label></li>
	</div>
    <div style="clear: both;"></div>
    <div id="payment" style="display:none;">
      <p><span class="register-numbering">3</span><span class="register-numbering-text">Payment Option</span> <a href="#" class="tooltip"><img src="files/images/icons/tool-tip.gif" alt=""/><span>Please provide us with your bank account details. This shall be the account which we will credit your net sales proceeds into</span></a> </p>
      <li class="pay_mess" style="color:#C30; display:none;"><img src="files/images/ajax-loader.gif" /></li>
        <li>
          <label> <span>Bank Name <a style="color:#c00; font-size:16px;">*</a></span>
            <div class="styled-select">
                <select name="bank_name" id="bank_name" style="width:100%;">
                  <option value="">Select One</option>
                  <option>ANZ</option>
                  <option>Bank of China</option>
                  <option>Citibank</option>
                  <option>DBS Bank</option>
                  <option>HSBC</option>
                  <option>Maybank</option>
                  <option>OCBC</option>
                  <option>POSB</option>
                  <option>Standard Chartered Bank</option>
                  <option>UOB</option>
                  <option>Others</option>
                </select>
            </div>
          </label>
        </li>
        <li class="bank_other" style="display:none">
          <label> <span>Other Bank Name <a style="color:#c00; font-size:16px;">*</a></span>
            <input type="text" name="bank_other" id="bank_other" placeholder="Please enter bank name" />
          </label>
        </li>
        <li>
          <label class="left-column"> <span>Bank Code <a style="color:#c00; font-size:16px;">*</a></span>
            <input type="text" name="bank_code" id="bank_code" placeholder="Please enter bank code" />
          </label>
          <label class="right-column"> <span>Branch Code <a style="color:#c00; font-size:16px;">*</a></span>
            <input type="text" name="branch_code" id="branch_code" placeholder="Please enter branch code" />
          </label>
        </li>
        <li>
          <label class="left-column"> <span>Account Type <a href="#" class="tooltip"><img src="files/images/icons/tool-tip.gif" alt="" /><b>Savings Account, Current Account</b></a> <a style="color:#c00; font-size:16px;">*</a></span>
            <input type="text" name="account_type" id="account_type" placeholder="Please enter account type" />
          </label>
          <label class="right-column"> <span>Account Number <a style="color:#c00; font-size:16px;">*</a></span>
            <input type="text" name="account_number" id="account_number" placeholder="Please enter account number" />
          </label>
        </li>
    </div>
    <div style="clear: both;"></div>
    <div id="checkbox">
      <li>
        <input name="accept_tnc" type="checkbox" id="accept_tnc" />
        <span class="unchecked-state"></span> <span class="checked-state"></span> </li>
      <label class="rem-me">I have read, understood and agreed to the <a href="terms_and_conditions" style="text-decoration:underline;">Terms and Conditions</a>.</label>
    </div>
    <div style="clear: both;"></div>
    <li>
      <div align="right">
        <button name="submit" type="button" id="create-account-submit">
        <div class="bold">
          <div class="thin">Create Account</div>
        </div>
        </button>
      </div>
    </li>
  </ul>
</form>
<!-- END OF REGISTER FORM --> 
<!-- BEGIN FOTGOT PASSWORD -->
<form id="forgot-form" action="" method="post">
  <div class="close"></div>
  <!-- close button of the sign in form -->
  <ul id="form-section">
    <p><span class="register-numbering-text">Forgot Password?</span></p>
    <li class="forg_mess" style="color:#C30; display:none;"><img src="files/images/ajax-loader.gif" /></li>
    <li><label><span>Email <a style="color:#c00; font-size:16px;">*</a></span>
    <input type="email" name="forgot_email" id="forgot_email" tabindex="1" autofocus placeholder="Please enter a valid email address" title="It must contain a valid email address e.g. 'someone@provider.com' !" required /></label></li>
    <li> <span style="float:left; margin-top:20px; font-weight:bold; letter-spacing:1px;"><a href="javascript:;" id="login_form1" class="profile_link" style="width:90px;">LOGIN</a><br />
      <br />
      <a href="javascript:;" id="register_form" class="profile_link" style="width:110px;">REGISTER</a></span>
      <button name="forgot-submit" type="button" id="forgot-submit" style="float:right">
      <div class="bold">
        <div class="thin">Send Password</div>
      </div>
      </button>
    </li>
    <li>&nbsp;</li>
    <div style="clear: both;"></div>
  </ul>
</form>
<!-- END OF FOTGOT PASSWORD --> 
<!-- BEGIN SUCCESS MESSAGE -->
<?php
$mes = '';
if(!$this->session->userdata('logged_in')){
	if(isset($path)){
		if($path == 'activated'){?>
<script type="text/javascript">
		$(window).load(function() {
			$("#success").fadeIn(300);
			$("#sign-in-form").fadeOut("normal");
			$("#register-form").fadeOut("normal");
			$("#background-on-popup").css("opacity", "0.7");
			$("#background-on-popup").fadeIn(300);
		});
		</script>
<?php
			$mes = '<input type="hidden" name="act" id="act" value="'.$path.'"><p><span class="register-numbering">&radic;</span><span class="register-numbering-text">Success</span></p><li><label><span>Your account has been activated, please login to continue</span></label></li>';
		}else if($path == 'not-activated'){?>
<script type="text/javascript">
		$(window).load(function() {
			$("#success").fadeIn(300);
			$("#sign-in-form").fadeOut("normal");
			$("#register-form").fadeOut("normal");
			$("#background-on-popup").css("opacity", "0.7");
			$("#background-on-popup").fadeIn(300);
		});
		</script>
<?php
			$mes = '<input type="hidden" name="act" id="act" value="'.$path.'">
				<li class="act_mess" style="color:#C30"></li>
				<li><label><span>Invalid activation link</span></label></li>
				<div style="clear: both;"></div>
				<p><span class="register-numbering">1</span><span class="register-numbering-text">Re-activate</span></p>
				<li>
					<label>
						<span>Email <a style="color:#c00; font-size:16px;">*</a></span>
						<input type="text" name="activation_email" id="activation_email" tabindex="1" placeholder="Please enter a valid email address" title="It must contain a valid email address e.g. \'someone@provider.com\' !" required autofocus />
					</label>
				</li>
				<div style="clear: both;"></div>
				<li><button name="reactivate-account" type="button" id="reactivate-account">Re-activate</button></li>';
		}else if($path == 'expired'){?>
<script type="text/javascript">
		$(window).load(function() {
			$("#success").fadeIn(300);
			$("#sign-in-form").fadeOut("normal");
			$("#register-form").fadeOut("normal");
			$("#background-on-popup").css("opacity", "0.7");
			$("#background-on-popup").fadeIn(300);
		});
		</script>
<?php
			$mes = '<input type="hidden" name="act" id="act" value="'.$path.'">
				<li class="act_mess" style="color:#C30"></li>
				<li><label><span>Activation link has been expired</span></label></li>
				<div style="clear: both;"></div>
				<p><span class="register-numbering">1</span><span class="register-numbering-text">Re-activate</span></p>
				<li>
					<label>
						<span>Email <a style="color:#c00; font-size:16px;">*</a></span>
						<input type="text" name="activation_email" id="activation_email" tabindex="1" placeholder="Please enter a valid email address" title="It must contain a valid email address e.g. \'someone@provider.com\' !" required autofocus />
					</label>
				</li>
				<div style="clear: both;"></div>
				<li><button name="reactivate-account" type="button" id="reactivate-account">Re-activate</button></li>';
		}else{
			$mes = '<input type="hidden" name="act" id="act" value="">';
		}
	}
}
?>
<div id="success">
  <div class="close"></div>
  <!-- close button of the register form -->
  <ul id="form-section">
    <?php if(isset($mes)){echo $mes;}?>
  </ul>
</div>
<!-- END OF SUCCESS MESSAGE -->

<div id="background-on-popup"></div>
<div class="container">
<header> <a href="<?php echo base_url()?>" id="logo" title="OpenSchoolbag">OpenSchoolbag</a>
  <div id="quick_links">
    <div class="welcome">Welcome
      <?php if($this->session->userdata('logged_in')){echo '<span style="font-weight:bold; color:#0b9444">'.$this->session->userdata('username').'</span>';}else{echo 'Guest';}?>
      you can
      <div class="quick">
        <?php if($this->session->userdata('logged_in')){?>
        <a href="dashboard" class="dashboard icon" title="Dashboard">Dashboard</a> <a href="profile" class="profile icon" title="Profile">Profile</a> <a href="logout" class="logout icon" title="Logout">Logout</a>
        <?php }?>
      </div>
    </div>
    <div class="login_cart">
      <?php if(!$this->session->userdata('logged_in')){
            echo '<a href="javascript:;" id="sign-in-tab" class="login button_green" title="Login"><div class="o1"><div>LOGIN</div></div></a> <span>OR</span> <a href="javascript:;" id="register-tab" class="register button_green" title="Register"><div class="o1"><div>REGISTER</div></div></a>';
        }?>
      <div class="cart button_green" title="Cart"><?php echo $this->cart->total_items();?> item(s) - S$ <?php echo number_format($this->cart->total(), 2);?></div>
    </div>
  </div>
</header>
<nav>
  <ul>
    <li><a href=".">Home</a></li>
    <li><a href="about">About Us</a></li>
    <li><a href="partners">Our Partners</a></li>
    <li><a href="products">Our Products</a></li>
    <li><a href="for_buyers">For Buyers</a></li>
    <li><a href="for_sellers">For Sellers</a></li>
  </ul>
</nav>