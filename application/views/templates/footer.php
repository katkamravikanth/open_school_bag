<footer>
  <div class="col1">
    <div class="call1"><span>&nbsp;</span>8264 4750&nbsp;</div>
    <!--<div class="call2"><span>&nbsp;</span>3697 6524&nbsp; | &nbsp;9876 2658</div>-->
    <div class="mail"><a href="mailto:contact@openschoolbag.com.sg"><span>&nbsp;</span>Click here to email us</a></div>
  </div>
  <div class="col2">
    <h4>INFORMATION</h4>
    <ul>
      <li><a href="about">About us</a></li>
      <li><a href="privacy_policy">Privacy Terms</a></li>
      <li><a href="terms_and_conditions">Terms and Conditions</a></li>
      <li><a href="sitemap">Site Map</a></li>
    </ul>
  </div>
  <div class="col3">
    <h4>CUSTOMER SERVICE</h4>
    <ul>
      <li><a href="contact">Contact us</a></li>
      <li><a href="faqs">FAQs</a></li>
    </ul>
  </div>
  <address>
  <!--[if IE 8 ]>
  <div style="color:#f30">The website is best viewed in Chrome, Safari, Firefox and IE 9+</div>
  <![endif]-->
  <div class="copyright"> Designed by <a href="http://www.dotspiders.com" target="_blank">Dot Spiders</a><br />
    Copyright 2013. All rights reserved. OpenSchoolbag </div>
  <div class="social"> <a href="http://www.facebook.com/OpenSchoolbagSG" class="fb" target="_blank">Facebook</a> <a href="http://twitter.com" class="tw" target="_blank">Twitter</a> <img src="files/images/icons/cards.gif" alt="card types" /> </div>
  </address>
</footer>
<!-- end .container -->
</div>
</body>
</html>