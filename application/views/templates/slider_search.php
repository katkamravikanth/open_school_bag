<link rel="stylesheet" href="files/css/nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="files/css/default/default.css" type="text/css" media="screen" />
<script type="text/javascript" src="files/js/jquery.nivo.slider.js"></script>

<section>
  <div class="slider_container">
    <div class="slider-wrapper theme-default">
      <div id="slider" class="nivoSlider">
        <?php foreach($banner as $bans){?>
        <img src="files/<?php echo $bans->photo;?>" data-thumb="files/<?php echo $bans->photo;?>" alt="" />
        <?php }?>
      </div>
    </div>
    <div class="search_box">
      <div class="title">
        <div class="l_tit">
          <div class="ll_tit"> Start your Search! </div>
        </div>
      </div>
      <form action="search" method="post" name="search_box" id="search_box">
        <!-- START: Modules Anywhere -->
        <div class="wk-accordion wk-accordion-default" data-widgetkit="accordion">
          <h3 class="toggler active">level</h3>
          <div style="overflow: hidden;" class="content-wrapper">
            <div class="content">
              <div class="styled-select">
                <select name="s_level" id="s_level" style="width:100%;">
                  <option value="">Select One</option>
                  <?php foreach($levels as $lev){?>
                  <option ><?php echo $lev->level;?></option>
                  <?php }?>
                </select>
              </div>
            </div>
          </div>
          <h3 class="toggler">Subject</h3>
          <div style="overflow: hidden;" class="content-wrapper">
            <div class="content">
              <div class="styled-select" id="search_subject">
                <select name="s_subject" id="s_subject" style="width:100%;">
                  <option value="">Select One</option>
                </select>
              </div>
            </div>
          </div>
          <h3 class="toggler">Resource type</h3>
          <div style="overflow: hidden;" class="content-wrapper">
            <div class="content">
              <div class="styled-select">
                <select name="s_resource_type" id="s_resource_type" style="width:100%;">
                  <option value="">Select One</option>
                  <?php foreach($resources as $source){?>
                  <option><?php echo $source->resources;?></option>
                  <?php }?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="search">
          <input type="text" name="s_search_text" id="s_search_text" placeholder="Product Name/Keyword" />
          <input type="submit" name="search" id="search" value="" />
        </div>
      </form>
    </div>
  </div>
</section>