<link href="files/css/inner-pages.css" rel="stylesheet" type="text/css">
<!--[if lt IE 9]>
<style>
h4 {
	font-size:14px;
}
.Registration .left h4 {
	height:30px;
]
</style>
<![endif]-->
<style type="text/css">
.profile_link {
	border:none;
	color:#fff !important;
	font-weight:bold;
	font-size:14px;
	cursor:pointer;
	background: #0b9444;
	border-radius: 3px;
	width: 220px;
	height: 39px;
	display:inline-block;
	line-height:39px;
	text-align:center;
	float:right;
	margin-bottom:15px;
}
.profile_link:hover {
	text-decoration:none;
	background: #ef4036;
}
</style>
<article class="content">
  <h1><a href="<?php echo base_url();?>">Home</a> &raquo; <?php echo $title;?></h1>
  <!-------------- start main-content ----------->
  <div id="main-content"> 
  	<a href="<?php echo base_url();?>Seller_Starter_Kit.pdf" class="profile_link" target="_blank">Download Seller Starter Kit</a>
  	<br>
    <br>
    <!--------------- start saller-page ----------------->
    <img src="files/<?php echo $images[0]->photo;?>" title="for seller" />
    <!--------------- end saller-page -----------------> 
    
  </div>
  <!-------------- start main-content -----------> 
</article>