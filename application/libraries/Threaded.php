<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class threaded
{

    public $parents  = array();
    public $children = array();

    /**
     * @param array $comments
     */
    public function arrange($comments){
        foreach ($comments as $comment){
            if ($comment['parent_id'] === NULL){
                $this->parents[$comment['qna_id']][] = $comment;
            }else{
                $this->children[$comment['parent_id']][] = $comment;
            }
        }
        $this->print_comments();
    }

    private function tabulate($depth){
        for ($depth; $depth > 0; $depth--){
            echo "";
        }
    }

    /**
     * @param array $comment
     * @param int $depth
     */
    private function format_comment($comment, $depth){

        $this->tabulate($depth+1);

        echo "<div class='reviews-main1' style='width: 100%; padding-bottom:5px;margin: 10px 0 10px 14px; position:relative;'>";
        echo "<h4 style='margin:0; padding:3px'><strong>".$comment['user_username']."</strong> :</h4>
			 <p style='padding-left:10px;'>".$comment['qna_body']."</p>";
		if($comment['parent_id'] == ''){
			echo '<input type="hidden" class="uid" name="'.$comment['qna_id'].'" value="'.$comment['uid'].'" />';
		}else{
			echo '<input type="hidden" class="uid" name="'.$comment['qna_id'].'" value="0" />';
		}
			 
		echo '<div style="position:absolute; top:3px; right:20px;" class="but_'.$comment['qna_id'].' buttns">
			 <img src="files/images/icons/reply.png" class="reply" name="'.$comment['qna_id'].'" alt="Reply" style="cursor:pointer;" />
			 <img src="files/images/icons/delete.png" width="16" class="delete" name="'.$comment['qna_id'].'" alt="Delete" style="cursor:pointer;" /></div>';
			 
		echo '<p id="reply_'.$comment['qna_id'].'" style="display:none; text-align:left !important;">
				<span id="msg_qna_'.$comment['qna_id'].'" style="display:none; text-align:left !important;"></span>
				<input type="hidden" name="reply_u'.$comment['qna_id'].'" id="reply_u'.$comment['qna_id'].'" value="'.$comment['uid'].'" />
				<textarea name="question_'.$comment['qna_id'].'" id="question_'.$comment['qna_id'].'" rows="5" style="width:90%;"></textarea><br />
                <input type="button" class="submit" name="reply_submit" alt="'.$comment['qna_id'].'" value="Reply" /><br /><br /></p>';
        echo "</div>";
    }

    /**
     * @param array $comment
     * @param int $depth
     */
    private function print_parent($comment, $depth = 0){
        $this->tabulate($depth);
        echo "<ul>";
		//print_r($comment);
        foreach ($comment as $c){
            $this->format_comment($c, $depth);
            if (isset($this->children[$c['qna_id']])){
                $this->print_parent($this->children[$c['qna_id']], $depth + 1);
            }
        }
        $this->tabulate($depth);
        echo "</ul>";
    }

    private function print_comments(){
		echo '<table class="pagination" rel="10" width="95%" align="center"><tbody style="background:#fff;">';
        foreach ($this->parents as $c){
			echo '<tr><td>';
            $this->print_parent($c);
			echo '</td></tr>';
        }
		echo '</tbody>
			<tfoot>
				<tr>
                  <td>&nbsp;</td>
                </tr>
            </tfoot>
        </table>';
    }
}
?>