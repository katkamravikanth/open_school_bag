<?php
class MY_Model extends CI_Model {
	
	protected $_table_name = '';
	protected $_primary_key = 'id';
	protected $_user_key = 'user_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = '';
	protected $_riles = array();
	protected $_timestamps = FALSE;
	
	function __construct(){
		parent::__construct();
	}
	
	public function get($id = NULL, $single = FALSE){
		
		if($id != NULL){
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->where($this->_primary_key, $id);
			$method = 'row';
		}elseif($single == TRUE){
			$method = 'row';
		}else{
			$method = 'result';
		}
		if(!count($this->db->ar_orderby)){
			$this->db->order_by($this->_order_by, 'desc');
		}
		return $this->db->get($this->_table_name)->$method();
	}
	
	public function get_by($where, $single = FALSE){
		$this->db->where($where);
		return $this->get(NULL, $single);
	}
	
	public function get_by_limit($num, $start, $id = FALSE, $data = FALSE){
		
		if($id != NULL){
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->where($this->_user_key, $id);
		}
		if($data != NULL){
			$this->db->where($data);
		}
		$this->db->limit($num, $start);
		if(!count($this->db->ar_orderby)){
			$this->db->order_by($this->_order_by, 'desc');
		}
		return $this->db->get($this->_table_name)->result();
	}
	
	public function total_no($id=NULL){
		if($id != NULL){
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->where($this->_user_key, $id);
			return $this->db->count_all_results($this->_table_name);
		}else{
			return $this->db->count_all($this->_table_name);
		}
	}
	
	//save or update records
	public function save($data, $rid = NULL){
		$id = '';
		// Set timestamps
		if($this->_timestamps == TRUE){
			$now = date('Y-m-d H:i:s');
			$rid || $data['created'] = $now;
			$data['modified'] = $now;
		}
		// Insert
		if($rid === NULL){
			!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
			$this->db->set($data);
			$this->db->insert($this->_table_name);
			$id = $this->db->insert_id();
		}
		// Update
		else{
			$filter = $this->_primary_filter;
			$rid = $filter($rid);
			$this->db->set($data);
			$this->db->where($this->_primary_key, $rid);
			$up = $this->db->update($this->_table_name);
			if($up){$id=$rid;}
		}
		return $id;
	}
	
	public function delete($id, $data){
		$filter = $this->_primary_filter;
		$id = $filter($id);
		
		if(!$id){
			return FALSE;
		}
		$this->db->set($data);
		$this->db->where($this->_primary_key, $id);
		$this->db->limit(1);
		$this->db->update($this->_table_name);
	}
	
	public function email($email, $subject, $message){
		$this->load->library('email');
			
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$config['protocol'] = 'sendmail';
		
		$this->email->initialize($config);
		$this->email->from('admin@openschoolbag.com.sg', 'Admin - OpenSchoolbag');
		$this->email->to($email); 
		
		$this->email->subject($subject);
		$this->email->message('<html><body>'.$message.'</body></html>');	
		
		$this->email->send();
		//print $this->email->print_debugger();
	}
}