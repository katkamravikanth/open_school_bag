<?php
class Review_m extends MY_Model{
	protected $_table_name = 'reviews';
	protected $_primary_key = 'review_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'created';
	protected $_riles = array();
	protected $_timestamps = TRUE;
	
	public function get_reviews($unique){
		$this->db->select('users.user_username, '.$this->_table_name.'.review_id, '.$this->_table_name.'.review, '.$this->_table_name.'.rate');
		$this->db->from($this->_table_name);
		$this->db->join('users', 'users.user_id = '.$this->_table_name.'.user_id');
		$this->db->where($this->_table_name.'.product_unique', $unique);
		
        return $this->db->get()->result();
	}
	
	public function get_user_reviews($puid){
		$this->db->select('users.user_username, '.$this->_table_name.'.review_id, '.$this->_table_name.'.review, '.$this->_table_name.'.rate');
		$this->db->from($this->_table_name);
		$this->db->join('users', 'users.user_id = '.$this->_table_name.'.user_id');
		//$this->db->join('user_profile', 'user_profile.user_id = '.$this->_table_name.'.user_id');
		$this->db->where($this->_table_name.'.to_user_id', $puid);
		$this->db->limit(9);
		
        return $this->db->get()->result();
	}
	
	public function total_rws($unique){
		$this->db->where('product_unique', $unique);
		return $this->db->count_all_results($this->_table_name);
	}
	
	public function get_seller_by($puid){
		$this->db->where('user_id', $puid);
		
		$to = 0;
		$review = 0;
		$query = $this->db->get('products')->result();
		foreach($query as $que){
			$this->db->where('product_unique', $que->product_unique);
			$reviews = $this->db->get('reviews')->result();
			foreach($reviews as $review){
				$to=$to+$review->rate;
			}
			$review = $review+count($reviews);
		}
        return round($to/$review);
	}
	
	public function delete_review($review_id){
		$filter = $this->_primary_filter;
		$id = $filter($review_id);
		$this->db->where($this->_primary_key, $id);
		$this->db->limit(1);
		$dele = $this->db->delete($this->_table_name);
		
		if($dele){
			return TRUE;
		}else{ return FALSE;}
	}
	
	function delete_user($id){
		$this->db->where('user_id', $id);
		$this->db->or_where('to_user_id', $id);
		$this->db->delete($this->_table_name);
	}
}