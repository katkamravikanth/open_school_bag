<?php
class Shipping_m extends MY_Model{
	protected $_table_name = 'order_shipping';
	protected $_primary_key = 'shipping_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'created';
	protected $_riles = array();
	protected $_timestamps = TRUE;
	
}