<?php
class Favourite_m extends MY_Model{
	protected $_table_name = 'favourite';
	protected $_primary_key = 'fav_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'created';
	protected $_riles = array();
	protected $_timestamps = TRUE;
	
/*	public function by_limit($user_id, $num, $start){
		$this->db->select('*');
		$this->db->from($this->_table_name);
		$this->db->join('products', 'products.product_unique = '.$this->_table_name.'.product_unique');
		$this->db->where($this->_table_name.'.user_id', $user_id);
		$this->db->limit($num, $start);
		
        return $this->db->get()->result();
	}
*/	
	public function by_limit($user_id, $num, $start){
		$this->db->join('products', 'products.product_unique = '.$this->_table_name.'.product_unique');
		$this->db->where($this->_table_name.'.user_id', $user_id);
		$this->db->limit($num, $start);
		if(!count($this->db->ar_orderby)){
			$this->db->order_by($this->_table_name.'.'.$this->_order_by);
		}
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		foreach($query as $pro){
			$arr[$i]['pro'] = $pro;
				$this->db->select('total');
				$this->db->order_by('modified', 'Desc');
				$this->db->where('product_unique', $pro->product_unique);
				$this->db->limit(1, 0);
				$rate = $this->db->get('ratings')->result();
				if(count($rate)){
					$rat = $rate[0]->total;
				}else{
					$rat = 0;
				}
			$arr[$i]['avg'] = $rat;
			$i++;
		}
		//echo '<pre>';print_r($arr);echo '</pre>';
		return $arr;
	}
}