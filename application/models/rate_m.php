<?php
class Rate_m extends MY_Model{
	protected $_table_name = 'ratings';
	protected $_primary_key = 'rate_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'modified';
	protected $_riles = array();
	protected $_timestamps = TRUE;
	
	public function total_rate($unique){
		$this->db->select_sum('rate_value');
		$this->db->where('product_unique', $unique);
		return $this->db->get($this->_table_name)->result();
	}
	
	public function rate_count($unique){
		$this->db->where('product_unique', $unique);
		return $this->db->count_all($this->_table_name);
	}
}