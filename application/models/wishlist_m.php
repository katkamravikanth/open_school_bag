<?php
class Wishlist_m extends MY_Model{
	protected $_table_name = 'wishlist';
	protected $_primary_key = 'wish_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'created';
	protected $_riles = array();
	protected $_timestamps = TRUE;
	
	public function by_limit($user_id, $num, $start){
		$this->db->join('products', 'products.product_unique = '.$this->_table_name.'.product_unique');
		$this->db->where($this->_table_name.'.user_id', $user_id);
		$this->db->limit($num, $start);
		if(!count($this->db->ar_orderby)){
			$this->db->order_by($this->_table_name.'.'.$this->_order_by);
		}
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		foreach($query as $pro){
			$arr[$i]['pro'] = $pro;
				$this->db->select('total');
				$this->db->order_by('modified', 'Desc');
				$this->db->where('product_unique', $pro->product_unique);
				$this->db->limit(1, 0);
				$rate = $this->db->get('ratings')->result();
				if(count($rate)){
					$rat = $rate[0]->total;
				}else{
					$rat = 0;
				}
			$arr[$i]['avg'] = $rat;
			
				$this->db->select('promo_percent');
				$this->db->order_by('created', 'Desc');
				$this->db->where('promo_type', 'On Product Amount');
				$this->db->where('product_title', $pro->product_name);
				$this->db->or_where('promo_category', $pro->product_level);
				$this->db->or_where('promo_category', $pro->product_subject);
				$this->db->or_where('promo_category', $pro->product_resource);
				$this->db->limit(1, 0);
				$disc = $this->db->get('promos')->result();
				if(count($disc)){
					$disd = $disc[0]->promo_percent;
					$dis = substr($disc[0]->promo_percent, -1);
					if($dis == '%'){$dis = $pro->product_price*substr($disc[0]->promo_percent, 0, -1)/100;}//else
					if($disd[0] == '$'){$dis = substr($disc[0]->promo_percent, 1);}else{$dis = 0;}
				}else{
					$dis = 0;
				}
			$arr[$i]['disc'] = $dis;
			$i++;
		}
		//echo '<pre>';print_r($arr);echo '</pre>';
		return $arr;
	}
	
	public function del_wish($unique){
		$this->db->where('product_unique', $unique);
		$this->db->limit(1);
		$dele = $this->db->delete($this->_table_name);
		
		if($dele || $del){
			return TRUE;
		}else{ return FALSE;}
	}
}