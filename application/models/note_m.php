<?php
class Note_m extends MY_Model{
	protected $_table_name = 'notifications';
	protected $_primary_key = 'note_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'created';
	protected $_riles = array();
	protected $_timestamps = TRUE;
	
	public function tot_no_note($id=NULL){
		if($id != NULL){
			$this->db->where('to_user_id', $id);
		}
		return $this->db->count_all_results($this->_table_name);
	}
	
	public function tot_no($id=NULL){
		if($id != NULL){
			$this->db->where('to_user_id', $id);
		}
		$this->db->where('note_status', 'Unread');
		return $this->db->count_all_results($this->_table_name);
	}
	
	public function make_read($id){
		
		// Set timestamps
		if($this->_timestamps == TRUE){
			$now = date('Y-m-d H:i:s');
			$id || $data['created'] = $now;
			$data['modified'] = $now;
		}
		$filter = $this->_primary_filter;
		$id = $filter($id);
		$this->db->set(array('note_status'=>'Read'));
		$this->db->where('to_user_id', $id);
		$this->db->update($this->_table_name);
		
		return $id;
	}
	
	function delete_user($id){
		$this->db->where('user_id', $id);
		$this->db->or_where('to_user_id', $id);
		$this->db->delete($this->_table_name);
	}
}