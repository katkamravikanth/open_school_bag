<?php
class Active_m extends MY_Model{
	protected $_table_name = 'user_activation';
	protected $_primary_key = 'active_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'user_id';
	protected $_riles = array();
	protected $_timestamps = TRUE;
}