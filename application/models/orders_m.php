<?php
class Orders_m extends MY_Model{
	protected $_table_name = 'orders_items';
	protected $_primary_key = 'order_item_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'created';
	protected $_riles = array();
	protected $_timestamps = TRUE;
	
	
	public function updated($data, $order_no){
		$this->db->set($data);
		$this->db->where('order_no', $order_no);
		$this->db->update($this->_table_name);
	}
	
	//save records
	public function save_order($data){
		$id = '';
		$now = date('Y-m-d H:i:s');
		$data['created'] = $now;
		$data['modified'] = $now;
		// Insert
		$this->db->set($data);
		$this->db->insert('orders');
		$id = $this->db->insert_id();
		
		return $id;
	}
	
	public function order_by($order, $first, $second){
		$this->db->select('*');
		$this->db->from($this->_table_name);
		$this->db->join('users', 'users.user_username = '.$this->_table_name.'.user_id');
		
		$this->db->where($this->_table_name.'.created >=', $first);
		$this->db->where($this->_table_name.'.created <=', $second);
		
		if(!count($this->db->ar_orderby)){
			$this->db->order_by($this->_table_name.'.'.$order);
		}
		return $this->db->get()->result();
	}
	
	// salses details
	public function total_sales($id, $first=false, $second=false){
		$this->db->select('SUM(order_pprice - order_discount) as total');
		//$this->db->select_sum('order_pprice', 'total');
		
		if($id != false){
			$this->db->where('puser_id', $id);
			$this->db->where('user_id !=', $id);
		}
		if($first != false && $second != false){
			$this->db->where('created >=', $first);
			$this->db->where('created <=', $second);
		}
		return $this->db->get($this->_table_name)->result();
	}
	
	public function sales_total_no($id=false, $first=false, $second=false, $unique=false, $pname=false){
		
		if($id != false && $id != ''){
			$this->db->where('puser_id', $id);
			$this->db->where('user_id !=', $id);
		}
		if($unique != false){
			$this->db->where('order_punique', $unique);
		}
		if($first != false && $second != false){
			$this->db->where('created >=', $first);
			$this->db->where('created <=', $second);
		}
		if($pname != false){
			$this->db->like('order_pname', $pname);
		}
		
		return $this->db->count_all_results($this->_table_name);
	}
	
	public function sales_by_limit($num=false, $start=false, $id=false, $first=false, $second=false, $by=false, $pname=false){
		
		if($num != false && $start != false){
			$this->db->limit($num, $start);
		}
		if($id != false){
			$this->db->where('puser_id', $id);
			$this->db->where('user_id !=', $id);
		}
		if($first != false && $second != false){
			$this->db->where('created >=', $first);
			$this->db->where('created <=', $second);
		}
		if($pname != false){
			$this->db->like('order_pname', $pname);
		}
		if($by != false && $by != ''){
			$this->db->order_by($by);
		}else{
			if(!count($this->db->ar_orderby)){
				$this->db->order_by($this->_order_by);
			}
		}
		return $this->db->get($this->_table_name)->result();
	}
	
	public function order_view($order_no){
		$users = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		$this->db->select($this->_table_name.".*, orders.*, users.user_fname, users.user_email, users.user_phone");
		$this->db->join('orders', 'orders.order_no = '.$this->_table_name.'.order_no');
		$this->db->join('users', 'users.user_username = '.$this->_table_name.'.user_id');
		$this->db->join('products', 'products.product_unique = '.$this->_table_name.'.order_punique');
		$this->db->where($this->_table_name.'.order_no', $order_no);
		$this->db->where('products.user_id', $users->user_id);
		
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		if(count($query)){
			foreach($query as $row){
				$arr[$i]['ord'] = $row;
				
					$this->db->where('order_no', $order_no);
					$usr = $this->db->get('order_shipping')->result();
					if(count($usr)){
						$arr[$i]['ship'] = $usr;
					}else{
						$arr[$i]['ship'] = array();
					}
				$i++;
			}
		}
		return $arr;
	}
	
	public function sales_by_auto($first=false, $second=false, $user=false){
		
		$this->db->select('COUNT(*) as TotalSales, SUM(order_pprice) as TotalAmount, order_pname, order_ptype, user_id, puser_id, order_pprice, order_discount, created, order_punique');
		$this->db->distinct();
		$this->db->group_by('order_pname');
		
		if($first != false && $second != false){
			$this->db->where('created >=', $first);
			$this->db->where('created <=', $second);
		}
		
		if($user != false){
			$this->db->where('puser_id', $user);
		}
		
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		foreach($query as $row){
			$arr[$i]['sale'] = $row;
				$this->db->select('order_discount, order_promo');
				$this->db->where('order_pname', $row->order_pname);
				$disc = $this->db->get($this->_table_name)->result();
				$disco = 0;
				foreach($disc as $dis){if($dis->order_promo == ''){$disco = $disco + $dis->order_discount;}}
			$arr[$i]['disc'] = $disco;
			$i++;
		}
		return $arr;
	}
	
	public function sales_auto_total($order_no){
		
		$this->db->select('SUM(final_amount) AS revenue');
		
		$this->db->where('order_no', $order_no);
		return $this->db->get('orders')->result();
	}
	
	// order details
	public function total_orders($id, $first=false, $second=false){
		$dates = '';
		$tags = '';
		
		if($first != false && $second != false){
			$dates = 'AND o.created BETWEEN "'.$first.'" AND "'.$second.'"';
		}
		if($id != false){
			$tags = 'AND oi.user_id = "'.$id.'"';
		}
		
		$query = $this->db->query('SELECT * FROM '.$this->_table_name.' oi, orders o WHERE oi.order_no = o.order_no '.$tags.' '.$dates.' GROUP BY oi.order_no');
		
		return $query->result();
		/*
		$this->db->select('SUM(orders.final_amount + orders.trans_fee) as total');
		$this->db->distinct($this->_table_name.'.order_no');
		$this->db->group_by($this->_table_name.'.order_no');
		$this->db->join('products', 'products.product_unique = '.$this->_table_name.'.order_punique');
		$this->db->join('orders', 'orders.order_no = '.$this->_table_name.'.order_no');
		
		if($id != false){
			$this->db->where($this->_table_name.'.user_id', $id);
		}
		if($first != false && $second != false){
			$this->db->where($this->_table_name.'.created >=', $first);
			$this->db->where($this->_table_name.'.created <=', $second);
		}
		return $this->db->get($this->_table_name)->result();*/
	}
	public function search_total_no($id=false, $first=false, $second=false){
		$this->db->where('order_status', 'Done');
		
		if($id != false){
			$this->db->where($this->_user_key, $id);
		}
		if($first != false && $second != false){
			$this->db->where('created >=', $first);
			$this->db->where('created <=', $second);
		}
		return $this->db->count_all_results($this->_table_name);
	}
	
	public function ord_by_limit($num=false, $start=false, $id=false, $first=false, $second=false, $by=false, $pname=false){
		
		if($id != false && $id != ''){
			$this->db->where($this->_table_name.'.user_id', $id);
		}
		
		if($first != false && $second != false){
			$this->db->where($this->_table_name.'.created >=', $first);
			$this->db->where($this->_table_name.'.created <=', $second);
		}
		
		if($num != false && $start != false){
			$this->db->limit($num, $start);
		}
		
		if($by != false && $by != ''){
			$this->db->order_by($by);
		}else{
			if(!count($this->db->ar_orderby)){
				$this->db->order_by($this->_order_by);
			}
		}
		if($pname != false && $pname != ''){
			$this->db->where('order_pname >=', $first);
		}
		
		return $this->db->get($this->_table_name)->result();
	}
	
	public function down_by_limit($num=false, $start=false, $id=false, $first=false, $second=false, $by=false){
		
		$this->db->select('*');
		$this->db->distinct($this->_table_name.'.order_pname');
		$this->db->group_by($this->_table_name.'.order_pname');
		$this->db->join('products', 'products.product_unique = '.$this->_table_name.'.order_punique');
		$this->db->where($this->_table_name.'.order_status', 'Done');
		$this->db->where('products.down_count !=', 0);
		
		if($id != false){
			$this->db->where($this->_table_name.'.puser_id', $id);
		}
		if($first != false && $second != false){
			$this->db->where($this->_table_name.'.created >=', $first);
			$this->db->where($this->_table_name.'.created <=', $second);
		}
		if($num != false && $start != false){
			$this->db->limit($num, $start);
		}
		if($by != false && $by != ''){
			$this->db->order_by($by);
		}else{
			if(!count($this->db->ar_orderby)){
				$this->db->order_by('products.down_count', 'desc');
			}
		}
		return $this->db->get($this->_table_name)->result();
	}
	
	public function ship($data){
		$now = date('Y-m-d H:i:s');
		$data['created'] = $now;
		$data['modified'] = $now;
		// Insert
		!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
		$this->db->set($data);
		$this->db->insert('order_shipping');
		$id = $this->db->insert_id();
		
		return $id;
	}
	
	function delete_user($id){
		$this->db->where('user_id', $id);
		$this->db->delete($this->_table_name);
	}
}