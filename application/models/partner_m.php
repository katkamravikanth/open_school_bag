<?php
class Partner_m extends MY_Model{
	protected $_table_name = 'partners';
	protected $_primary_key = 'partner_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'created';
	protected $_riles = array();
	protected $_timestamps = TRUE;
	
	public function get_by_lt($single){
		$this->db->join('users', 'users.user_id = '.$this->_table_name.'.user_id');
		$this->db->limit($single);
		return $this->db->get($this->_table_name)->result();
		
	}
	
	public function update($data, $id){
		
		// Set timestamps
		if($this->_timestamps == TRUE){
			$now = date('Y-m-d H:i:s');
			$id || $data['created'] = $now;
			$data['modified'] = $now;
		}
		$this->db->set($data);
		$this->db->where('user_id', $id);
		$this->db->update($this->_table_name);
		return $id;
	}
	
	function delete_user($id){
		$prods = $this->db->where(array('user_id'=>$id))->limit(1)->get($this->_table_name)->result();
		if(count($prods)){
			foreach($prods as $prod){
				unlink('files/'.$prod->partner_logo);
			}
		}
		$this->db->where('user_id', $id);
		$this->db->delete($this->_table_name);
	}
}