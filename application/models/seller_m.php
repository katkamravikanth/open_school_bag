<?php
class Seller_m extends MY_Model{
	protected $_table_name = 'orders';
	protected $_primary_key = 'order_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'created';
	protected $_riles = array();
	protected $_timestamps = TRUE;
	
	
	// salses details
	public function total_sales($sdata){
		$this->db->where('order_status', 'Done');
		$this->db->where($sdata);
		//return $this->db->count_all_results($this->_table_name);
		return $this->db->get($this->_table_name)->result();
	}
}