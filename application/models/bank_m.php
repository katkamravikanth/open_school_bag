<?php
class Bank_m extends MY_Model{
	protected $_table_name = 'bank';
	protected $_primary_key = 'bank_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'bank_name';
	protected $_riles = array();
	protected $_timestamps = FALSE;
	
	function delete_bank($id){
		$this->db->where('user_id', $id);
		$this->db->delete($this->_table_name); 
	}
}