<?php
class Cart_m extends MY_Model{
	protected $_table_name = 'temp_orders';
	protected $_primary_key = 'temp_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'created';
	protected $_riles = array();
	protected $_timestamps = TRUE;
	
	public function clear_cart($data){
		$this->db->where($data);
		$this->db->delete($this->_table_name);
	}
}