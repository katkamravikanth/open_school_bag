<?php
class Users_m extends MY_Model{
	protected $_table_name = 'users';
	protected $_primary_key = 'user_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'created';
	protected $_riles = array();
	protected $_timestamps = TRUE;
	
	public function pword_string($length) {
		$key = '';
		$keys = array_merge(range(0, 9), range('a', 'z'));
	
		for ($i = 0; $i < $length; $i++) {
			$key .= $keys[array_rand($keys)];
		}
	
		return $key;
	}
	
	public function order_by($first=false, $second=false, $ss=false, $ord=false){
		
		if($ss != false && $ss != ''){
			$this->db->like('user_username', $ss);
			$this->db->or_like('user_email', $ss);
			$this->db->or_like('user_username', $ss);
		}
		if($ord != false && $ord != ''){
			$this->db->where('user_level', $ord);
		}
		if($first != false && $second != false){
			$this->db->where('created >=', $first);
			$this->db->where('created <=', $second);
		}
		
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		foreach($query as $row){
			$arr[$i]['usr'] = $row;
			if($row->user_level != 1 && $row->user_level != 9){
				$this->db->where('user_id', $row->user_id);
				$this->db->limit(1);
				$bank = $this->db->get('bank')->row();
				$arr[$i]['bank'] = $bank;
			}else{
				$arr[$i]['bank'] = array();
			}
			$i++;
		}
		return $arr;
	}
	
	public function get_selle_by(){
		$this->db->where('user_level', 2);
		$this->db->or_where('user_level', 3);
		return $this->get(NULL);
	}
}