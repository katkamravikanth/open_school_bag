<?php
class Orders_model extends MY_Model{
	protected $_table_name = 'orders_items';
	protected $_primary_key = 'order_item_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'created';
	protected $_riles = array();
	protected $_timestamps = TRUE;
	
	public function sales_total_no($first=false, $second=false, $by=false, $tag=false){
		
		if($first != false && $second != false){
			$this->db->where('created >=', $first);
			$this->db->where('created <=', $second);
		}
		if($by == 'level'){
			if($tag != false && $tag != '' && $tag != 'All'){
				$this->db->like('order_cat', $tag);
			}
		}else if($by == 'subject'){
			if($tag != false && $tag != '' && $tag != 'All'){
				$this->db->like('order_cat', $tag);
			}
		}else if($by == 'product_type'){
			if($tag != false && $tag != ''){
				if($tag == 'Digital'){
					$this->db->where('order_status', 'Done');
				}
				if($tag != 'All'){
					$this->db->where('order_ptype', $tag);
				}
			}
		}
		
		return $this->db->count_all_results($this->_table_name);
	}
	
	public function seller_total($first=false, $second=false, $by=false, $tag=false){
		
		if($first != false && $second != false){
			$this->db->where($this->_table_name.'.created >=', $first);
			$this->db->where($this->_table_name.'.created <=', $second);
		}
		
		return $this->db->count_all_results($this->_table_name);
	}
	
	public function sales_no_total($first=false, $second=false, $by=false, $tag=false){
		$this->db->from($this->_table_name);
		$this->db->distinct();
		$this->db->join('products', 'products.product_unique = '.$this->_table_name.'.order_punique');
		$this->db->join('users', 'users.user_id = products.user_id');
		$this->db->group_by($this->_table_name.'.order_pname');
		
		if($first != false && $second != false){
			$this->db->where($this->_table_name.'.created >=', $first);
			$this->db->where($this->_table_name.'.created <=', $second);
		}
		
		if($tag != false && $tag != ''){
			if($tag != 'All'){
				$this->db->where($this->_table_name.'.order_pname', $tag);
			}
		}
		
		return $this->db->count_all_results();
	}
	
	public function saller_total_no($first=false, $second=false, $tag=false){
		$dates = '';
		$tags = '';
		if($first != false && $second != false){
			$dates = 'AND o.created BETWEEN "'.$first.'" AND "'.$second.'"';
		}
		if($tag != false && $tag != '' && $tag != 'All'){
			$tags = 'AND u.user_username = "'.$tag.'"';
		}
		
		$query = $this->db->query('SELECT count(1) AS totalOrders, u.user_username AS productOwner, SUM(o.order_pprice) AS totlaSale FROM '.$this->_table_name.' o, products p, users u WHERE o.order_punique = p.product_unique AND u.user_id = p.user_id '.$tags.' '.$dates.' GROUP BY p.user_id');
		
		return $query->num_rows();
	}
	
	public function sales_by_limit($num=false, $start=false, $first=false, $second=false, $by=false, $tag=false){
		
		if($first != false && $second != false){
			$this->db->where('created >=', $first);
			$this->db->where('created <=', $second);
		}
		
		if($num != false && $start != false){
			$this->db->limit($num, $start);
		}
		
		if($by == 'level'){
			if($tag != false && $tag != '' && $tag != 'All'){
				$this->db->like('order_cat', $tag);
			}
		}else if($by == 'subject'){
			if($tag != false && $tag != '' && $tag != 'All'){
				$this->db->like('order_cat', $tag);
			}
		}else if($by == 'product_type'){
			if($tag != false && $tag != ''){
				if($tag == 'Digital'){
					$this->db->where('order_status', 'Done');
				}
				if($tag != 'All'){
					$this->db->where('order_ptype', $tag);
				}
			}
		}
		return $this->db->get($this->_table_name)->result();
	}
	
	public function seller_limit($num=false, $start=false, $first=false, $second=false, $by=false, $tag=false){
		
		if($first != false && $second != false){
			$this->db->where($this->_table_name.'.created >=', $first);
			$this->db->where($this->_table_name.'.created <=', $second);
		}
		
		if($num != false && $start != false){
			$this->db->limit($num, $start);
		}
		
		if($by == 'seller'){
			if($tag != false && $tag != '' && $tag != 'All'){
				$this->db->where('users.user_username', $tag);
			}
		}
		return $this->db->get($this->_table_name)->result();
	}
	
	public function sales_limit_by($num=false, $start=false, $first=false, $second=false, $by=false, $tag=false){
		$this->db->select('COUNT(*) as TotalSales, SUM(order_pprice) as TotalAmount, order_pname, order_ptype, user_id, puser_id, order_pprice, order_discount, created');
		$this->db->distinct();
		$this->db->group_by('order_pname');
		
		if($num != false && $start != false){
			$this->db->limit($num, $start);
		}
		if($first != false && $second != false){
			$this->db->where('created >=', $first);
			$this->db->where('created <=', $second);
		}
		if($by == 'product'){
			if($tag != false && $tag != '' && $tag != 'All'){
				$this->db->where('order_pname', $tag);
			}
		}else if($by == 'level' || $by == 'subject'){
			if($tag != false && $tag != '' && $tag != 'All'){
				$this->db->like('order_cat', $tag);
			}
		}else if($by == 'product_type'){
			if($tag != false && $tag != ''){
				if($tag == 'Digital'){
					$this->db->where('order_status', 'Done');
				}
				if($tag != 'All'){
					$this->db->where('order_ptype', $tag);
				}
			}
		}
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		foreach($query as $row){
			$arr[$i]['sale'] = $row;
				$this->db->select('order_discount, order_promo');
				$this->db->where('order_pname', $row->order_pname);
				$disc = $this->db->get($this->_table_name)->result();
				$disco = 0;
				foreach($disc as $dis){if($dis->order_promo == ''){$disco = $disco + $dis->order_discount;}}
			$arr[$i]['disc'] = $disco;
			$i++;
		}
		return $arr;
	}
	
	public function saller_by_limit($num=false, $start=false, $first=false, $second=false, $tag=false){
		$dates = '';
		$limit = '';
		$tags = '';
		
		if($first != false && $second != false){
			$dates = 'AND o.created BETWEEN "'.$first.'" AND "'.$second.'"';
		}
		if($num != false && $start != false){
			$limit = 'LIMIT '.$start.', '.$num.'';
		}
		if($tag != false && $tag != '' && $tag != 'All'){
			$tags = 'AND u.user_username = "'.$tag.'"';
		}
		
		$query = $this->db->query('SELECT COUNT(1) AS totalOrders, o.puser_id AS productOwner, SUM(o.order_pprice) AS totlaSale, b.bank_name, b.bank_other, b.bank_code, b.bank_branch_code, b.bank_account_type, b.bank_account_number FROM '.$this->_table_name.' o, users u, bank b WHERE u.user_username = o.puser_id AND b.user_id = u.user_id '.$tags.' '.$dates.' GROUP BY o.puser_id ORDER BY o.puser_id '.$limit.'');
		
		//return $query->result();
		
		$quey = $query->result();
		$arr = array();
		$i=0;
		foreach($quey as $row){
			$arr[$i]['sale'] = $row;
				$this->db->select('order_discount, order_promo');
				$this->db->where('puser_id', $row->productOwner);
				$disc = $this->db->get($this->_table_name)->result();
				$disco = 0;
				foreach($disc as $dis){if($dis->order_promo == ''){$disco = $disco + $dis->order_discount;}}
			$arr[$i]['disc'] = $disco;
			$i++;
		}
		return $arr;
	}
	
	public function total_saller_limit($first=false, $second=false, $tag=false){
		$dates = '';
		$tags = '';
		if($first != false && $second != false){
			$dates = 'AND o.created BETWEEN "'.$first.'" AND "'.$second.'"';
		}
		if($tag != false && $tag != '' && $tag != 'All'){
			$tags = 'AND o.order_pname = "'.$tag.'"';
		}
		
		$query = $this->db->query('SELECT count(1) AS totalOrders, u.user_fname AS productOwner, SUM(o.order_pprice - o.order_discount) AS totlaSale FROM '.$this->_table_name.' o, products p, users u WHERE o.order_punique = p.product_unique AND u.user_id = p.user_id '.$tags.' '.$dates.' GROUP BY p.user_id ORDER BY u.user_fname');
		
		$que = $query->result();
		$total = 0;
		foreach($que as $quer){
			$total = $total+$quer->totlaSale;
		}
		return $total;
	}
	
	public function sales_by($first=false, $second=false, $by=false, $tag=false){
		$this->db->select('*');
		$this->db->distinct();
		$this->db->from($this->_table_name);
		$this->db->join('products', 'products.product_unique = '.$this->_table_name.'.order_punique');
		$this->db->join('users', 'users.user_id = products.user_id');
		$this->db->join('bank', 'bank.user_id = products.user_id');
		$this->db->where($this->_table_name.'.order_status', 'Done');
		
		$this->db->where($this->_table_name.'.created >=', $first);
		$this->db->where($this->_table_name.'.created <=', $second);
		
		if($tag != false && $tag != '' && $tag != 'All'){
			$this->db->where($this->_table_name.'.order_pname', $tag);
		}
		
		return $this->db->get()->result();
	}
	
	public function sales_limit($num=false, $start=false){
		
		$this->db->select('users.user_username, '.$this->_table_name.'.order_pname, '.$this->_table_name.'.created, '.$this->_table_name.'.order_ptype, '.$this->_table_name.'.order_no, products.user_id, '.$this->_table_name.'.order_no, '.$this->_table_name.'.order_pprice');
		$this->db->join('products', 'products.product_unique = '.$this->_table_name.'.order_punique');
		$this->db->join('users', 'users.user_username = '.$this->_table_name.'.user_id');
		
		$this->db->limit($num, $start);
		$this->db->order_by($this->_table_name.'.order_no');
		
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		if(count($query)){
			foreach($query as $row){
				$this->db->select('user_username')->where('user_id', $row->user_id);
				$usr = $this->db->get('users')->result();
				if(count($usr)){
					$arr[$i]['usr'] = $usr[0]->user_username;
					$arr[$i]['ord'] = $row;
					$i++;
				}
			}
		}
		return $arr;
	}
	
	public function sales_date($num, $start, $first, $second){
		
		$this->db->select('users.user_username, '.$this->_table_name.'.order_pname, '.$this->_table_name.'.created, '.$this->_table_name.'.order_ptype, '.$this->_table_name.'.order_no, products.user_id, '.$this->_table_name.'.order_no, '.$this->_table_name.'.order_pprice');
		$this->db->join('products', 'products.product_unique = '.$this->_table_name.'.order_punique');
		$this->db->join('users', 'users.user_username = '.$this->_table_name.'.user_id');
		
		$this->db->limit($num, $start);
		$this->db->where($this->_table_name.'.created >=', $first);
		$this->db->where($this->_table_name.'.created <=', $second);
		
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		if(count($query)){
			foreach($query as $row){
				$this->db->select('user_username')->where('user_id', $row->user_id);
				$usr = $this->db->get('users')->result();
				if(count($usr)){
					$arr[$i]['usr'] = $usr[0]->user_username;
					$arr[$i]['ord'] = $row;
					$i++;
				}
			}
		}
		return $arr;
	}
	
	public function sales_pname($num, $start, $pname){
		
		$this->db->select('users.user_username, '.$this->_table_name.'.order_pname, '.$this->_table_name.'.created, '.$this->_table_name.'.order_ptype, '.$this->_table_name.'.order_no, products.user_id, '.$this->_table_name.'.order_no, '.$this->_table_name.'.order_pprice');
		$this->db->join('products', 'products.product_unique = '.$this->_table_name.'.order_punique');
		$this->db->join('users', 'users.user_username = '.$this->_table_name.'.user_id');
		
		$this->db->limit($num, $start);
		$this->db->like($this->_table_name.'.order_pname', $pname);
		
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		if(count($query)){
			foreach($query as $row){
				$this->db->select('user_username')->where('user_id', $row->user_id);
				$usr = $this->db->get('users')->result();
				if(count($usr)){
					$arr[$i]['usr'] = $usr[0]->user_username;
					$arr[$i]['ord'] = $row;
					$i++;
				}
			}
		}
		return $arr;
	}
	
	public function sales_date_report($first, $second){
		
		$this->db->select('users.user_username, '.$this->_table_name.'.order_pname, '.$this->_table_name.'.created, '.$this->_table_name.'.order_ptype, '.$this->_table_name.'.order_no, products.user_id, '.$this->_table_name.'.order_no, '.$this->_table_name.'.order_pprice');
		$this->db->join('products', 'products.product_unique = '.$this->_table_name.'.order_punique');
		$this->db->join('users', 'users.user_username = '.$this->_table_name.'.user_id');
		
		$this->db->where($this->_table_name.'.created >=', $first);
		$this->db->where($this->_table_name.'.created <=', $second);
		
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		if(count($query)){
			foreach($query as $row){
				$arr[$i]['ord'] = $row;
				
					$this->db->select('user_username')->where('user_id', $row->user_id);
					$usr = $this->db->get('users')->result();
				$arr[$i]['usr'] = $usr[0]->user_username;
				$i++;
			}
		}
		return $arr;
	}
	
	public function sales_pname_report($pname){
		
		$this->db->select('users.user_username, '.$this->_table_name.'.order_pname, '.$this->_table_name.'.created, '.$this->_table_name.'.order_ptype, '.$this->_table_name.'.order_no, products.user_id, '.$this->_table_name.'.order_no, '.$this->_table_name.'.order_pprice, '.$this->_table_name.'.order_status');
		$this->db->join('products', 'products.product_unique = '.$this->_table_name.'.order_punique');
		$this->db->join('users', 'users.user_username = '.$this->_table_name.'.user_id');
		
		$this->db->like($this->_table_name.'.order_pname', $pname);
		
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		if(count($query)){
			foreach($query as $row){
				$this->db->select('user_username')->where('user_id', $row->user_id);
				$usr = $this->db->get('users')->result();
				if(count($usr)){
					$arr[$i]['usr'] = $usr[0]->user_username;
					$arr[$i]['ord'] = $row;
					$i++;
				}
			}
		}
		return $arr;
	}
	
	public function down_total_no($first, $second, $by, $tag){
		
		$this->db->select('*');
		$this->db->distinct();
		$this->db->from($this->_table_name);
		$this->db->join('products', 'products.product_unique = '.$this->_table_name.'.order_punique');
		$this->db->join('users', 'users.user_id = products.user_id');
		$this->db->where($this->_table_name.'.order_status', 'Done');
		$this->db->where('products.down_count !=', 0);
		$this->db->where('products.order_count !=', 0);
		$this->db->group_by($this->_table_name.'.order_pname');
		
		$this->db->where($this->_table_name.'.created >=', $first);
		$this->db->where($this->_table_name.'.created <=', $second);
		
		if($by == 'seller'){
			if($tag != 'All'){
				$this->db->where('users.user_username', $tag);
			}
		}else if($by == 'product'){
			if($tag != 'All'){
				$this->db->where($this->_table_name.'.order_pname', $tag);
			}
		}else if($by == 'product_type'){
			if($tag != 'All'){
				$this->db->where($this->_table_name.'.order_ptype', $tag);
			}
		}else if($by == 'level'){
			if($tag != 'All'){
				$this->db->where('products.product_level', $tag);
			}
		}else if($by == 'subject'){
			if($tag != false && $tag != '' && $tag != 'All'){
				$this->db->or_like('products.product_subject', $tag);
			}
		}
		
		return $this->db->count_all_results();
	}
	
	public function down_limit($num=false, $start=false, $first, $second, $by, $tag){
		
		$this->db->select('*');
		$this->db->distinct();
		$this->db->from($this->_table_name);
		$this->db->join('products', 'products.product_unique = '.$this->_table_name.'.order_punique');
		$this->db->join('users', 'users.user_id = products.user_id');
		$this->db->where($this->_table_name.'.order_status', 'Done');
		$this->db->where('products.down_count !=', 0);
		$this->db->where('products.order_count !=', 0);
		$this->db->group_by($this->_table_name.'.order_pname');
		
		$this->db->where($this->_table_name.'.created >=', $first);
		$this->db->where($this->_table_name.'.created <=', $second);
		
		if($by == 'seller'){
			if($tag != 'All'){
				$this->db->where('users.user_username', $tag);
			}
		}else if($by == 'product'){
			if($tag != 'All'){
				$this->db->where($this->_table_name.'.order_pname', $tag);
			}
		}else if($by == 'product_type'){
			if($tag != 'All'){
				$this->db->where($this->_table_name.'.order_ptype', $tag);
			}
		}else if($by == 'level'){
			if($tag != 'All'){
				$this->db->where('products.product_level', $tag);
			}
		}else if($by == 'subject'){
			if($tag != false && $tag != '' && $tag != 'All'){
				$this->db->or_like('products.product_subject', $tag);
			}
		}
		if($num != false && $start != false){
			$this->db->limit($num, $start);
		}
		
		return $this->db->get()->result();
	}
	
	public function order_view($order_no){
		$users = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		$this->db->select($this->_table_name.".*, orders.*, users.user_phone");
		$this->db->join('users', 'users.user_username = '.$this->_table_name.'.user_id');
		$this->db->join('orders', 'orders.order_no = '.$this->_table_name.'.order_no');
		$this->db->where($this->_table_name.'.order_no', $order_no);
		
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		if(count($query)){
			foreach($query as $row){
				$arr[$i]['ord'] = $row;
				
					$this->db->where('order_no', $order_no);
					$usr = $this->db->get('order_shipping')->result();
					if(count($usr)){
						$arr[$i]['ship'] = $usr;
					}else{
						$arr[$i]['ship'] = array();
					}
				$i++;
			}
		}
		return $arr;
	}
	
	function seller_there($uid){
		$this->db->where('user_id', $uid);
		$query = $this->db->get('products')->result();
		$i=0;
		if(count($query)){
			foreach($query as $row){
				$this->db->where('order_punique', $row->product_unique);
				$this->db->where('order_ptype', 'Physical');
				$this->db->where('order_status !=', 'Delivered');
				$querys = $this->db->get($this->_table_name)->result();
				if(count($querys)){
					$i++;
				}
			}
		}
		return $i;
	}
	
	function buyer_there($uid){
		$this->db->where('user_id', $uid);
		$this->db->where('order_ptype', 'Physical');
		$this->db->where('order_status !=', 'Delivered');
		
		$query = $this->db->get($this->_table_name)->result();
		$i=0;
		if(count($query)){
			foreach($query as $row){
				$i++;
			}
		}
		
		$this->db->where('user_id', $uid);
		$this->db->where('order_ptype', 'Digital');
		$query = $this->db->get($this->_table_name)->result();

		if(count($query)){
			foreach($query as $row){
				$this->db->where('product_unique', $row->order_punique);
				$querys = $this->db->get('products')->result();
				if(count($querys)){
					foreach($querys as $rows)
					$date = $row->created;
					$date = strtotime($date);
					$date = strtotime("+".$rows->product_down_days." day", $date);
					$date = date('Y-m-d H:i:s', $date);
					if($date > date('Y-m-d H:i:s')){
						$i++;
					}
				}
			}
		}
		return $i;
	}
}