<?php
class Promo_model extends MY_Model {
	
	protected $_table_name = 'promos';
	protected $_primary_key = 'promo_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'created';
	protected $_riles = array();
	protected $_timestamps = TRUE;
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct(){
		parent::__construct();
    }
	
	function redeem_it($code){
		$used = $this->get_by(array('promo_code'=> $code), 1);
		$tamount = $this->cart->total();
		
		$this->db->where('promo_type', 'On Cart Amount');
		$this->db->where('promo_code', $code);
		$query = $this->db->get($this->_table_name)->result();
		if(count($query)){
			if($query[0]->promo_end < date('Y-m-d') || $query[0]->promo_users < $query[0]->promo_used_count){
				$this->db->set(array('promo_status'=>'Expired'));
				$this->db->where('promo_code', $code);
				$this->db->update($this->_table_name);
			}
			
			$this->db->where('promo_type', 'On Cart Amount');
			$query = $this->db->get($this->_table_name)->result();
			if(count($query)){
				$this->db->where('promo_users >', $used->promo_used_count);
				$rec = $this->db->get($this->_table_name)->result();
				if(count($rec)){
					$this->db->where('promo_min_amount <=', $tamount);
					$query = $this->db->get($this->_table_name)->result();
					if(count($query)){
						$this->db->where('promo_end >', date('Y-m-d 00:00:00'));
						$this->db->where('promo_status','Active');
						$query = $this->db->get($this->_table_name)->result();
						if(count($query)){
							return 0;
						}else{
							print 'Promo code expired';
						}
					}else{
						print 'Minimum amount should be S$ '.$rec[0]->promo_min_amount;
					}
				}else{
					print 'We are sorry! The promotion has been fully redeemed.';
				}
			}else{
				print 'The promo code you have entered is not found. Please check and try again. Also note that the promo code is case sensitive';
			}
		}else{
			print 'The promo code you have entered is not found. Please check and try again. Also note that the promo code is case sensitive';
		}
	}
	
	public function total_promo($string=false, $order=false){
		if($string != false || $order != false){
			if($order != false && $order != ''){
				if($order == 'Active' || $order == 'Expired'){
					$this->db->where('promo_status', $order);
				}else{
					$this->db->where('promo_type', $order);
				}
			}
			if($string != false && $string != ''){
				$this->db->like('promo_percent', $string);
				$this->db->or_like('promo_code', $string);
			}
			return $this->db->count_all_results($this->_table_name);
		}else{
			return $this->db->count_all($this->_table_name);
		}
	}
	
	public function get_by_promo($num, $start, $string=false, $order=false){
		if($order != false && $order != ''){
			if($order == 'Active' || $order == 'Expired'){
				$this->db->where('promo_status', $order);
			}else{
				$this->db->where('promo_type', $order);
			}
		}
		if($string != false && $string != ''){
			$this->db->like('promo_percent', $string);
			$this->db->or_like('promo_code', $string);
		}
		
		$this->db->limit($num, $start);
		
		$this->db->order_by($this->_order_by, 'desc');
		return $this->db->get($this->_table_name)->result();
	}
	
	public function promo_used($data){
		$this->db->set($data);
		$this->db->insert('promo_used');
		return $this->db->insert_id();
	}
	
	public function update_used($ftotal, $gtotal, $id){
		$this->db->set(array('original_price' => $ftotal, 'purchased_amount' => $gtotal));
		$this->db->where('puid', $id);
		$this->db->update('promo_used');
	}
	
	public function totals($first=false, $second=false){
		$this->db->where('start_date >=', $first);
		$this->db->where('end_date <=', $second);
	
		return $this->db->count_all_results('promo_used');
	}
	
	public function listed($num=false, $start=false, $first=false, $second=false){
		$this->db->where('start_date >=', $first);
		$this->db->where('end_date <=', $second);
		
		if($num != false && $start != false){
			$this->db->limit($num, $start);
		}
	
		return $this->db->get('promo_used')->result();
	}
	
	public function promo_details($title, $cat){
		
		$this->db->where('promo_type', 'On Product Amount');
		$this->db->where('product_title', $title);
		
		$cat = explode(':', $cat);
		foreach($cat as $cate){
			$subject = explode(',',$cate);
			if(count($subject)){
				foreach($subject as $subj){
					$this->db->or_where('promo_category', $subj);
				}
			}else{
				$this->db->or_where('promo_category', $cate);
			}
		}
		
		$this->db->limit(1, 0);
		
		return $this->db->get('promos')->result();
	}
	 
    /**
    * Delete product
    * @param int $id - product id
    * @return boolean
    */
	function delete_promo($id){
		$this->db->where($this->_primary_key, $id);
		$this->db->delete($this->_table_name); 
	}
}
?>