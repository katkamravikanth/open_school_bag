<?php
class Products_model extends MY_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct(){
		parent::__construct();
    }

    /**
    * Get product by his is
    * @param int $product_id 
    * @return array
    */
    public function get_product_by_id($id)
    {
		$this->db->select('*');
		$this->db->from('products');
		$this->db->where('product_id', $id);
		$this->db->where('product_status', 'Active');
		$query = $this->db->get();
		return $query->result_array(); 
    }

    /**
    * Fetch products data from the database
    * possibility to mix search, filter and order
    * @param int $manufacuture_id 
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_products($user_id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end){
	    
		$this->db->select('products.product_id, products.product_name, products.product_pic, products.product_resource, products.product_price, products.user_id, products.product_unique, users.user_username as user_username');
		$this->db->from('products');
		if($user_id != null && $user_id != 0){
			$this->db->where('user_id', $user_id);
		}
		if($search_string){
			$this->db->like('description', $search_string);
		}

		$this->db->join('users', 'products.user_id = users.user_id', 'left');
		$this->db->where('products.product_status', 'Active');
		$this->db->group_by('products.product_id');

		if($order){
			$this->db->order_by($order, $order_type);
		}else{
		    $this->db->order_by('product_id', $order_type);
		}


		$this->db->limit($limit_start, $limit_end);
		//$this->db->limit('4', '4');


		$query = $this->db->get();
		
		return $query->result_array(); 	
    }

    /**
    * Count the number of rows
    * @param int $user_id
    * @param int $search_string
    * @param int $order
    * @return int
    */
    function count_products($user_id=null, $search_string=null, $order=null)
    {
		$this->db->select('*');
		$this->db->from('products');
		if($user_id != null && $user_id != 0){
			$this->db->where('user_id', $user_id);
		}
		if($search_string){
			$this->db->like('product_desc', $search_string);
		}
		if($order){
			$this->db->order_by($order, 'Asc');
		}else{
		    $this->db->order_by('product_id', 'Asc');
		}
		$this->db->where('product_status', 'Active');
		$query = $this->db->get();
		return $query->num_rows();        
    }
	
	public function prod_limit($sdata=false, $by=false){
		$this->db->select('*');
		$this->db->join('users', 'users.user_id = products.user_id');
		
		if($sdata != false && $sdata != ''){
			$this->db->or_like($sdata);
		}
		
		if($by != false && $by != ''){
			$this->db->where('products.product_type', $by);
		}
		
		$this->db->where('products.product_status', 'Active');
		
		return $this->db->get('products')->result();
	}

    /**
    * Delete product
    * @param int $id - product id
    * @return boolean
    */
	function delete_product($id){
		$data = array('product_status' => 'Inactive');
		$this->db->set($data);
		$this->db->where('product_unique', $id);
		$this->db->limit(1);
		$this->db->update('products');
	}
	function delete_user($id){
		$prods = $this->db->where(array('user_id'=> $id))->get('products')->result();
		if(count($prods)){
			foreach($prods as $prod){
				/*if(file_exists('files/'.$prod->product_pic) && $prod->product_pic != ''){
					unlink('files/'.$prod->product_pic);
				}
				if(file_exists('files/'.$prod->product_file) && $prod->product_file != ''){
					unlink('files/'.$prod->product_file);
				}*/
				$data = array('product_status' => 'Inactive');
				$this->db->set($data);
				$this->db->where('product_unique', $prod->product_unique);
				$this->db->limit(1);
				$this->db->update('products');
			}
		}
	}
 
}
?>