<?php
class Categories_model extends MY_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct(){
		parent::__construct();
    }
	
	public function to_levels(){
		$this->db->order_by('level', 'asc');
		return $this->db->get('cat_levels')->result();
	}
	
	public function levels($num, $start){
		$this->db->limit($num, $start);
		$this->db->order_by('level', 'asc');
		return $this->db->get('cat_levels')->result();
	}
	
	public function subjects($num, $start){
		$this->db->join('cat_levels', 'cat_levels.level_id = cat_subjects.level_id');
		$this->db->limit($num, $start);
		$this->db->order_by('cat_subjects.level_id', 'asc');
		return $this->db->get('cat_subjects')->result();
	}
	
	public function resources($num, $start){
		$this->db->limit($num, $start);
		$this->db->order_by('resources', 'asc');
		return $this->db->get('cat_resources')->result();
	}
	
	public function t_levels(){
		return $this->db->count_all('cat_levels');
	}
	
	public function t_subjects(){
		return $this->db->count_all('cat_subjects');
	}
	
	public function t_resources(){
		return $this->db->count_all('cat_resources');
	}
	
	//save records
	public function s_level($data){
		$this->db->set($data);
		$this->db->insert('cat_levels');
		$id = $this->db->insert_id();
		return $id;
	}
	
	//save records
	public function s_subject($data){
		$this->db->set($data);
		$this->db->insert('cat_subjects');
		$id = $this->db->insert_id();
		return $id;
	}
	
	//save records
	public function s_resource($data){
		$this->db->set($data);
		$this->db->insert('cat_resources');
		$id = $this->db->insert_id();
		return $id;
	}
	
	//update records
	public function e_level($data, $rid){
		$this->db->set($data);
		$this->db->where('level_id', $rid);
		$up = $this->db->update('cat_levels');
		if($up){$id=$rid;}
		
		return $id;
	}
	
	//update records
	public function e_subject($data, $rid){
		$this->db->set($data);
		$this->db->where('subject_id', $rid);
		$up = $this->db->update('cat_subjects');
		if($up){$id=$rid;}
		
		return $id;
	}
	
	//update records
	public function e_resource($data, $rid){
		$this->db->set($data);
		$this->db->where('resource_id', $rid);
		$up = $this->db->update('cat_resources');
		if($up){$id=$rid;}
		
		return $id;
	}
	
	//update records
	public function by_subject($id){
		$this->db->where('subject_id', $id);
		return $this->db->get('cat_subjects')->result();
	}
	
	//update records
	public function by_resource($id){
		$this->db->where('resource_id', $id);
		return $this->db->get('cat_resources')->result();
	}

    /**
    * Delete product
    * @param int $id - product id
    * @return boolean
    */
	function d_level($id){
		$this->db->where('level_id', $id);
		$this->db->delete('cat_levels'); 
	}
	function d_subjects($id){
		$this->db->where('subject_id', $id);
		$this->db->delete('cat_subjects'); 
	}
	function d_resources($id){
		$this->db->where('resource_id', $id);
		$this->db->delete('cat_resources'); 
	}
 
}
?>