<?php
class Cms_model extends MY_Model {
	protected $_table_name = 'cms_pages';
	protected $_primary_key = 'cms_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'title';
	protected $_riles = array();
	protected $_timestamps = TRUE;
	
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct(){
		parent::__construct();
    }

    /**
    * Get product by his is
    * @param int $product_id 
    * @return array
    */
    public function get_cms_by_id($id){
		$this->db->select('*');
		$this->db->from($this->_table_name);
		$this->db->where('cms_id', $id);
		$query = $this->db->get();
		return $query->result_array(); 
    }    

    /**
    * Fetch cms_pages data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_cms($search_string=null, $order=null, $order_type='Asc', $limit_start=null, $limit_end=null){
	    
		$this->db->select('*');
		$this->db->from($this->_table_name);

		if($search_string){
			$this->db->like('title', $search_string);
		}
		$this->db->group_by($this->_primary_key);

		if($order){
			$this->db->order_by($order, $order_type);
		}else{
		    $this->db->order_by($this->_primary_key, $order_type);
		}

        if($limit_start && $limit_end){
          $this->db->limit($limit_start, $limit_end);	
        }

        if($limit_start != null){
          $this->db->limit($limit_start, $limit_end);    
        }
        
		$query = $this->db->get();
		
		return $query->result_array(); 	
    }

    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
    function count_cms($search_string=null, $order=null){
		$this->db->select('*');
		$this->db->from($this->_table_name);
		if($search_string){
			$this->db->like('title', $search_string);
		}
		if($order){
			$this->db->order_by($order, 'Asc');
		}else{
		    $this->db->order_by($this->_primary_key, 'Asc');
		}
		$query = $this->db->get();
		return $query->num_rows();        
    }

    /**
    * Delete user
    * @param int $id - user id
    * @return boolean
    */
	function delete_page($id){
		$this->db->where($this->_primary_key, $id);
		$id = $this->db->delete($this->_table_name);
		
		if($id){
			return true;
		}else{
			return false;
		}
	}
}
?>