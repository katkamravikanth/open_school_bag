<?php
class Partner_model extends MY_Model {
	protected $_table_name = 'partners';
	protected $_primary_key = 'partner_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'partner_title';
	protected $_riles = array();
	protected $_timestamps = TRUE;
}
?>