<?php
class Users_model extends MY_Model {
	protected $_table_name = 'users';
	protected $_primary_key = 'user_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'created';
	protected $_riles = array();
	protected $_timestamps = TRUE;
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct(){
		parent::__construct();
    }

    /**
    * Get product by his is
    * @param int $product_id 
    * @return array
    */
    public function get_user_by_id($id)
    {
		$this->db->select('*');
		$this->db->from($this->_table_name);
		$this->db->where('user_id', $id);
		$query = $this->db->get();
		return $query->result_array(); 
    }
	
	public function get_by_users(){
		$this->db->where('user_level != ', 1);
		$this->db->where('user_level != ', 9);
		$this->db->order_by('user_username');
		return $this->db->get($this->_table_name)->result();
	} 

    /**
    * Fetch users data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_users($search_string=null, $order=null, $limit_start=null, $limit_end=null)
    {
	    
		$this->db->select('*');
		$this->db->from($this->_table_name);
		
		$this->db->distinct();

		if($search_string){
			$this->db->like('user_username', $search_string);
			$this->db->or_like('user_email', $search_string);
			$this->db->or_like('user_username', $search_string);
		}
		$this->db->group_by('user_id');

		if($order){
			$this->db->where('user_level', $order);
		}

        if($limit_start || $limit_end){
          $this->db->limit($limit_start, $limit_end);	
        }
        $this->db->order_by($this->_order_by, 'desc');
		$query = $this->db->get()->result();
		//$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		foreach($query as $user){
			$arr[$i]['user'] = $user;
				$this->db->select('profile_pic');
				$this->db->where('user_id', $user->user_id);
				$this->db->limit(1, 0);
				$pic = $this->db->get('user_profile')->result();
				if(count($pic)){
					$pic = $pic[0]->profile_pic;
				}else{
					$pic = '';
				}
			$arr[$i]['pic'] = $pic;
			$i++;
		}
		//echo '<pre>';print_r($arr);echo '</pre>';
		return $arr;
		//return $query->result_array(); 	
    }

    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
    function count_users($search_string=null, $order=null)
    {
		if($search_string){
			$this->db->like('user_username', $search_string);
			$this->db->or_like('user_email', $search_string);
			$this->db->or_like('user_username', $search_string);
		}
		if($order){
			$this->db->where('user_level', $order);
		}
		return $this->db->count_all_results($this->_table_name);
    }

    /**
    * Delete user
    * @param int $id - user id
    * @return boolean
    */
	function delete_user($id){
		$data = array('user_status' => '2');
		$this->db->set($data);
		$this->db->where($this->_primary_key, $id);
		$this->db->limit(1);
		$this->db->update($this->_table_name);
		//$this->db->delete($this->_table_name); 
	}
}
?>