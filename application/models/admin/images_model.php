<?php
class Images_model extends MY_Model {
	protected $_table_name = 'images_banners';
	protected $_primary_key = 'image_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'created';
	protected $_riles = array();
	protected $_timestamps = TRUE;
	
	
	public function by_limit($num, $start){
		$this->db->limit($num, $start);
		
		if(!count($this->db->ar_orderby)){
			$this->db->order_by('path', 'desc');
			$this->db->order_by($this->_order_by, 'desc');
		}
		return $this->db->get($this->_table_name)->result();
	}
	
	public function image_by($where, $single = FALSE){
		$this->db->where($where);
		$this->db->order_by('order', 'Asc');
		return $this->get(NULL, $single);
	}
	
	public function total_images(){
		return $this->db->count_all($this->_table_name);
	}
	
	
	function delete_images($id){
		$this->db->where($this->_primary_key, $id);
		$this->db->limit(0,1);
		$banner = $this->db->get($this->_table_name)->result();
		if(count($banner)){
			unlink('files/icons/'.$banner->banner_photo);
		}
		$this->db->where($this->_primary_key, $id);
		$this->db->delete($this->_table_name); 
	}
}
?>