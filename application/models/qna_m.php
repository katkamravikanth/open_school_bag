<?php
class QnA_m extends MY_Model{
	protected $_table_name = 'qna';
	protected $_primary_key = 'qna_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'created';
	protected $_riles = array();
	protected $_timestamps = TRUE;
	
	public function get_qna($unique){
		$this->db->select('users.user_username, '.$this->_table_name.'.qna_body, '.$this->_table_name.'.parent_id, '.$this->_table_name.'.qna_id, '.$this->_table_name.'.product_unique, '.$this->_table_name.'.user_id as uid');
		$this->db->from($this->_table_name);
		$this->db->join('users', 'users.user_id = '.$this->_table_name.'.user_id');
		$this->db->where($this->_table_name.'.product_unique', $unique);
		$rows = $this->db->get()->result();
		$array = array(); 
		foreach ($rows as $key=>$value){
			$array[]=(array) $value; 
		}
        return $array;
	}
	
	public function delete_qna($qna_id){
		$filter = $this->_primary_filter;
		$id = $filter($qna_id);
		$this->db->where($this->_primary_key, $id);
		$this->db->limit(1);
		$dele = $this->db->delete($this->_table_name);
		
		$this->db->where('parent_id', $id);
		$this->db->limit(1);
		$del = $this->db->delete($this->_table_name);
		
		if($dele || $del){
			return TRUE;
		}else{ return FALSE;}
	}
	
	function delete_user($id){
		$this->db->where('user_id', $id);
		$this->db->delete($this->_table_name);
	}
}