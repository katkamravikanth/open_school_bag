<?php
class Product_m extends MY_Model{
	protected $_table_name = 'products';
	protected $_primary_key = 'product_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'created';
	protected $_riles = array();
	protected $_timestamps = TRUE;
	
	public function levels(){
		$this->db->order_by('level', 'asc');
		return $this->db->get('cat_levels')->result();
	}
	
	public function subjects(){
		$this->db->select('subjects');
		$this->db->distinct();
		$this->db->order_by('subjects', 'asc');
		return $this->db->get('cat_subjects')->result();
	}
	
	public function resources(){
		$this->db->order_by('resources', 'asc');
		return $this->db->get('cat_resources')->result();
	}
	
	public function by_levels($level){
		$this->db->where('level', $level);
		$this->db->limit(1);
		return $this->db->get('cat_levels')->result();
	}
	
	public function by_subjects($id){
		$this->db->where('level_id', $id);
		$this->db->order_by('subjects', 'asc');
		return $this->db->get('cat_subjects')->result();
	}
	
	public function total(){
		$this->db->where('product_status', 'Active');
		return $this->db->count_all_results($this->_table_name);
	}
	
	public function search($num, $start, $level=false, $subject=false, $rtype=false, $stext=false, $cat=false){
		if($level != false && $level != ''){
			$this->db->where('product_level', $level);
		}
		if($subject != false && $subject != ''){
			$this->db->where('product_subject', $subject);
		}
		if($rtype != false && $rtype != ''){
			$this->db->where('product_resource', $rtype);
		}
		if($stext != false && $stext != ''){
			$this->db->like('product_name', $stext);
		}
		if($cat != false && $cat != ''){
			$this->db->where('product_type', $cat);
			$this->db->or_where('product_level', $cat);
			$this->db->or_where('product_subject', $cat);
			$this->db->or_where('product_resource', $cat);
		}
		$this->db->where('product_status', 'Active');
		$this->db->limit($num, $start);
		if(!count($this->db->ar_orderby)){
			$this->db->order_by($this->_order_by, 'desc');
		}
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		foreach($query as $pro){
			$arr[$i]['pro'] = $pro;
				$this->db->select('rate');
				$this->db->order_by('modified', 'Desc');
				$this->db->where('product_unique', $pro->product_unique);
				$rate = $this->db->get('reviews')->result();
				if(count($rate)){
					$rates = 0;
					foreach($rate as $rat){
						$rates = $rates + $rat->rate;
					}
					$rates = $rates/count($rate);
				}else{
					$rates = 0;
				}
			$arr[$i]['avg'] = round($rates);
		
				$this->db->select('promo_percent, promo_percent_type, product_title, promo_users, promo_used_count');
				$this->db->where('promo_type', 'On Product Amount');
				$this->db->where('product_title', $pro->product_name);
				$this->db->or_where('promo_category', $pro->product_level);
				
				$subject = explode(',',$pro->product_subject);
				foreach($subject as $subj){
					$this->db->or_where('promo_category', $subj);
				}
				
				$this->db->or_where('promo_category', $pro->product_resource);
				$this->db->limit(1, 0);
				$disc = $this->db->get('promos')->result();
				if(count($disc)){
					if($disc[0]->promo_users > $disc[0]->promo_used_count){
						if($disc[0]->promo_percent_type == '%'){$dis = $pro->product_price * $disc[0]->promo_percent / 100;}else
						if($disc[0]->promo_percent_type == 'S$'){$dis = $disc[0]->promo_percent;}else{$dis = 0;}
						$prod = $disc[0]->product_title;
					}else{
						$dis = 0;
						$prod = '';
					}
				}else{
					$dis = 0;
					$prod = '';
				}
			$arr[$i]['disc'] = $dis;
			$arr[$i]['prod'] = $prod;
			$i++;
		}
		return $arr;
	}
	
	public function search_total_no($level=false, $subject=false, $rtype=false, $stext=false, $cat=false, $sdata=false){
		if($level != false && $level != ''){
			$this->db->where('product_level', $level);
		}
		if($subject != false && $subject != ''){
			$this->db->where('product_subject', $subject);
		}
		if($rtype != false && $rtype != ''){
			$this->db->where('product_resource', $rtype);
		}
		if($stext != false && $stext != ''){
			$this->db->like('product_name', $stext);
		}
		if($sdata != false && $sdata != ''){
			$this->db->or_like($sdata);
		}
		if($cat != false && $cat != ''){
			$this->db->where('product_type', $cat);
			$this->db->or_where('product_level', $cat);
			$this->db->or_where('product_subject', $cat);
			$this->db->or_where('product_resource', $cat);
		}
		$this->db->where('product_status', 'Active');
		return $this->db->count_all_results($this->_table_name);
	}
	
	public function order_by($order, $first, $second){
		$this->db->select('*');
		$this->db->from($this->_table_name);
		$this->db->join('users', 'users.user_id = '.$this->_table_name.'.user_id');
		$this->db->where($this->_table_name.'.product_status', 'Active');
		$this->db->where($this->_table_name.'.created >=', $first);
		$this->db->where($this->_table_name.'.created <=', $second);
		
		if(!count($this->db->ar_orderby)){
			$this->db->order_by($this->_table_name.'.'.$order, 'desc');
		}
		
		return $this->db->get()->result();
	}
	
	public function s_total_no($id=false, $first=false, $second=false, $by=false, $tag=false){
		$this->db->select('*');
		$this->db->join('users', 'users.user_id = '.$this->_table_name.'.user_id');
		
		if($id != false){
			$this->db->where($this->_table_name.'.user_id', $id);
		}
		
		if($first != false && $second != false){
			$this->db->where($this->_table_name.'.created >=', $first);
			$this->db->where($this->_table_name.'.created <=', $second);
		}
		
		if($by == 'seller'){
			if($tag != 'All'){
				$this->db->where('users.user_username', $tag);
			}
		}else if($by == 'level'){
			if($tag != 'All'){
				$this->db->where($this->_table_name.'.product_level', $tag);
			}
		}else if($by == 'subject'){
			if($tag != false && $tag != '' && $tag != 'All'){
				$this->db->or_like($this->_table_name.'.product_subject', $tag);
			}
		}else if($by == 'product_type'){
			if($tag != 'All'){
				$this->db->where($this->_table_name.'.product_type', $tag);
			}
		}
		
		if(!count($this->db->ar_orderby)){
			$this->db->order_by($this->_table_name.'.'.$this->_order_by, 'desc');
		}
		
		$this->db->where($this->_table_name.'.product_status', 'Active');
		
		return $this->db->count_all_results($this->_table_name);
	}
	
	public function prod_by_limit($num=false, $start=false, $id=false, $first=false, $second=false, $by=false, $tag=false){
		$this->db->select('*');
		$this->db->join('users', 'users.user_id = '.$this->_table_name.'.user_id');
		
		if($id != false){
			$this->db->where($this->_table_name.'.user_id', $id);
		}
		
		if($first != false && $second != false){
			$this->db->where($this->_table_name.'.created >=', $first);
			$this->db->where($this->_table_name.'.created <=', $second);
		}
		
		if($num != false && $start != false){
			$this->db->limit($num, $start);
		}
		
		if($by == 'seller'){
			if($tag != 'All'){
				$this->db->where('users.user_username', $tag);
			}
		}else if($by == 'level'){
			if($tag != 'All'){
				$this->db->where($this->_table_name.'.product_level', $tag);
			}
		}else if($by == 'subject'){
			if($tag != false && $tag != '' && $tag != 'All'){
				$this->db->or_like($this->_table_name.'.product_subject', $tag);
			}
		}else if($by == 'product_type'){
			if($tag != 'All'){
				$this->db->where($this->_table_name.'.product_type', $tag);
			}
		}
		
		if(!count($this->db->ar_orderby)){
			$this->db->order_by($this->_table_name.'.'.$this->_order_by, 'desc');
		}
		
		$this->db->where($this->_table_name.'.product_status', 'Active');
		
		return $this->db->get($this->_table_name)->result();
	}
	
	public function by_product($unique){
		$this->db->where('product_unique', $unique);
		$this->db->where('product_status', 'Active');
		$this->db->order_by($this->_order_by, 'desc');
		$this->db->limit(1);
		$query = $this->db->get($this->_table_name)->result();
		$arr=array();
		$arr['pro'] = $query[0];
		
		$promo = $this->db->get('promos')->result();
		if(count($promo)){
			foreach($promo as $rows){
				if($rows->promo_end < date('Y-m-d') || $rows->promo_users < $rows->promo_used_count){
					$this->db->set(array('promo_status'=>'Expired'));
					$this->db->where('promo_code', $rows->promo_code);
					$this->db->update('promos');
				}else if($rows->promo_start < date('Y-m-d') && $rows->promo_end > date('Y-m-d')){
					$this->db->set(array('promo_status'=>'Active'));
					$this->db->where('promo_code', $rows->promo_code);
					$this->db->update('promos');
				}
			}
		}
		
		$this->db->select('promo_percent, promo_percent_type, product_title, promo_users, promo_used_count');
		$this->db->where('promo_type', 'On Product Amount');
		$this->db->where('product_title', $query[0]->product_name);
		$this->db->or_where('promo_category', $query[0]->product_level);
		
		$subject = explode(',',$query[0]->product_subject);
		foreach($subject as $subj){
			$this->db->or_where('promo_category', $subj);
		}
		
		$this->db->or_where('promo_category', $query[0]->product_resource);
		$this->db->limit(1, 0);
		$disc = $this->db->get('promos')->result();
		if(count($disc)){
			if($disc[0]->promo_users > $disc[0]->promo_used_count){
				if($disc[0]->promo_percent_type == '%'){$dis = $query[0]->product_price * $disc[0]->promo_percent / 100;}else
				if($disc[0]->promo_percent_type == 'S$'){$dis = $disc[0]->promo_percent;}else{$dis = 0;}
				$prod = $disc[0]->product_title;
			}else{
				$dis = 0;
				$prod = '';
			}
		}else{
			$dis = 0;
			$prod = '';
		}
		$arr['disc'] = $dis;
		$arr['prod'] = $prod;
		
		return $arr;
	}
	
	public function by_limit($num, $start, $cat=FALSE, $order=false, $string=false){
		$this->db->where('product_status', 'Active');
		
		if($cat == TRUE && $cat != ''){
			$this->db->or_like($cat);
		}
		if($order == TRUE && $order != ''){
			$this->db->where(array('product_type'=>$order));
		}
		
		if($string == TRUE && $string != ''){
			$this->db->or_like(array('product_name'=>$string, 'product_desc'=>$string));
		}
		$this->db->limit($num, $start);
		
		$this->db->order_by($this->_order_by, 'desc');
		
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		foreach($query as $pro){
			$arr[$i]['pro'] = $pro;
				$this->db->select('rate');
				$this->db->order_by('modified', 'Desc');
				$this->db->where('product_unique', $pro->product_unique);
				$rate = $this->db->get('reviews')->result();
				if(count($rate)){
					$rates = 0;
					foreach($rate as $rat){
						$rates = $rates + $rat->rate;
					}
					$rates = $rates/count($rate);
				}else{
					$rates = 0;
				}
			$arr[$i]['avg'] = round($rates);
		
				$this->db->select('promo_percent, promo_percent_type, product_title, promo_users, promo_used_count');
				$this->db->where('promo_type', 'On Product Amount');
				$this->db->where('product_title', $pro->product_name);
				$this->db->or_where('promo_category', $pro->product_level);
				
				$subject = explode(',',$pro->product_subject);
				foreach($subject as $subj){
					$this->db->or_where('promo_category', $subj);
				}
				
				$this->db->or_where('promo_category', $pro->product_resource);
				$this->db->limit(1, 0);
				$disc = $this->db->get('promos')->result();
				if(count($disc)){
					if($disc[0]->promo_users > $disc[0]->promo_used_count){
						if($disc[0]->promo_percent_type == '%'){$dis = $pro->product_price * $disc[0]->promo_percent / 100;}else
						if($disc[0]->promo_percent_type == 'S$'){$dis = $disc[0]->promo_percent;}else{$dis = 0;}
						$prod = $disc[0]->product_title;
					}else{
						$dis = 0;
						$prod = '';
					}
				}else{
					$dis = 0;
					$prod = '';
				}
			$arr[$i]['disc'] = $dis;
			$arr[$i]['prod'] = $prod;
			$i++;
		}
		return $arr;
	}
	
	public function my_limit($num, $start, $id){
		$this->db->where('user_id', $id);
		$this->db->where('product_status', 'Active');
		$this->db->limit($num, $start);
		if(!count($this->db->ar_orderby)){
			$this->db->order_by($this->_order_by, 'desc');
		}
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		foreach($query as $pro){
			$arr[$i]['pro'] = $pro;
				$this->db->select('rate');
				$this->db->order_by('modified', 'Desc');
				$this->db->where('product_unique', $pro->product_unique);
				$rate = $this->db->get('reviews')->result();
				if(count($rate)){
					$rates = 0;
					foreach($rate as $rat){
						$rates = $rates + $rat->rate;
					}
					$rates = $rates/count($rate);
				}else{
					$rates = 0;
				}
			$arr[$i]['avg'] = round($rates);
		
				$this->db->select('promo_percent, promo_percent_type, product_title, promo_users, promo_used_count');
				$this->db->where('promo_type', 'On Product Amount');
				$this->db->where('product_title', $pro->product_name);
				$this->db->or_where('promo_category', $pro->product_level);
				
				$subject = explode(',',$pro->product_subject);
				foreach($subject as $subj){
					$this->db->or_where('promo_category', $subj);
				}
				
				$this->db->or_where('promo_category', $pro->product_resource);
				$this->db->limit(1, 0);
				$disc = $this->db->get('promos')->result();
				if(count($disc)){
					if($disc[0]->promo_users > $disc[0]->promo_used_count){
						if($disc[0]->promo_percent_type == '%'){$dis = $pro->product_price * $disc[0]->promo_percent / 100;}else
						if($disc[0]->promo_percent_type == 'S$'){$dis = $disc[0]->promo_percent;}else{$dis = 0;}
						$prod = $disc[0]->product_title;
					}else{
						$dis = 0;
						$prod = '';
					}
				}else{
					$dis = 0;
					$prod = '';
				}
			$arr[$i]['disc'] = $dis;
			$arr[$i]['prod'] = $prod;
			$i++;
		}
		//echo '<pre>';print_r($arr);echo '</pre>';
		return $arr;
	}
	
	public function top_seller($num, $start, $order, $order1){
		$this->db->limit($num, $start);
		$this->db->order_by($order, 'desc');
		$this->db->order_by($order1, 'desc');
		$this->db->where('product_status', 'Active');
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		foreach($query as $pro){
			$arr[$i]['pro'] = $pro;
				$this->db->select('promo_percent, promo_percent_type, product_title, promo_users, promo_used_count');
				$this->db->where('promo_type', 'On Product Amount');
				$this->db->where('product_title', $pro->product_name);
				$this->db->or_where('promo_category', $pro->product_level);
				
				$subject = explode(',',$pro->product_subject);
				foreach($subject as $subj){
					$this->db->or_where('promo_category', $subj);
				}
				
				$this->db->or_where('promo_category', $pro->product_resource);
				$this->db->limit(1, 0);
				$disc = $this->db->get('promos')->result();
				if(count($disc)){
					if($disc[0]->promo_users > $disc[0]->promo_used_count){
						if($disc[0]->promo_percent_type == '%'){$dis = $pro->product_price * $disc[0]->promo_percent / 100;}else
						if($disc[0]->promo_percent_type == 'S$'){$dis = $disc[0]->promo_percent;}else{$dis = 0;}
					}else{
						$dis = 0;
					}
				}else{
					$dis = 0;
				}
			$arr[$i]['disc'] = $dis;
			$i++;
		}
		return $arr;
		//return $this->db->get($this->_table_name)->result();
	}
	
	public function to_seller($id, $num, $start){
		$this->db->where('user_id', $id['user_id']);
		if(!count($this->db->ar_orderby)){
			$this->db->order_by($this->_order_by, 'desc');
		}
		$this->db->where('product_status', 'Active');
		$this->db->limit($num, $start);
		$query = $this->db->get($this->_table_name)->result();
		$arr = array();
		$i=0;
		foreach($query as $pro){
			$arr[$i]['pro'] = $pro;
				$this->db->select('rate');
				$this->db->order_by('modified', 'Desc');
				$this->db->where('product_unique', $pro->product_unique);
				$rate = $this->db->get('reviews')->result();
				if(count($rate)){
					$rates = 0;
					foreach($rate as $rat){
						$rates = $rates + $rat->rate;
					}
					$rates = $rates/count($rate);
				}else{
					$rates = 0;
				}
			$arr[$i]['avg'] = round($rates);
		
				$this->db->select('promo_percent, promo_percent_type, product_title, promo_users, promo_used_count');
				$this->db->where('promo_type', 'On Product Amount');
				$this->db->where('product_title', $pro->product_name);
				$this->db->or_where('promo_category', $pro->product_level);
				
				$subject = explode(',',$pro->product_subject);
				foreach($subject as $subj){
					$this->db->or_where('promo_category', $subj);
				}
				
				$this->db->or_where('promo_category', $pro->product_resource);
				$this->db->limit(1, 0);
				$disc = $this->db->get('promos')->result();
				if(count($disc)){
					if($disc[0]->promo_users > $disc[0]->promo_used_count){
						if($disc[0]->promo_percent_type == '%'){$dis = $pro->product_price * $disc[0]->promo_percent / 100;}else
						if($disc[0]->promo_percent_type == 'S$'){$dis = $disc[0]->promo_percent;}else{$dis = 0;}
						$prod = $disc[0]->product_title;
					}else{
						$dis = 0;
						$prod = '';
					}
				}else{
					$dis = 0;
					$prod = '';
				}
			$arr[$i]['disc'] = $dis;
			$arr[$i]['prod'] = $prod;
			$i++;
		}
		//echo '<pre>';print_r($arr);echo '</pre>';
		return $arr;
	}
	
	public function get_by_owner($id){
		$this->db->select($this->_table_name.".*, users.user_username, users.user_fname, users.user_email");
		$this->db->join('users', 'users.user_id = '.$this->_table_name.'.user_id');
		$this->db->where($this->_table_name.'.product_unique', $id);
		$this->db->where($this->_table_name.'.product_status', 'Active');
		$this->db->limit(1);
		return $this->db->get($this->_table_name)->result();
	}
}