<?php
class Site_m extends MY_Model{
	protected $_table_name = 'site';
	protected $_primary_key = 'site_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'email';
	protected $_riles = array();
	protected $_timestamps = FALSE;
}