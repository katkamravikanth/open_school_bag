<?php
class Profile_m extends MY_Model{
	protected $_table_name = 'user_profile';
	protected $_primary_key = 'user_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'user_id';
	protected $_riles = array();
	protected $_timestamps = FALSE;
	
	//save or update records
	public function p_save($data){
		$this->db->set($data);
		$this->db->insert($this->_table_name);
		$id = $this->db->insert_id();
		return $id;
	}
	
	function delete_user($id){
		$this->db->where('user_id', $id);
		$this->db->delete($this->_table_name);
	}
}