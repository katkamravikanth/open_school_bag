<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reports extends Frontend_Controller {
	
    public function __construct(){
        parent::__construct();
        if (isset($_SERVER['HTTP_REFERER'])){
            $this->session->set_userdata('prev_url', $_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_userdata('prev_url', base_url());
        }
		//load our new PHPExcel library
		$this->load->library('excel');
		
		$this->load->model('admin/images_model');
		$this->load->model('orders_m');
		$this->load->model('product_m');
		$this->load->model('users_m');
		$this->load->model('profile_m');
    }
	
	function index(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		$this->load->library('pagination');
		$config['per_page'] = 15;
		
		if ($this->input->get('generate')){
			$first = $this->input->get('from');
			$second = date_create($this->input->get('to'));
			date_add($second, date_interval_create_from_date_string('1 days'));
			$second = date_format($second, 'Y-m-d');
			$by = $this->input->get('report_by');
			
			if($by == 'Downloads'){
				redirect('reports/'.$by.'/from/'.$first.'/to/'.$second);
			}else if($by == 'Sales'){
				redirect('reports/'.$by.'/from/'.$first.'/to/'.$second);
			}else if($by == 'Products'){
				redirect('reports/'.$by.'/from/'.$first.'/to/'.$second);
			}
		}
		if($this->uri->segment(2)){
			$config["uri_segment"] = 7;
			$page = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			
			$first = $this->uri->segment(4);
			$second = $this->uri->segment(6);
			$by = $this->uri->segment(2);
			
			if($by == 'Products'){
				$data['prd'] = $by;
				$config['total_rows'] = $this->product_m->s_total_no($user->user_id, $first, $second);
				$data['orders'] = $this->product_m->prod_by_limit($config["per_page"], $page, $user->user_id, $first, $second);
				
				$data['link'] = '<a href="reports/products_report/?from='.$first.'&to='.$second.'&report_by='.$by.'" target="_blank"><h1 style="border:0;background:none;margin-top:-3px;">Export</h1></a>';
			}else if($by == 'Sales'){
				
				$data['prd'] = $by;
				$config['total_rows'] = $this->orders_m->sales_total_no($user->user_username, $first, $second);
				$data['orders'] = $this->orders_m->sales_by_limit($config["per_page"], $page, $user->user_username, $first, $second);
				
				$data['link'] = '<a href="reports/sales_report/?from='.$first.'&to='.$second.'&report_by='.$by.'" target="_blank"><h1 style="border:0;background:none;margin-top:-3px;">Export</h1></a>';
			}else if($by == 'Downloads'){
				
				$data['prd'] = $by;
				$config['total_rows'] = $this->orders_m->search_total_no($user->user_username, $first, $second);
				$data['orders'] = $this->orders_m->down_by_limit($config["per_page"], $page, $user->user_username, $first, $second);
				
				$data['link'] = '<a href="reports/downloads_report/?from='.$first.'&to='.$second.'&report_by='.$by.'" target="_blank"><h1 style="border:0;background:none;margin-top:-3px;">Export</h1></a>';
			}
			
			$config['base_url'] = base_url().'reports/'.$by.'/from/'.$first.'/to/'.$second;
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] = round($choice);
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';
			$config['next_link'] = '&gt;&gt;';
			$config['prev_link'] = '&lt;&lt;';
			
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
		}
		
		$data['title'] = 'Reports';
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('reports', $data);
		$this->load->view('templates/footer', $data);
	}
	
	function sales_report(){
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		$first = $this->input->get('from');
		$second = date_create($this->input->get('to'));
		date_add($second, date_interval_create_from_date_string('1 days'));
		$second = date_format($second, 'Y-m-d');
		
		$products = $this->orders_m->sales_by_limit('','',$user->user_username, $first, $second);
		
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Sales Report');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Product Name');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Order Date');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Buyer');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Product Type');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Product Price');
		$i = 2;
		$j = 1;
		foreach($products as $product){//echo '<pre>';print_r($product);echo '</pre>';
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $product->order_pname);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, $product->created);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $product->user_id);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $product->order_ptype);
			if($product->order_promo == ''){
				$this->excel->getActiveSheet()->setCellValue('F'.$i, number_format($product->order_pprice - $product->order_discount, 2));
			}else{
				$this->excel->getActiveSheet()->setCellValue('F'.$i, number_format($product->order_pprice, 2));
			}
			$i++;
			$j++;
		}
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='Sales_Report.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
		redirect($this->session->userdata('prev_url'), 'refresh');
	}
	
	function orders_report(){
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		$first = $this->input->get('from');
		$second = date_create($this->input->get('to'));
		date_add($second, date_interval_create_from_date_string('1 days'));
		$second = date_format($second, 'Y-m-d');
		
		$products = $this->orders_m->ord_by_limit('','',$user->user_username, $first, $second);
		
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Orders Report');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Order No.');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Product Name');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Product Type');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Product Price');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Order Status');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Order Date');
		$i = 2;
		$j = 1;
		foreach($products as $product){//echo '<pre>';print_r($product);echo '</pre>';
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $product->order_no);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, $product->order_pname);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $product->order_ptype);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, number_format($product->order_pprice - $product->order_discount, 2));
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $product->order_status);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $product->created);
			$i++;
			$j++;
		}
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='Orders_Report('.$first.'to'.$second.').xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	
	function downloads_report(){
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		$first = $this->input->get('from');
		$second = date_create($this->input->get('to'));
		date_add($second, date_interval_create_from_date_string('1 days'));
		$second = date_format($second, 'Y-m-d');
		
		$products = $this->orders_m->down_by_limit('','',$user->user_username, $first, $second);
		
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Downloads Report');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Order No.');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Product Name');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Product Type');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Product Price');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Order Status');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Download Count');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Order Date');
		$i = 2;
		$j = 1;
		foreach($products as $product){//echo '<pre>';print_r($product);echo '</pre>';
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $product->order_no);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, $product->order_pname);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $product->order_ptype);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, number_format($product->order_pprice, 2));
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $product->order_status);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $product->down_count);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $product->created);
			$i++;
			$j++;
		}
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='Downloads_Report.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	
	function products_report(){
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		$first = $this->input->get('from');
		$second = date_create($this->input->get('to'));
		date_add($second, date_interval_create_from_date_string('1 days'));
		$second = date_format($second, 'Y-m-d');
		$by = $this->input->get('report_by');
		
		$products = $this->product_m->prod_by_limit('','', $user->user_id, $first, $second);
		
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Products Report');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Product Name');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Product Type');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Product Level');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Product Subject');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Product Resource');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Product Price');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Product Pages');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Product Weight');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Product Desclaimer');
		$this->excel->getActiveSheet()->setCellValue('K1', 'No of Orders');
		$this->excel->getActiveSheet()->setCellValue('L1', 'No of Downloads');
		$this->excel->getActiveSheet()->setCellValue('M1', 'Product Status');
		$this->excel->getActiveSheet()->setCellValue('N1', 'Product Created');
		$i = 2;
		$j = 1;
		foreach($products as $product){//echo '<pre>';print_r($product);echo '</pre>';
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, stripslashes($product->product_name));
			$this->excel->getActiveSheet()->setCellValue('C'.$i, $product->product_type);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $product->product_level);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $product->product_subject);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $product->product_resource);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, number_format($product->product_price, 2));
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $product->product_pages);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $product->product_weight);
			$this->excel->getActiveSheet()->setCellValue('J'.$i, $product->product_desclaimer);
			$this->excel->getActiveSheet()->setCellValue('K'.$i, $product->order_count);
			$this->excel->getActiveSheet()->setCellValue('L'.$i, $product->down_count);
			$this->excel->getActiveSheet()->setCellValue('M'.$i, $product->product_status);
			$this->excel->getActiveSheet()->setCellValue('N'.$i, $product->created);
			$i++;
			$j++;
		}
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('K1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('L1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('M1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('N1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('N1')->getFont()->setBold(true);
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='Products_Report.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
		//$objWriter->save($fileName);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */