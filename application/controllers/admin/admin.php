<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {
	
    public function __construct(){
        parent::__construct();
        if (isset($_SERVER['HTTP_REFERER'])){
            $this->session->set_userdata('prev_url', $_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_userdata('prev_url', base_url());
        }
		$this->load->model('users_m');
		$this->load->model('profile_m');
		$this->load->model('site_m');
		$this->load->model('note_m');
    }
	
	function index(){
		if($this->session->userdata('is_logged_in')){
			if ( ! file_exists('application/views/admin/dashboard.php')){
				// Whoops, we don't have a page for that!
				show_404();
			}
			$users = $this->users_m->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
			$data['notif'] = $this->note_m->tot_no($users->user_id);
			
			$data['title'] = 'Welcome';
			$data['profile'] = $this->profile_m->get_by(array('user_id' => $users->user_id), 1);
			
			$this->load->view('admin/templates/header', $data);
			$this->load->view('admin/templates/navigation');
			$this->load->view('admin/dashboard');
			$this->load->view('admin/templates/footer', $data);
			//redirect('admin');
        }else{
        	$this->load->view('admin/login');
        }
	}

    /**
    * check the username and the password with the database
    * @return void
    */
	function validate_credentials(){	


		$user_name = $this->input->post('user_name');
		$password = hash('sha512', $this->input->post('password'));

		$is_valid = $this->users_m->get_by(array('user_username' => $user_name, 'user_password' => $password, 'user_status' => 1, 'user_level' => 9), 1);
		
		if($is_valid)
		{
			$data = array(
				'user_name' => $user_name,
				'is_logged_in' => true
			);
			$this->session->set_userdata($data);
			redirect('admin');
		}
		else // incorrect username or password
		{
			$data['message_error'] = TRUE;
			$this->load->view('admin/login', $data);	
		}
	}
	
	public function forgot(){
		$email = mysql_real_escape_string($this->input->post('email'));
		$user = $this->users_m->get_by(array('user_email' => $email, 'user_level' => 9), 1);
		if(count($user)){
			$id = $user->user_id;
			$password = $this->users_m->pword_string(12);
			
			$forgoted = $this->users_m->save(array('user_password' => hash('sha512', $password)), $id);
			
			$subject = 'New Password from OpenSchoolbag';
			$message = '<p>Please find the new password for your account below<br />
			<br />Password: <strong>'.$password.'</strong></p><br /><br />
			<p>Thanks and Regards<br />
			<strong>OpenSchoolbag Team</strong></p>';
			
			$this->users_m->email($email, $subject, $message);
			if($forgoted){
				print 0;
			}else{
				print 1;
			}
		}else{
			print 1;
		}
	}
	
	public function notifications(){
		if(!$this->session->userdata('is_logged_in')){
			redirect($this->session->userdata('prev_url'), 'refresh');
		}
		$users = $this->users_m->get_by(array('user_username' => $this->session->userdata('user_name'), 'user_level'=>9), 1);
		
		$this->note_m->make_read($users->user_id);
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'admin/notifications';
		$config['total_rows'] = $this->note_m->tot_no_note($users->user_id);
		$config['per_page'] = 9;
		//$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		$this->pagination->initialize($config); 
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
		$data['title'] = 'Notifications';
		$data['note'] = $this->note_m->get_by_limit($config["per_page"], $page, '', array('to_user_id' => $users->user_id));
		
		$this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
		$this->load->view('admin/notifications');
		$this->load->view('admin/templates/footer', $data);
	}
	
	public function settings(){
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
            $this->form_validation->set_rules('email', 'Email Address', 'required');
            $this->form_validation->set_rules('phone', 'Phone No.', 'required');
            $this->form_validation->set_rules('margin', 'Margin', 'required');
            $this->form_validation->set_rules('trans_fee', 'Transaction Fee', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run()){
                $data_to_store = array(
                    'email' => $this->input->post('email'),
                    'phone' => $this->input->post('phone'),
                    'margin' => $this->input->post('margin'),
					'trans_fee' => $this->input->post('trans_fee'),
                );
                //if the insert has returned true then we show the flash message
                if($this->site_m->save($data_to_store, 1)){
                    $data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //load the view
		$data['title'] = 'Site Settings';
		$data['user'] = $users = $this->users_m->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
		$data['site'] = $this->site_m->get_by(array('site_id' => 1), 1);
		$this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
		$this->load->view('admin/settings/settings');
		$this->load->view('admin/templates/footer', $data);
	}
	
	public function profile(){
		if(!$this->session->userdata('is_logged_in')){
			redirect($this->session->userdata('prev_url'), 'refresh');
		}
		$data['title'] = 'Update Profile';
		$data['user'] = $users = $this->users_m->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
		$this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
		$this->load->view('admin/settings/configu');
		$this->load->view('admin/templates/footer', $data);
	}
	
	public function config(){
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
            $this->form_validation->set_rules('fname', 'First Name', 'required');
            $this->form_validation->set_rules('lname', 'Last Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('telephone', 'Phone', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run()){
				$now = date('Y-m-d H:i:s');
				$uid = $this->input->post('uid');
                $data_to_store = array(
                    'user_username' => $this->input->post('fname').' '.$this->input->post('lname'),
                    'user_email' => $this->input->post('email'),
                    'user_phone' => $this->input->post('telephone'),
                );
                //if the insert has returned true then we show the flash message
                if($this->users_m->save($data_to_store, $uid)){
                    $data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }
            }
        }
        //load the view
		$data['title'] = 'Update Profile';
		$data['user'] = $users = $this->users_m->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		$this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
		$this->load->view('admin/settings/configu');
		$this->load->view('admin/templates/footer', $data);
	}
	
	public function cpass(){
		if(!$this->session->userdata('is_logged_in')){
			redirect(site_url("admin"));
		}
		$data['title'] = 'Change Password';
		$users = $this->users_m->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
		$this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
		$this->load->view('admin/settings/cpass');
		$this->load->view('admin/templates/footer', $data);
	}
	
	public function cpword(){
		$cpass = hash('sha512', mysql_real_escape_string($this->input->post('cpass')));
		$newpass = hash('sha512', mysql_real_escape_string($this->input->post('newpass')));
		
		$user = $this->users_m->get_by(array('user_password' => $cpass, 'user_username' => $this->session->userdata('user_name')), 1);
		
		if(count($user)){
			$id = $this->users_m->save(array('user_password' => $newpass), $user->user_id);
			if($id != ''){
				print $id;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
	
	/**
    * Destroy the session, and logout the user.
    * @return void
    */
	function logout()
	{
		$this->session->sess_destroy();
		redirect('admin');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */