<?php
class Admin_cms extends Admin_Controller {

    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/cms_model');
		$this->load->model('admin/users_model');
		$this->load->model('note_m');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index(){

        //all the posts sent by the view
        $search_string = $this->input->post('search_string');        
        $order = $this->input->post('order'); 
        $order_type = $this->input->post('order_type'); 

        //pagination settings
        $config['per_page'] = 6;

        $config['base_url'] = base_url().'admin/cms/pages';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        //limit end
        $page = $this->uri->segment(4);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        } 

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';    
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;        


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($search_string !== false && $order !== false || $this->uri->segment(3) == true){ 
           
            /*
            The comments here are the same for line 79 until 99

            if post is not null, we store it in session data array
            if is null, we use the session data already stored
            we save order into the the var to load the view with the param already selected       
            */
            if($search_string){
                $filter_session_data['search_string_selected'] = $search_string;
            }else{
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if($order){
                $filter_session_data['order'] = $order;
            }
            else{
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;

            //save session data into the session
            if(isset($filter_session_data)){
              $this->session->set_userdata($filter_session_data);    
            }
            
            //fetch sql data into arrays
            $data['count_cms']= $this->cms_model->count_cms($search_string, $order);
            $config['total_rows'] = $data['count_cms'];

            //fetch sql data into arrays
            if($search_string){
                if($order){
                    $data['pages'] = $this->cms_model->get_cms($search_string, $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['pages'] = $this->cms_model->get_cms($search_string, '', $order_type, $config['per_page'],$limit_end);           
                }
            }else{
                if($order){
                    $data['pages'] = $this->cms_model->get_cms('', $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['pages'] = $this->cms_model->get_cms('', '', $order_type, $config['per_page'],$limit_end);        
                }
            }

        }else{

            //clean filter data inside section
            $filter_session_data['manufacture_selected'] = null;
            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
            $filter_session_data['order_type'] = null;
            $this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';
            $data['order'] = 'user_id';

            //fetch sql data into arrays
            $data['count_cms']= $this->cms_model->count_cms();
            $data['pages'] = $this->cms_model->get_cms('', '', $order_type, $config['per_page'],$limit_end);        
            $config['total_rows'] = $data['count_cms'];

        }//!isset($search_string) && !isset($order)
         
        //initializate the panination helper 
        $this->pagination->initialize($config);   

        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Manage CMS Pages';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/cms/list', $data);
        $this->load->view('admin/templates/footer');

    }//index

    public function add(){
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){

            //form validation
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('body', 'Body', 'required');
            $this->form_validation->set_rules('slug', 'URL', 'required');
            $this->form_validation->set_rules('order', 'Order', 'required|is_natural');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run()){
                $data_to_store = array(
                    'title' => $this->input->post('title'),
                    'slug' => $this->input->post('slug'),
                    'order' => $this->input->post('order'),
                    'body' => $this->input->post('body')
                );
                //if the insert has returned true then we show the flash message
                if($this->cms_model->save($data_to_store)){
                    $data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Add Page';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/cms/add');
        $this->load->view('admin/templates/footer');
    }

    public function update(){
		$id = $this->uri->segment(4);
		
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){

            //form validation
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('body', 'Body', 'required');
            $this->form_validation->set_rules('slug', 'URL', 'required');
            $this->form_validation->set_rules('order', 'Order', 'required|is_natural');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run()){
				$uid = $this->input->post('uid');
                $data_to_store = array(
                    'title' => $this->input->post('title'),
                    'slug' => $this->input->post('slug'),
                    'order' => $this->input->post('order'),
                    'body' => $this->input->post('body')
                );
                //if the insert has returned true then we show the flash message
                if($this->cms_model->save($data_to_store, $uid)){
                    $data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Update Page';
		$data['page'] = $this->cms_model->get_by(array('cms_id' => $id));
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/cms/edit');
        $this->load->view('admin/templates/footer');
    }
	
	public function page_slug(){
		$slug = mysql_real_escape_string($this->input->post('slug'));
		if($this->input->post('id')){
			$id = $this->input->post('id');
			$data = array('slug' => $slug, 'cms_id !=' => $id);
		}else{
			$data = array('slug' => $slug);
		}
		$users = $this->cms_model->get_by($data, 1);
		if(!count($users)){
			return FALSE;
		}else{
			print 1;
		}
	}

    /**
    * Delete product by his id
    * @return void
    */
    public function delete(){
        //product id 
        $id = $this->uri->segment(4);
        $ids = $this->cms_model->delete_page($id);
		if($ids){
			redirect('admin/cms/pages/state/yes');
		}else{
			redirect('admin/cms/pages/state/no');
		}
    }//edit

}