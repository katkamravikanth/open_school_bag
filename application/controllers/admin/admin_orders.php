<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_orders extends Admin_Controller {
	
    public function __construct(){
        parent::__construct();
        if (isset($_SERVER['HTTP_REFERER'])){
            $this->session->set_userdata('prev_url', $_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_userdata('prev_url', base_url());
        }
		//load our new PHPExcel library
		$this->load->library('excel');
		$this->load->model('admin/orders_model');
		$this->load->model('orders_m');
		$this->load->model('product_m');
		$this->load->model('users_m');
		$this->load->model('profile_m');
		$this->load->model('note_m');
    }
	
	function index(){
		if(!$this->session->userdata('is_logged_in')){
			redirect('admin');
		}
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'admin/orders/';
		$config['total_rows'] = $this->orders_model->sales_total_no();
		$config['per_page'] = 15;
		$config["uri_segment"] = 3;
		
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		$data['orders'] = $this->orders_model->sales_limit($config["per_page"], $page);
		$data['link'] = '<a href="admin/order_report" target="_blank" style="color:#fff"><h1 style="border:0;background:none;">Export</h1></a>';
		
		
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		
		$data['title'] = 'Orders';
		$users = $this->users_m->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
		$this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
		$this->load->view('admin/orders');
		$this->load->view('admin/templates/footer', $data);
	}
	
	function search(){
		if(!$this->session->userdata('is_logged_in')){
			redirect('admin');
		}
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		$first = $this->input->get('from');
		$second = $this->input->get('to');
		
		if($first != '' && $second != ''){
			redirect('admin/orders/search/from/'.$first.'/to/'.$second);
		}
		
		$first = $this->uri->segment(5);
		$second = date_create($this->uri->segment(7));
		date_add($second, date_interval_create_from_date_string('1 days'));
		$second = date_format($second, 'Y-m-d');
		
		$page = ($this->uri->segment(8)) ? $this->uri->segment(8) : 0;
		
		$this->load->library('pagination');
		$config['total_rows'] = $this->orders_model->sales_total_no('', $first, $second);
		$config['base_url'] = base_url().'admin/orders/search/from/'.$first.'/to/'.$second;
		$config['per_page'] = 9;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		$data['orders'] = $this->orders_model->sales_date($config["per_page"], $page, $first, $second);
		$data['link'] = '<a href="admin/order_report/?from='.$first.'&to='.$second.'" target="_blank" style="color:#fff"><h1 style="border:0;background:none;">Export</h1></a>';
		
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		
		$data['title'] = 'Orders';
		$users = $this->users_m->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
		$this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
		$this->load->view('admin/orders');
		$this->load->view('admin/templates/footer', $data);
	}
	
	function pname(){
		if(!$this->session->userdata('is_logged_in')){
			redirect('admin');
		}
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		if($this->input->get('pname')){
			redirect('admin/orders/pname/'.$this->input->get('pname'));
		}
		
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		
		$pname = $this->uri->segment(4);
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'admin/orders/pname/'.$pname;
		$config['total_rows'] = $this->orders_model->sales_total_no('', '', '', '', $pname);
		$config['per_page'] = 9;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$config["uri_segment"] = 3;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';

		//if searched by product name
		$data['link'] = '<a href="admin/order_report?pname='.$pname.'" target="_blank" style="color:#fff"><h1 style="border:0;background:none;">Export</h1></a>';
		$data['orders'] = $this->orders_model->sales_pname($config["per_page"], $page, $pname);
		
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		
		$data['title'] = 'Orders';
		$users = $this->users_m->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
		$this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
		$this->load->view('admin/orders');
		$this->load->view('admin/templates/footer', $data);
	}
	
	function view(){
		if(!$this->session->userdata('is_logged_in')){
			redirect('admin');
		}
		$order_no = $this->uri->segment(3);
		
		$data['title'] = 'Orders';
		$data['orders'] = $this->orders_model->order_view($order_no);
		
		$users = $this->users_m->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
		$this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
		$this->load->view('admin/order_view');
		$this->load->view('admin/templates/footer', $data);
	}
	
	function orders_report(){
		$first = $this->input->get('from');
		$second = date_create($this->input->get('to'));
		date_add($second, date_interval_create_from_date_string('1 days'));
		$second = date_format($second, 'Y-m-d');
		
		if($this->input->get('pname')){
			$pname = $this->input->get('pname');
			$products = $this->orders_model->sales_pname_report($pname);
		}else{
			$products = $this->orders_model->sales_date_report($first, $second);
		}
		
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Orders Report');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Order No.');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Order Date');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Product Name');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Buyer');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Seller');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Product Price');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Product Type');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Order Status');
		$i = 2;
		$j = 1;
		foreach($products as $product){//echo '<pre>';print_r($product);echo '</pre>';
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $product['ord']->order_order_no);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, $product['ord']->created);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $product['ord']->order_pname);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $product['ord']->user_username);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $product['usr']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $product['ord']->order_pprice);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $product['ord']->order_ptype);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $product['ord']->order_status);
			$i++;
			$j++;
		}
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='Orders_Report.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */