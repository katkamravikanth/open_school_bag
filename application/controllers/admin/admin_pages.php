<?php
class Admin_pages extends Admin_Controller {

    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
		$this->load->model('admin/users_model');
		$this->load->model('note_m');
        $this->load->model('page_m');
        $this->load->model('partner_m');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
	
    public function about(){
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){
            //form validation
            $this->form_validation->set_rules('body', 'Section 1', 'required');
            $this->form_validation->set_rules('body1', 'Section 2', 'required');
            $this->form_validation->set_rules('body2', 'Why OpenSchoolbag?', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run()){
				$id = $this->input->post('page_id');
                $data_to_store = array(
                    'body' => $this->input->post('body'),
                );
				$id1 = $this->input->post('page_id1');
               	$data_to = array(
                   	'body' => $this->input->post('body1'),
               	);
				$this->page_m->save($data_to, $id1);
				
				$id2 = $this->input->post('page_id2');
               	$data_to = array(
                   	'body' => $this->input->post('body2'),
               	);
				$this->page_m->save($data_to, $id2);
                //if the insert has returned true then we show the flash message
                if($this->page_m->save($data_to_store, $id) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/page/about');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data
       	$data['page'] = $this->page_m->get_by(array('slug' => 'about'));
		$data['page1'] = $this->page_m->get_by(array('slug' => 'about1'));
		$data['page2'] = $this->page_m->get_by(array('slug' => 'why_osb'));
        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Update About Us';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/page/about', $data);
        $this->load->view('admin/templates/footer');
    }
	
    public function partners(){
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){
            //form validation
            $this->form_validation->set_rules('body', 'Body', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run()){
				$id = $this->input->post('page_id');
                $data_to_store = array(
                    'body' => $this->input->post('body'),
                );
                //if the insert has returned true then we show the flash message
                if($this->page_m->save($data_to_store, $id) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/page/partners');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data
		$data['page'] = $this->page_m->get_by(array('slug' => 'partners'));
        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Update Partners';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/page/partners', $data);
        $this->load->view('admin/templates/footer');
    }
	
    public function why_osb(){
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){
            //form validation
            $this->form_validation->set_rules('body', 'Body', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run()){
				$id = $this->input->post('page_id');
                $data_to_store = array(
                    'body' => $this->input->post('body'),
                );
                //if the insert has returned true then we show the flash message
                if($this->page_m->save($data_to_store, $id) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/page/why_osb');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data
		$data['page'] = $this->page_m->get_by(array('slug' => 'why_osb'));
        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Why OpenSchoolBag';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/page/why_osb', $data);
        $this->load->view('admin/templates/footer');
    }
	
    public function faqs(){
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){
            //form validation
            $this->form_validation->set_rules('body', 'Body', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run()){
				$id = $this->input->post('page_id');
                $data_to_store = array(
                    'body' => $this->input->post('body'),
                );
                //if the insert has returned true then we show the flash message
                if($this->page_m->save($data_to_store, $id) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/page/faqs');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data
		$data['page'] = $this->page_m->get_by(array('slug' => 'faqs'));
        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = "FAQs";
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/page/faqs', $data);
        $this->load->view('admin/templates/footer');
    }
	
    public function privacy_policy(){
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){
            //form validation
            $this->form_validation->set_rules('body', 'Body', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run()){
				$id = $this->input->post('page_id');
                $data_to_store = array(
                    'body' => $this->input->post('body'),
                );
                //if the insert has returned true then we show the flash message
                if($this->page_m->save($data_to_store, $id) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/page/privacy_policy');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data
		$data['page'] = $this->page_m->get_by(array('slug' => 'privacy_policy'));
        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Privacy Policy';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/page/privacy_policy', $data);
        $this->load->view('admin/templates/footer');
    }
	
    public function tnc(){
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){
            //form validation
            $this->form_validation->set_rules('body', 'Body', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run()){
				$id = $this->input->post('page_id');
                $data_to_store = array(
                    'body' => $this->input->post('body'),
                );
                //if the insert has returned true then we show the flash message
                if($this->page_m->save($data_to_store, $id) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/page/tnc');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data
		$data['page'] = $this->page_m->get_by(array('slug' => 'tnc'));
        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Terms and Conditions';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/page/tnc', $data);
        $this->load->view('admin/templates/footer');
    }
}