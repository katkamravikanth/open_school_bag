<?php
class Admin_categories extends Admin_Controller {
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/users_model');
        $this->load->model('admin/categories_model');
        $this->load->model('product_m');
        $this->load->model('note_m');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function levels(){
		
        $this->load->library('pagination');
		$config['base_url'] = base_url().'admin/categories/levels';
        $config['total_rows'] = $this->categories_model->t_levels();
		$config["num_links"] = 20;
		$config['per_page'] = 15;
		$config["uri_segment"] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
        //initializate the panination helper 
        $this->pagination->initialize($config);
		
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
        //fetch sql data into arrays
        $data['levels'] = $this->categories_model->levels($config['per_page'], $page);
		
        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Levels';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/categories/levels', $data);
        $this->load->view('admin/templates/footer');  

    }
    public function subjects(){
		
        $this->load->library('pagination');
		$config['base_url'] = base_url().'admin/categories/subjects';
        $config['total_rows'] = $this->categories_model->t_subjects();
		$config["num_links"] = 20;
		$config['per_page'] = 15;
		$config["uri_segment"] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
        //initializate the panination helper 
        $this->pagination->initialize($config);
		
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
        //fetch sql data into arrays
        $data['subjects'] = $this->categories_model->subjects($config['per_page'], $page);
		
        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Subjects';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/categories/subjects', $data);
        $this->load->view('admin/templates/footer');  

    }
    public function resources(){
		
        $this->load->library('pagination');
		$config['base_url'] = base_url().'admin/categories/resources';
        $config['total_rows'] = $this->categories_model->t_resources();
		$config["num_links"] = 20;
		$config['per_page'] = 5;
		$config["uri_segment"] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
        //initializate the panination helper 
        $this->pagination->initialize($config);
		
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
        //fetch sql data into arrays
        $data['resources'] = $this->categories_model->resources($config['per_page'], $page);
		
        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Resources';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/categories/resources', $data);
        $this->load->view('admin/templates/footer');  

    }
	
	//save
	public function s_level(){
		$level = $this->input->post('level');
		
		return $this->categories_model->s_level(array('level' => $level));
	}
	public function s_subject(){
		$subject = $this->input->post('subject');
		
		return $this->categories_model->s_subject(array('subjects' => $subject));
	}
	public function s_resource(){
		$resource = $this->input->post('resource');
		$icon = '';
		if($this->session->userdata('ppic')){
			$icon = mysql_real_escape_string($this->session->userdata('ppic'));
		}
		
		return $this->categories_model->s_resource(array('resources' => $resource, 'icon' => $icon));
	}
	
	//edit
	public function e_level(){
		$id = $this->input->post('id');
		$level = $this->input->post('level');
		
		$id = $this->categories_model->e_level(array('level' => $level), $id);
		if($id){
			print $id;
		}else{
			return false;
		}
	}
	public function e_subject(){
		$id = $this->input->post('id');
		$subject = $this->input->post('subject');
		
		$id = $this->categories_model->e_subject(array('subjects' => $subject), $id);
		if($id){
			print $id;
		}else{
			return false;
		}
	}
	public function e_resource(){
		$id = $this->input->post('id');
		$resource = $this->input->post('resource');
		$cppic = mysql_real_escape_string($this->input->post('cppic'));
		$icon = '';
		if($this->session->userdata('ppic1')){
			if($cppic != ''){unlink('files/'.$cppic);}
			$icon = mysql_real_escape_string($this->session->userdata('ppic1'));
		}else{
			$icon = $cppic;
		}
		
		$id = $this->categories_model->e_resource(array('resources' => $resource, 'icon' => $icon), $id);
		if($id){
			print $id;
		}else{
			return false;
		}
	}
	
	//subject add
    public function subjects_add(){
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){

            //form validation
            $this->form_validation->set_rules('level', 'Level', 'file_required');
			$this->form_validation->set_rules('subject', 'Subject', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
			
            //if the form has passed through the validation
            if ($this->form_validation->run()){
                $data_to_store = array(
                    'level_id' => $this->input->post('level'),
                    'subjects' => $this->input->post('subject'),
                );
                //if the insert has returned true then we show the flash message
                if($this->categories_model->s_subject($data_to_store)){
					$data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
		$data['levels'] = $this->categories_model->to_levels();
		
        $data['title'] = 'Add Subject';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/categories/subjects_add');
        $this->load->view('admin/templates/footer');
    }
	
	//Resource add
    public function resources_add(){
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){

            //form validation
            $this->form_validation->set_rules('icon', 'Icon', 'file_required');
			$this->form_validation->set_rules('resource', 'Resource', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
			
            //if the form has passed through the validation
            if ($this->form_validation->run()){
				$config['upload_path'] = 'files/icons/';
				$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
				$config['max_size']  = 1024 * 99;
				$config['encrypt_name'] = TRUE;
				/* Load the upload library */
				$this->load->library('upload', $config);
				
				/* Handle the file upload */
				$upload = $this->upload->do_upload('icon');
				/* Get the data about the file */
				$dat = $this->upload->data();
				
				if($dat['is_image'] == 1) {
					$icon = 'icons/'.$dat['file_name'];
				}else{
					$icon = '';
				}
                $data_to_store = array(
                    'icon' => $icon,
                    'resources' => $this->input->post('resource'),
                );
                //if the insert has returned true then we show the flash message
                if($this->categories_model->s_resource($data_to_store)){
					$data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Add Resource';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/categories/resources_add');
        $this->load->view('admin/templates/footer');
    }
	
	//Subject edit
    public function subjects_edit(){
		$id = $this->uri->segment(4);
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){

            //form validation
            $this->form_validation->set_rules('level', 'Level', 'file_required');
			$this->form_validation->set_rules('subject', 'Subject', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
			
            //if the form has passed through the validation
            if ($this->form_validation->run()){
                $data_to_store = array(
                    'level_id' => $this->input->post('level'),
                    'subjects' => $this->input->post('subject'),
                );
                //if the insert has returned true then we show the flash message
                if($this->categories_model->e_subject($data_to_store, $id)){
					$data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //load the view
		$data['subject'] = $this->categories_model->by_subject($id, 1);
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
		$data['levels'] = $this->categories_model->to_levels();
		
        $data['title'] = 'Update Subject';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/categories/subjects_edit');
        $this->load->view('admin/templates/footer');
    }
	
	//resource edit
    public function resources_edit(){
		$id = $this->uri->segment(4);
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){

            //form validation
            $this->form_validation->set_rules('icon', 'Icon', 'file_required');
			$this->form_validation->set_rules('resource', 'Resource', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
			
            //if the form has passed through the validation
            if ($this->form_validation->run()){
				$config['upload_path'] = 'files/icons/';
				$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
				$config['max_size']  = 1024 * 99;
				$config['encrypt_name'] = TRUE;
				/* Load the upload library */
				$this->load->library('upload', $config);
				
				/* Handle the file upload */
				$upload = $this->upload->do_upload('icon');
				/* Get the data about the file */
				$dat = $this->upload->data();
				
				if($dat['is_image'] == 1) {
					$icon = 'icons/'.$dat['file_name'];
				}else{
					$icon = $this->input->post('icons');
				}
                $data_to_store = array(
                    'icon' => $icon,
                    'resources' => $this->input->post('resource'),
                );
                //if the insert has returned true then we show the flash message
                if($this->categories_model->e_resource($data_to_store, $id)){
					$data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //load the view
		$data['resource'] = $this->categories_model->by_resource($id, 1);
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Update Resource';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/categories/resources_edit');
        $this->load->view('admin/templates/footer');
    }

    /**
    * Delete by id
    * @return void
    */
    public function d_level(){
        $id = $this->uri->segment(4);
        $this->categories_model->d_level($id);
        redirect('admin/categories/levels');
    }
    public function d_subjects(){
        $id = $this->uri->segment(4);
        $this->categories_model->d_subjects($id);
        redirect('admin/categories/subjects');
    }
    public function d_resources(){
		$res = $this->categories_model->by_resource($id, 1);
		if(count($res)){
			unlink('files/'.$res->icon);
		}
        $id = $this->uri->segment(4);
        $this->categories_model->d_resources($id);
        redirect('admin/categories/resources');
    }
}