<?php
class Admin_images extends Admin_Controller {

    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/images_model');
		$this->load->model('admin/users_model');
		$this->load->model('note_m');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index(){
        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'admin/images/';
		$config['total_rows'] = $this->images_model->total_images();
		$config['per_page'] = 6;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
        $data['title'] = 'Images';
		$data['banner'] = $this->images_model->by_limit($config["per_page"], $page);
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/images/list', $data);
        $this->load->view('admin/templates/footer');

    }//index

    public function add(){
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){

            //form validation
            $this->form_validation->set_rules('photo', 'Photo', 'file_required');
			$this->form_validation->set_rules('path', 'Path', 'required');
			if($this->input->post('path') == 'product-icon'){
				$this->form_validation->set_rules('title', 'Icon Title', 'required');
			}
            $this->form_validation->set_rules('order', 'Order', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
			
            //if the form has passed through the validation
            if ($this->form_validation->run()){
				if($this->input->post('path') == 'product-icon'){
					$config['upload_path'] = 'files/icons/';
				}else{
					$config['upload_path'] = 'files/banner/';
				}
				$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
				$config['max_size']  = 1024 * 99;
				$config['encrypt_name'] = TRUE;
				/* Load the upload library */
				$this->load->library('upload', $config);
				
				/* Handle the file upload */
				$upload = $this->upload->do_upload('photo');
				/* Get the data about the file */
				$dat = $this->upload->data();
				
				if($dat['is_image'] == 1) {
					if($this->input->post('path') == 'product-icon'){
						$photo = 'icons/'.$dat['file_name'];
					}else{
						$photo = 'banner/'.$dat['file_name'];
					}
				}else{
					$photo = '';
				}
                $data_to_store = array(
                    'photo' => $photo,
                    'path' => $this->input->post('path'),
                    'title' => $this->input->post('title'),
                    'order' => $this->input->post('order'),
                );
                //if the insert has returned true then we show the flash message
                if($this->images_model->save($data_to_store)){
					$data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Add Image';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/images/add');
        $this->load->view('admin/templates/footer');
    }

    public function update(){
		$id = $this->input->post('id');
		$order = $this->input->post('order');
		$arra = array('order' => $order);
		$upd = $this->images_model->save($arra, $id);
		if($upd){
			return TRUE;
		}else{
			return FALSE;
		}
    }

    /**
    * Delete product by his id
    * @return void
    */
    public function delete(){
        //product id 
        $id = $this->uri->segment(4);
        $this->images_model->delete_images($id);
        redirect('admin/images');
    }//edit

}