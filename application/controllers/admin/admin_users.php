<?php
class Admin_users extends Admin_Controller {

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/users';
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/users_model');
        $this->load->model('admin/products_model');
        $this->load->model('admin/orders_model');
		$this->load->model('note_m');
		$this->load->model('users_m');
		$this->load->model('profile_m');
		$this->load->model('partner_m');
		$this->load->model('qna_m');
		$this->load->model('orders_m');
		$this->load->model('review_m');
		$this->load->model('bank_m');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index(){

        //all the posts sent by the view
        $search_string = $this->input->get('search_string');        
        $order = $this->input->get('order');
		
		if($search_string != '' && $order == ''){
			redirect('admin/users/ss/'.$search_string);
		}else if($order != '' && $search_string == ''){
			redirect('admin/users/ord/'.$order);
		}else if($search_string != '' && $order != ''){
			redirect('admin/users/ss/'.$search_string.'/ord/'.$order);
		}
        //pagination settings
        $config['per_page'] = 20;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		if(!$this->uri->segment(5) || ($this->uri->segment(5) && $this->uri->segment(5) != 'ord')){
			if($this->uri->segment(3) == 'ss'){
				//limit end
				$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
				$search_string = urldecode($this->uri->segment(4));
				
				$config['base_url'] = base_url().'admin/users/ss/'.$search_string;
				$config["uri_segment"] = 5;
				
				//fetch sql data into arrays
				$config['total_rows'] = $data['count_products']= $this->users_model->count_users($search_string, '');
                $data['users'] = $this->users_model->get_users($search_string, '', $config['per_page'], $page);
				$data['link'] = '<a href="admin/reports/users_report/?ss='.$search_string.'" target="_blank" style="color:#fff;"><h1 style="border:0;background:none;">Export</h1></a>';
			}else
			if($this->uri->segment(3) == 'ord'){
				//limit end
				$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
				$order = $this->uri->segment(4);
				
				$config['base_url'] = base_url().'admin/users/ord/'.$order;
				$config["uri_segment"] = 5;
				
				//fetch sql data into arrays
				$config['total_rows'] = $data['count_products']= $this->users_model->count_users('', $order);
                $data['users'] = $this->users_model->get_users('', $order, $config['per_page'], $page);
				$data['link'] = '<a href="admin/reports/users_report/?ord='.$order.'" target="_blank" style="color:#fff;"><h1 style="border:0;background:none;">Export</h1></a>';
			}else{
				$config['base_url'] = base_url().'admin/users';
				$config["uri_segment"] = 3;
				//limit end
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				
				//fetch sql data into arrays      
				$config['total_rows'] = $data['count_products'] = $this->users_model->count_users();
				$data['users'] = $this->users_model->get_users('', '', $config['per_page'], $page);
				$data['link'] = '<a href="admin/reports/users_report/?from=2013-09-01&to='.date("Y-m-d").'" target="_blank" style="color:#fff;"><h1 style="border:0;background:none;">Export</h1></a>';
			}
		}else if($this->uri->segment(5) == 'ord'){
			$search_string = urldecode($this->uri->segment(4));
			$order = $this->uri->segment(6);
			
			$config['base_url'] = base_url().'admin/users/ss/'.$search_string.'/ord/'.$order;
			$config["uri_segment"] = 7;
			//limit end
			$page = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			
			//fetch sql data into arrays
			$config['total_rows'] = $data['count_products']= $this->users_model->count_users($search_string, $order);
            $data['users'] = $this->users_model->get_users($search_string, $order, $config['per_page'], $page);
			$data['link'] = '<a href="admin/reports/users_report/?ss='.$search_string.'&ord='.$order.'" target="_blank" style="color:#fff;"><h1 style="border:0;background:none;">Export</h1></a>';
		}
		
        $choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
        //initializate the panination helper 
        $this->pagination->initialize($config);   

        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Users';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/users/list', $data);
        $this->load->view('admin/templates/footer');

    }//index
	
    public function add(){
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){
            //form validation
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('level', 'User Level', 'required');
            if($this->input->post('level') == 3){
	            $this->form_validation->set_rules('partner_title', 'Publisher Title', 'required');
	            $this->form_validation->set_rules('partner_slogan', 'Publisher Slogan', 'required');
	            $this->form_validation->set_rules('partner_body', 'Publisher Body', 'required');
	            $this->form_validation->set_rules('partner_fb', 'Facebook Profile', 'required');
	            $this->form_validation->set_rules('partner_tw', 'Twitter Profile', 'required');
			}
            if($this->input->post('level') == 2){
				$this->form_validation->set_rules('payment_option', 'Payment Option', 'required');
				if($this->input->post('payment_option') == 'PayPal Account'){
					$this->form_validation->set_rules('paypal_email', 'Paypal Email Address', 'trim|required|valid_email');
				}else if($this->input->post('payment_option') == 'Bank Account'){
					$this->form_validation->set_rules('bank_name', 'Bank Name', 'required');
					if($this->input->post('bank_name') == 'Other'){
						$this->form_validation->set_rules('bank_other', 'Other Bank Name', 'required');
					}
					$this->form_validation->set_rules('bank_code', 'Bank Code', 'required');
					$this->form_validation->set_rules('bank_branch_code', 'Branch Code', 'required');
					$this->form_validation->set_rules('account_type', 'Account Type', 'required');
					$this->form_validation->set_rules('account_number', 'Account Number', 'required');
				}
			}
			
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run()){
				if($this->input->post('level') == 9 || $this->input->post('level') == 3 || $this->input->post('level') == 2){
					$seller = 'Yes';
				}else{
					$seller = 'No';
				}
				
				$now = date('Y-m-d H:i:s');
                $data_to_store = array(
                    'user_fname' => $this->input->post('name'),
                    'user_email' => $this->input->post('email'),
                    'user_username' => $this->input->post('username'),
                    'user_password' => hash('sha512', $this->input->post('password')),
					'user_seller' => $seller,
                    'user_level' => $this->input->post('level'),
                    'user_status' => 1,
					'created' => $now,
					'modified' => $now,
                );
                //if the insert has returned true then we show the flash message
				$user = $this->users_model->save($data_to_store);
                if($user){
					if($this->input->post('level') == 3){
						$config['upload_path'] = 'files/partners/';
						$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
						$config['max_size']  = 1024 * 99;
						$config['encrypt_name'] = TRUE;
						/* Load the upload library */
						$this->load->library('upload', $config);
				 
						/* Handle the file upload */
						$upload = $this->upload->do_upload('partner_logo');
						/* Get the data about the file */
						$dat = $this->upload->data();
				 
						if($dat['is_image'] == 1) {
							$logo = 'partners/'.$dat['file_name'];
						}else{
							$logo = '';
						}
						$data_to = array(
							'user_id' => $user,
							'partner_title' => $this->input->post('partner_title'),
							'partner_slogan' => $this->input->post('partner_slogan'),
							'partner_logo' => $logo,
							'partner_body' => $this->input->post('partner_body'),
							'partner_fb' => $this->input->post('partner_fb'),
							'partner_tw' => $this->input->post('partner_tw'),
						);
						$this->partner_m->save($data_to);
					}
					if($this->input->post('level') == 2){
						$datto = array(
							'user_id' => $user,
							'bank_name' => $this->input->post('bank_name'),
							'bank_other' => $this->input->post('bank_other'),
							'bank_code' => $this->input->post('bank_code'),
							'bank_branch_code' => $this->input->post('bank_branch_code'),
							'bank_account_type' => $this->input->post('account_type'),
							'bank_account_number' => $this->input->post('account_number'),
						);
						$this->bank_m->save($datto);
					}
					
					$subject = 'Welcome to OpenSchoolbag.com.sg!';
			
					$message = '<div style="color:#129749; font-size:15px;"><img src="'.base_url().'files/emails/welcome.jpg" alt="" /><br>
						<p>Dear '.$this->input->post('name').',<p>
						<p>Welcome to OpenSchoolbag, where you can get all your educational needs anytime!</p>
						<p>Username: '.$this->input->post('username').'</p>
						<p>Password: '.$this->input->post('password').'</p>
						
						<p>&nbsp;</p>
						<p>Thank you.</p>
						
						<p>Regards,<br>
						OpenSchoolbag team<br><br>
						<a href="'.base_url().'faqs" style="color:#e16b68">FAQs</a> | <a href="'.base_url().'for_buyers" style="color:#e16b68">Buyer Guide</a> | <a href="'.base_url().'for_sellers" style="color:#e16b68">Seller Guide</a> | <a href="'.base_url().'products" style="color:#e16b68">Shop Now</a> | <a href="'.base_url().'contact" style="color:#e16b68">Contact Us</a></p></div>';
					$this->users_m->email($this->input->post('email'), $subject, $message);
					
                    $data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }
            }
        }
        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Add Users';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/users/add');
        $this->load->view('admin/templates/footer');
    }
	
    public function update(){
		$id = $this->uri->segment(4);
		
		
		$data['user'] = $this->users_model->get_by(array('user_id' => $id), 1);
		
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){
			//print_r($_FILES);exit();
            $this->form_validation->set_rules('partner_title', 'Publisher Title', 'required');
            $this->form_validation->set_rules('partner_slogan', 'Publisher Slogan', 'required');
            $this->form_validation->set_rules('partner_body', 'Publisher Body', 'required');
            $this->form_validation->set_rules('partner_fb', 'Facebook Profile', 'required');
            $this->form_validation->set_rules('partner_tw', 'Twitter Profile', 'required');
			
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run()){
                //if the insert has returned true then we show the flash message
				$data_to = array(
					'partner_title' => $this->input->post('partner_title'),
					'partner_slogan' => $this->input->post('partner_slogan'),
					'partner_body' => $this->input->post('partner_body'),
					'partner_fb' => $this->input->post('partner_fb'),
					'partner_tw' => $this->input->post('partner_tw'),
				);
				if($_FILES){
					$config['upload_path'] = 'files/partners/';
					$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
					$config['max_size']  = 1024 * 99;
					$config['encrypt_name'] = TRUE;
					/* Load the upload library */
					$this->load->library('upload', $config);
				
					/* Handle the file upload */
					$upload = $this->upload->do_upload('partner_logo');
					/* Get the data about the file */
					$dat = $this->upload->data();
					if($dat['is_image'] == 1) {
						$data_to['partner_logo'] = 'partners/'.$dat['file_name'];
						$partner = $this->partner_m->get_by(array('user_id' => $id), 1);
						unlink('files/'.$partner->partner_logo);
					}
				}
				$this->partner_m->update($data_to, $id);
				$data['flash_message'] = TRUE; 
            }else{
				$data['flash_message'] = FALSE; 
            }
        }
        //load the view
		$data['partner'] = $this->partner_m->get_by(array('user_id' => $id), 1);
		
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Update Publisher';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/users/edit', $data);
        $this->load->view('admin/templates/footer');
		
    }

    /**
    * Delete product by his id
    * @return void
    */
    public function delete(){
        //product id 
        $id = $this->uri->segment(4);
		$users = $this->users_model->get_by(array('user_id' => $id), 1);
		
		if($users->user_level == 2 || $users->user_level == 3){
			$status = $this->orders_model->seller_there($id);
			if($status != 0){
				redirect('admin/users/no');
				exit();
			}
			//delete all products
			$this->products_model->delete_user($id);
		}
		if($users->user_level == 3){
			//delete from Partners list
			$this->partner_m->delete_user($id);
		}
		if($users->user_level == 1){
			$status = $this->orders_model->buyer_there($users->user_username);
			if($status != 0){
				redirect('admin/users/no');
				exit();
			}
		}

        $this->users_model->delete_user($id);
        redirect('admin/users/yes');
    }

}