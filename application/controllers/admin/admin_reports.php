<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_reports extends Admin_Controller {
	
    public function __construct(){
        parent::__construct();
        if (isset($_SERVER['HTTP_REFERER'])){
            $this->session->set_userdata('prev_url', $_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_userdata('prev_url', base_url());
        }
		//load our new PHPExcel library
		$this->load->library('excel');
		$this->load->model('admin/products_model');
		$this->load->model('admin/orders_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/promo_model');
		$this->load->model('orders_m');
		$this->load->model('product_m');
		$this->load->model('users_m');
		$this->load->model('profile_m');
		$this->load->model('note_m');
		$this->load->model('site_m');
    }
	
	function index(){
		//get the admin percentage ------------------------------------------------------------------------------------------------------------------------------------------------------------
		$data['site'] = $this->site_m->get_by(array('site_id' => 1),1);
		
		if(!$this->session->userdata('is_logged_in')){
			redirect('admin');
		}
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		$data['users'] = $this->users_model->get_by_users(array("user_level !="=>1, "user_level !="=>9));
		$data['products'] = $this->product_m->get();
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->library('pagination');
		
		$config['per_page'] = 9;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		if($this->input->get('sales_but')){
			if($this->input->get('by') == 'by_seller'){
				redirect('admin/reports/sales/from/'.$this->input->get('from').'/to/'.$this->input->get('to').'/by/seller/'.$this->input->get('seller'));
				
			}else if($this->input->get('by') == 'by_product'){
				redirect('admin/reports/sales/from/'.$this->input->get('from').'/to/'.$this->input->get('to').'/by/product/'.$this->input->get('product'));
				
			}else if($this->input->get('by') == 'product_level'){
				redirect('admin/reports/sales/from/'.$this->input->get('from').'/to/'.$this->input->get('to').'/by/level/'.$this->input->get('level'));
				
			}else if($this->input->get('by') == 'product_subject'){
				redirect('admin/reports/sales/from/'.$this->input->get('from').'/to/'.$this->input->get('to').'/by/subject/'.$this->input->get('subject'));
				
			}else if($this->input->get('by') == 'product_type'){
				redirect('admin/reports/sales/from/'.$this->input->get('from').'/to/'.$this->input->get('to').'/by/product_type/'.$this->input->get('product_type'));
				
			}else if($this->input->get('by') == 'summery'){
				redirect('admin/reports/sales/from/'.$this->input->get('from').'/to/'.$this->input->get('to').'/by/summery/all');
			}
		}else if($this->input->get('downloads_but')){
			if($this->input->get('by') == 'by_seller'){
				redirect('admin/reports/downloads/from/'.$this->input->get('from').'/to/'.$this->input->get('to').'/by/seller/'.$this->input->get('seller'));
				
			}else if($this->input->get('by') == 'by_product'){
				redirect('admin/reports/downloads/from/'.$this->input->get('from').'/to/'.$this->input->get('to').'/by/product/'.$this->input->get('product'));
				
			}else if($this->input->get('by') == 'product_level'){
				redirect('admin/reports/downloads/from/'.$this->input->get('from').'/to/'.$this->input->get('to').'/by/level/'.$this->input->get('level'));
				
			}else if($this->input->get('by') == 'product_subject'){
				redirect('admin/reports/downloads/from/'.$this->input->get('from').'/to/'.$this->input->get('to').'/by/subject/'.$this->input->get('subject'));
			}
		}else if($this->input->get('products_but')){
			if($this->input->get('by') == 'by_seller'){
				redirect('admin/reports/products/from/'.$this->input->get('from').'/to/'.$this->input->get('to').'/by/seller/tag/'.$this->input->get('seller'));
				
			}else if($this->input->get('by') == 'product_level'){
				redirect('admin/reports/products/from/'.$this->input->get('from').'/to/'.$this->input->get('to').'/by/level/tag/'.$this->input->get('level'));
				
			}else if($this->input->get('by') == 'product_subject'){
				redirect('admin/reports/products/from/'.$this->input->get('from').'/to/'.$this->input->get('to').'/by/subject/tag/'.$this->input->get('subject'));
				
			}else if($this->input->get('by') == 'product_type'){
				redirect('admin/reports/products/from/'.$this->input->get('from').'/to/'.$this->input->get('to').'/by/product_type/tag/'.$this->input->get('product_type'));
			}
		}else if($this->input->get('promotions_but')){
			redirect('admin/reports/promotions/from/'.$this->input->get('from').'/to/'.$this->input->get('to'));
		}
		
		if($this->uri->segment(3) == 'sales'){
			$page = ($this->uri->segment(11)) ? $this->uri->segment(11) : 0;
			
			$first = $this->uri->segment(5);
			$second = date_create($this->uri->segment(7));
			date_add($second, date_interval_create_from_date_string('1 days'));
			$second = date_format($second, 'Y-m-d');
			$by = $this->uri->segment(9);
			$tag = urldecode($this->uri->segment(10));
			
			$data['prd'] = 'Sales';
			
			if($this->uri->segment(9) == 'level' || $this->uri->segment(9) == 'subject' || $this->uri->segment(9) == 'product_type'){
				$config["uri_segment"] = 11;
				$config['total_rows'] = $data['total'] = $this->orders_model->sales_total_no($first, $second, $by, $tag);
				$config['base_url'] = base_url().'admin/reports/sales/from/'.$first.'/to/'.$this->uri->segment(7).'/by/'.$by.'/'.$tag;
				$choice = $config["total_rows"] / $config["per_page"];
				$config["num_links"] = round($choice);
				
				$data['orders'] = $this->orders_model->sales_by_limit($config["per_page"], $page, $first, $second, $by, $tag);
				$data['link'] = '<a href="admin/reports/sales_report/?from='.$first.'&to='.$second.'&by='.$by.'&tag='.$tag.'" target="_blank"><h1 style="border:0;background:none;">Export</h1></a>';
				
			}else if($this->uri->segment(9) == 'product'){
				$config["uri_segment"] = 11;
				$config['total_rows'] = $data['total'] = $this->orders_model->sales_no_total($first, $second, $by, $tag);
				$config['base_url'] = base_url().'admin/reports/sales/from/'.$first.'/to/'.$this->uri->segment(7).'/by/'.$by.'/'.$tag;
				$choice = $config["total_rows"] / $config["per_page"];
				$config["num_links"] = round($choice);
				
				$data['orders'] = $this->orders_model->sales_limit_by($config["per_page"], $page, $first, $second, $by, $tag);
				$data['link'] = '<a href="admin/reports/sales_report_by/?from='.$first.'&to='.$second.'&by='.$by.'&tag='.$tag.'" target="_blank"><h1 style="border:0;background:none;">Export</h1></a>';
				
			}else if($this->uri->segment(9) == 'seller'){
				$config["uri_segment"] = 11;
				$config['total_rows'] = $data['total'] = $this->orders_model->seller_total($first, $second, $tag);
				$config['base_url'] = base_url().'admin/reports/sales/from/'.$first.'/to/'.$this->uri->segment(7).'/by/'.$by.'/'.$tag;
				$choice = $config["total_rows"] / $config["per_page"];
				$config["num_links"] = round($choice);
				
				$data['orders'] = $this->orders_model->seller_limit($config["per_page"], $page, $first, $second, $by, $tag);
				$data['link'] = '<a href="admin/reports/seller_report_by/?from='.$first.'&to='.$second.'&tag='.$tag.'" target="_blank"><h1 style="border:0;background:none;">Export</h1></a>';
				
			}else{
				//by summery
				$config["uri_segment"] = 11;
				$config['total_rows'] = $data['total'] = $this->orders_model->saller_total_no($first, $second);
				$config['base_url'] = base_url().'admin/reports/sales/from/'.$first.'/to/'.$this->uri->segment(7).'/by/'.$by;
				$choice = $config["total_rows"] / $config["per_page"];
				$config["num_links"] = round($choice);
				
				$data['orders'] = $this->orders_model->saller_by_limit($config["per_page"], $page, $first, $second);
				//$data['gtotal'] = $this->orders_model->total_saller_limit($first, $second);
				$data['link'] = '<a href="admin/reports/saller_report/?from='.$first.'&to='.$second.'" target="_blank"><h1 style="border:0;background:none;">Export</h1></a>';
			
			}
		}else if($this->uri->segment(3) == 'downloads'){
			$page = ($this->uri->segment(11)) ? $this->uri->segment(11) : 0;
			
			$first = $this->uri->segment(5);
			$second = date_create($this->uri->segment(7));
			date_add($second, date_interval_create_from_date_string('1 days'));
			$second = date_format($second, 'Y-m-d');
			$by = $this->uri->segment(9);
			$tag = urldecode($this->uri->segment(10));
		
			$data['prd'] = 'Downloads';
			
			$config["uri_segment"] = 11;
			$config['total_rows'] = $data['total'] = $this->orders_model->down_total_no($first, $second, $by, $tag);
			$config['base_url'] = base_url().'admin/reports/downloads/from/'.$first.'/to/'.$this->uri->segment(7).'/by/'.$by.'/'.$tag;
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] = round($choice);
			
			$data['orders'] = $this->orders_model->down_limit($config["per_page"], $page, $first, $second, $by, $tag);
			
			$data['link'] = '<a href="admin/reports/orders_report/?from='.$first.'&to='.$second.'&by='.$by.'&tag='.$tag.'" target="_blank"><h1 style="border:0;background:none;">Export</h1></a>';
			
		}else if($this->uri->segment(3) == 'products' && !$this->uri->segment(10)){
			
			$page = ($this->uri->segment(10)) ? $this->uri->segment(10) : 0;
			
			$first = $this->uri->segment(5);
			$second = date_create($this->uri->segment(7));
			date_add($second, date_interval_create_from_date_string('1 days'));
			$second = date_format($second, 'Y-m-d');
			$by = $this->uri->segment(9);
		
			$data['prd'] = 'Products';
			
			$config["uri_segment"] = 10;
			$config['total_rows'] = $data['total'] = $this->product_m->s_total_no($user->user_id, $first, $second, $by);
			$config['base_url'] = base_url().'admin/reports/products/from/'.$first.'/to/'.$this->uri->segment(7).'/by/'.$by;
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] = round($choice);
			
			$data['orders'] = $this->product_m->prod_by_limit($config["per_page"], $page, '', $first, $second, $by);
			
			$data['link'] = '<a href="admin/reports/products_reports/?from='.$first.'&to='.$second.'&by='.$by.'" target="_blank"><h1 style="border:0;background:none;">Export</h1></a>';
			
		}else if($this->uri->segment(3) == 'products' && $this->uri->segment(10) == 'tag'){
			
			$page = ($this->uri->segment(12)) ? $this->uri->segment(12) : 0;
			
			$first = $this->uri->segment(5);
			$second = date_create($this->uri->segment(7));
			date_add($second, date_interval_create_from_date_string('1 days'));
			$second = date_format($second, 'Y-m-d');
			$by = $this->uri->segment(9);
			$tag = urldecode($this->uri->segment(11));
		
			$data['prd'] = 'Products';
			
			$config["uri_segment"] = 13;
			$config['total_rows'] = $data['total'] = $this->product_m->s_total_no($user->user_id, $first, $second, $by, $tag);
			$config['base_url'] = base_url().'admin/reports/products/from/'.$first.'/to/'.$this->uri->segment(7).'/by/'.$by;
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] = round($choice);
			
			$data['orders'] = $this->product_m->prod_by_limit($config["per_page"], $page, '', $first, $second, $by, $tag);
			
			$data['link'] = '<a href="admin/reports/products_reports/?from='.$first.'&to='.$second.'&by='.$by.'&tag='.$tag.'" target="_blank"><h1 style="border:0;background:none;">Export</h1></a>';
			
		}else if($this->uri->segment(3) == 'promotions'){
			$page = ($this->uri->segment(8)) ? $this->uri->segment(8) : 0;
			
			$first = $this->uri->segment(5);
			$second = date_create($this->uri->segment(7));
			date_add($second, date_interval_create_from_date_string('1 days'));
			$second = date_format($second, 'Y-m-d');
		
			$data['prd'] = 'Promotions';
			
			$config["uri_segment"] = 8;
			$config['total_rows'] = $data['total'] = $this->promo_model->totals($first, $second);
			$config['base_url'] = base_url().'admin/reports/promotions/from/'.$first.'/to/'.$this->uri->segment(7);
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] = round($choice);
			
			$data['orders'] = $this->promo_model->listed($config["per_page"], $page, $first, $second);
			
			$data['link'] = '<a href="admin/reports/promotions_report/?from='.$first.'&to='.$second.'" target="_blank"><h1 style="border:0;background:none;">Export</h1></a>';
			
		}
			
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		
		$users = $this->users_m->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
		$data['title'] = 'Reports';
		$this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
		$this->load->view('admin/reports');
		$this->load->view('admin/templates/footer', $data);
	}
	
	function sales_report(){
		$first = $this->input->get('from');
		$second = date_create($this->input->get('to'));
		date_add($second, date_interval_create_from_date_string('1 days'));
		$second = date_format($second, 'Y-m-d');
		$by = urldecode($this->input->get('by'));
		$tag = urldecode($this->input->get('tag'));
		
		$products = $this->orders_model->sales_by_limit('', '', $first, $second, $by, $tag);
		
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Sales Report');
		if($by == 'product_type'){
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Product Type');
			$this->excel->getActiveSheet()->setCellValue('C1', 'Product Level');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Product Subject');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Product Name');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Product Price');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Discount');
			$this->excel->getActiveSheet()->setCellValue('H1', 'Purchased Price');
			$this->excel->getActiveSheet()->setCellValue('I1', 'Purchase Date');
			$this->excel->getActiveSheet()->setCellValue('J1', 'Seller Username');
			$this->excel->getActiveSheet()->setCellValue('K1', 'Buyer Username');
			$i = 2;
			$j = 1;
			foreach($products as $product){$cat = explode(':',$product->order_cat);
				//set cell A1 content with some text
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $product->order_ptype);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, $cat[0]);
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $cat[1]);
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $product->order_pname);
				$this->excel->getActiveSheet()->setCellValue('F'.$i, number_format($product->order_pprice, 2));
				if($product->order_promo == ''){$discount = $product->order_discount;}else{$discount = 0;}
				$this->excel->getActiveSheet()->setCellValue('G'.$i, number_format($discount, 2));
				$this->excel->getActiveSheet()->setCellValue('H'.$i, number_format($product->order_pprice - $discount, 2));
				$this->excel->getActiveSheet()->setCellValue('I'.$i, $product->created);
				$this->excel->getActiveSheet()->setCellValue('J'.$i, $product->puser_id);
				$this->excel->getActiveSheet()->setCellValue('K'.$i, $product->user_id);
				$i++;
				$j++;
			}
		}else if($by == 'level'){
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Product Level');
			$this->excel->getActiveSheet()->setCellValue('C1', 'Product Type');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Product Subject');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Product Name');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Product Price');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Discount');
			$this->excel->getActiveSheet()->setCellValue('H1', 'Purchased Price');
			$this->excel->getActiveSheet()->setCellValue('I1', 'Purchase Date');
			$this->excel->getActiveSheet()->setCellValue('J1', 'Seller Username');
			$this->excel->getActiveSheet()->setCellValue('K1', 'Buyer Username');
			$i = 2;
			$j = 1;
			foreach($products as $product){$cat = explode(':',$product->order_cat);
				//set cell A1 content with some text
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $cat[0]);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, $product->order_ptype);
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $cat[1]);
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $product->order_pname);
				$this->excel->getActiveSheet()->setCellValue('F'.$i, number_format($product->order_pprice, 2));
				if($product->order_promo == ''){$discount = $product->order_discount;}else{$discount = 0;}
				$this->excel->getActiveSheet()->setCellValue('G'.$i, number_format($discount, 2));
				$this->excel->getActiveSheet()->setCellValue('H'.$i, number_format($product->order_pprice - $discount, 2));
				$this->excel->getActiveSheet()->setCellValue('I'.$i, $product->created);
				$this->excel->getActiveSheet()->setCellValue('J'.$i, $product->puser_id);
				$this->excel->getActiveSheet()->setCellValue('K'.$i, $product->user_id);
				$i++;
				$j++;
			}
		}else if($by == 'subject'){
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Product Subject');
			$this->excel->getActiveSheet()->setCellValue('C1', 'Product Type');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Product Level');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Product Name');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Product Price');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Discount');
			$this->excel->getActiveSheet()->setCellValue('H1', 'Purchased Price');
			$this->excel->getActiveSheet()->setCellValue('I1', 'Purchase Date');
			$this->excel->getActiveSheet()->setCellValue('J1', 'Seller Username');
			$this->excel->getActiveSheet()->setCellValue('K1', 'Buyer Username');
			$i = 2;
			$j = 1;
			foreach($products as $product){$cat = explode(':',$product->order_cat);
				//set cell A1 content with some text
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $cat[1]);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, $product->order_ptype);
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $cat[0]);
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $product->order_pname);
				$this->excel->getActiveSheet()->setCellValue('F'.$i, number_format($product->order_pprice, 2));
				if($product->order_promo == ''){$discount = $product->order_discount;}else{$discount = 0;}
				$this->excel->getActiveSheet()->setCellValue('G'.$i, number_format($discount, 2));
				$this->excel->getActiveSheet()->setCellValue('H'.$i, number_format($product->order_pprice - $discount, 2));
				$this->excel->getActiveSheet()->setCellValue('I'.$i, $product->created);
				$this->excel->getActiveSheet()->setCellValue('J'.$i, $product->puser_id);
				$this->excel->getActiveSheet()->setCellValue('K'.$i, $product->user_id);
				$i++;
				$j++;
			}
		}
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('K1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='Sales_Report_'.$by.' ('.$first.' to '.$second.').xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	
	function sales_report_by(){
		$first = $this->input->get('from');
		$second = date_create($this->input->get('to'));
		date_add($second, date_interval_create_from_date_string('1 days'));
		$second = date_format($second, 'Y-m-d');
		$by = urldecode($this->input->get('by'));
		$tag = urldecode($this->input->get('tag'));
		
		$products = $this->orders_model->sales_limit_by('', '', $first, $second, $by, $tag);
		
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Sales Report');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Product Name');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Product Type');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Product Price');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Product Date');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Seller Username');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Discount');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Total Sales');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Total Sales Amount');
		$i = 2;
		$j = 1;
		foreach($products as $product){
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, stripslashes($product['sale']->order_pname));
			$this->excel->getActiveSheet()->setCellValue('C'.$i, $product['sale']->order_ptype);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, number_format($product['sale']->order_pprice, 2));
			$this->excel->getActiveSheet()->setCellValue('E'.$i, date('Y-m-d', strtotime($product['sale']->created)));
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $product['sale']->puser_id);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, number_format($product['disc'], 2));
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $product['sale']->TotalSales);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, number_format($product['sale']->TotalAmount - $product['disc'], 2));
			$i++;
			$j++;
		}
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='Sales_Report ('.$first.' to '.$second.').xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	
	function seller_report_by(){
		$first = $this->input->get('from');
		$second = date_create($this->input->get('to'));
		date_add($second, date_interval_create_from_date_string('1 days'));
		$second = date_format($second, 'Y-m-d');
		$by = urldecode($this->input->get('by'));
		$tag = urldecode($this->input->get('tag'));
		
		$products = $this->orders_model->seller_limit('', '', $first, $second, $by, $tag);
		
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Sales Report');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Seller Username');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Product Name');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Product Price');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Purchase Date');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Discount');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Purchased Product Price');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Buyer Username');
		$i = 2;
		$j = 1;
		foreach($products as $product){
			//set cell A1 content with some text
			
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $product->puser_id);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, $product->order_pname);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, number_format($product->order_pprice, 2));
			$this->excel->getActiveSheet()->setCellValue('E'.$i, date('Y-m-d', strtotime($product->created)));
			if($product->order_promo == ''){$discount = $product->order_discount;}else{$discount = 0;}
			$this->excel->getActiveSheet()->setCellValue('F'.$i, number_format($discount, 2));
			$this->excel->getActiveSheet()->setCellValue('G'.$i, number_format($product->order_pprice - $discount, 2));
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $product->user_id);
			$i++;
			$j++;
		}
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='Sales_Report_'.$by.' ('.$first.' to '.$second.').xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	
	function saller_report(){
		$first = $this->input->get('from');
		$second = date_create($this->input->get('to'));
		date_add($second, date_interval_create_from_date_string('1 days'));
		$second = date_format($second, 'Y-m-d');
		$tag = urldecode($this->input->get('tag'));
		
		$products = $this->orders_model->saller_by_limit('', '', $first, $second, $tag);
		
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Saller Report');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Seller Name.');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Total Sales');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Total Sales Amount');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Net Sales Amount');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Bank Name');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Bank Code');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Branch Code');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Account Type');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Account Number');
		$i = 2;
		$j=1;
		foreach($products as $product){
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $product['sale']->productOwner);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, $product['sale']->totalOrders);
			$total = $product['sale']->totlaSale - $product['disc'];
			$this->excel->getActiveSheet()->setCellValue('D'.$i, number_format($total, 2));
			$this->excel->getActiveSheet()->setCellValue('E'.$i, number_format(($total - ($total*15/100)), 2));
			if($product['sale']->bank_name == 'Others'){
				$this->excel->getActiveSheet()->setCellValue('F'.$i, $product['sale']->bank_other);
			}else{
				$this->excel->getActiveSheet()->setCellValue('F'.$i, $product['sale']->bank_name);
			}
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $product['sale']->bank_code);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $product['sale']->bank_branch_code);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $product['sale']->bank_account_type);
			$this->excel->getActiveSheet()->setCellValue('J'.$i, $product['sale']->bank_account_number);
			$i++;
			$j++;
		}
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='Saller_Report_'.$tag.'.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	
	function orders_report(){
		$first = $this->input->get('from');
		$second = date_create($this->input->get('to'));
		date_add($second, date_interval_create_from_date_string('1 days'));
		$second = date_format($second, 'Y-m-d');
		$by = $this->input->get('by');
		$tag = $this->input->get('tag');
		
		$products = $this->orders_model->down_limit('', '', $first, $second, $by, $tag);
		
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Orders Report');
		if($by == 'product'){
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Product Name');
			$this->excel->getActiveSheet()->setCellValue('C1', 'Product Level');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Product Subject');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Seller Name');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Product Price');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Download Count');
			$i = 2;
			$j = 1;
			foreach($products as $product){
				//set cell A1 content with some text
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, stripslashes($product->order_pname));
				$this->excel->getActiveSheet()->setCellValue('C'.$i, $product->product_level);
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $product->product_subject);
				$this->excel->getActiveSheet()->setCellValue('E'.$i, stripslashes($product->user_username));
				$this->excel->getActiveSheet()->setCellValue('F'.$i, number_format($product->order_pprice, 2));
				$this->excel->getActiveSheet()->setCellValue('G'.$i, $product->down_count);
				$i++;
				$j++;
			}
		}else if($by == 'seller'){
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Seller Name');
			$this->excel->getActiveSheet()->setCellValue('C1', 'Product Name');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Product Level');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Product Subject');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Product Price');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Download Count');
			$i = 2;
			$j = 1;
			foreach($products as $product){
				//set cell A1 content with some text
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, stripslashes($product->user_username));
				$this->excel->getActiveSheet()->setCellValue('C'.$i, stripslashes($product->order_pname));
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $product->product_level);
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $product->product_subject);
				$this->excel->getActiveSheet()->setCellValue('F'.$i, number_format($product->order_pprice, 2));
				$this->excel->getActiveSheet()->setCellValue('G'.$i, $product->down_count);
				$i++;
				$j++;
			}
		}else if($by == 'subject'){
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Product Subject');
			$this->excel->getActiveSheet()->setCellValue('C1', 'Seller Name');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Product Name');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Product Level');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Product Price');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Download Count');
			$i = 2;
			$j = 1;
			foreach($products as $product){
				//set cell A1 content with some text
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $product->product_subject);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, stripslashes($product->user_username));
				$this->excel->getActiveSheet()->setCellValue('D'.$i, stripslashes($product->order_pname));
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $product->product_level);
				$this->excel->getActiveSheet()->setCellValue('F'.$i, number_format($product->order_pprice, 2));
				$this->excel->getActiveSheet()->setCellValue('G'.$i, $product->down_count);
				$i++;
				$j++;
			}
		}else if($by == 'level'){
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Product Level');
			$this->excel->getActiveSheet()->setCellValue('C1', 'Seller Name');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Product Name');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Product Subject');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Product Price');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Download Count');
			$i = 2;
			$j = 1;
			foreach($products as $product){
				//set cell A1 content with some text
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $product->product_level);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, stripslashes($product->user_username));
				$this->excel->getActiveSheet()->setCellValue('D'.$i, stripslashes($product->order_pname));
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $product->product_subject);
				$this->excel->getActiveSheet()->setCellValue('F'.$i, number_format($product->order_pprice, 2));
				$this->excel->getActiveSheet()->setCellValue('G'.$i, $product->down_count);
				$i++;
				$j++;
			}
		}
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='Orders_Report_'.$by.' ('.$first.' to '.$second.').xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	
	function products_report(){
		$first = $this->input->get('from');
		$second = date_create($this->input->get('to'));
		date_add($second, date_interval_create_from_date_string('1 days'));
		$second = date_format($second, 'Y-m-d');
		$by = $this->input->get('by');
		
		if($this->input->get('ss') && (!$this->input->get('ord') || $this->input->get('ord') == '')){
			$products = $this->products_model->prod_limit(array('products.product_name'=>$this->input->get('ss'), 'products.product_desc'=>$this->input->get('ss')));
			
		}else if((!$this->input->get('ss') || $this->input->get('ss') == '') && $this->input->get('ord')){
			$products = $this->products_model->prod_limit('', $this->input->get('ord'));
			
		}else if($this->input->get('ss') && $this->input->get('ord')){
			$products = $this->products_model->prod_limit(array('products.product_name'=>$this->input->get('ss'), 'products.product_desc'=>$this->input->get('ss')), $this->input->get('ord'));
			
		}else if($this->input->get('by') == 'category' || $this->input->get('by') == 'seller' || $this->input->get('by') == 'product_type'){
			$products = $this->product_m->prod_by_limit('','','', $first, $second, $by, $this->input->get('tag'));
			
		}else{
			$products = $this->product_m->prod_by_limit('','','', $first, $second, $by);
		}
		
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Products Report');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
		$this->excel->getActiveSheet()->setCellValue('B1', 'User Name');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Product Name');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Product Type');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Product Level');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Product Subject');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Product Resource');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Product Price');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Product Pages');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Product Weight');
		$this->excel->getActiveSheet()->setCellValue('K1', 'Order Count');
		$this->excel->getActiveSheet()->setCellValue('L1', 'Download Count');
		$this->excel->getActiveSheet()->setCellValue('M1', 'Product Created');
		$i = 2;
		$j = 1;
		foreach($products as $product){
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $product->user_username);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, $product->product_name);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $product->product_type);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $product->product_level);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $product->product_subject);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $product->product_resource);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, number_format($product->product_price, 2));
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $product->product_pages);
			$this->excel->getActiveSheet()->setCellValue('J'.$i, stripslashes($product->product_weight));
			$this->excel->getActiveSheet()->setCellValue('K'.$i, $product->order_count);
			$this->excel->getActiveSheet()->setCellValue('L'.$i, $product->down_count);
			$this->excel->getActiveSheet()->setCellValue('M'.$i, $product->created);
			$i++;
			$j++;
		}
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('K1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('L1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('M1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='Products_Report_'.$by.' ('.$first.' to '.$second.').xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	
	function products_reports(){
		$first = $this->input->get('from');
		$second = date_create($this->input->get('to'));
		date_add($second, date_interval_create_from_date_string('1 days'));
		$second = date_format($second, 'Y-m-d');
		$by = $this->input->get('by');
		
		if($this->input->get('by') == 'level' || $this->input->get('by') == 'subject' || $this->input->get('by') == 'seller' || $this->input->get('by') == 'product_type'){
			$products = $this->product_m->prod_by_limit('','','', $first, $second, $by, $this->input->get('tag'));
			
		}else{
			$products = $this->product_m->prod_by_limit('','','', $first, $second, $by);
		}
		
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Products Report');
		if($by == 'seller'){
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Seller Name');
			$this->excel->getActiveSheet()->setCellValue('C1', 'Product Name');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Product Level');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Product Subject');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Product Price');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Product Type');
			$i = 2;
			$j = 1;
			foreach($products as $product){
				//set cell A1 content with some text
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $product->user_username);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, $product->product_name);
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $product->product_level);
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $product->product_subject);
				$this->excel->getActiveSheet()->setCellValue('F'.$i, number_format($product->product_price, 2));
				$this->excel->getActiveSheet()->setCellValue('G'.$i, $product->product_type);
				$i++;
				$j++;
			}
		}else if($by == 'level'){
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Product Level');
			$this->excel->getActiveSheet()->setCellValue('C1', 'Seller Name');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Product Name');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Product Subject');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Product Price');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Product Type');
			$i = 2;
			$j = 1;
			foreach($products as $product){
				//set cell A1 content with some text
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $product->product_level);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, $product->user_username);
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $product->product_name);
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $product->product_subject);
				$this->excel->getActiveSheet()->setCellValue('F'.$i, number_format($product->product_price, 2));
				$this->excel->getActiveSheet()->setCellValue('G'.$i, $product->product_type);
				$i++;
				$j++;
			}
		}else if($by == 'subject'){
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Product Subject');
			$this->excel->getActiveSheet()->setCellValue('C1', 'Seller Name');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Product Name');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Product Level');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Product Price');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Product Type');
			$i = 2;
			$j = 1;
			foreach($products as $product){
				//set cell A1 content with some text
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $product->product_subject);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, $product->user_username);
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $product->product_name);
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $product->product_level);
				$this->excel->getActiveSheet()->setCellValue('F'.$i, number_format($product->product_price, 2));
				$this->excel->getActiveSheet()->setCellValue('G'.$i, $product->product_type);
				$i++;
				$j++;
			}
		}else if($by == 'product_type'){
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Product Type');
			$this->excel->getActiveSheet()->setCellValue('C1', 'Seller Name');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Product Name');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Product Level');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Product Subject');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Product Price');
			$i = 2;
			$j = 1;
			foreach($products as $product){
				//set cell A1 content with some text
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $product->product_type);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, $product->user_username);
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $product->product_name);
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $product->product_level);
				$this->excel->getActiveSheet()->setCellValue('F'.$i, $product->product_subject);
				$this->excel->getActiveSheet()->setCellValue('G'.$i, number_format($product->product_price, 2));
				$i++;
				$j++;
			}
		}
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='Products_Report_'.$by.' ('.$first.' to '.$second.').xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	
	function promotions_report(){
		$first = $this->input->get('from');
		$second = date_create($this->input->get('to'));
		date_add($second, date_interval_create_from_date_string('1 days'));
		$second = date_format($second, 'Y-m-d');
		
		$products = $this->promo_model->listed('','', $first, $second);
		
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Promotions Report');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Start Date');
		$this->excel->getActiveSheet()->setCellValue('C1', 'End Date');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Discount Value');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Promo Code');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Promo For');
		$this->excel->getActiveSheet()->setCellValue('G1', 'User Name');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Date Used');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Order No');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Purchased Amount');
		$i = 2;
		$j = 1;
		foreach($products as $product){
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $product->start_date);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, $product->end_date);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $product->discount_values);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $product->promo_code);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $product->promo_for);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $product->username);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $product->date_used);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $product->order_no);
			$this->excel->getActiveSheet()->setCellValue('J'.$i, number_format($product->purchased_amount, 2));
			$i++;
			$j++;
		}
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='Promotions_Report ('.$first.' to '.$second.').xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	
	function users_report(){
		if($this->input->get('from') && $this->input->get('to')){
			$first = $this->input->get('from');
			$second = date_create($this->input->get('to'));
			date_add($second, date_interval_create_from_date_string('1 days'));
			$second = date_format($second, 'Y-m-d');
			
		}else{$first = ''; $second = '';}
		
		if($this->input->get('ss')){
			$ss = $this->input->get('ss');
		}else{$ss = '';}
		
		if($this->input->get('ord')){
			$ord = $this->input->get('ord');
		}else{$ord = '';}
		
		$products = $this->users_m->order_by($first, $second, $ss, $ord);
		//echo '<pre>'; print_r($products); echo '</pre>'; exit();
		
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Users Report');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'S.No');
		$this->excel->getActiveSheet()->setCellValue('B1', 'First Name');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Last Name');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Username');
		$this->excel->getActiveSheet()->setCellValue('E1', 'User Email');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Phone');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Seller');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Level');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Regoistered On');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Bank Name');
		$this->excel->getActiveSheet()->setCellValue('K1', 'Bank Code');
		$this->excel->getActiveSheet()->setCellValue('L1', 'Branch Code');
		$this->excel->getActiveSheet()->setCellValue('M1', 'Account Type');
		$this->excel->getActiveSheet()->setCellValue('N1', 'Account Number');
		$i = 2;
		$j = 1;
		foreach($products as $product){
			if($product['usr']->user_level == 9){$level = 'Admin';}elseif($product['usr']->user_level == 3){$level = 'Publisher';}elseif($product['usr']->user_level == 2){$level = 'Seller';}elseif($product['usr']->user_level == 1){$level = 'Buyer';}
			//set cell A1 content with some text
			$fname = explode(' ', $product['usr']->user_fname);
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $j);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $fname[0]);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, $fname[1]);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $product['usr']->user_username);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $product['usr']->user_email);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $product['usr']->user_phone);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $product['usr']->user_seller);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $level);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $product['usr']->created);
			if(count($product['bank'])){
				if($product['bank']->bank_name != 'Others'){
					$this->excel->getActiveSheet()->setCellValue('J'.$i, $product['bank']->bank_name);
				}else{
					$this->excel->getActiveSheet()->setCellValue('J'.$i, $product['bank']->bank_other);
				}
				$this->excel->getActiveSheet()->setCellValue('K'.$i, $product['bank']->bank_code);
				$this->excel->getActiveSheet()->setCellValue('L'.$i, $product['bank']->bank_branch_code);
				$this->excel->getActiveSheet()->setCellValue('M'.$i, $product['bank']->bank_account_type);
				$this->excel->getActiveSheet()->setCellValue('N'.$i, $product['bank']->bank_account_number);
			}else{
				$this->excel->getActiveSheet()->setCellValue('J'.$i, '');
				$this->excel->getActiveSheet()->setCellValue('K'.$i, '');
				$this->excel->getActiveSheet()->setCellValue('L'.$i, '');
				$this->excel->getActiveSheet()->setCellValue('M'.$i, '');
				$this->excel->getActiveSheet()->setCellValue('N'.$i, '');
			}
			$i++;
			$j++;
		}
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('K1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('L1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('M1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('N1')->getFont()->setSize(13);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('N1')->getFont()->setBold(true);
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='Users_Report ('.$first.' to '.$second.').xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */