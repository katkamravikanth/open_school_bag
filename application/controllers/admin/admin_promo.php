<?php
class Admin_promo extends Admin_Controller {
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/promo_model');
		$this->load->model('admin/users_model');
		$this->load->model('note_m');
        $this->load->model('product_m');
        $this->load->model('users_m');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index(){
		
        //all the posts sent by the view
        $search_string = $this->input->get('search_string');        
        $order = $this->input->get('order');
		
		if($search_string != '' && $order == ''){
			redirect('admin/promotions/ss/'.$search_string);
		}else if($order != '' && $search_string == ''){
			redirect('admin/promotions/ord/'.$order);
		}else if($search_string != '' && $order != ''){
			redirect('admin/promotions/ss/'.$search_string.'/ord/'.$order);
		}
		
        $this->load->library('pagination');
		$config['per_page'] = 6;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		if(!$this->uri->segment(5) || ($this->uri->segment(5) && $this->uri->segment(5) != 'ord')){
			if($this->uri->segment(3) == 'ss'){
				//limit end
				$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
				$search_string = urldecode($this->uri->segment(4));
				
				$config['base_url'] = base_url().'admin/promotions/ss/'.$search_string;
				$config["uri_segment"] = 5;
				
				//fetch sql data into arrays
				$config['total_rows'] = $this->promo_model->total_promo($search_string);
            	$data['promotions'] = $this->promo_model->get_by_promo($config['per_page'], $page, $search_string);
			}else
			if($this->uri->segment(3) == 'ord'){
				//limit end
				$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
				$order = urldecode($this->uri->segment(4));
				
				$config['base_url'] = base_url().'admin/promotions/ord/'.$order;
				$config["uri_segment"] = 5;
				
				//fetch sql data into arrays
				$config['total_rows'] = $this->promo_model->total_promo('', $order);
            	$data['promotions'] = $this->promo_model->get_by_promo($config['per_page'], $page, '', $order);
			
			}else{
				$config['base_url'] = base_url().'admin/promotions';
				$config["uri_segment"] = 3;
				//limit end
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				
				//fetch sql data into arrays      
				$config['total_rows'] = $this->promo_model->total_promo();
            	$data['promotions'] = $this->promo_model->get_by_promo($config['per_page'], $page);
			}
		}else if($this->uri->segment(5) == 'ord'){
			$search_string = urldecode($this->uri->segment(4));
			$order = urldecode($this->uri->segment(6));
			
			$config['base_url'] = base_url().'admin/promotions/ss/'.$search_string.'/ord/'.$order;
			$config["uri_segment"] = 7;
			//limit end
			$page = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			
			//fetch sql data into arrays
			$config['total_rows'] = $this->promo_model->total_promo($search_string, $order);
            $data['promotions'] = $this->promo_model->get_by_promo($config['per_page'], $page, $search_string, $order);
		}
		
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
        
        //initializate the panination helper 
        $this->pagination->initialize($config);

        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
        $data['title'] = 'Manage Promotions';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/promo/list', $data);
        $this->load->view('admin/templates/footer');  

    }//index
	
	// add or update products
	public function add(){
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){
            //form validation
            $this->form_validation->set_rules('ptype', 'Promo Type', 'trim|required');
            $this->form_validation->set_rules('ppercent', 'Discount Value', 'trim|required');
            $this->form_validation->set_rules('pstart', 'Start Date', 'trim|required');
            $this->form_validation->set_rules('pend', 'End Date', 'trim|required');
            $this->form_validation->set_rules('no_users', 'No of users', 'trim|required');
			
			if($this->input->post('ptype') == 'On Cart Amount'){
				$this->form_validation->set_rules('pmin', 'Minimum Amount', 'trim|required');
				$this->form_validation->set_rules('pertype', 'Discount Type', 'trim|required');
				$this->form_validation->set_rules('pcode', 'Promotion Code', 'trim|required');
			}
			
			$this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
			
			if($this->input->post('ptype') == 'On Product Amount'){
				if($this->input->post('ptitle') != '' || $this->input->post('plevel') != ''){
		
					//if the form has passed through the validation
					if ($this->form_validation->run() == TRUE){
						
						if(date('Y-m-d') != mysql_real_escape_string($this->input->post('pstart'))){$status = 'Not Started';}else{$status = 'Active';}
						$sdata = array(
							'promo_type' => mysql_real_escape_string($this->input->post('ptype')),
							'promo_start' => mysql_real_escape_string($this->input->post('pstart')),
							'promo_end' => mysql_real_escape_string($this->input->post('pend')),
							'promo_percent' => mysql_real_escape_string($this->input->post('ppercent')),
							'promo_percent_type' => mysql_real_escape_string('%'),
							'product_title' => mysql_real_escape_string($this->input->post('ptitle')),
							'promo_category' => mysql_real_escape_string($this->input->post('plevel')),
							'promo_users' => mysql_real_escape_string($this->input->post('no_users')),
							'promo_status' => $status
						);
						$prormo = $this->promo_model->save($sdata);
						if($prormo){
							$data['flash_message'] = TRUE; 
						}else{
							$data['flash_message'] = FALSE; 
						}
					}
				}else{
					$data['perror_message'] = FALSE; 
				}
			}else if($this->input->post('ptype') == 'On Cart Amount'){
				$promoc = $this->promo_model->get_by(array('promo_code'=>mysql_real_escape_string($this->input->post('pcode'))));
				if(!count($promoc)){
					//if the form has passed through the validation
					if ($this->form_validation->run() == TRUE){
						$sdata = array(
							'promo_type' => mysql_real_escape_string($this->input->post('ptype')),
							'promo_min_amount' => mysql_real_escape_string($this->input->post('pmin')),
							'promo_code' => mysql_real_escape_string($this->input->post('pcode')),
							'promo_start' => mysql_real_escape_string($this->input->post('pstart')),
							'promo_end' => mysql_real_escape_string($this->input->post('pend')),
							'promo_percent' => mysql_real_escape_string($this->input->post('ppercent')),
							'promo_percent_type' => mysql_real_escape_string($this->input->post('pertype')),
							'promo_users' => mysql_real_escape_string($this->input->post('no_users')),
							'promo_status' => 'Active'
						);
						$prormo = $this->promo_model->save($sdata);
						if($prormo){
							$data['flash_message'] = TRUE; 
						}else{
							$data['flash_message'] = FALSE; 
						}
					}
				}else{
					$data['cflash_message'] = FALSE; 
				}
			}
		}
		$data['title'] = 'Add Promotion';
		$data['products'] = $this->product_m->get_by(array('product_status' => 'Active'));
		
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
		$this->load->view('admin/promo/add', $data);
		$this->load->view('admin/templates/footer', $data);
	}
	
	// add or update products
	public function update($id){
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST'){
            //form validation
            $this->form_validation->set_rules('ptype', 'Promo Type', 'trim|required');
            $this->form_validation->set_rules('ppercent', 'Discount Value', 'trim|required');
            $this->form_validation->set_rules('pstart', 'Start Date', 'trim|required');
            $this->form_validation->set_rules('pend', 'End Date', 'trim|required');
            $this->form_validation->set_rules('no_users', 'No of users', 'trim|required');
			
			if($this->input->post('ptype') == 'On Cart Amount'){
				$this->form_validation->set_rules('pmin', 'Minimum Amount', 'trim|required');
				$this->form_validation->set_rules('pertype', 'Discount Type', 'trim|required');
				$this->form_validation->set_rules('pcode', 'Promotion Code', 'trim|required');
			}
			$this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
			
			if($this->input->post('ptype') == 'On Product Amount'){
				if($this->input->post('ptitle') != '' || $this->input->post('plevel') != ''){
		
					//if the form has passed through the validation
					if ($this->form_validation->run() == TRUE){
						$sdata = array(
							'promo_type' => mysql_real_escape_string($this->input->post('ptype')),
							'promo_start' => mysql_real_escape_string($this->input->post('pstart')),
							'promo_end' => mysql_real_escape_string($this->input->post('pend')),
							'promo_percent' => mysql_real_escape_string($this->input->post('ppercent')),
							'promo_percent_type' => mysql_real_escape_string('%'),
							'product_title' => mysql_real_escape_string($this->input->post('ptitle')),
							'promo_category' => mysql_real_escape_string($this->input->post('plevel')),
							'promo_users' => mysql_real_escape_string($this->input->post('no_users')),
							'promo_status' => 'Active'
						);
						$prormo = $this->promo_model->save($sdata, $id);
						if($prormo){
							$data['flash_message'] = TRUE; 
						}else{
							$data['flash_message'] = FALSE; 
						}
					}
				}else{
					$data['error_message'] = FALSE; 
				}
			}else if($this->input->post('ptype') == 'On Cart Amount'){
				//if the form has passed through the validation
				if ($this->form_validation->run() == TRUE){
					$sdata = array(
						'promo_type' => mysql_real_escape_string($this->input->post('ptype')),
						'promo_min_amount' => mysql_real_escape_string($this->input->post('pmin')),
						'promo_code' => mysql_real_escape_string($this->input->post('pcode')),
						'product_title' => '',
						'promo_start' => mysql_real_escape_string($this->input->post('pstart')),
						'promo_end' => mysql_real_escape_string($this->input->post('pend')),
						'promo_percent' => mysql_real_escape_string($this->input->post('ppercent')),
						'promo_percent_type' => mysql_real_escape_string($this->input->post('pertype')),
						'promo_users' => mysql_real_escape_string($this->input->post('no_users')),
						'promo_status' => 'Active'
					);
					$prormo = $this->promo_model->save($sdata, $id);
					if($prormo){
						$data['flash_message'] = TRUE; 
					}else{
						$data['flash_message'] = FALSE; 
					}
				}
			}
		}
		$data['title'] = 'Update Promotion';
		$data['promo'] = $this->promo_model->get_by(array('promo_id'=>$id), 1);
		$data['products'] = $this->product_m->get_by(array('product_status' => 'Active'));
		
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
		$this->load->view('admin/promo/edit', $data);
		$this->load->view('admin/templates/footer', $data);
	}

    /**
    * Delete product by his id
    * @return void
    */
    public function delete(){
        //product id 
        $id = $this->uri->segment(4);
        $this->promo_model->delete_promo($id);
        redirect('admin/promotions');
    }//edit
}