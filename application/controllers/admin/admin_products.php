<?php
class Admin_products extends Admin_Controller {
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
		$this->load->library('threaded');
		
        $this->load->model('admin/products_model');
		$this->load->model('admin/users_model');
		$this->load->model('note_m');
		$this->load->model('orders_m');
        $this->load->model('product_m');
        $this->load->model('review_m');
        $this->load->model('qna_m');
        $this->load->model('users_m');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index(){

        //all the posts sent by the view
        $search_string = $this->input->get('search_string');        
        $order = $this->input->get('order');
		
		if($search_string != '' && $order == ''){
			redirect('admin/products/ss/'.$search_string);
		}else if($order != '' && $search_string == ''){
			redirect('admin/products/ord/'.$order);
		}else if($search_string != '' && $order != ''){
			redirect('admin/products/ss/'.$search_string.'/ord/'.$order);
		}
		
        $this->load->library('pagination');
		$config['per_page'] = 15;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		if(!$this->uri->segment(5) || ($this->uri->segment(5) && $this->uri->segment(5) != 'ord')){
			if($this->uri->segment(3) == 'ss'){
				//limit end
				$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
				$search_string = urldecode($this->uri->segment(4));
				
				$config['base_url'] = base_url().'admin/products/ss/'.$search_string;
				$config["uri_segment"] = 5;
				
				//fetch sql data into arrays
				$config['total_rows'] = $data['total'] = $this->product_m->search_total_no('','','','','',array('product_name'=>$search_string, 'product_desc'=>$search_string));
            	$data['products'] = $this->product_m->by_limit($config['per_page'], $page, '', '', $search_string);
				$data['link'] = '<a href="admin/reports/products_report/?ss='.$search_string.'" target="_blank" style="color:#fff;"><h1 style="border:0;background:none;">Export</h1></a>';
			}else
			if($this->uri->segment(3) == 'ord'){
				//limit end
				$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
				$order = $this->uri->segment(4);
				
				$config['base_url'] = base_url().'admin/products/ord/'.$order;
				$config["uri_segment"] = 5;
				
				//fetch sql data into arrays
				$config['total_rows'] = $data['total'] = $this->product_m->search_total_no('','','','','',array('product_type'=>$order));
            	$data['products'] = $this->product_m->by_limit($config['per_page'], $page, '', $order);
				$data['link'] = '<a href="admin/reports/products_report/?ord='.$order.'" target="_blank" style="color:#fff;"><h1 style="border:0;background:none;">Export</h1></a>';
			
			}else{
				$config['base_url'] = base_url().'admin/products';
				$config["uri_segment"] = 3;
				//limit end
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				
				//fetch sql data into arrays      
				$config['total_rows'] = $data['total'] = $this->product_m->total();
				$data['products'] = $this->product_m->by_limit($config['per_page'], $page);
				$data['link'] = '<a href="admin/reports/products_report/?from=2013-08-01&to='.date("Y-m-d").'" target="_blank" style="color:#fff;"><h1 style="border:0;background:none;">Export</h1></a>';
			}
		}else if($this->uri->segment(5) == 'ord'){
			$search_string = urldecode($this->uri->segment(4));
			$order = $this->uri->segment(6);
			
			$config['base_url'] = base_url().'admin/products/ss/'.$search_string.'/ord/'.$order;
			$config["uri_segment"] = 7;
			//limit end
			$page = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			
			//fetch sql data into arrays
			$config['total_rows'] = $data['total'] = $this->product_m->search_total_no('','','','','',array('product_name'=>$search_string, 'product_desc'=>$search_string));
            $data['products'] = $this->product_m->by_limit($config['per_page'], $page, '', $order, $search_string);
			$data['link'] = '<a href="admin/reports/products_report/?ss='.$search_string.'&ord='.$order.'" target="_blank" style="color:#fff;"><h1 style="border:0;background:none;">Export</h1></a>';
		}
		
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		
        //initializate the panination helper 
        $this->pagination->initialize($config);

        //load the view
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		$data['resources'] = $this->product_m->resources();
		
        $data['title'] = 'Products';
        $this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
        $this->load->view('admin/products/list', $data);
        $this->load->view('admin/templates/footer');  
    }//index
	
	// add or update products
	public function add($unique=FALSE){
		
		// clear file names from session variables
		$newdata = array(
			'ppic'  => $this->session->userdata('ppic'),
			'pfile'  => $this->session->userdata('pfile'),
		);
		$this->session->unset_userdata($newdata);
		if($unique == TRUE){
			$data['title'] = 'Edit Product';
			$data['product'] = $prod = $this->product_m->get_by(array('product_unique' => $unique, 'product_status' => 'Active'), 1);
			$data['ulevel'] = $this->users_model->get_by(array('user_id' => $prod->user_id), 1);
		}else{
			$data['title'] = 'Add Product';
		}
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		$data['publishers'] = $this->users_model->get_by(array('user_level' => 3));
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
		$this->load->view('admin/products/add_product', $data);
		$this->load->view('admin/templates/footer', $data);
	}
	
	// add or update products
	public function r_q_n($unique){
		$this->load->helper('file');
		
		$data['title'] = 'Edit Product';
		$data['product'] = $this->product_m->get_by(array('product_unique' => $unique, 'product_status' => 'Active'), 1);
		$data['review'] = $this->review_m->get_reviews($unique);
		$data['qna'] = $this->qna_m->get_qna($unique);
		
		$users = $this->users_model->get_by(array('user_username' => $this->session->userdata('user_name')), 1);
		$data['notif'] = $this->note_m->tot_no($users->user_id);
		
		$this->load->view('admin/templates/header', $data);
		$this->load->view('admin/templates/navigation');
		$this->load->view('admin/products/product_rqna', $data);
		$this->load->view('admin/templates/footer', $data);
	}
	
    /**
    * Delete product by his id
    * @return void
    */
    public function delete(){
		//product id 
        $id = $this->uri->segment(4);
        $prod = $this->product_m->get_by(array('product_id' => $id, 'product_status' => 'Active'), 1);
		
		if(!count($prod)){
			redirect('admin/products', 'refresh');
		}
		$ord = $this->orders_m->get_by(array('order_punique' => $prod->product_unique));
		$i=0;
		if(count($ord)){
			foreach($ord as $order){
				if($order->order_ptype == 'Physical'){
					if($order->order_status != 'Shipped'){
						$i++;
					}
				}else{
					$date = $order->created;
					$date = strtotime($date);
					$date = strtotime("+".$prod->product_down_days." day", $date);
					$date = date('Y-m-d H:i:s', $date);
					if($date > date('Y-m-d H:i:s')){
						$i++;
					}
				}
			}
		}
		if($i == 0){
			$data = array('product_status' => 'Inactive');
			$this->product_m->delete($id, $data);
			
			redirect('admin/products/state/yes');
		}else{
			redirect('admin/products/state/no');
		}
    }//edit
}