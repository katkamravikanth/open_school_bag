<?php
class Cron extends Frontend_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('site_m');
		$this->load->model('users_m');
		$this->load->model('bank_m');
		$this->load->model('product_m');
		$this->load->model('seller_m');
		$this->load->model('orders_m');
	}
	
	public function send_alert(){
		$users = $this->users_m->get_selle_by();
		$site = $this->site_m->get_by(array('site_id' => 1),1);
		
		$payment = '';
		$message = '';
		$total = 0;
		$totals = 0;
		$Gtotal = 0;
		$price = 0;
		$i = 1;
		$date = '';
		$first = '';
		$second = '';
		$lastmonth = mktime(0, 0, 0, date("m")-1);
		
		if(date('j') == 16){
			$date = '01 '.date('M Y').' to 15 '.date('M Y');
			$first = date('Y-m').'-01';
			$second = date('Y-m').'-15';
		}else if(date('j') == 1){
			$date = '16 '.date('M', $lastmonth).' '.date('Y').' to '.date('t', $lastmonth).' '.date('M', $lastmonth).' '.date('Y');
			$first = date('Y').'-'.date('m', $lastmonth).'-16';
			$second = date('Y').'-'.date('m', $lastmonth).'-'.date('t', $lastmonth);
		}
		
		$subject = 'Sales Summary Report for Period '.$date;
		
		foreach($users as $user){
			$bank = $this->bank_m->get_by(array('user_id' => $user->user_id), 1);
			
			if(count($bank)){$payment = 'Bank Account (Bank Name: '.$bank->bank_name.$bank->bank_other.')';}
			
			$message .= '<div style="color:#129749; font-size:15px; font-family:Gotham, \'Helvetica Neue\', Helvetica, Arial, sans-serif"><img src="'.base_url().'files/emails/sales_reports.jpg" alt="" /><br>
				<p>Dear <span style="text-transform:capitalize">'.$user->user_fname.'</span></p>
				<p>We are pleased to inform you that the completed sales transactions for the period '.$date.' are as follow:</p>';
				
			$message .= '<table width="100%" border="1" cellspacing="0" cellpadding="0">
			  <tr style="color:#fff; height:40px; background:#333;">
				<th>S/N</th>
				<th>Product Name</th>
				<th>Product Type</th>
				<th>Quantity Sold</th>
				<th>Your Revenue</th>
			  </tr>';
			
			$orders = $this->orders_m->sales_by_auto($first, $second, $user->user_username);
				
			if(count($orders)){
				foreach($orders as $carts){
					if($i%2){$bg = 'background-color:#efefef;';}else{$bg = '';}
					
					$message .= '<tr style="height:40px; '.$bg.'">
						<td>'.$i.'</td>
						<td><a href="'.base_url().'product/'.$carts['sale']->order_punique.'" style="color:#000; text-decoration:underline;">'.$carts['sale']->order_pname.'</a></td>
						<td align="center">'.$carts['sale']->order_ptype.'</td>
						<td align="center">'.$carts['sale']->TotalSales.'</td>
						<td align="center">S$ '.number_format($carts['sale']->TotalAmount - $carts['disc'], 2).'</td>
				  	</tr>';
					$i++;
					$Gtotal = (float)$Gtotal + ((float)$carts['sale']->TotalAmount - (float)$carts['disc']);
					//echo gettype ($carts->revenue);
					//echo $Gtotal;
				}
				$total = $Gtotal - ($Gtotal * $site->margin / 100);
			}
			$message .= '</table><br /><br />
				<p>The settlement amount payable to you after deducting our service fees is <strong>S$ '.number_format($total, 2).'</strong>. This amount shall be credited into your '.$payment.' registered with us.</p>

				<p>In the event of any discrepancies, please contact us within three days from this email date at contact@openschoolbag.com.sg . If we do not hear from you, we will assume that the above are in order.</p>
				
				<p>&nbsp;</p>
				<p>Thank you for your support.</p>
				
				<p>Regards,<br>
				OpenSchoolbag team<br><br>
				<a href="'.base_url().'faqs" style="color:#e16b68">FAQs</a> | <a href="'.base_url().'for_buyers" style="color:#e16b68">Buyer Guide</a> | <a href="'.base_url().'for_sellers" style="color:#e16b68">Seller Guide</a> | <a href="'.base_url().'products" style="color:#e16b68">Shop Now</a> | <a href="'.base_url().'contact" style="color:#e16b68">Contact Us</a></p></div>';
			//echo $i;
			if($i != 1){
				$i=1;
				//print_r($message);
				$this->users_m->email($user->user_email, $subject, $message);
				$Gtotal = 0;
				$total = 0;
				$message = '';
			}else{
				$Gtotal = 0;
				$total = 0;
				$message = '';
			}
			//print_r($message);
			//var_dump($orders);
		}
	}
}

/* End of file users.php */
/* Location: ./application/controllers/users.php */