<?php
class Users extends Frontend_Controller {
	
	public function __construct(){
		parent::__construct();
        if (isset($_SERVER['HTTP_REFERER'])){
            $this->session->set_userdata('prev_url', $_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_userdata('prev_url', base_url());
        }
		$this->load->model('admin/images_model');
		$this->load->model('users_m');
		$this->load->model('active_m');
		
		$this->load->model('profile_m');
		$this->load->model('bank_m');
		$this->load->model('note_m');
		
		$this->load->model('product_m');
		$this->load->model('review_m');
	}
	
	public function login(){
		$username = mysql_real_escape_string($this->input->post('username'));
		$password = hash('sha512', $this->input->post('password'));
		$users = $this->users_m->get_by(array('user_username' => $username, 'user_password' => $password, 'user_status' => 1), 1);
		
		if(count($users)){
			$newdata = array(
				'fullname'  => $users->user_username,
				'username'  => $users->user_username,
				'email'     => $users->user_email,
				'phone'     => $users->user_phone,
				'level'     => $users->user_level,
				'logged_in' => TRUE
			);
			$this->session->set_userdata($newdata);
			print 0;
		}else{
			print 1;
		}
	}
	
	public function username(){
		$username = mysql_real_escape_string($this->input->post('username'));
		$users = $this->users_m->get_by(array('user_username' => $username), 1);
		if(!count($users)){
			return FALSE;
		}else{
			print 1;
		}
	}
	
	public function email(){
		$email = mysql_real_escape_string($this->input->post('email'));
		if($this->input->post('id')){
			$data = array('user_email' => $email, 'user_id !=' => $this->input->post('id'));
		}else{
			$data = array('user_email' => $email);
		}
		$users = $this->users_m->get_by($data, 1);
		if(!count($users)){
			return FALSE;
		}else{
			print 1;
		}
	}
	
	public function register(){
		$fname = mysql_real_escape_string($this->input->post('fname'));
		$email = mysql_real_escape_string($this->input->post('email'));
		$phone = mysql_real_escape_string($this->input->post('phone'));
		$username = mysql_real_escape_string($this->input->post('username'));
		$password = hash('sha512', $this->input->post('password'));
		$user_seller = $this->input->post('user_seller');
		
		$bank_name = mysql_real_escape_string($this->input->post('bank_name'));
		$bank_other = mysql_real_escape_string($this->input->post('bank_other'));
		$bank_code = mysql_real_escape_string($this->input->post('bank_code'));
		$branch_code = mysql_real_escape_string($this->input->post('branch_code'));
		$account_type = mysql_real_escape_string($this->input->post('account_type'));
		$account_number = mysql_real_escape_string($this->input->post('account_number'));
		
		if($user_seller == 'No'){
			$user_level = 1;
		}else{
			$user_level = 2;
		}
		if($fname != '' && $email != '' && $username != '' && $password != '' && $user_seller != ''){
			
			$id = $this->users_m->save(array('user_fname' => $fname, 'user_email' => $email, 'user_phone' => $phone, 'user_username' => $username, 'user_password' => $password, 'user_seller' => $user_seller, 'user_level' => $user_level, 'user_status' => 0));
		
			if($id != ''){
				
				$active_code = rand().md5($username).rand();
				$this->active_m->save(array('user_id' => $id, 'active_code' => $active_code));
				
				$subject = 'Welcome to OpenSchoolbag.com.sg!';
				
				$message = '<div style="color:#129749; font-size:15px;"><img src="'.base_url().'files/emails/welcome.jpg" alt="" /><br>
					<p>Dear '.$fname.',<p>
					<p>Welcome to OpenSchoolbag, where you can get all your educational needs anytime!</p>
					<p>Username: '.$username.'</p>
					<p>To start making purchases or posting products, please activate your account here:</p>
					<p><a href="'.base_url().'activate/'.$id.'/'.$active_code.'" style="color:#e16b68">'.base_url().'activate/'.$id.'/'.$active_code.'</a></p>
					
					<p>&nbsp;</p>
					<p>Thank you.</p>
					
					<p>Regards,<br>
					OpenSchoolbag team<br><br>
					<a href="'.base_url().'faqs" style="color:#e16b68">FAQs</a> | <a href="'.base_url().'for_buyers" style="color:#e16b68">Buyer Guide</a> | <a href="'.base_url().'for_sellers" style="color:#e16b68">Seller Guide</a> | <a href="'.base_url().'products" style="color:#e16b68">Shop Now</a> | <a href="'.base_url().'contact" style="color:#e16b68">Contact Us</a></p></div>';
				
				$this->users_m->email($this->input->post('email'), $subject, $message);
				
				if($user_seller == 'Yes'){
					$this->bank_m->save(array('user_id' => $id, 'bank_name' => $bank_name, 'bank_other' => $bank_other, 'bank_code' => $bank_code, 'bank_branch_code' => $branch_code, 'bank_account_type' => $account_type, 'bank_account_number' => $account_number));
				}
				
				// Notifications
				$puser = $this->users_m->get_by(array('user_level' => 9));
				if(count($puser)){
					foreach($puser as $ppu){
						$puser_id = $ppu->user_id;
						$message = 'New user ( <strong>'.$fname.'</strong> ) registered';
						$this->note_m->save(array('user_id'=>$id, 'to_user_id'=>$puser_id, 'note_message'=>$message));
					}
				}
				
				print $id;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
	
	public function forgot(){
		$email = mysql_real_escape_string($this->input->post('email'));
		$user = $this->users_m->get_by(array('user_email' => $email), 1);
		if(count($user)){
			$id = $user->user_id;
			$password = $this->users_m->pword_string(12);
			
			$forgoted = $this->users_m->save(array('user_password' => hash('sha512', $password)), $id);
			
			$subject = 'OpenSchoolbag- Password Reset';
			$message = '<div style="color:#129749; font-size:15px;"><img src="'.base_url().'files/emails/password_reset.jpg" alt="" /><br>
				<p>Dear '.$user->user_fname.',<p>
				<p>We have received your password reset request for username: '.$user->user_username.'.<br />Your new temporary password is:
				<br />Password: <strong>'.$password.'</strong></p><br /><br />
				
				<p>&nbsp;</p>
				<p>Thank you.</p>
				
				<p>Regards,<br>
				OpenSchoolbag team<br><br>
				<a href="'.base_url().'faqs" style="color:#e16b68">FAQs</a> | <a href="'.base_url().'for_buyers" style="color:#e16b68">Buyer Guide</a> | <a href="'.base_url().'for_sellers" style="color:#e16b68">Seller Guide</a> | <a href="'.base_url().'products" style="color:#e16b68">Shop Now</a> | <a href="'.base_url().'contact" style="color:#e16b68">Contact Us</a></p></div>';
			
			$this->users_m->email($email, $subject, $message);
			if($forgoted){
				print 0;
			}else{
				print 1;
			}
		}else{
			print 1;
		}
	}
	
	public function activate($user_id, $code){
		$active = $this->active_m->get_by(array('active_code' => $code, 'user_id' => $user_id), 1);
		if(count($active)){
			if($activate->created <= date('Y-m-d H:i:s', strtotime(date("Y-m-d")) - 86400)){
				$this->users_m->save(array('user_status' => 1), $user_id);
				redirect('u/activated', 'refresh');
			}else{
				redirect('u/expired', 'refresh');
			}
		}else{
			redirect('u/not-activated', 'refresh');
		}
	}
	
	public function re_acrivate(){
		$email = mysql_real_escape_string($this->input->post('email'));
		$active = $this->users_m->get_by(array('user_email' => $email), 1);
		if(count($active)){
			$id = $active->user_id;
			$active_code = rand().md5($active->user_username).rand();
			
			$activated = $this->active_m->save(array('user_id' => $id, 'active_code' => $active_code));
			
			$subject = 'OpenSchoolbag - Activation Email';
			$message = '<div style="color:#129749; font-size:15px;"><img src="'.base_url().'files/emails/welcome.jpg" alt="" /><br>
				<p>Dear '.$active->user_fname.',<p>
				<p>Username: '.$active->user_username.'</p>
				<p>Please find the new activation link below:</p>
				<p><a href="'.base_url().'activate/'.$id.'/'.$active_code.'">'.base_url().'activate/'.$id.'/'.$active_code.'</a></p><br /><br />
				
				<p>&nbsp;</p>
				<p>Thank you.</p>
				
				<p>Regards,<br>
				OpenSchoolbag team<br><br>
				<a href="'.base_url().'faqs" style="color:#e16b68">FAQs</a> | <a href="'.base_url().'for_buyers" style="color:#e16b68">Buyer Guide</a> | <a href="'.base_url().'for_sellers" style="color:#e16b68">Seller Guide</a> | <a href="'.base_url().'products" style="color:#e16b68">Shop Now</a> | <a href="'.base_url().'contact" style="color:#e16b68">Contact Us</a></p></div>';
			
			$this->active_m->email($email, $subject, $message);
			if($activated){
				print 0;
			}else{
				print 1;
			}
		}else{
			print 1;
		}
	}
	
	public function dashboard(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		$data['title'] = 'Dashboard';
		$data['user'] = $users = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		$data['profile'] = $this->profile_m->get_by(array('user_id' => $users->user_id), 1);
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		$data['note'] = $this->note_m->tot_no($users->user_id);
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('user/dashboard');
		$this->load->view('templates/footer', $data);
	}
	
	public function profile(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		$data['title'] = $this->session->userdata('fullname');
		$data['user'] = $users = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		$data['profile'] = $this->profile_m->get_by(array('user_id' => $users->user_id), 1);
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		$data['bank'] = $this->bank_m->get_by(array('user_id' => $users->user_id), 1);
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('user/profile');
		$this->load->view('templates/footer', $data);
	}
	
	public function profile_update(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		$users = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		$id = '';
		
		//general info update
		if($this->input->post('type') == 'b'){
			$fname = mysql_real_escape_string($this->input->post('fname'));
			$email = mysql_real_escape_string($this->input->post('email'));
			$phone = mysql_real_escape_string($this->input->post('phone'));
			
			$id = $this->users_m->save(array('user_fname' => $fname, 'user_email' => $email, 'user_phone' => $phone), $users->user_id);
		}
		
		//bank update
		if($this->input->post('type') == 'bank'){
			$user_seller = 'Yes';
			
			$bank_name = mysql_real_escape_string($this->input->post('bank_name'));
			$bank_other = mysql_real_escape_string($this->input->post('bank_other'));
			$bank_code = mysql_real_escape_string($this->input->post('bank_code'));
			$branch_code = mysql_real_escape_string($this->input->post('branch_code'));
			$account_type = mysql_real_escape_string($this->input->post('account_type'));
			$account_number = mysql_real_escape_string($this->input->post('account_number'));
			
			$this->users_m->save(array('user_seller' => $user_seller), $users->user_id);
			
			$bank = $this->bank_m->get_by(array('user_id' => $users->user_id), 1);
			if(count($bank)){
				$bank_id = mysql_real_escape_string($this->input->post('bank_id'));
				
				$id = $this->bank_m->save(array('user_id' => $users->user_id, 'bank_name' => $bank_name, 'bank_other' => $bank_other, 'bank_code' => $bank_code, 'bank_branch_code' => $branch_code, 'bank_account_type' => $account_type, 'bank_account_number' => $account_number), $bank_id);
			}else{
				if($users->user_level != 3 && $users->user_level != 9){
					$this->users_m->save(array('user_level' => 2), $users->user_id);
				}
				$id = $this->bank_m->save(array('user_id' => $users->user_id, 'bank_name' => $bank_name, 'bank_other' => $bank_other, 'bank_code' => $bank_code, 'bank_branch_code' => $branch_code, 'bank_account_type' => $account_type, 'bank_account_number' => $account_number));
			}
		}
		
		//publish profile
		if($this->input->post('type') == 'q'){
			$qualific = mysql_real_escape_string($this->input->post('qualific'));
			$total_exp = mysql_real_escape_string($this->input->post('total_exp'));
			$special = mysql_real_escape_string($this->input->post('special'));
			
			if($this->profile_m->get_by(array('user_id' => $users->user_id), 1)){
				$id = $this->profile_m->save(array('profile_qualifications' => $qualific, 'profile_total_exp' => $total_exp, 'profile_special' => $special), $users->user_id);
			}else{
				$id = $this->profile_m->save(array('user_id' => $users->user_id, 'profile_qualifications' => $qualific, 'profile_total_exp' => $total_exp, 'profile_special' => $special));
			}
		}
		
		$profile_pic = $this->session->userdata('profile_pic');
		
		if($id != ''){
			print $id;
		}else{
			return FALSE;
		}
	}
	
	//profile picture update
	public function profile_upload_file(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		
		$users = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		/* Create the config for upload library */
		$config['upload_path'] = 'files/profile/'; /* NB! create this dir! */
		$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
		$config['max_size']  = 1024 * 99;
		$config['encrypt_name'] = TRUE;
		/* Load the upload library */
		$this->load->library('upload', $config);
		/* Handle the file upload */
		$upload = $this->upload->do_upload('profile_pic');
		/* File failed to upload - continue */
		/* Get the data about the file */
		$dat = $this->upload->data();
	 
		if($dat['is_image'] == 1) {
			$id = 0;
			$row = $this->profile_m->get_by(array('user_id' => $users->user_id), 1);
			if(count($row)){
				unlink('files/'.$row->profile_pic);
				$id = $this->profile_m->save(array('profile_pic'  => 'profile/'.$dat['file_name']), $users->user_id);
			}else{
				$id = $this->profile_m->p_save(array('user_id' => $users->user_id, 'profile_pic'  => 'profile/'.$dat['file_name']));
			}
			if($id!=0){
				$data['p_message'] = TRUE;
				if(isset($_POST['publish'])) {
					redirect('publish_profile/state/yes');
				}else if(isset($_POST['profile'])) {
					redirect('profile/state/yes');
				}
			}else{
				$data['p_message'] = FALSE;
				if(isset($_POST['publish'])) {
					redirect('publish_profile/state/no');
				}else if(isset($_POST['profile'])) {
					redirect('profile/state/no');
				}
			}
		}else{
			$data['ep_message'] = TRUE;
			if(isset($_POST['publish'])) {
				redirect('publish_profile/state/nopic');
			}else if(isset($_POST['profile'])) {
				redirect('profile/state/nopic');
			}
		}
	}
	
	public function publish_profile(){
		if(!$this->session->userdata('logged_in') && $this->session->userdata('level') != 2){
			redirect(base_url());
		}
		$data['user'] = $users = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		$profile = $this->profile_m->get_by(array('user_id' => $users->user_id), 1);
		
		if ($this->input->server('REQUEST_METHOD') === 'POST'){
            //form validation
            $this->form_validation->set_rules('qualific', 'Qualifications', 'trim|required');
            $this->form_validation->set_rules('total_exp', 'Total Experience', 'trim|required');
            $this->form_validation->set_rules('special', 'Specialization', 'trim|required');
			
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run()){
				//print_r($_POST);exit();
				$uid = $this->input->post('uid');
                
                //if the insert has returned true then we show the flash message
				if(count($profile)){
					$data_to_store = array(
                    	'profile_qualifications' => $this->input->post('qualific'),
                    	'profile_total_exp' => $this->input->post('total_exp'),
                    	'profile_special' => $this->input->post('special'),
                	);
					$user = $this->profile_m->save($data_to_store, $uid);
				}else{
					$data_to_store = array(
						'user_id' => $uid,
                    	'profile_qualifications' => $this->input->post('qualific'),
                    	'profile_total_exp' => $this->input->post('total_exp'),
                    	'profile_special' => $this->input->post('special'),
                	);
					//print_r($data_to_store);exit();
					$user = $this->profile_m->p_save($data_to_store);
				}
                if($user){
                    $data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }
            }
        }
		$data['title'] = 'Publish Profile';
		$data['profile'] = $this->profile_m->get_by(array('user_id' => $users->user_id), 1);
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('user/publish_profile');
		$this->load->view('templates/footer', $data);
	}
	
	public function cpass(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		$data['title'] = 'Change Password';
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('user/cpass');
		$this->load->view('templates/footer', $data);
	}
	
	public function cpword(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		$cpass = hash('sha512', mysql_real_escape_string($this->input->post('cpass')));
		$newpass = hash('sha512', mysql_real_escape_string($this->input->post('newpass')));
		
		$user = $this->users_m->get_by(array('user_password' => $cpass, 'user_username' => $this->session->userdata('username')), 1);
		
		if(count($user)){
			$id = $this->users_m->save(array('user_password' => $newpass), $user->user_id);
			if($id != ''){
				print $id;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
	
	public function notifications(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		$users = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		$this->note_m->make_read($users->user_id);
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'my_notify';
		$config['total_rows'] = $this->note_m->tot_no($users->user_id);
		$config['per_page'] = 9;
		$config["uri_segment"] = 2;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		$this->pagination->initialize($config); 
		
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		
		$data['title'] = 'Notifications';
		$data['note'] = $this->note_m->get_by_limit($config["per_page"], $page, '', array('to_user_id' => $users->user_id));
		$data['pagination'] = $this->pagination->create_links();
		
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('my_notify');
		$this->load->view('templates/footer', $data);
	}
	
	public function seller(){
		if($this->session->userdata('username') != ''){
			$cuser = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
			$data['user_id'] = $cuser->user_id;
		}else{$data['user_id'] = 0;}
		
		$data['user'] = $user = $this->users_m->get_by(array('user_username' => urldecode($this->uri->segment(2))), 1);
		$data['profile'] = $this->profile_m->get_by(array('user_id' => $user->user_id), 1);
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'seller/'.$this->uri->segment(2);
		$config['total_rows'] = $this->product_m->total_no($user->user_id);
		$config['per_page'] = 8;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		$this->pagination->initialize($config); 
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		$data['products'] = $this->product_m->to_seller(array('user_id' => $user->user_id), $config["per_page"], $page);
		$data['reviews'] = $this->review_m->get_user_reviews($user->user_id);
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$data['selrate'] = $this->review_m->get_seller_by($user->user_id);
		$data['resources'] = $this->product_m->resources();
		$data['pagination'] = $this->pagination->create_links();
		
		$data['title'] = $user->user_username;
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('user/seller');
		$this->load->view('templates/footer', $data);
	}
	
	
	public function make_seller(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		
		$data['title'] = 'Make me a seller';
		$data['user'] = $users = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('user/make_seller');
		$this->load->view('templates/footer', $data);
	}
}

/* End of file users.php */
/* Location: ./application/controllers/users.php */