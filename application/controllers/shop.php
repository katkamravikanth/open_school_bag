<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shop extends Frontend_Controller {
 
	public function __construct(){
		parent::__construct();
        if (isset($_SERVER['HTTP_REFERER'])){
            $this->session->set_userdata('prev_url', $_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_userdata('prev_url', base_url());
        }
		$this->load->library('cart');
		
		$this->load->model('admin/images_model');
		$this->load->model('admin/promo_model');
		$this->load->model('product_m');
		$this->load->model('site_m');
		$this->load->model('users_m');
		$this->load->model('note_m');
		$this->load->model('cart_m');
		$this->load->model('orders_m');
	}
	
	// redirect user to paypal -----------------------------------------------------------------------------------------------------------------------------------------------------------------
    public function index(){
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
        $this->load->library( 'Paypal' );
        $this->paypal->initialize();
 
        $this->paypal->add_field( 'item_name', "OpenSchoolBag");
		//get the total amount -----------------------------------------------------------------------------------------------------------------------------------------------------------------
        if($page != 0){
			$page = substr(substr($page, 0, -4), 2);
			$page1 = substr($page, 0, -2);
			$page2 = substr($page, -2);
			$page = $page1.'.'.$page2;
			$this->paypal->add_field( 'amount', $page);
			$prmcde = $this->input->get('prmcde');
		}else{
			$this->paypal->add_field( 'amount', $this->input->post('tamount'));
			$prmcde  = date('y').$this->input->post('promocode').date('md');
		}
        $this->paypal->add_field( 'return', site_url( 'shop/success?prmcde='.$prmcde ) );
        $this->paypal->add_field( 'cancel_return', site_url( 'shop/cancel' ) );
        $this->paypal->add_field( 'notify_url', site_url( 'shop/ipn' ) );
		
        $this->paypal->add_field( 'quantity', '1');
		$this->paypal->paypal_auto_form();
    }
 
    public function ipn() {
        $this->load->library( 'Paypal' );
        if ( $this->paypal->validate_ipn() ) {
            $pdata = $this->paypal->ipn_data;
            if ($pdata['txn_type'] == "web_accept") {
                if($pdata['payment_status'] == "Completed"){
                    if($pdata['business'] == $this->config->item( 'paypal_email' )) {
						//handle payment... ----------------------------------------------------------------------------------------------------------------------------------------------------
						$data['title'] = 'Completed';
                        $data['banner'] = $this->images_model->image_by(array('path' => 'home'));
						
						$this->load->view('templates/header', $data);
						//$this->load->view('templates/slider_search');
						$this->load->view('orders/order_status', $data);
						$this->load->view('templates/footer', $data);
                    }
                }
            }
        }
    }
 
    public function success() {
		if($this->input->post('payer_id')){
			//get the transaction fee --------------------------------------------------------------------------------------------------------------------------------------------------------------
			$site = $this->site_m->get_by(array('site_id' => 1),1);
			if($this->input->get('prmcde')){$prmcde = substr(substr($this->input->get('prmcde'), 0, -4), 2);}else{$prmcde = '';}
			
			$fname = ''; $email = ''; $pmessage = ''; $bmessage = ''; $umessage = ''; $mmessage = ''; $prmessage = ''; $apmessage = ''; $amessage = ''; 
			$shipadd = ''; $link = ''; $order_no = '';
			$i = 0; $j = 0; $k = 0; $dis = 0; $discount = 0; $sdiscount = 0; $sprice = 0; $gtotal = 0; $sgtotal = 0; $ftotal = 0; $promo_used = 0;
			
			//getting buyer info ---------------------------------------------------------------------------------------------------------------------------------------------------------------
			$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
			
			//buyer email notification ---------------------------------------------------------------------------------------------------------------------------------------------------------
			$subject = 'OpenSchoolbag - We have received your order';
			$message = '<div style="color:#129749; font-size:15px;"><img src="'.base_url().'files/emails/order_details.png" alt="" /><br>
				<p>Dear '.$user->user_fname.',<p>
				<p>We have received your order(s) below:</p><br />
				
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr height="40" style="background-color:#cd3127; color:#fff;">
					<th>Order no</th><th>Product Name</th><th>Product type</th><th>Price</th>
				  </tr>';
			
			if($this->cart->total_items() != 0){
				//fetching all item from cart and saving to orders table ---------------------------------------------------------------------------------------------------------------------------
				foreach ($this->cart->contents() as $items){
					//product & sellers info -------------------------------------------------------------------------------------------------------------------------------------------------------
					$puser = $this->product_m->get_by_owner($items['options']['punique']);
					
					$fname = $puser[0]->user_fname;
					$email = $puser[0]->user_email;
					$puser_id = $puser[0]->user_id;
					
					$dat = array(
						'user_id' => $user->user_username,
						'puser_id' => $puser[0]->user_username,
						'order_pname' => $items['name'],
						'order_pqty' => $items['qty'],
						'order_pprice' => $items['price'] + $items['options']['disc'],
						'order_punique' => $items['options']['punique'],
						'order_ptype' => $items['options']['type'],
						'order_cat' => $items['options']['cat'],
						'order_no' => $this->session->userdata('order_no')
					);
					
					if($items['options']['type'] == 'Physical'){
						$dat['order_status'] = 'Pending';
						$i++;
						
						$sprice = $sprice + $sprice['options']['sprice'];
						$gtotal = $gtotal + $sprice + $items['price'];
						$ftotal = $ftotal + $items['price'] + $sprice;
					}else{
						$exp_date = $puser[0]->created;
						$exp_date = strtotime($exp_date);
						$exp_date = strtotime("+".$puser[0]->product_down_days." day", $exp_date);
						$exp_date = date('Y-m-d H:i:s', $exp_date);
						
						$dat['order_d_expir'] = $exp_date;
						$gtotal = $gtotal + $items['price'];
						$ftotal = $ftotal + $items['price'];
					}
					
					if($items['options']['disc'] != 0){
						$dat['order_discount'] = $items['options']['disc'];
					}
					
					if($prmcde != ''){
						$dat['order_promo'] = $prmcde;
						// get the discounted amount -----------------------------------------------------------------------------------------------------------------------------------------------
						$get = $this->promo_model->get_by(array('promo_code' => $prmcde, 'promo_type' => 'On Cart Amount'), 1);
						if($get->promo_percent_type == '%'){$dis = ($items['price'] * $get->promo_percent) / 100;}else
						if($get->promo_percent_type == 'S$'){$dis = $items['price']/$this->cart->total() * $get->promo_percent;}else{$dis = 0;}
						$dat['order_discount'] = number_format($dis, 2);
						$gtotal = $gtotal - $dis;
						$discount = $discount + $dis;
					}
					
					//update product order count ---------------------------------------------------------------------------------------------------------------------------------------------------
					$cart = $this->orders_m->save($dat);
					
					//get product order count ------------------------------------------------------------------------------------------------------------------------------------------------------
					$cont = $puser[0]->order_count+1;
					//increase order count ---------------------------------------------------------------------------------------------------------------------------------------------------------
					$this->product_m->save(array('order_count' => $cont), $puser[0]->product_id);
					
					//Notifications ---------------------------------------------------------------------------------------------------------------------------------------------------------------
					//notification message ---------------------------------------------------------------------------------------------------------------------------------------------------------
					$umessage = 'You have a new order for the product :<strong>'.$items['name'].'</strong>';
					$amessage = 'New order for the product :<strong>'.$items['name'].'</strong>';
					$bmessage = 'You have purchased product :<strong>'.$items['name'].'</strong>';
					//notification links -----------------------------------------------------------------------------------------------------------------------------------------------------------
					$link = base_url().'mysale/'.$this->session->userdata('order_no');
					$link1 = base_url().'admin/order/'.$this->session->userdata('order_no');
					$link2 = base_url().'product/'.$items['options']['punique'];
					
					//for buyer --------------------------------------------------------------------------------------------------------------------------------------------------------------------
					$this->note_m->save(array('user_id'=>$puser[0]->user_id, 'to_user_id'=>$user->user_id, 'note_message'=>$bmessage, 'note_link'=>$link2, 'note_linkname'=>'Go to Product'));
					
					//notification to product owner -------------------------------------------------------------------------------------------------------------------------------------------
					$this->note_m->save(array('user_id'=>$user->user_id, 'to_user_id'=>$puser[0]->user_id, 'note_message'=>$umessage, 'note_link'=>$link, 'note_linkname'=>'Go to Order'));
					
					//notification to admin -------------------------------------------------------------------------------------------------------------------------------------------
					$auser = $this->users_m->get_by(array('user_level' => 9));
					if(count($auser)){
						foreach($auser as $apu){
							$this->note_m->save(array('user_id'=>$user->user_id, 'to_user_id'=>$apu->user_id, 'note_message'=>$amessage, 'note_link'=>$link1, 'note_linkname'=>'Go to Order'));
						}
					}
					
					//promo notification to seller and buyer ---------------------------------------------------------------------------------------------------------------------------------------
					if($items['options']['disc'] != 0){
						$prmessage = '<strong>'.$user->user_username.'</strong> got discount of <strong>S$ '.number_format($items['options']['disc'], 2).'</strong> on <strong>'.$items['name'].'</strong>';
						$prumessage = 'You got discount of <strong>S$ '.number_format($items['options']['disc'], 2).'</strong> on <strong>'.$items['name'].'</strong>';
						
						$get = $this->promo_model->promo_details($items['name'], $items['options']['cat']);
						$count = $get[0]->promo_used_count+1;
						
						$this->promo_model->save(array('promo_used_count'=>$count), $get[0]->promo_id);
						
						//for owner ----------------------------------------------------------------------------------------------------------------------------------------------------------------
						$this->note_m->save(array('user_id'=>$user->user_id, 'to_user_id'=>$puser[0]->user_id, 'note_message'=>$prmessage, 'note_link'=>'', 'note_linkname'=>''));
						//for buyer ----------------------------------------------------------------------------------------------------------------------------------------------------------------
						$this->note_m->save(array('user_id'=>$puser[0]->user_id, 'to_user_id'=>$user->user_id, 'note_message'=>$prumessage, 'note_link'=>'', 'note_linkname'=>''));
						// for admin ---------------------------------------------------------------------------------------------------------------------------------------------------------------
						$auser = $this->users_m->get_by(array('user_level' => 9));
						if(count($auser)){
							foreach($auser as $apu){
								//promo notification to admin --------------------------------------------------------------------------------------------------------------------------------------
								$this->note_m->save(array('user_id'=>$user->user_id, 'to_user_id'=>$apu->user_id, 'note_message'=>$prmessage, 'note_link'=>'', 'note_linkname'=>''));
							}
						}
					}
					//buyer email notification product details ---------------------------------------------------------------------------------------------------------------------------------
					$message .= '<tr height="40">
						<td align="center" style="padding:3px;">'.$this->session->userdata('order_no').'</td>
						<td align="left" style="padding:3px;">'.$items['name'].'</td>
						<td align="left" style="padding:3px;">'.$items['options']['type'].'</td>
						<td align="right" style="padding:3px;">S$ '.number_format($items['price'], 2).'</td>
					  </tr>';
						  
					// Email Notification ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// if product Physical than a notification to product owner and order details for email to buyer and seller --------------------------------------------------------------------
					if($items['options']['type'] == 'Physical'){
						$this->db->where('order_no', $this->session->userdata('order_no'));
						$this->db->limit(1);
						$ship = $this->db->get('order_shipping')->result();
						$pmessage = 'You have a new order for shipment of product :<strong>'.$items['name'].'</strong>';
						$apmessage = 'New order for shipment of product :<strong>'.$items['name'].'</strong>';
						
						//seller email notification product details --------------------------------------------------------------------------------------------------------------------------------
						$mmessage .='<tr style="min-height:40px;">
							<td align="center" style="padding:3px;">'.$this->session->userdata('order_no').'</td>
							<td align="left" style="padding:3px;">'.$items['name'].'</td>
							<td align="left" style="padding:3px;">'.$items['options']['type'].'</td>
							<td align="right" style="padding:3px;">S$ '.number_format($items['price'], 2).'</td>
						  </tr>';
						  
						//seller email notification shipping details -------------------------------------------------------------------------------------------------------------------------------
						$shipadd ='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr style="min-height:40px;">
							<td style="padding:3px;">Name</td>
							<td style="padding:3px;">'.$ship[0]->shipping_fname.'</td>
						  </tr>
						  <tr style="min-height:40px;">
							<td style="padding:3px;">Address</td>
							<td style="padding:3px;">'.$ship[0]->shipping_address.',<br />
							City: '.$ship[0]->shipping_city.',<br />
							Postal Code: '.$ship[0]->shipping_pincode.'</td>
						  </tr>
						</table>';
						
						$sgtotal = $sgtotal + $items['price'];
						// get the discounted amount -----------------------------------------------------------------------------------------------------------------------------------------------
						if($prmcde != ''){
							$get = $this->promo_model->get_by(array('promo_code' => $prmcde, 'promo_type' => 'On Cart Amount'), 1);
							if($get->promo_percent_type == '%'){$dis = ($items['price'] * $get->promo_percent) / 100;}else
							if($get->promo_percent_type == 'S$'){$dis = $items['price']/$this->cart->total() * $get->promo_percent;}else{$dis = 0;}
							//$sgtotal = $sgtotal - $dis;
							//$sdiscount = $sdiscount + $dis;
						}
						$k++;
						// notification to product owner -------------------------------------------------------------------------------------------------------------------------------------------
						$this->note_m->save(array('user_id'=>$user->user_id, 'to_user_id'=>$puser[0]->user_id, 'note_message'=>$pmessage, 'note_link'=>$link, 'note_linkname'=>'Go to Order'));
						
						// notification to admin ---------------------------------------------------------------------------------------------------------------------------------------------------
						$auser = $this->users_m->get_by(array('user_level' => 9));
						if(count($auser)){
							foreach($auser as $apu){
								$this->note_m->save(array('user_id'=>$user->user_id, 'to_user_id'=>$apu->user_id, 'note_message'=>$apmessage, 'note_link'=>$link1, 'note_linkname'=>'Go to Order'));
							}
						}
						$i++;
						//seller email notification for physical product ---------------------------------------------------------------------------------------------------------------------------
						$ssubject = 'OpenSchoolbag - Please prepare delivery for order received';
						
						$smessage = '<div style="color:#129749; font-size:15px;"><img src="'.base_url().'files/emails/order_received.jpg" alt="" /><br>
							<p>Dear '.$fname.',<p>
							<p>You have an order for the following product(s):</p><br />
							
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							  <tr height="40" style="background-color:#cd3127; color:#fff;">
								<th>Order no</th><th>Product Name</th><th>Product type</th><th>Price</th>
							  </tr>
							  '.$mmessage.'
							  <tr height="40">
								<td align="right" colspan="4" style="padding:3px;">Discount</td>
								<td style="padding:3px;">S$ '.number_format(0 + $items['options']['cat'], 2).'</td>
							  </tr>';
							  /*<tr height="40">
								<td align="right" colspan="4" style="padding:3px;">Transaction Fee</td>
								<td style="padding:3px;">S$ '.number_format($site->trans_fee, 2).'</td>
							  </tr>*/
						$smessage .= '<tr height="40">
								<td align="right" colspan="4" style="padding:3px;">Grand Total</td>
								<td style="padding:3px;">S$ '.number_format($sgtotal, 2).'</td>
							  </tr>
							</table>
							<br /><br />
							<p>Please mail the order to:</p><br />
							  '.$shipadd.'
							<p>The product(s) must be mailed out within 10 working days. Please update the status of delivery from <a href="'.$link.'">View Sales</a></p>
						
							<p>&nbsp;</p>
							<p>Thank you.</p>
							
							<p>Regards,<br>
							OpenSchoolbag team<br><br>
							<a href="'.base_url().'faqs" style="color:#e16b68">FAQs</a> | <a href="'.base_url().'for_buyers" style="color:#e16b68">Buyer Guide</a> | <a href="'.base_url().'for_sellers" style="color:#e16b68">Seller Guide</a> | <a href="'.base_url().'products" style="color:#e16b68">Shop Now</a> | <a href="'.base_url().'contact" style="color:#e16b68">Contact Us</a></p></div>';
									
						$this->users_m->email($email, $ssubject, $smessage);
					}
					
					//promo notification to seller and buyer ---------------------------------------------------------------------------------------------------------------------------------------
					if($items['options']['disc'] != 0){
						$get = $this->promo_model->promo_details($items['name'], $items['options']['cat']);
						
						$values = array('start_date' => $get[0]->promo_start, 'end_date' => $get[0]->promo_end, 'discount_values' => $get[0]->promo_percent_type.' '.$get[0]->promo_percent, 'promo_code' => $get[0]->promo_code, 'promo_for' => $get[0]->product_title.''.$get[0]->promo_category, 'username' => $user->user_username, 'order_no' => $this->session->userdata('order_no'), 'date_used' => date("Y-m-d"), 'original_price' => $ftotal, 'purchased_amount' => $gtotal);
						
						//store in promo_used table for reports ------------------------------------------------------------------------------------------------------------------------------------
						$this->promo_model->promo_used($values);
					}
				}
				
				//if used promo code ---------------------------------------------------------------------------------------------------------------------------------------------------------------
				if($prmcde != ''){
					//get the promo code details ---------------------------------------------------------------------------------------------------------------------------------------------------
					$get = $this->promo_model->get_by(array('promo_code' => $prmcde, 'promo_type' => 'On Cart Amount'), 1);
					
					//promo code used notification messages ----------------------------------------------------------------------------------------------------------------------------------------
					$prmessage = '<strong>'.$user->user_username.'</strong> used a promo code <strong>'.$prmcde.'</strong> on total amount of <strong>S$ '.number_format($ftotal, 2).'</strong>';
					$prumessage = 'You have used a promo code <strong>'.$prmcde.'</strong> on total amount of <strong>S$ '.number_format($ftotal, 2).'</strong>';
					
					//increase promo code used value -----------------------------------------------------------------------------------------------------------------------------------------------
					if($j == 0){
						$count = $get->promo_used_count+1;
						$this->promo_model->save(array('promo_used_count'=>$count), $get->promo_id);
					}
					$j++;
					
					//for owner --------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//$this->note_m->save(array('user_id'=>$user->user_id, 'to_user_id'=>$puser[0]->user_id, 'note_message'=>$prmessage, 'note_link'=>'', 'note_linkname'=>''));
					//for buyer --------------------------------------------------------------------------------------------------------------------------------------------------------------------
					$this->note_m->save(array('user_id'=>$puser[0]->user_id, 'to_user_id'=>$user->user_id, 'note_message'=>$prumessage, 'note_link'=>'', 'note_linkname'=>''));
					// for admin -------------------------------------------------------------------------------------------------------------------------------------------------------------------
					$auser = $this->users_m->get_by(array('user_level' => 9));
					if(count($auser)){
						foreach($auser as $apu){
							//promo notification to admin ------------------------------------------------------------------------------------------------------------------------------------------
							$this->note_m->save(array('user_id'=>$user->user_id, 'to_user_id'=>$apu->user_id, 'note_message'=>$prmessage, 'note_link'=>'', 'note_linkname'=>''));
						}
					}
					
					$values = array('start_date' => $get->promo_start, 'end_date' => $get->promo_end, 'discount_values' => $get->promo_percent_type.' '.$get->promo_percent, 'promo_code' => $get->promo_code, 'promo_for' => $get->promo_type, 'username' => $user->user_username, 'order_no' => $this->session->userdata('order_no'), 'date_used' => date("Y-m-d"));
				}
				//if used promo code ---------------------------------------------------------------------------------------------------------------------------------------------------------------
				
				//transaction fee, final amount and discounted amount store in orders table --------------------------------------------------------------------------------------------------------
				$this->orders_m->save_order(array('order_no' => $this->session->userdata('order_no'), 'trans_fee' => $site->trans_fee, 'total_amount' => $ftotal, 'final_amount' => $gtotal));
				
				//update promo_used table purchased amount for reports -----------------------------------------------------------------------------------------------------------------------------
				if($prmcde != ''){
					$values['original_price'] = $ftotal;
					$values['purchased_amount'] = $gtotal;
					
					//store in promo_used table for reports ----------------------------------------------------------------------------------------------------------------------------------------
					$promo_used = $this->promo_model->promo_used($values);
				}
				//destroy the cart history ---------------------------------------------------------------------------------------------------------------------------------------------------------
				$this->cart->destroy();
				//destroy the cart history ---------------------------------------------------------------------------------------------------------------------------------------------------------
			}
			//Email Notification -------------------------------------------------------------------------------------------------------------------------------------------------------------------
			//buyer email notification -------------------------------------------------------------------------------------------------------------------------------------------------------------
			$message .= '<tr height="40">
					<td align="right" colspan="4" style="padding:3px;">Discount</td>
					<td style="padding:3px;">S$ '.number_format(0 + $discount, 2).'</td>
				  </tr>
				  <tr height="40">
					<td align="right" colspan="4" style="padding:3px;">Transaction Fee</td>
					<td style="padding:3px;">S$ '.number_format(0 + $site->trans_fee, 2).'</td>
				  </tr>
				  <tr height="40">
					<td align="right" colspan="4" style="padding:3px;">Grand Total</td>
					<td style="padding:3px;">S$ '.number_format($gtotal+$site->trans_fee, 2).'</td>
				  </tr>
				</table>';
			if($i!=0){
				$message .= '<br /><br />
					<p>Your order(s) will be mailed to:</p><br />
					  '.$shipadd.'
					<p>Your order(s) will be mailed out within 10 working days. You can check the status of delivery from your account under <a href="'.base_url().'my_orders">View Purchases</a></p>';
			}
			$message .= '<p>Please post a review and rating on the products that you have purchased.</p>
				<p>&nbsp;</p>
				<p>Thank you.</p>
				
				<p>Regards,<br>
				OpenSchoolbag team<br><br>
				<a href="'.base_url().'faqs" style="color:#e16b68">FAQs</a> | <a href="'.base_url().'for_buyers" style="color:#e16b68">Buyer Guide</a> | <a href="'.base_url().'for_sellers" style="color:#e16b68">Seller Guide</a> | <a href="'.base_url().'products" style="color:#e16b68">Shop Now</a> | <a href="'.base_url().'contact" style="color:#e16b68">Contact Us</a></p></div>';
					
			$this->users_m->email($user->user_email, $subject, $message);
			
			$array_items = array('order_no' => '');
			$this->session->unset_userdata($array_items);
			redirect('shop/status');
		}else{
			redirect('shop/failed');
		}
    }
	
	public function status() {
		
		$data['title'] = 'Success';
		$data['message'] = 'Thank you. Your purchase transaction is completed. Please go to Order History to download your purchased digital products or check the delivery status of your purchased physical products.';
		
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('orders/order_status', $data);
		$this->load->view('templates/footer', $data);
	}
 
    public function cancel() {
		$this->cart->destroy();
		$data['title'] = 'Cancelled / Failed';
		$data['message'] = '';
		
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('orders/order_status', $data);
		$this->load->view('templates/footer', $data);
    }
 
    public function failed() {
		$this->cart->destroy();
		$data['title'] = 'Failed';
		$data['message'] = '';
		
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('orders/order_status', $data);
		$this->load->view('templates/footer', $data);
    }
}