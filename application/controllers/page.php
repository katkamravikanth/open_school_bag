<?php
class Page extends Frontend_Controller{
	
	public function __construct(){
		parent::__construct();
        if (isset($_SERVER['HTTP_REFERER'])){
            $this->session->set_userdata('prev_url', $_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_userdata('prev_url', base_url());
        }
		$this->load->model('site_m');
		$this->load->model('page_m');
		$this->load->model('product_m');
		$this->load->model('partner_m');
		$this->load->model('admin/images_model');
		$this->load->model('admin/cms_model');
	}
	
	public function about(){
		$data['title'] = 'About Us';
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		$data['slider'] = $this->images_model->image_by(array('path' => 'about-slider'));
		$data['who'] = $this->images_model->image_by(array('path' => 'who-we-care'));
		$data['icons'] = $this->images_model->image_by(array('path' => 'product-icon'));
		
		$data['page'] = $this->page_m->get_by(array('slug' => 'about'));
		$data['page1'] = $this->page_m->get_by(array('slug' => 'about1'));
		$data['page2'] = $this->page_m->get_by(array('slug' => 'why_osb'));
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/slider_search');
		$this->load->view('about', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function partners(){
		$data['title'] = 'Our Partners';
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		$data['page'] = $this->page_m->get_by(array('slug' => 'partners'));
		$data['partners'] = $this->partner_m->get_by_lt(4);
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/slider_search');
		$this->load->view('partners', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function buyers(){
		$data['title'] = 'For Buyers';
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		$data['images'] = $this->images_model->image_by(array('path' => 'buyer'));
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/slider_search');
		$this->load->view('for_buyer', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function sellers(){
		$data['title'] = 'For Seller';
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		$data['images'] = $this->images_model->image_by(array('path' => 'seller'));
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/slider_search');
		$this->load->view('for_seller', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function privacy_policy(){
		$data['title'] = 'Privacy Policy';
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		$data['page'] = $this->page_m->get_by(array('slug' => 'privacy_policy'));
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/slider_search');
		$this->load->view('privacy_policy', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function tnc(){
		$data['title'] = 'Terms and Conditions';
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		$data['page'] = $this->page_m->get_by(array('slug' => 'tnc'));
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/slider_search');
		$this->load->view('tnc', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function contact(){
		$this->load->helper('file');
		//if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
            $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[4]|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric|min_length[8]|xss_clean');
            $this->form_validation->set_rules('comment_type', 'Comment Type', 'required');
            $this->form_validation->set_rules('comment', 'Comment', 'trim|required|min_length[4]|xss_clean');
			
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run()){
				$subject = 'Contact form Details from OpenSchoolbag';
				$message = '<div style="color:#129749; font-size:15px;"><img src="'.base_url().'files/emails/enquiry_received.jpg" alt="" /><br>
				<p>Please find the below details of contact form</p>
				<p>&nbsp;</p>
				<p>Name    : '.$this->input->post('name').'</p>
				<p>Email   : '.$this->input->post('email').'</p>
				<p>Phone   : '.$this->input->post('phone').'</p>
				<p>Company : '.$this->input->post('company').'</p>
				<p>Comment Type : '.$this->input->post('comment_type').'</p>
				<p>Comment : '.$this->input->post('comment').'</p>
				<p>&nbsp;</p>
				<p>Thanks and Regards<br />
				<strong>OpenSchoolbag Team</strong></p></div>';
				
				//mail for user
				$subjec = 'OpenSchoolbag- Thank you for your enquiry';
				$messages = '<div style="color:#129749; font-size:15px;"><img src="'.base_url().'files/emails/enquiry_received.jpg" alt="" /><br>
				<p>Dear '.$this->input->post('name').',<br><br>
				We have received your enquiry and will get back to you within the two business days.</p>
				
				<p>&nbsp;</p>
				<p>Thank you.</p>
				
				<p>Regards,<br>
				OpenSchoolbag team<br><br>
				<a href="'.base_url().'faqs" style="color:#e16b68">FAQs</a> | <a href="'.base_url().'for_buyers" style="color:#e16b68">Buyer Guide</a> | <a href="'.base_url().'for_sellers" style="color:#e16b68">Seller Guide</a> | <a href="'.base_url().'products" style="color:#e16b68">Shop Now</a> | <a href="'.base_url().'contact" style="color:#e16b68">Contact Us</a></p></div>';
				
				//if the insert has returned true then we show the flash message
                $this->load->library('email');
			
				$config['charset'] = 'utf-8';
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = 'html';
				$config['protocol'] = 'sendmail';
				
				$this->email->initialize($config);
				$this->email->from('contact@openschoolbag.com.sg', 'Admin - OpenSchoolbag');
				$this->email->to('contact@openschoolbag.com.sg');
				//$this->email->bcc('info@openschoolbag.com.sg');
				
				$this->email->subject($subject);
				$this->email->message('<html><body>'.$message.'</body></html>');	
				
				//$this->email->send();
				
				if($this->email->send()){
					$this->email->initialize($config);
					$this->email->from('contact@openschoolbag.com.sg', 'Admin - OpenSchoolbag');
					$this->email->to($this->input->post('email'));
					
					$this->email->subject($subjec);
					$this->email->message('<html><body>'.$messages.'</body></html>');	
					$this->email->send();
					
					//$this->page_m->email($this->input->post('email'), $subject, $message)
					//$this->page_m->email('info@openschoolbag.com.sg', $subject, $message);
					//print $this->email->print_debugger();
                    $data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }
            }
        }
		$data['title'] = 'Contact Us';
		$data['captcha'] = $this->captcha();
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		$data['site'] = $this->site_m->get_by(array('site_id' => 1),1);
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/slider_search');
		$this->load->view('contact', $data);
		$this->load->view('templates/footer', $data);
	}
	
	function captcha(){
		$this->load->helper('captcha');
		$vals = array(
			'img_path' => './files/captcha/',
			'img_url' => base_url().'files/captcha/'
		);
		$cap = create_captcha($vals);
		$data = array(
			'captcha_time' => $cap['time'],
			'ip_address' => $this->input->ip_address(),
			'word' => $cap['word']
		);
		$this->session->set_userdata($data);
		return $cap['image'];
	}
	
	function captchajax(){
		$this->load->helper('captcha');
		$vals = array(
			'img_path' => './files/captcha/',
			'img_url' => base_url().'files/captcha/'
		);
		$cap = create_captcha($vals);
		$data = array(
			'captcha_time' => $cap['time'],
			'ip_address' => $this->input->ip_address(),
			'word' => $cap['word']
		);
		$this->session->set_userdata($data);
		print $cap['image'];
	}
	
	function captcha_keyword(){
		print $this->session->userdata('word');
	}
	
	public function faqs(){
		$data['title'] = "FAQs";
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		$data['page'] = $this->page_m->get_by(array('slug' => 'faqs'));
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/slider_search');
		$this->load->view('faqs', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function sitemap(){
		$data['title'] = 'SiteMap';
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		$data['page'] = $this->cms_model->get();
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/slider_search');
		$this->load->view('sitemap', $data);
		$this->load->view('templates/footer', $data);
	}
	
	//CMS pages
	
	public function cms_pages($page){
		$data['page'] = $pages = $this->cms_model->get_by(array('slug' => $page));
		if(!count($pages)){
			// Whoops, we don't have a page for that!
			show_404();
		}
		$data['title'] = $pages[0]->title;
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/slider_search');
		$this->load->view('pages', $data);
		$this->load->view('templates/footer', $data);
	}
}