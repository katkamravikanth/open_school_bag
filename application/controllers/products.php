<?php
class Products extends Frontend_Controller {
	
	public function __construct(){
		parent::__construct();
        if (isset($_SERVER['HTTP_REFERER'])){
            $this->session->set_userdata('prev_url', $_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_userdata('prev_url', base_url());
        }
		$this->load->library('cart');
		$this->load->library('threaded');
		//$this->load->library('Ajax_pagination');
		
		$this->load->model('admin/images_model');
		$this->load->model('product_m');
		$this->load->model('qna_m');
		$this->load->model('review_m');
		$this->load->model('note_m');
		$this->load->model('users_m');
		$this->load->model('profile_m');
		$this->load->model('cart_m');
		$this->load->model('orders_m');
	}
	
	public function index(){
		
		if($this->session->userdata('username')!=''){
			$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
			$data['user_id'] = $user->user_id;
		}else{
			$data['user_id'] = 0;
		}
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'products';
		$config['total_rows'] = $this->product_m->total();
		$config['per_page'] = 9;
		$config["uri_segment"] = 2;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		
		$data['title'] = 'Our Products';
		$data['products'] = $this->product_m->by_limit($config["per_page"], $page);
		$data['pagination'] = $this->pagination->create_links();
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/slider_search');
		$this->load->view('product/products', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function my_products(){
		if(!$this->session->userdata('logged_in') && ($this->session->userdata('level') != 2 || $this->session->userdata('level') != 9)){
			redirect(base_url());
		}
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'my_products';
		$config['total_rows'] = $this->product_m->total_no($user->user_id);
		$config['per_page'] = 9;
		$config["uri_segment"] = 2;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		$this->pagination->initialize($config); 
		
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		
		$data['title'] = 'My Products';
		$data['user_id'] = $user->user_id;
		$data['products'] = $this->product_m->my_limit($config["per_page"], $page, $user->user_id);
		$data['pagination'] = $this->pagination->create_links();
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('product/my_products', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function product_view($unique){
		$this->load->helper('file');
		
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		$data['title'] = 'Product Details';
		$data['product'] = $prod = $this->product_m->by_product($unique);
		
		if(!count($prod) || !count($prod['pro'])){
			redirect('products', 'refresh');
		}
		
		$data['user'] = $this->users_m->get_by(array('user_id' => $prod['pro']->user_id), 1);
		$data['profile'] = $this->profile_m->get_by(array('user_id' => $prod['pro']->user_id), 1);
		$data['user_id'] = $user->user_id;
		
		$data['rate'] = $this->review_m->get_by(array('product_unique' => $unique));
		$data['selrate'] = $this->review_m->get_seller_by($prod['pro']->user_id);
		
		$data['qna'] = $this->qna_m->get_qna($unique);
		$data['review'] = $this->review_m->get_reviews($unique);
		$data['captcha'] = $this->captcha();
		$data['re_show'] = $this->orders_m->get_by(array('user_id' => $user->user_username, 'order_punique' => $unique));
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/slider_search');
		$this->load->view('product/product_view', $data);
		$this->load->view('templates/footer', $data);
	}
	
	function captcha(){
		$this->load->helper('captcha');
		$vals = array(
			'img_path' => './files/captcha/',
			'img_url' => base_url().'files/captcha/'
		);
		$cap = create_captcha($vals);
		$data = array(
			'captcha_time' => $cap['time'],
			'ip_address' => $this->input->ip_address(),
			'word' => $cap['word']
		);
		$this->session->set_userdata($data);
		return $cap['image'];
	}
	
	public function add_product($unique=FALSE){
		if(!$this->session->userdata('logged_in') && ($this->session->userdata('level') != 2 || $this->session->userdata('level') != 9)){
			redirect(base_url());
		}
		
		// clear file names from session variables
		$newdata = array(
			'ppic'  => $this->session->userdata('ppic'),
			'pfile'  => $this->session->userdata('pfile'),
		);
		$this->session->unset_userdata($newdata);
		if($unique == TRUE){
			$array_items = array('ppic' => '', 'ppview' => '', 'ppview1' => '', 'ppview2' => '', 'ppview3' => '', 'ppview4' => '', 'pfile' => '');
			$this->session->unset_userdata($array_items);
			
			$data['title'] = 'Edit Product';
			$data['product'] = $this->product_m->get_by(array('product_unique' => $unique, 'product_status' => 'Active'), 1);
			if(!count($data['product'])){
				redirect('products', 'refresh');
			}
		}else{
			$data['title'] = 'Add Product';
		}
		
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('product/add_product', $data);
		$this->load->view('templates/footer', $data);
	}
	
	//product images and product file upload and store to session
	public function upload_file(){
		if(!$this->session->userdata('logged_in') && ($this->session->userdata('level') != 2 || $this->session->userdata('level') != 9)){
			redirect(base_url());
		}
		$status = "";
		$msg = "";
		$file_element_name = $this->input->post('p_name');
		$path = '';
		$type = '';
		
		if($this->input->post('p_name') == 'ppic'){
			$path = 'product_pic/';
		}else if($this->input->post('p_name') == 'pfile'){
			$path = 'product_file/';
		}
		
		if ($status != "error"){
			$config['upload_path'] = 'files/'.$path;
			$config['allowed_types'] = '*';
			$config['max_size']  = 1024 * 99;
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config);
			
			if(!$this->upload->do_upload($file_element_name)){
				$status = 'error';
				$msg = $this->upload->display_errors('', '');
			}else{
				$data = $this->upload->data();
				$newdata = array(
					$this->input->post('p_name')  => $path.$data['file_name'],
				);
				$this->session->set_userdata($newdata);
				
				if($data['file_name']){
					$status = "success";
					$msg = "File successfully uploaded";
				}else{
					unlink($data['full_path']);
					$status = "error";
					$msg = "Something went wrong when saving the file, please try again.";
				}
			}
			@unlink($_FILES[$file_element_name]);
		}
		echo json_encode(array('status' => $status, 'msg' => $msg));
	}
	
	//product preview images upload and store to session
	public function preview_file(){
		if(!$this->session->userdata('logged_in') && ($this->session->userdata('level') != 2 || $this->session->userdata('level') != 9)){
			redirect(base_url());
		}
		$status = "";
		$msg = "";
		$file_element_name = $this->input->post('p_name');
		
		if ($status != "error"){
			$config['upload_path'] = 'files/product_pic';
			$config['allowed_types'] = '*';
			$config['max_size']  = 1024 * 99;
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config);
			
			if(!$this->upload->do_upload($file_element_name)){
				$status = 'error';
				$msg = $this->upload->display_errors('', '');
			}else{
				$data = $this->upload->data();
				$newdata = array(
					$this->input->post('p_name')  => 'product_pic/'.$data['file_name'],
				);
				$this->session->set_userdata($newdata);
				
				if($data['file_name']){
					$status = "success";
					$msg = "File successfully uploaded";
				}else{
					unlink($data['full_path']);
					$status = "error";
					$msg = "Something went wrong when saving the file, please try again.";
				}
			}
			@unlink($_FILES[$file_element_name]);
		}
		echo json_encode(array('status' => $status, 'msg' => $msg));
	}
	
	//product adding or editing	
	public function addit_product(){
		
		$publishers = mysql_real_escape_string($this->input->post('publishers'));
		if($publishers == 0 || $publishers == ''){
			$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
			$uid = $user->user_id;
		}else{
			$uid = $publishers;
			$user = $this->users_m->get_by(array('user_id' => $uid), 1);
		}
		$ptype = mysql_real_escape_string($this->input->post('ptype'));
		$pname = mysql_real_escape_string($this->input->post('pname'));
		$plevel = $this->input->post('plevel');
		$psubject = mysql_real_escape_string($this->input->post('psubject'));
		$presource = $this->input->post('presource');
		$pdesc = mysql_real_escape_string($this->input->post('pdesc'));
		$pprice = mysql_real_escape_string($this->input->post('pprice'));
		$pdownd = mysql_real_escape_string($this->input->post('pdownd'));
		$ppages = mysql_real_escape_string($this->input->post('ppages'));
		$pweight = mysql_real_escape_string($this->input->post('pweight'));
		$pdisc = mysql_real_escape_string($this->input->post('pdisc'));
		$sprice = mysql_real_escape_string($this->input->post('sprice'));
		$ppviews = mysql_real_escape_string($this->input->post('ppviews'));
		$cppic = mysql_real_escape_string($this->input->post('cppic'));
		$cpfile = mysql_real_escape_string($this->input->post('cpfile'));
		
		$unid = $this->users_m->pword_string(5).time().$this->users_m->pword_string(5);
		
		//product picture check whether it is uploaded or not
		if($this->session->userdata('ppic')){
			if($cppic != ''){unlink('files/'.$cppic);}
			$ppic = mysql_real_escape_string($this->session->userdata('ppic'));
		}else{
			$ppic = $cppic;
		}
		
		//preview images check whether it is uploaded or not
		$ppview = '';
		if($this->session->userdata('ppview')){
			$ppview .= mysql_real_escape_string($this->session->userdata('ppview')).',';
		}
		if($this->session->userdata('ppview1')){
			$ppview .= mysql_real_escape_string($this->session->userdata('ppview1')).',';
		}
		if($this->session->userdata('ppview2')){
			$ppview .= mysql_real_escape_string($this->session->userdata('ppview2')).',';
		}
		if($this->session->userdata('ppview3')){
			$ppview .= mysql_real_escape_string($this->session->userdata('ppview3')).',';
		}
		if($this->session->userdata('ppview4')){
			$ppview .= mysql_real_escape_string($this->session->userdata('ppview4')).',';
		}
		$ppview = rtrim($ppview, ',');
		
		if($ppview == ''){$ppview = $ppviews;}
		
		//product file check whether it is uploaded or not
		if($this->session->userdata('pfile')){
			if($cpfile != ''){unlink('files/'.$cpfile);}
			$pfile = mysql_real_escape_string($this->session->userdata('pfile'));
		}else{
			$pfile = $cpfile;
		}
		
		if($this->input->post('punique')){
			// Update
			$punique = $this->input->post('punique');
			$prod = $this->product_m->get_by(array('product_unique' => $punique, 'product_status' => 'Active'), 1);
			
			if(!count($prod)){
				redirect('products', 'refresh');
			}
			
			$id = $this->product_m->save(array('user_id' => $uid, 'product_type' => $ptype, 'product_name' => $pname, 'product_level' => $plevel, 'product_subject' => $psubject, 'product_resource' => $presource, 'product_desc' => $pdesc, 'product_price' => $pprice, 'product_down_days' => $pdownd, 'product_pages' => $ppages, 'product_weight' => $pweight, 'product_pic' => $ppic, 'product_preview' => $ppview, 'product_file' => $pfile, 'product_desclaimer' => $pdisc, 'product_shipping' => $sprice, 'product_status' => 'Active'), $prod->product_id);
		}else{
			// Save
			$id = $this->product_m->save(array('user_id' => $uid, 'product_type' => $ptype, 'product_name' => $pname, 'product_level' => $plevel, 'product_subject' => $psubject, 'product_resource' => $presource, 'product_desc' => $pdesc, 'product_price' => $pprice, 'product_down_days' => $pdownd, 'product_pages' => $ppages, 'product_weight' => $pweight, 'product_pic' => $ppic, 'product_preview' => $ppview, 'product_file' => $pfile, 'product_desclaimer' => $pdisc, 'product_shipping' => $sprice, 'product_unique' => $unid, 'product_status' => 'Active'));
		}
		
		if($id != ''){
			$array_items = array('ppic' => '', 'ppview' => '', 'ppview1' => '', 'ppview2' => '', 'ppview3' => '', 'ppview4' => '', 'pfile' => '');
			$this->session->unset_userdata($array_items);
			
			$umessage = 'New product posted by :<strong>'.$user->user_username.'</strong>, Product name :<strong>'.$pname.'</strong>';
			$smessage = 'Your have posted new product, Product name :<strong>'.$pname.'</strong>';
			$link = base_url().'product/'.$unid;
			
			$this->note_m->save(array('user_id'=>$uid, 'to_user_id'=>$uid, 'note_message'=>$smessage, 'note_link'=>$link, 'note_linkname'=>'Go to Product'));
			
			// notification to admin
			$auser = $this->users_m->get_by(array('user_level' => 9));
			if(count($auser)){
				foreach($auser as $apu){
					$this->note_m->save(array('user_id'=>$uid, 'to_user_id'=>$apu->user_id, 'note_message'=>$umessage, 'note_link'=>$link, 'note_linkname'=>'Go to Product'));
				}
			}
			
			if($this->input->post('punique') != ''){
				print $this->input->post('punique');
			}else{
				print $unid;
			}
		}else{
			return FALSE;
		}
	}
	
	public function del_product($unique){
		if(!$this->session->userdata('logged_in') && ($this->session->userdata('level') != 2 || $this->session->userdata('level') != 9)){
			redirect(base_url());
		}
		$prod = $this->product_m->get_by(array('product_unique' => $unique, 'product_status' => 'Active'), 1);
		
		if(!count($prod)){
			redirect('products', 'refresh');
		}
		
		$ord = $this->orders_m->get_by(array('order_punique' => $prod->product_unique));
		$i=0;
		if(count($ord)){
			foreach($ord as $order){
				if($order->order_ptype == 'Physical'){
					if($order->order_status != 'Shipped'){
						$i++;
					}
				}else{
					$date = $order->created;
					$date = strtotime($date);
					$date = strtotime("+".$prod->product_down_days." day", $date);
					$date = date('Y-m-d H:i:s', $date);
					if($date > date('Y-m-d H:i:s')){
						$i++;
					}
				}
			}
		}
		if($i == 0){
			$data = array('product_status' => 'Inactive');
			$this->product_m->delete($prod->product_id, $data);
			redirect('my_products/state/yes');
		}else{
			redirect('product/'.$unique.'/no');
		}
	}
	
	public function star(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		$rate = $this->input->post("rate_val", true);
		$product_unique = $this->input->post("product_unique", true);
		
		if($this->session->userdata('logged_in')){  //get_user_id() return login user id
			$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
			$rated = $this->review_m->get_by(array('user_id' => $user->user_id, 'product_unique' => $product_unique, 'rate !=' => 0), 1);
			if (!count($rated)){
				$this->session->set_userdata(array('rate'  => $rate));
			}
		}else{
			echo print_r("You have to login to rate the product");
		}
		//exit(0);
    }
	
	public function add_review(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		$comment = $this->input->post("comment");
		$unique = $this->input->post("unique");
		
		$rated = $this->review_m->get_by(array('user_id' => $user->user_id, 'product_unique' => $unique, 'rate' => 0));
		if (!count($rated)){
			$prod = $this->product_m->get_by(array('product_unique' => $unique, 'product_status' => 'Active'), 1);
			$rate = $this->session->userdata('rate');
			$review = $this->review_m->save(array('user_id'=>$user->user_id, 'to_user_id' => $prod->user_id, 'product_unique'=>$unique, 'review'=>$comment, 'rate'=>$rate));
			
			// Notifications
			$puser = $this->product_m->get_by(array('product_unique' => $unique, 'product_status' => 'Active'), 1);
			$puser_id = $puser->user_id;
			$message = 'You have new review for the product :'.$puser->product_name;
			$link = base_url().'product/'.$unique;
			$this->note_m->save(array('user_id'=>$user->user_id, 'to_user_id'=>$puser_id, 'note_message'=>$message, 'note_link'=>$link, 'note_linkname'=>'Go to Product'));
			
			if($review){
				print_r('Your review has been posted and will appear after you refresh');
			}else print_r('Your review not posted');
		}else{
			print_r('Your have already posted a review');
		}
	}
	
	public function delete_review(){
		$review_id = $this->input->post("review_id");
		$quest = $this->review_m->delete_review($review_id);
		if($quest){
			print_r('Review deleted');
		}else print_r('Review not deleted');
	}
	
	public function add_qna(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		$question = $this->input->post("question");
		$unique = $this->input->post("unique");
		$quest = $this->qna_m->save(array('user_id' => $user->user_id, 'product_unique' => $unique, 'qna_body' => $question));
		
		// Notifications
		$puser = $this->product_m->get_by(array('product_unique' => $unique, 'product_status' => 'Active'), 1);
		$puser_id = $puser->user_id;
		$message = 'You have new question for the product :'.$puser->product_name;
		$link = base_url().'product/'.$unique;
		$this->note_m->save(array('user_id'=>$user->user_id, 'to_user_id'=>$puser_id, 'note_message'=>$message, 'note_link'=>$link, 'note_linkname'=>'Go to Product'));
		
		if($quest){
			print_r('Your question has been posted and will appear after you refresh');
		}else print_r('Your question not posted');
	}
	
	public function add_qna_reply(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		$uid = $this->input->post("uid");
		$qna_id = $this->input->post("qna_id");
		$question = $this->input->post("question");
		$unique = $this->input->post("unique");
		
		$quest = $this->qna_m->save(array('user_id' => $user->user_id, 'product_unique' => $unique, 'qna_body' => $question, 'parent_id' => $qna_id));
		
		// Notifications
		$puser = $this->product_m->get_by(array('product_unique' => $unique, 'product_status' => 'Active'), 1);
		$puser_id = $puser->user_id;
		$message = 'You have answer to your question for the product :'.$puser->product_name;
		$link = base_url().'product/'.$unique;
		$this->note_m->save(array('user_id'=>$puser_id, 'to_user_id'=>$uid, 'note_message'=>$message, 'note_link'=>$link, 'note_linkname'=>'Go to Product'));
		
		if($quest){
			print_r('Your reply has been posted and will appear after you refresh');
		}else print_r('Your reply not posted');
	}
	
	public function delete_qna(){
		$qna_id = $this->input->post("qna_id");
		$quest = $this->qna_m->delete_qna($qna_id);
		if($quest){
			print_r('Question and Answers deleted');
		}else print_r('Question and Answers not deleted');
	}
	
	// Cart
	public function cucart(){
		if($this->cart->total_items() != 0){
			$i = 1;
			$data = '<div class="bottom-button" style="margin:10px 0; padding:0; float:left;">
                    <div class="bottom-button1">
                      <div class="bottom-button2"> <a href="javascript:;" id="clear_cart"><h3 style="margin:0 0 0 5px; font-size:14px; padding:0;">Clear Cart</h3></a> </div>
                    </div>
                  </div><br />
                  <table width="170" cellpadding="0" cellspacing="0"  border="0" style="border:1px solid #093; text-align:left;">
                    <tr>
                      <th scope="col" style="border-bottom:1px solid #093; background:#edf4f0; padding:8px;">Name</th>
                      <th scope="col" style="border-bottom:1px solid #093; background:#edf4f0;">Price</th>
                    </tr>';
			foreach ($this->cart->contents() as $items){
				form_hidden($i.'[rowid]', $items['rowid']);
				$data .= '<tr><td style="padding:10px;">'.$items['name'].'</td><td style="padding-right:3px;">S$ '.number_format($items['price'], 2).'</td></tr>';
			}
			$data .= '</table>
					<table width="170" cellpadding="0" cellspacing="0"  border="0" style="border:1px solid #093; text-align:left; margin:20px 0;">
                    <tr>
                      <th scope="col" style="background:#edf4f0; padding:8px;">Total</th>
                      <th scope="col" style="background:#edf4f0; text-align:right; padding-right:5px;">S$ '.number_format($this->cart->total(), 2).'</th>
                    </tr>
                  </table>
                  <div class="bottom-button" style="margin:-5px 0 40px 0; padding:0; float:left;">
                    <div class="bottom-button1">
                      <div class="bottom-button2"> <input type="button" name="place_order" id="place_order" class="place_order_but" value="Checkout" /> </div>
                    </div>
                  </div>';
			
			print $data;
		}else{
			print '<br /><table width="170" cellpadding="0" cellspacing="0"  border="0" style="border:1px solid #093; text-align:left;">
                    <tr>
                      <th scope="col" style="border-bottom:1px solid #093; background:#edf4f0; padding:8px;">Name</th>
                      <th scope="col" style="border-bottom:1px solid #093; background:#edf4f0;">Price</th>
                    </tr>
                    <tr>
                      <td style="padding:10px;" colspan="2">You don\'t have any product(s) in your cart</td>
                    </tr>
                  </table>
                  <table width="170" cellpadding="0" cellspacing="0"  border="0" style="border:1px solid #093; text-align:left; margin:20px 0;">
                    <tr>
                      <th scope="col" style="background:#edf4f0; padding:8px;">Total</th>
                      <th scope="col" style="background:#edf4f0; text-align:right; padding-right:5px;"> S$0.00</th>
                    </tr>
                  </table>';
		}
	}
	
	public function cart_cp(){
		$data = '0 item(s) - $0.00';
		if($this->cart->total_items() != 0){$no = $this->cart->total_items();$tot = $this->cart->total();}else{$no = 0;$tot = 0;}
		$data = $no.' item(s) - S$'.number_format($tot, 2);
		print $data;
	}
	
	public function add_cart(){
		if(!$this->session->userdata('order_no')){
			$newdata = array('order_no'  => time()+1000);
			$this->session->set_userdata($newdata);
		}
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		$unique = mysql_real_escape_string($this->input->post('unique'));
		$disc = mysql_real_escape_string($this->input->post('disc'));
		$sprice = mysql_real_escape_string($this->input->post('sprice'));
		
		$puser = $this->product_m->get_by(array('product_unique' => $unique, 'product_status' => 'Active'), 1);
		
		$uid = $user->user_id;
		$pic = $puser->product_pic;
		$pname = $puser->product_name;
		$price = $puser->product_price - $disc;
		$level = $puser->product_level;
		$subject = $puser->product_subject;
		$resource = $puser->product_resource;
		$order_no = $this->session->userdata('order_no');
		
		$cat = $level.':'.$subject.':'.$resource;
		
		$data = array(
			'id' => $order_no,
			'qty' => 1,
			'name' => $pname,
			'price' => $price,
			'options' => array('punique' =>$unique, 'user_id' => $uid, 'type' => $puser->product_type, 'pic' => $pic, 'disc' => $disc, 'sprice' => $sprice, 'cat' => $cat),
		);
		$id = $this->cart->insert($data);
		
		if($id != ''){
			print $id;
		}else{
			return FALSE;
		}
	}
	
	public function remove_cart(){
		$rowid = mysql_real_escape_string($this->input->post('rowid'));
		$data = array(
			'rowid' => $rowid,
			'qty' => 0,
		);
		$id = $this->cart->update($data);
		
		if($id != ''){
			print $id;
		}else{
			return FALSE;
		}
	}
	
	public function clear_cart(){
		$this->cart->destroy();
	}
	
	// search function
	public function search(){
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		if(!$this->session->userdata('searched')){
			$level = mysql_real_escape_string($this->input->post('s_level'));
			$subject = mysql_real_escape_string($this->input->post('s_subject'));
			$rtype = mysql_real_escape_string($this->input->post('s_resource_type'));
			$stext = mysql_real_escape_string($this->input->post('s_search_text'));
		}else{
			$newdata = array(
				'level'  => mysql_real_escape_string($this->input->post('s_level')),
				'subject'  => mysql_real_escape_string($this->input->post('s_subject')),
				'rtype'     => mysql_real_escape_string($this->input->post('s_resource_type')),
				'stext'     => mysql_real_escape_string($this->input->post('s_search_text')),
				'searched' => TRUE
			);
			$this->session->set_userdata($newdata);
			
			$level = mysql_real_escape_string($this->session->userdata('level'));
			$subject = mysql_real_escape_string($this->session->userdata('subject'));
			$rtype = mysql_real_escape_string($this->session->userdata('rtype'));
			$stext = mysql_real_escape_string($this->session->userdata('stext'));
		}
		
		$title = '';
		
		if($level != ''){
			$title .= $level.' > ';
		}
		if($subject != ''){
			$title .= $subject.' > ';
		}
		if($rtype != ''){
			$title .= $rtype.' > ';
		}
		if($stext != ''){
			$title .= $stext;
		}
		
		$uri = 2;
		$this->load->library('pagination');
		$config['base_url'] = base_url().'search';
		$config['total_rows'] = $this->product_m->search_total_no($level, $subject, $rtype, $stext);
		$config['per_page'] = 9;
		$config["uri_segment"] = $uri;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		$this->pagination->initialize($config); 
		
		$page = ($this->uri->segment($uri)) ? $this->uri->segment($uri) : 0;
		
		$data['title'] = rtrim($title, ' > ');
		$data['user_id'] = $user->user_id;
		$data['products'] = $this->product_m->search($config["per_page"], $page, $level, $subject, $rtype, $stext);
		$data['pagination'] = $this->pagination->create_links();
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/slider_search');
		$this->load->view('product/products', $data);
		$this->load->view('templates/footer', $data);
	}
	
	// search by category function
	public function search_by_cat($cat){
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'search';
		$config['total_rows'] = $this->product_m->search_total_no('', '', '', '', urldecode($cat));
		$config['per_page'] = 9;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		$this->pagination->initialize($config); 
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		$data['title'] = urldecode($cat);
		$data['user_id'] = $user->user_id;
		$data['products'] = $this->product_m->search($config["per_page"], $page, '', '', '', '', urldecode($cat));
		$data['pagination'] = $this->pagination->create_links();
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/slider_search');
		$this->load->view('product/products', $data);
		$this->load->view('templates/footer', $data);
	}
	
	function download(){
		$this->load->helper('download');
		$unique = $this->uri->segment(2);
		$file = $this->product_m->get_by(array('product_unique'=>$unique, 'product_status' => 'Active'), 1);
		
		$cont = $file->down_count+1;
		$this->product_m->save(array('down_count' => $cont),$file->product_id);
		
		$data = file_get_contents("files/".$file->product_file); // Read the file's contents
		$name = $file->product_file;

		force_download($name, $data);
	}
	
	//get the subjects by ajax according to the level
	public function level_subjects(){
		$level = $this->input->post('level');
		
		$levels = $this->product_m->by_levels($level);
		$select = '';
		if(count($levels)){
			$subjects = $this->product_m->by_subjects($levels[0]->level_id);
			$select .= '<select name="s_resource_type" id="s_resource_type" style="width:100%;"><option value="">Select</option>';
			foreach($subjects as $subject){
				$select .= '<option>'.$subject->subjects.'</option>';
			}
			$select .= '<option>Others</option></select>';
		}else{
			$select .= '<select name="s_resource_type" id="s_resource_type" style="width:100%;"><option value="">Select</option></select>';
		}
		print $select;
	}
	
	//get the subjects by ajax according to the level
	public function subjects(){
		$level = $this->input->post('level');
		
		$levels = $this->product_m->by_levels($level);
		$select = '';
		if(count($levels)){
			$subjects = $this->product_m->by_subjects($levels[0]->level_id);
			$select .= '<select name="psubject" id="psubject" multiple size="5" style="width:200px; font-style:normal !important; color:#000;"><option value="">Select</option>';
			foreach($subjects as $subject){
				$select .= '<option>'.$subject->subjects.'</option>';
			}
			$select .= '<option>Others</option></select>';
		}else{
			$select .= '<select name="psubject" id="psubject" multiple size="5" style="width:200px; font-style:normal !important; color:#000;"><option value="">Select</option></select>';
		}
		print $select;
	}
	
	//get the subjects by level ajax according to the level
	public function subject(){
		$level = $this->input->post('level');
		$psubject = $this->input->post('psubject');
		
		$levels = $this->product_m->by_levels($level);
		$select = '';
		if(count($levels)){
			$psubjects = explode(",", $psubject);
			
			$subjects = $this->product_m->by_subjects($levels[0]->level_id);
			$select .= '<select name="psubject" id="psubject" multiple size="5" style="width:200px; font-style:normal !important; color:#000;"><option value="">Select</option>';
			foreach($subjects as $subject){
				$select .= '<option ';
				foreach($psubjects as $psub){
					if($subject->subjects == $psub){
						$select .= 'selected';
					}
				}
				$select .= '>'.$subject->subjects.'</option>';
			}
			$select .= '<option>Others</option></select>';
		}else{
			$select .= '<select name="psubject" id="psubject" multiple size="5" style="width:200px; font-style:normal !important; color:#000;"><option value="">Select</option></select>';
		}
		print $select;
	}
}