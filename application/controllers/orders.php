<?php
class Orders extends Frontend_Controller {
	
	public function __construct(){
		parent::__construct();
        if (isset($_SERVER['HTTP_REFERER'])){
            $this->session->set_userdata('prev_url', $_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_userdata('prev_url', base_url());
        }
		$this->load->library('cart');
		
		$this->load->model('admin/images_model');
		$this->load->model('admin/promo_model');
		$this->load->model('product_m');
		$this->load->model('users_m');
		$this->load->model('site_m');
		$this->load->model('cart_m');
		$this->load->model('note_m');
		$this->load->model('orders_m');
		$this->load->model('shipping_m');
	}
	
	public function index(){
	}
	
	public function place_order(){
		//if save button was clicked, get the data sent via post
		if ($this->input->server('REQUEST_METHOD') === 'POST'){
            //form validation
            $this->form_validation->set_rules('fname', 'Full Name', 'trim|required|min_length[4]|xss_clean');
            $this->form_validation->set_rules('pin_code', 'Postal Code', 'trim|required|numeric|min_length[5]|xss_clean');
            $this->form_validation->set_rules('address', 'Address', 'trim|required|min_length[4]|xss_clean');
            $this->form_validation->set_rules('city', 'City', 'trim|required|min_length[4]|xss_clean');
			
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run() == TRUE){
				$sdata = array(
					'order_no' => $this->session->userdata('order_no'),
					'shipping_fname' => $this->input->post('fname'),
					'shipping_pincode' => $this->input->post('pin_code'),
					'shipping_address' => $this->input->post('address'),
					'shipping_city' => $this->input->post('city'),
				);
				if($this->orders_m->ship($sdata)){
					$amount = date('y').str_replace(',', '', str_replace('.', '', $this->input->post('tamount'))).date('md');
					redirect('shop/place/'.str_replace(',', '', $amount).'?prmcde='.date('y').$this->input->post('promocode').date('md'));
				}
            }
        }
		$data['site'] = $this->site_m->get_by(array('site_id' => 1),1);
		
		$data['title'] = 'Place Order';
		$this->load->view('templates/header', $data);
		$this->load->view('orders/place_order', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function redeem_it(){
		$code = $this->input->post('code');
		$tamount = $this->cart->total();
		
		$get = $this->promo_model->get_by(array('promo_code' => $code, 'promo_type' => 'On Cart Amount'), 1);
		if(count($get)){
			$gett = $this->promo_model->get_by(array('promo_code' => $code, 'promo_type' => 'On Cart Amount', 'promo_users >' => $get->promo_used_count), 1);
			//print_r(count($gett));exit;
			if(count($gett)){
				$getit = $this->promo_model->get_by(array('promo_code' => $code, 'promo_type' => 'On Cart Amount', 'promo_min_amount <=' => $tamount, 'promo_users >' => $get->promo_used_count), 1);
				if(count($getit)){
					$statu = $this->promo_model->redeem_it($code);
					if($statu == 0){
						if($get->promo_percent_type == '%'){$dis = ($tamount * $get->promo_percent) / 100;}else
						if($get->promo_percent_type == 'S$'){$dis = $get->promo_percent;}else{$dis = 0;}
						print number_format($tamount-$dis, 2);
					}else if($statu != 0){
						print $statu;
					}
				}else{
					print 'Minimum amount should be S$ '.$get->promo_min_amount;
				}
			}else{
				print 'We are sorry! The promotion has been fully redeemed.';
			}
		}else{
			print 'The promo code you have entered is not found. Please check and try again. Also note that the promo code is case sensitive';
		}
	}
	
	public function redeem_user(){
		$code = $this->input->post('code');
		$tamount = $this->cart->total();
		
		$get = $this->promo_model->get_by(array('promo_code' => $code, 'promo_type' => 'On Cart Amount'), 1);
		if(count($get)){
			$gett = $this->promo_model->get_by(array('promo_code' => $code, 'promo_type' => 'On Cart Amount', 'promo_users >' => $get->promo_used_count), 1);
			//print_r(count($gett));exit;
			if(count($gett)){
				$getit = $this->promo_model->get_by(array('promo_code' => $code, 'promo_type' => 'On Cart Amount', 'promo_min_amount <=' => $tamount, 'promo_users >' => $get->promo_used_count), 1);
				if(count($getit)){
					$statu = $this->promo_model->redeem_it($code);
					if($statu == 0){
						if($get->promo_percent_type == '%'){$dis = ($tamount * $get->promo_percent) / 100;}else
						if($get->promo_percent_type == 'S$'){$dis = $get->promo_percent;}else{$dis = 0;}
						print number_format($dis, 2);
					}else if($statu != 0){
						print $statu;
					}
				}else{
					print 'Minimum amount should be S$ '.$get->promo_min_amount;
				}
			}else{
				print 'We are sorry! The promotion has been fully redeemed.';
			}
		}else{
			print 'The promo code you have entered is not found. Please check and try again. Also note that the promo code is case sensitive';
		}
	}
	
	public function my_orders(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		if($this->input->get('from') && $this->input->get('from') != '' && $this->input->get('to') && $this->input->get('to') != ''){
			redirect('my_order/from/'.$this->input->get('from').'/to/'.$this->input->get('to'));
		}
		
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'my_orders';
		$config['total_rows'] = $this->orders_m->search_total_no($user->user_username, '', '');
		$config['per_page'] = 9;
		$config["uri_segment"] = 2;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		$data['orders'] = $this->orders_m->ord_by_limit($config["per_page"], $page, $user->user_username);
		$data['total_sales'] = $this->orders_m->total_orders($user->user_username);
		$data['link'] = '<a href="reports/orders_report" target="_blank"><h1 style="border:0;background:none;margin-top:-3px;">Export</h1></a>';
		
		
		$this->pagination->initialize($config);
		
		$data['title'] = 'My Orders';
		$data['pagination'] = $this->pagination->create_links();
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('my_orders', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function my_orders_p(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		$first = $this->uri->segment(3);
		$second = date_create($this->uri->segment(5));
		date_add($second, date_interval_create_from_date_string('1 days'));
		$second = date_format($second, 'Y-m-d');
		
		$page = ($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'my_orders/from/'.$first.'/to/'.$second;
		$config['total_rows'] = $this->orders_m->search_total_no($user->user_username, $first, $second);
		$config['per_page'] = 9;
		$config["uri_segment"] = 2;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
			
		$this->pagination->initialize($config);
		
		$data['orders'] = $this->orders_m->ord_by_limit($config["per_page"], $page, $user->user_username, $first, $second);
		$data['total_sales'] = $this->orders_m->total_orders($user->user_username, $first, $second);
		$data['link'] = '<a href="reports/orders_report?from='.$first.'&to='.$second.'" target="_blank"><h1 style="border:0;background:none;margin-top:-3px;">Export</h1></a>';
		
		$data['title'] = 'My Orders';
		$data['pagination'] = $this->pagination->create_links();
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('my_orders', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function my_sales(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		if($this->input->get('from') && $this->input->get('from') != '' && $this->input->get('to') && $this->input->get('to') != ''){
			redirect('my_sale/from/'.$this->input->get('from').'/to/'.$this->input->get('to'));
		}
		
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'my_sales';
		$config['total_rows'] = $this->orders_m->sales_total_no($user->user_username);
		$config['per_page'] = 9;
		$config["uri_segment"] = 2;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		$data['orders'] = $this->orders_m->sales_by_limit($config["per_page"], $page, $user->user_username);
		$data['total_sales'] = $this->orders_m->total_sales($user->user_username);
		$data['link'] = '<a href="reports/sales_report" target="_blank"><h1 style="border:0;background:none;margin-top:-3px;">Export</h1></a>';
		
		$this->pagination->initialize($config);
		
		$data['title'] = 'My Sales';
		$data['pagination'] = $this->pagination->create_links();
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('my_sales', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function my_sales_p(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
		
		$first = $this->uri->segment(3);
		$second = date_create($this->uri->segment(5));
		date_add($second, date_interval_create_from_date_string('1 days'));
		$second = date_format($second, 'Y-m-d');
		
		$page = ($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'my_sale/from/'.$first.'/to/'.$second;
		$config['total_rows'] = $this->orders_m->sales_total_no($user->user_username, $first, $second);
		$config['per_page'] = 9;
		$config["uri_segment"] = 2;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = '&gt;&gt;';
		$config['prev_link'] = '&lt;&lt;';
		
		$data['orders'] = $this->orders_m->sales_by_limit($config["per_page"], $page, $user->user_username, $first, $second);
		$data['total_sales'] = $this->orders_m->total_sales($user->user_username, $first, $second);
			
		$data['link'] = '<a href="reports/sales_report?from='.$first.'&to'.$second.'" target="_blank"><h1 style="border:0;background:none;margin-top:-3px;">Export</h1></a>';
		
		$this->pagination->initialize($config);
		
		$data['title'] = 'My Sales';
		$data['pagination'] = $this->pagination->create_links();
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('my_sales', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function order_view(){
		if(!$this->session->userdata('logged_in')){
			redirect(base_url());
		}
		$order_no = $this->uri->segment(2);
		
		$data['title'] = 'Order View';
		$data['orders'] = $this->orders_m->order_view($order_no);
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		
		$this->load->view('templates/header', $data);
		//$this->load->view('templates/slider_search');
		$this->load->view('order_view', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function order_status(){
		$order_id = $this->input->post('order_id');
		$status = $this->input->post('status');
		
		if($status == 'Delivered'){
			$ord = $this->orders_m->get_by(array('order_item_id' => $order_id), 1);
			$prod = $this->product_m->get_by(array('product_unique' => $ord->order_punique, 'product_status' => 'Active'), 1);
			$cont = $prod->order_count+1;
			$this->product_m->save(array('order_count' => $cont),$prod->product_id);
		}
		
		$id = $this->orders_m->save(array('order_status' => $status), $order_id);
		
		if($id){
			$user = $this->users_m->get_by(array('user_username' => $this->session->userdata('username')), 1);
			$order = $this->orders_m->get_by(array('order_item_id' => $order_id), 1);
			if(count($order)){
				$puser = $this->users_m->get_by(array('user_username' => $order->user_id), 1);
				$umessage = 'Your order status changed to <strong>'.$status.'</strong> for the product of <strong>'.$order->order_pname.'</strong>';
				
				$this->note_m->save(array('user_id'=>$user->user_id, 'to_user_id'=>$puser->user_id, 'note_message'=>$umessage));
			}
			print 'Status updated';
		}else{
			print 'Status not updated try later';
		}
	}
}