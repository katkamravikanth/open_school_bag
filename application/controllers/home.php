<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends Cron_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct(){
        parent::__construct();
        if (isset($_SERVER['HTTP_REFERER'])){
            $this->session->set_userdata('prev_url', $_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_userdata('prev_url', base_url());
        }
		$this->load->model('product_m');
		$this->load->model('admin/images_model');
		$this->load->model('admin/promo_model');
    }
	
	public function index($path = FALSE){
		if ( ! file_exists('application/views/home_page.php')){
			// Whoops, we don't have a page for that!
			show_404();
		}
		if($path !== FALSE){
			$data['path'] = $path;
		}else{
			$data['path'] = '';
		}
		$this->send_alert();
		
		$data['title'] = 'Welcome';
		$data['top_products'] = $this->product_m->top_seller(4, 0, 'order_count', 'down_count');
		$data['promos'] = $this->promo_model->get_by_limit(2,0);
		$data['banner'] = $this->images_model->image_by(array('path' => 'home'));
		$data['promobg'] = $this->images_model->image_by(array('path' => 'promo-bg'));
		
		$data['levels'] = $this->product_m->levels();
		$data['subjects'] = $this->product_m->subjects();
		$data['resources'] = $this->product_m->resources();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/slider_search');
		$this->load->view('home_page');
		$this->load->view('templates/footer', $data);
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url(), 'refresh');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */