$(function(){
	// add or update banner
	$('#banner_photo').change(function(e) {
		$('.banner_mess').show();
		$('.banner_mess').html('<img src="files/images/ajax-loader.gif" />');
		e.preventDefault();
		$.ajaxFileUpload({
			url				:'admin/banner/banner_upload_file', 
			secureuri		:false,
			fileElementId	:'banner_photo',
			dataType    	: 'json',
			data			: {
            	'banner_photo'	: 'banner_photo',
			},
			success  : function (data, status){
				if(data.status != 'error'){
					$('.banner_mess').html('<p>Reloading files...</p>');
					$('.banner_mess').show();
				}
				$('.banner_mess').html(data.msg);
				$('.banner_mess').show();
				setInterval(function() {window.location.reload();}, 5000);
			}
		});
		return false;
	});
	
	$('.change_order').change(function(e) {
        id = $(this).attr('name');
		//alert(id);
		order = $(this).val();
		$.post('admin/images/update', {id:id, order:order}, function(data){
			if(data != true){
				$('.banner_mess').addClass('alert-success');
				$('.banner_mess').html('Order changed');
				$('.banner_mess').show();
			}else{
				$('.banner_mess').addClass('alert-error');
				$('.banner_mess').html('Order not changed');
				$('.banner_mess').show();
			}
		})
    });
});
