var Admin = {

  toggleLoginRecovery: function(){
    var is_login_visible = $('#modal-login').is(':visible');
    (is_login_visible ? $('#modal-login') : $('#modal-recovery')).slideUp(300, function(){
      (is_login_visible ? $('#modal-recovery') : $('#modal-login')).slideDown(300, function(){
        $(this).find('input:text:first').focus();
      });
    });
  }
   
};

$(function(){
	$("#forgot-submit").click(function() {
		$("div.forg_mess").show();
        email = $("#forgot_email").val();
		
		if(email == '' || !IsEmail(email)){
			$("div.forg_mess").html("Please valid email address");
			return false;
		}
		$("div.forg_mess").html('<img src="files/images/ajax-loader.gif" />');
		
		$.post("admin/forgot",{email:email}, function(data){
			if(data == 1){
				$("div.forg_mess").html("!Invalid email address");
			}else{
				$("div.forg_mess").html('<img src="files/images/ajax-loader.gif" />');
				$("div.forg_mess").html('<p>We have sent a mail with your new password please check you mail and login</p>');
			}
		})
    });
	
	$("#add_user #username").blur(function(){
		if($(this).val() != ''){
			$("#add_user .mess").html('<img src="files/images/ajax-loader.gif" />');
			$("#add_user .mess").show();
			$.post('username',{username:$(this).val()}, function (data){
				if(data){
					$("#add_user .mess").html("<strong>Oh snap!</strong> Username already exists");
					return false;
				}else{
					$("#add_user .mess").hide();
					$("#add_user .mess").html('<img src="files/images/ajax-loader.gif" />')
				}
			});
		}
	});
	$("#add_user #email").blur(function(){
		if($(this).val() != ''){
			$("#add_user .mess").html('<img src="files/images/ajax-loader.gif" />');
			$("#add_user .mess").show();
			$.post('email',{email:$(this).val(), id:$('#add_user #uid').val()}, function (data){
				if(data){
					$("#add_user .mess").html("<strong>Oh snap!</strong> Email already exists"+$('#add_user #uid').val());
					return false;
				}else{
					$("#add_user .mess").hide();
					$("#add_user .mess").html('<img src="files/images/ajax-loader.gif" />');
				}
			});
		}
	});
	
	$("#level").change(function() {
        id = $(this).val();
		if(id == 3){
			$('#seller').slideUp();
			$('#publisher').slideDown();
		}else if(id == 2){
			$('#publisher').slideUp();
			$('#seller').slideDown();
		}else{
			$('#seller').slideUp();
			$('#publisher').slideUp();
		}
    });
	
	$("#payment_option").change(function() {
        val = $(this).val();
		if(val == 'PayPal Account'){
			$('#bank').slideUp();
			$('#paypal').slideDown();
		}else if(val == 'Bank Account'){
			$('#paypal').slideUp();
			$('#bank').slideDown();
		}else{
			$('#paypal').slideUp();
			$('#bank').slideUp();
		}
    });
	
	$('#add_user').submit(function() {
        if($("#add_user .mess").html() == '<strong>Oh snap!</strong> Email already exists' || $("#add_user .mess").html() == '<strong>Oh snap!</strong> Username already exists'){
			return false;
		}
    });
	
	
	$("#profile #cpass").click(function() {
		$("#profile .mess").show();
		
		cpass = $("#profile #current").val();
		newpass = $("#profile #new").val();
		renewpass = $("#profile #re_new").val();
		
		if(cpass == ''){
			$("#profile .mess").html("Please enter your current password");
			return false;
		}
		if(newpass == ''){
			$("#profile .mess").html("Please enter your new password");
			return false;
		}
		if(renewpass == ''){
			$("#profile .mess").html("Please enter your re-new password");
			return false;
		}
		if(cpass == newpass){
			$("#profile .mess").html("Password should not match with current password");
			return false;
		}
		if(newpass != renewpass){
			$("#profile .mess").html("Re-New Password does not match with new password");
			return false;
		}
		$.post("admin/settings/cpword",{cpass:cpass, newpass:newpass}, function(data){
			if(!data){
				$("#profile .mess").html("!Failed Please try later");
			}else{
				$("#profile .mess").html('Successfully updated');
				$("#profile #current, #profile #new, #profile #re_new").val('');
			}
		});
	});
	
	$('.toggle-login-recovery').click(function(e){
		Admin.toggleLoginRecovery();
		e.preventDefault();
	});
});

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}