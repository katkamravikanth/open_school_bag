$(document).ready(function(){
	$('#level').change(function(e) {
		if($(this).val() == 3){
        	$('#publisher').slideDown(300);
		}
    });
	// add or update product 
	$('#partner_logo').change(function(e) {
		$('.partner_mess').show();
		e.preventDefault();
		$.ajaxFileUpload({
			url				:'admin/users/user_upload_file', 
			secureuri		:false,
			fileElementId	:'partner_logo',
			dataType    	: 'json',
			data			: {
            	'p_name'	: 'partner_logo',
			},
			success  : function (data, status){
				if(data.status != 'error'){
					$('.partner_mess').html('<p>Reloading files...</p>');
					$('.partner_mess').show();
				}
				$('.partner_mess').html(data.msg);
				$('.partner_mess').show();
			}
		});
		return false;
	});
});
