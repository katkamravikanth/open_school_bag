// JavaScript Document
$(function() {
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content
	
	//On Click Event
	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
		var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active content
		return false;
	});
	
	$('.reviews-main1 .reply').hide();
	$('.reviews-main1 .delete').show();
	
	//delete review
	$("#tab1 img.delete").on( 'click', function () {
		id = $(this).attr('name');
		reset();
		alertify.confirm("Are you sure you want to delete this review?", function (e) {
			if(e){
				$.post('delete_review', {review_id:id}, function(data){
					alertify.success(data);
				});
			}
		});
		return false;
	});
	
	//delete question
	$("#tab2 img.delete").on( 'click', function () {
		id = $(this).attr('name');
		reset();
		alertify.confirm("Are you sure you want to delete this question?", function (e) {
			if(e){
				$.post('delete_qna', {qna_id:id}, function(data){
					alertify.success(data);
				});
			}
		});
		return false;
	});
	
	// Awesome tables with dynamically generated pagination
	$('table.pagination').each(function() {
		var perPage = $(this).attr("rel"); // Number of items per page
	
		$(this).children("tbody").children().hide(); // Hide all table-entries
		var childrenCount = $(this).children("tbody").children().size(); // Get total count of entries
		var pageCount = Math.ceil(childrenCount / perPage);
		if(pageCount <= 1){$("tfoot").hide();}
			$(this).find("tfoot tr td").append('<div class="pagination" />');
			$(this).find(".pagination").append('<a href="#first" rel="first">First</a>');
			
			for(var i = 0; i < pageCount; i++) {
				$(this).find(".pagination").append('<a href="#" class="graybutton pagelink" rel="' + (i + 1) + '">' + (i + 1) + '</a>');
			}
			
			$(this).find(".pagination").append('<a href="#last" rel="last">Last</a>');
			
			for(var i = 0; i < perPage; i++) { // Loop through entries
				$(this).children("tbody").children("tr:nth-child(" + (i + 1) + ")").show(); // Show requested entry
				$(this).find("tfoot .pagination a:nth-child(2)").addClass("active");
			}
			
			$(this).find('tfoot .pagination a[rel="first"]').click(function() {
				$(this).siblings().removeClass("active");
				$(this).siblings('a:nth-child(2)').addClass("active");
				
				$(this).parent().parent().parent().parent().parent().children("tbody").children().hide();
				
				for(var i = 0; i < perPage; i++) {
					$(this).parent().parent().parent().parent().parent().children("tbody").children("tr:nth-child(" + (i + 1) + ")").show();
				}
				return false;
			});
			
			$(this).find('tfoot .pagination a[rel="last"]').click(function() {
				$(this).siblings().removeClass("active");
				$(this).siblings('a:nth-child(' + (pageCount + 1) + ')').addClass("active");
				
				$(this).parent().parent().parent().parent().parent().children("tbody").children().hide();
				
				for(var i = 0; i < perPage; i++) {
					$(this).parent().parent().parent().parent().parent().children("tbody").children("tr:nth-child(" + (i + (pageCount - 1) * perPage + 1) + ")").show();
				}
				return false;
			});
			
			$(this).find('tfoot .pagination a.pagelink').click(function() {
				$(this).siblings().removeClass("active"); // Remove all .active classes
				$(this).addClass("active"); // Add class .active
				
				var offset = perPage * ($(this).attr("rel") - 1); // Define offset
			
				$(this).parent().parent().parent().parent().parent().children("tbody").children().hide(); // Hide all other entries
			
				for(var i = 0; i < perPage; i++) { // Loop required entries
					$(this).parent().parent().parent().parent().parent().children("tbody").children("tr:nth-child(" + (i + 1 + offset) + ")").show(); // Show requested entry
				}
				return false;
			});
		});
});