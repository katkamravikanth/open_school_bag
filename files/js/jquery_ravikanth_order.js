// JavaScript Document
$(function() {
	function reset () {
		$("#toggleCSS").attr("href", "files/css/themes/alertify.default.css");
		alertify.set({
			labels : {
				ok     : "OK",
				cancel : "Cancel"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}
	
	// Clear cart
	$("#main-content #clear_cart").on( 'click', function () {
		reset();
		alertify.confirm("Are you sure you want to clear the cart?", function (e) {
			if(e){
				$.post('clear_cart', {}, function(){
					window.location.reload();
				});
			}
		});
		return false;
	});
	
	$(".shopping-cart .remove_cart").on( 'click', function () {
		rowid = $(this).attr('name');
		reset();
		alertify.confirm("Are you sure you want to remove this item from the cart?", function (e) {
			if(e){
				$.post('remove_cart', {rowid:rowid}, function(data){
					if(data){
						window.location.reload();
					}
				});
			}
		});
		return false;
	});
	
	$('#place_order').click(function() {
		if($("#otype").val() != 0){
			if($.trim($(".shopping-contact #fname").val()) == ''){
				$(".shopping-contact #fname_mess").show();
				$(".shopping-contact #fname_mess").html("Please enter the full name");
				return false;
			}else{
				$(".shopping-contact #fname_mess").html('').hide();
			}
			if($.trim($("#address").val()) == ''){
				$("#add_mess").show();
				$("#add_mess").html("Please enter the address");
				return false;
			}else{
				$("#add_mess").html('').hide();
			}
			if($.trim($("#pin_code").val()) == ''){
				$("#pin_mess").show();
				$("#pin_mess").html("Please enter the postal code");
				return false;
			}else{
				$("#pin_mess").html('').hide();
			}
			if($.trim($("#city").val()) == ''){
				$("#city_mess").show();
				$("#city_mess").html("Please enter the city");
				return false;
			}else{
				$("#city_mess").html('').hide();
			}
		}
		loadSuccess();
    });
	$('#add_it').click(function(e) {
        if($("#pdisc:checked").length == 1 ){
			$('#shop').submit();
		}else{
			$("#pop_mess").html("Please accept the Disclaimer to Checkout");
		}
	});
	
	//check promo code and update amount if valid
	$('#promo').on('click', '#promo_but', function() {
		code = $.trim($('#promo_code').val());
		
		if(code == ''){$('#promo_mess').html('Please enter promo code');}
		
		$.post('redeem_it', {code:code}, function(data){
			if(!$.isNumeric(data)){
				$('#promo_mess').html(data);
				$('#promo_mess').show();
			}else{
				$('#promo_mess').html("Congratulations! You have successfully made a redemption!");
				$('#promo_mess').css('color','#090');
				$('#promo_mess').show();
				//$('#stamount').html(data);
				trans = $('#trans_fee').html();
				//sprice = $('#sprice').html();
				sprice = 0;
				total = parseFloat(data) + parseFloat(trans) + parseFloat(sprice);
				$('#gtotal').html(total.toFixed(2));
				$('#tamount').val(total.toFixed(2));
				$('#promocode').val(code);
			}
		});
		$.post('redeem_user', {code:code}, function(data){
			if(!$.isNumeric(data)){
			}else{
				$('#userPromoValue').html('-'+data);
				$('#promoValueDisc').show();
			}
		});
    });
	
	$("div.close, .mask_post").click(function() {
		disablePopup();  // function to close pop-up forms
	});
	
	function loadSuccess() {
		$("html, body").animate({ scrollTop: "0px" });
		$(".post_product").fadeIn(300);
		$(".post_product").css('top', '15%');
		$(".mask_post").css("opacity", "0.7");
		$(".mask_post").fadeIn(300);
	}
	function disablePopup() {
		$(".post_product").fadeOut("normal");
		$(".mask_post").fadeOut("normal");
	}
});