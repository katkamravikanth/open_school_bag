// JavaScript Document
$(function() {
	function reset () {
		$("#toggleCSS").attr("href", "files/css/themes/alertify.default.css");
		alertify.set({
			labels : {
				ok     : "OK",
				cancel : "Cancel"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}
	
	col1 = $('#left-product #left-indiv').height();
	col2 = $('#left-product #right-indiv').height();
	if(col1<col2){
		$('#left-product #left-indiv').height(col2);
	}
	
	$.post('cart_cp',{}, function(data){
		$('#quick_links .login_cart .cart').html(data);
	});
	$.post('cart',{}, function(data){
		$('#left-product #right-indiv #cucart').html(data);
	});
	
	//add to cart
	$('#left-product #left-indiv .product-1 .footer .add_cart').click(function(e) {
		$("#left-product #right-indiv #cucart").html('<img src="files/images/ajax-loader.gif" />')
		
		id = $(this).attr('name');
		price = $('#price_'+id).html();
		sprce = $('#sprice_'+id).val();
		discacr = $('#disc_'+id).val();
		
		$.post('add_cart', {unique:id, price:price, disc:discacr, sprice:sprce}, function(data){
			if(data){
				$.post('cart',{}, function(data){
					$('#left-product #right-indiv #cucart').html(data);
				});
				$.post('cart_cp',{}, function(data){
					$('#quick_links .login_cart .cart').html(data);
				});
			}
		});
    });
	
	// Clear cart
	$("#left-product #right-indiv").on( 'click', '#clear_cart', function () {
		reset();
		alertify.confirm("Are you sure you want to clear the cart?", function (e) {
			if(e){
				$("#left-product #right-indiv #cucart").html('<img src="files/images/ajax-loader.gif" />');
				$.post('clear_cart', {}, function(){
					$.post('cart',{}, function(data){$('#left-product #right-indiv #cucart').html(data);});
					$.post('cart_cp',{}, function(data){$('#quick_links .login_cart .cart').html(data);});
				});
			}
		});
		return false;
	});
	
	$('#right-indiv').on('click', '#place_order', function() {
		window.location = 'place_order';
	});
	
	$('#loader').hide();
	//subject drop down menu
	$('#plevel').change(function(e) {
		$('#loader').show();
        level = $(this).val();
		$.post('subjects', {level:level}, function(data){
			$('#subject_drop').html(data);
			
			$('#loader').hide();
		});
    });
	
	if($('#plevel').val() != ''){
		$('#loader').show();
		pvalevel = $('#plevel').val();
		psubject = $('#pbject').val();
		$.post('subject', {level:pvalevel, psubject:psubject}, function(data){
			$('#subject_drop').html(data);
			
			$('#loader').hide();
		});
	}
	
	// add preview product 
	var _picFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];
	$('#ppview').change(function(e) {
		id = $(this).attr('alt');
		var sFileName = $(this).val();
		if (sFileName.length > 0) {
			var blnValid = false;
			for (var j = 0; j < _picFileExtensions.length; j++) {
				var sCurExtension = _picFileExtensions[j];
				if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
					blnValid = true;
					break;
				}
			}
			if (!blnValid) {
				$("#ppreview").val();
				$('.ppreview_mess').html("The file format you have uploaded is not valid. Please check and try again.");
				$('.ppreview_mess').css('color', '#f30');
				$('.ppreview_mess').show();
				return false;
			}
		}
		$("#ppview1").show();
		$('#ppreview_but').removeAttr('disabled');
		$("#ppreview_but").show();
		return true;
	});
	
	$('#ppreview_but').click(function(e) {
		$('#ppreview_but').attr('value','Uploading...');
		$('#ppreview_but').attr('disabled','disabled');
		$('.ppreview_mess').html('<img src="files/images/ajax-loader.gif" />');
		$('.ppreview_mess').show();
		e.preventDefault();
		$.ajaxFileUpload({
			url				:'preview_file', 
			secureuri		:false,
			fileElementId	:'ppview',
			dataType    	: 'json',
			data			: {
            	'p_name'	: 'ppview',
			},
			success  : function (data, status){
				if(data.status != 'error'){
					$('.ppreview_mess').html(data.msg);
					$('.ppreview_mess').show();
				}
				if($('.ppreview_mess').html() != 'File successfully uploaded'){
					$('#ppview').removeAttr('disabled');
					$('#ppreview_but').removeAttr('disabled');
					$('#ppreview_but').attr('value','Click to Upload');
					return false;
				}
				$('.ppreview_mess').hide();
				$('#ppview').attr('disabled','disabled');
				$('#ppreview_but').attr('value','Uploaded');
			}
		});
		return false;
	});
	
	$('#ppview1').change(function(e) {
		var sFileName = $(this).val();
		if (sFileName.length > 0) {
			var blnValid = false;
			for (var j = 0; j < _picFileExtensions.length; j++) {
				var sCurExtension = _picFileExtensions[j];
				if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
					blnValid = true;
					break;
				}
			}
			if (!blnValid) {
				$("#ppview1").val();
				$('.ppreview_mess1').html("The file format you have uploaded is not valid. Please check and try again.");
				$('.ppreview_mess1').css('color', '#f30');
				$('.ppreview_mess1').show();
				return false;
			}
		}
		$("#ppview2").show();
		$('#ppreview_but1').removeAttr('disabled');
		$("#ppreview_but1").show();
		return true;
	});
	
	$('#ppreview_but1').click(function(e) {
		$('#ppreview_but1').attr('value','Uploading...');
		$('#ppreview_but1').attr('disabled','disabled');
		$('.ppreview_mess1').html('<img src="files/images/ajax-loader.gif" />');
		$('.ppreview_mess1').show();
		e.preventDefault();
		$.ajaxFileUpload({
			url				:'preview_file', 
			secureuri		:false,
			fileElementId	:'ppview1',
			dataType    	: 'json',
			data			: {
            	'p_name'	: 'ppview1',
			},
			success  : function (data, status){
				if(data.status != 'error'){
					$('.ppreview_mess1').html(data.msg);
					$('.ppreview_mess1').show();
				}
				if(data.msg != 'File successfully uploaded'){
					$('#ppview1').removeAttr('disabled');
					$('#ppreview_but1').removeAttr('disabled');
					$('#ppreview_but1').attr('value','Click to Upload');
				}
				$('.ppreview_mess1').hide();
				$('#ppview1').attr('disabled','disabled');
				$('#ppreview_but1').attr('value','Uploaded');
			}
		});
		return false;
	});
	
	$('#ppview2').change(function(e) {
		var sFileName = $(this).val();
		if (sFileName.length > 0) {
			var blnValid = false;
			for (var j = 0; j < _picFileExtensions.length; j++) {
				var sCurExtension = _picFileExtensions[j];
				if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
					blnValid = true;
					break;
				}
			}
			if (!blnValid) {
				$("#ppview2").val();
				$('.ppreview_mess2').html("The file format you have uploaded is not valid. Please check and try again.");
				$('.ppreview_mess2').css('color', '#f30');
				$('.ppreview_mess2').show();
				return false;
			}
		}
		$("#ppview3").show();
		$('#ppreview_but2').removeAttr('disabled');
		$("#ppreview_but2").show();
		return true;
	});
	
	$('#ppreview_but2').click(function(e) {
		$('#ppreview_but2').attr('value','Uploading...');
		$('#ppreview_but2').attr('disabled','disabled');
		$('.ppreview_mess2').html('<img src="files/images/ajax-loader.gif" />');
		$('.ppreview_mess2').show();
		e.preventDefault();
		$.ajaxFileUpload({
			url				:'preview_file', 
			secureuri		:false,
			fileElementId	:'ppview2',
			dataType    	: 'json',
			data			: {
            	'p_name'	: 'ppview2',
			},
			success  : function (data, status){
				if(data.status != 'error'){
					$('.ppreview_mess2').html(data.msg);
					$('.ppreview_mess2').show();
				}
				if(data.msg != 'File successfully uploaded'){
					$('#ppview2').removeAttr('disabled');
					$('#ppreview_but2').removeAttr('disabled');
					$('#ppreview_but2').attr('value','Click to Upload');
				}
				$('.ppreview_mess2').hide();
				$('#ppview2').attr('disabled','disabled');
				$('#ppreview_but2').attr('value','Uploaded');
			}
		});
		return false;
	});
	
	$('#ppview3').change(function(e) {
		var sFileName = $(this).val();
		if (sFileName.length > 0) {
			var blnValid = false;
			for (var j = 0; j < _picFileExtensions.length; j++) {
				var sCurExtension = _picFileExtensions[j];
				if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
					blnValid = true;
					break;
				}
			}
			if (!blnValid) {
				$("#ppview3").val();
				$('.ppreview_mess3').html("The file format you have uploaded is not valid. Please check and try again.");
				$('.ppreview_mess3').css('color', '#f30');
				$('.ppreview_mess3').show();
				return false;
			}
		}
		$("#ppview4").show();
		$('#ppreview_but3').removeAttr('disabled');
		$("#ppreview_but3").show();
		return true;
	});
	
	$('#ppreview_but3').click(function(e) {
		$('#ppreview_but3').attr('value','Uploading...');
		$('#ppreview_but3').attr('disabled','disabled');
		$('.ppreview_mess3').html('<img src="files/images/ajax-loader.gif" />');
		$('.ppreview_mess3').show();
		e.preventDefault();
		$.ajaxFileUpload({
			url				:'preview_file', 
			secureuri		:false,
			fileElementId	:'ppview3',
			dataType    	: 'json',
			data			: {
            	'p_name'	: 'ppview3',
			},
			success  : function (data, status){
				$('#ppview3').attr('disabled','disabled');
				if(data.status != 'error'){
					$('.ppreview_mess3').html(data.msg);
					$('.ppreview_mess3').show();
				}
				if(data.msg != 'File successfully uploaded'){
					$('#ppview3').removeAttr('disabled');
					$('#ppreview_but3').removeAttr('disabled');
					$('#ppreview_but3').attr('value','Click to Upload');
				}
				$('.ppreview_mess3').hide();
				$('#ppview3').attr('disabled','disabled');
				$('#ppreview_but3').attr('value','Uploaded');
			}
		});
		return false;
	});
	
	$('#ppview4').change(function(e) {
		var sFileName = $(this).val();
		if (sFileName.length > 0) {
			var blnValid = false;
			for (var j = 0; j < _picFileExtensions.length; j++) {
				var sCurExtension = _picFileExtensions[j];
				if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
					blnValid = true;
					break;
				}
			}
			if (!blnValid) {
				$("#ppview4").val();
				$('.ppreview_mess4').html("The file format you have uploaded is not valid. Please check and try again.");
				$('.ppreview_mess4').css('color', '#f30');
				$('.ppreview_mess4').show();
				return false;
			}
		}
		$('#ppreview_but4').removeAttr('disabled');
		$("#ppreview_but4").show();
		return true;
	});
	
	$('#ppreview_but4').click(function(e) {
		$('#ppreview_but4').attr('value','Uploading...');
		$('#ppreview_but4').attr('disabled','disabled');
		$('.ppreview_mess4').html('<img src="files/images/ajax-loader.gif" />');
		$('.ppreview_mess4').show();
		e.preventDefault();
		$.ajaxFileUpload({
			url				:'preview_file', 
			secureuri		:false,
			fileElementId	:'ppview4',
			dataType    	: 'json',
			data			: {
            	'p_name'	: 'ppview4',
			},
			success  : function (data, status){
				$('#ppview4').attr('disabled','disabled');
				if(data.status != 'error'){
					$('.ppreview_mess4').html(data.msg);
					$('.ppreview_mess4').show();
				}
				if(data.msg != 'File successfully uploaded'){
					$('#ppview4').removeAttr('disabled');
					$('#ppreview_but4').removeAttr('disabled');
					$('#ppreview_but4').attr('value','Click to Upload');
				}
				$('.ppreview_mess4').hide();
				$('#ppview4').attr('disabled','disabled');
				$('#ppreview_but4').attr('value','Uploaded');
			}
		});
		return false;
	});
	
	// add or update product 
	$('#ppic').change(function(e) {
		var sFileName = $(this).val();
		if (sFileName.length > 0) {
			var blnValid = false;
			for (var j = 0; j < _picFileExtensions.length; j++) {
				var sCurExtension = _picFileExtensions[j];
				if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
					blnValid = true;
					break;
				}
			}
			if (!blnValid) {
				$("#ppic").val();
				$('.pic_mess').html("The file format you have uploaded is not valid. Please check and try again.");
				$('.pic_mess').css('color', '#f30');
				$('.pic_mess').show();
				return false;
			}
		}
		$('#ppic_but').removeAttr('disabled');
		$("#ppic_but").show();
		return true;
	});
	
	$('#ppic_but').click(function(e) {
		$('#ppic_but').attr('value','Uploading...');
		$('#ppic_but').attr('disabled','disabled');
		$('.pic_mess').html('<img src="files/images/ajax-loader.gif" />');
		$('.pic_mess').show();
		e.preventDefault();
		$.ajaxFileUpload({
			url				:'product_upload_file', 
			secureuri		:false,
			fileElementId	:'ppic',
			dataType    	: 'json',
			data			: {
            	'p_name'	: 'ppic',
			},
			success  : function (data, status){
				$('#ppic').attr('disabled','disabled');
				if(data.status != 'error'){
					$('.pic_mess').html(data.msg);
					$('.pic_mess').show();
				}
				if(data.msg != 'File successfully uploaded'){
					$('#ppic').removeAttr('disabled');
					$('#ppic_but').removeAttr('disabled');
					$('#ppic_but').attr('value','Click to Upload');
				}
				$('.pic_mess').hide();
				$('#ppic').attr('disabled','disabled');
				$('#ppic_but').attr('value','Uploaded');
			}
		});
		return false;
	});
	
	var _FileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png", ".doc", ".docx", ".rtf", ".odt", ".pdf", ".ppt", ".pptx", ".pps", ".ppsx", ".xls", ".xlsx", ".mp3", ".avi", ".mp4", ".m4a", ".m4v", ".ogg", ".wav", ".mov", ".wmv", ".mpg", ".ogv", ".3gp", ".3g2"];
	
	$('#pfile').change(function(e) {
		var sFileName = $(this).val();
		if (sFileName.length > 0) {
			var blnValid = false;
			for (var j = 0; j < _FileExtensions.length; j++) {
				var sCurExtension = _FileExtensions[j];
				if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
					blnValid = true;
					break;
				}
			}
			if (!blnValid) {
				$("#pfile").val();
				$('.file_mess').html("The file format you have uploaded is not valid. Please check and try again.");
				$('.file_mess').css('color', '#f30');
				$('.file_mess').show();
				return false;
			}
		}
		$('#pfile_but').removeAttr('disabled');
		$("#pfile_but").show();
		return true;
	});
	
	$('#pfile_but').click(function(e) {
		$('#pfile_but').attr('value','Uploading...');
		$('#pfile_but').attr('disabled','disabled');
		$('.file_mess').html('<img src="files/images/ajax-loader.gif" />');
		$('.file_mess').show();
		e.preventDefault();
		$.ajaxFileUpload({
			url				:'product_upload_file', 
			secureuri		:false,
			fileElementId	:'pfile',
			dataType    	: 'json',
			data			: {
            	'p_name'	: 'pfile',
			},
			success  : function (data, status){
				$('#pfile').attr('disabled','disabled');
				if(data.status != 'error'){
					$('.file_mess').html(data.msg);
					$('.file_mess').show();
				}
				if(data.msg != 'File successfully uploaded'){
					$('#pfile').removeAttr('disabled');
					$('#pfile_but').removeAttr('disabled');
					$('#pfile_but').attr('value','Click to Upload')
				}
				$('.file_mess').hide();
				$('#pfile').attr('disabled','disabled');
				$('#pfile_but').attr('value','Uploaded');
			}
		});
		return false;
	});
	
	// add or update product
	$('#ptype').change(function(e) {
		if($(this).val() == 'Digital'){
			$('.physical').slideUp();
        	$('.digital').slideDown();
		}else if($(this).val() == 'Physical'){
			$('.digital').slideUp();
        	$('.physical').slideDown();
		}
    });
	
	$("#save").click(function() {
		$(".mess").html('<img src="files/images/ajax-loader.gif" />');
		$(".mess").show();
		
		usleve = $("#ulevel").val();
		punique = $("#punique").val();
		publishers = $("#publishers").val();
		ptype = $("#ptype").val();
		pname = $.trim($("#pname").val());
		prlevel = $("#plevel").val();
        //psubject = $("#psubject").val();
        presource = $("#presource").val();
        pdesc = $.trim($("#pdesc").val());
        pprice = $.trim($("#pprice").val());
        pdownd = $.trim($("#pdownd").val());
        ppages = $.trim($("#ppages").val());
        pweight = $.trim($("#pweight").val());
        //sprice = $.trim($("#sprice").val());
		sprice = 0;
		
		ppviews = $("#ppviews").val();
		cppic = $("#cppic").val();
		cpfile = $("#cpfile").val();
		
		//prlevel = '';
		psubject = '';
		//presource = '';
		delim=",";
		
		/*$("#plevel :selected").each(function(i, selected){ 
			 if(prlevel=="") delim=""; else delim=",";
			 prlevel = prlevel+delim+$(selected).val(); 
		});*/
		$("#psubject :selected").each(function(i, selected){ 
			 if(psubject=="") delim=""; else delim=",";
			 psubject = psubject+delim+$(selected).val(); 
		});
		/*$("#presource :selected").each(function(i, selected){ 
			 if(presource=="") delim=""; else delim=",";
			 presource = presource+delim+$(selected).val(); 
		});*/
		
		if(usleve == 3 || usleve == ''){
			if(publishers == ''){
				$("html, body").animate({ scrollTop: "150px" });
				$(".mess").html("Please select a publisher");
				return false;
			}
		}
		if(ptype == '' || pname == '' || prlevel == '' || psubject == '' || presource == '' || pdesc == '' || pprice == ''){
			$("html, body").animate({ scrollTop: "150px" });
			$(".mess").html("* marked fields are mandatory");
			return false;
		}
		
		if(ptype == 'Digital'){
			if(pdownd == '' || ppages == ''){
				$("html, body").animate({ scrollTop: "150px" });
				$(".mess").html("Product Downloadable days or Product Pages are missing");
				return false;
			}
		}else if(ptype == 'Physical'){
			if(pweight == ''){
				$("html, body").animate({ scrollTop: "150px" });
				$(".mess").html("Product Weight missing");
				return false;
			}
			/*if(sprice == ''){
				$("html, body").animate({ scrollTop: "150px" });
				$(".mess").html("Product shipping charges missing");
				return false;
			}*/
		}
		
		if(ptype == 'Digital'){
			if(cpfile == ''){
				if($('#pfile_but').is(":visible")){}else{
					$("html, body").animate({ scrollTop: "150px" });
					$(".mess").html("Please upload product file");
					return false;
				}
				if($('#pfile_but').val() != 'Uploaded'){
					$("html, body").animate({ scrollTop: "150px" });
					$(".mess").html("Please upload product file");
					return false;
				}
				if($('.file_mess').is(':visible') && $('.file_mess').html() == '<img src="files/images/ajax-loader.gif" />'){
					$("html, body").animate({ scrollTop: "150px" });
					$(".mess").html("Please wait while your file uploading");
					return false;
				}
			}
		}
		
		$(".mess").html('<img src="files/images/ajax-loader.gif" />');
		if($('#title').val() == 'Edit Product' || $('#user').val() != 'User'){
			
			$.post("additproduct",{punique:punique, publishers:publishers, ptype:ptype, pname:pname, plevel:prlevel, psubject:psubject, presource:presource, pdesc:pdesc, pprice:pprice, pdownd:pdownd, ppages:ppages, pweight:pweight, pdisc:'Yes', sprice:sprice, cppic:cppic, cpfile:cpfile, ppviews:ppviews}, function(data){
				
				if(!data){
					$(this).removeAttr('disabled');
					$("html, body").animate({ scrollTop: "50px" });
					$(".mess").html("!Failed Please try later");
				}else{
					if($("#punique").val() == ''){
						$("#ptype").val('');$("#pname").val('');$("#pdesc").val('');$("#pprice").val('');
						$("#pdownd").val('');$("#ppages").val('');$("#pweight").val('');$("#sprice").val('');
						
						$("#publishers").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
						$("#plevel").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
						$("#psubject").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
						$("#presource").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
					}
					
					$('.pic_mess, .ppreview_mess, .ppreview_mess1, .ppreview_mess2, .ppreview_mess3, .ppreview_mess4, .file_mess').html('<img src="files/images/ajax-loader.gif" />')
					$('.pic_mess, .ppreview_mess, .ppreview_mess1, .ppreview_mess2, .ppreview_mess3, .ppreview_mess4, .file_mess').hide()
					
					$("html, body").animate({ scrollTop: "50px" });
					$(".success_product #message_post").css('color', '#090');
					
					if(punique != ''){mes = 'updated your product information';}else{mes='posted a product';}
					if($('#user').val() != 'Admin'){
						$(".success_product #message_post").html('You have successfully '+mes+'<br /> <br /><input type="button" value="View Product" class="submit" onClick="window.location.href=\'product/'+data+'\'" /> &nbsp; | &nbsp <input type="button" value="Close" class="submit close"');
					}else{
						$(".success_product #message_post").html('You have successfully '+mes);
					}
					$(".mess").hide();
					$(".post_product").fadeOut("normal");
					loadSuccess();
					$("div.close, a.close, .mask_post").click(function() {location.reload();});
				}
			});
		}else if($('#user').val() == 'User'){
			loadDeclare();
		}
    });
	$('#add_it').click(function(e) {
        if($("#pdisc:checked").length == 1 ){
			
			$("#pop_mess").html('<img src="files/images/ajax-loader.gif" />');
			
			punique = $("#punique").val();
			publishers = $("#publishers").val();
			ptype = $("#ptype").val();
			pname = $.trim($("#pname").val());
			prlevel = $("#plevel").val();
			//psubject = $("#psubject").val();
			presource = $("#presource").val();
			pdesc = $.trim($("#pdesc").val());
			pprice = $.trim($("#pprice").val());
			pdownd = $.trim($("#pdownd").val());
			ppages = $.trim($("#ppages").val());
			pweight = $.trim($("#pweight").val());
			pdisc = 'Yes';
			//sprice = $.trim($("#sprice").val());
			sprice = 0;
			
			ppviews = $("#ppviews").val();
			cppic = $("#cppic").val();
			cpfile = $("#cpfile").val();
			
			//prlevel = '';
			psubject = '';
			//presource = '';
			delim=",";
			
			/*$("#plevel :selected").each(function(i, selected){ 
				 if(prlevel=="") delim=""; else delim=",";
				 prlevel = prlevel+delim+$(selected).val(); 
			});*/
			$("#psubject :selected").each(function(i, selected){ 
				 if(psubject=="") delim=""; else delim=",";
				 psubject = psubject+delim+$(selected).val(); 
			});
			/*$("#presource :selected").each(function(i, selected){ 
				 if(presource=="") delim=""; else delim=",";
				 presource = presource+delim+$(selected).val(); 
			});*/
		
			$.post("additproduct",{punique:punique, publishers:publishers, ptype:ptype, pname:pname, plevel:prlevel, psubject:psubject, presource:presource, pdesc:pdesc, pprice:pprice, pdownd:pdownd, ppages:ppages, pweight:pweight, pdisc:pdisc, sprice:sprice, cppic:cppic, cpfile:cpfile, ppviews:ppviews}, function(data){
				
				if(!data){
					$(this).removeAttr('disabled');
					$("html, body").animate({ scrollTop: "50px" });
					$(".mess").html("!Failed Please try later");
				}else{
					if($("#punique").val() == ''){
						$("#ptype").val('');$("#pname").val('');$("#pdesc").val('');$("#pprice").val('');
						$("#pdownd").val('');$("#ppages").val('');$("#pweight").val('');$("#sprice").val('');
						
						$("#publishers").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
						$("#plevel").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
						$("#psubject").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
						$("#presource").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
					}
					
					$('.pic_mess, .ppreview_mess, .ppreview_mess1, .ppreview_mess2, .ppreview_mess3, .ppreview_mess4, .file_mess').html('<img src="files/images/ajax-loader.gif" />')
					$('.pic_mess, .ppreview_mess, .ppreview_mess1, .ppreview_mess2, .ppreview_mess3, .ppreview_mess4, .file_mess').hide()
					
					$("html, body").animate({ scrollTop: "50px" });
					$(".success_product #message_post").css('color', '#090');
					if(punique != ''){mes = 'updated your product information';}else{mes='posted a product';}
					$(".success_product #message_post").html('You have successfully '+mes+'<br /> <br /><input type="button" value="View Product" class="submit" onClick="window.location.href=\'product/'+data+'\'" /> &nbsp; | &nbsp <input type="button" value="Close" class="submit close" />');
					
					$(".mess").hide();
					$(".post_product").fadeOut("normal");
					loadSuccess();	
					$("div.close, input.close, .mask_post").click(function() {location.reload();});
				}
			});
		}else{
			$(this).removeAttr('disabled');
			$("#pop_mess").html("Please accept the Disclaimer to post product");
		}
    });
	
	
	$("div.close, .mask_post").click(function() {
		disablePopup();  // function to close pop-up forms
	});
	
	function loadDeclare() {
		$("html, body").animate({ scrollTop: "50px" });
		$(".post_product").fadeIn(300);
		$(".post_product").css('top', '25%');
		$(".mask_post").css("opacity", "0.7");
		$(".mask_post").fadeIn(300);
	}
	
	function loadSuccess() {
		$("html, body").animate({ scrollTop: "50px" });
		$(".success_product").fadeIn(300);
		$(".success_product").css('top', '25%');
		$(".mask_post").css("opacity", "0.7");
		$(".mask_post").fadeIn(300);
	}
	function disablePopup() {
		$(".success_product").fadeOut("normal");
		$(".post_product").fadeOut("normal");
		$(".mask_post").fadeOut("normal");
	}
});