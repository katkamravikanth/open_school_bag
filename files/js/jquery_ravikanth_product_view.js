// JavaScript Document
$(function() {
	function reset () {
		$("#toggleCSS").attr("href", "files/css/themes/alertify.default.css");
		alertify.set({
			labels : {
				ok     : "OK",
				cancel : "Cancel"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}
	
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content
	
	//On Click Event
	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
		var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active content
		return false;
	});
	
	// delete product
	$("#tab1 #delete").on( 'click', function () {
		unique = $(this).attr('name');
		reset();
		alertify.confirm("Are you sure do you want to delete this product?", function (e) {
			if(e){
				window.location='products/delete/'+unique;
			}
		});
		return false;
	});
	
	$('input.auto-submit-star').rating({
		required: true,
		callback: function(value, link) {
			$.post('star', {product_unique:$('#hidden_product_unique').val(), rate_val:value}, function(e){
				$('p#msg_rate').html(e);
                $('p#msg_rate').fadeIn();
                $('p#msg_rate').fadeOut(10000);
			});
		}
	});
	
	$('#refresh').click(function(e) {
        $.post('captcha',{},function(data){
			$('#captchaimg').html(data);
			$('#captchaimg img').css('width', '230px');
			$('#captchaimg img').css('height', '40px');
			console.log(data);
			$.post('captcha_keyword',{},function(e){console.log(e);
				$('#precap').val(e);
			});
		})
    });
	
	//review post
	$('#tab3 #submit').click(function() {
		comment = $.trim($('#tab3 #comment').val());
		unique = $('#hidden_product_unique').val();
		captcha = $('#tab3 #captcha').val();
		precap = $('#tab3 #precap').val();
		
		if(comment == '' && captcha == ''){return false;}
		if(precap != captcha){
    		$('#tab3 p#msg_qna').html('The security code is incorrect. Please try again.');
			$('#tab3 p#msg_qna').fadeIn();
			$('#tab3 p#msg_qna').fadeOut(10000);
		}else{
		$.post('add_review', {unique:unique, comment:comment, captcha:captcha}, function(e){
			//console.log(unique)
    		$('#tab3 p#msg_qna').html(e);
			$('#tab3 p#msg_qna').fadeIn();
			$('#tab3 p#msg_qna').fadeOut(10000);
			$('#tab3 #comment').val('');
			$('#tab3 #captcha').val('');
		});
		}
	});
	
	//delete review
	$("#tab3 img.delete").on( 'click', function () {
		id = $(this).attr('name');
		reset();
		alertify.confirm("Are you sure you want to delete this review?", function (e) {
			if(e){
				$.post('delete_review', {review_id:id}, function(data){
					alertify.success(data);
				});
			}
		});
		return false;
	});
	
	$.post('reviews_load', {unique:$('#hidden_product_unique').val()}, function(data){
		$('#reviews_load').html(data);
	});
	
	// check whether user is the owner or not if not the owner hide reply and delete buttons
   	if($('#uhidden_id').val() != $("#hidden_id").val()){
		$('.reviews-main1 .reply').hide();
		$('.reviews-main1 .delete').hide();
	}
	
	//Question post
	$('#tab4 #add_submit').click(function() {
        question = $.trim($('#tab4 #question').val());
		unique = $('#hidden_product_unique').val();
		
		if(question == ''){return false;}
		$.post('add_qna', {unique:unique, question:question}, function(e){
			//console.log(unique)
			$('p#msg_qna').html(e);
            $('p#msg_qna').fadeIn();
            $('p#msg_qna').fadeOut(10000);
			$('#tab4 #question').val('');
		});
    });
	
	//Question reply post
	$('#tab4 img.reply').click(function() {
		id = $(this).attr('name');
		//alert(id)
		if($(this).attr('alt') == 'Reply'){
			$(this).attr('src', 'files/images/icons/close.png');
			height = $('#tab4').height();
			$('#tab4').height(height+200);
			$(this).attr('alt', 'Close');
		}else{
			$(this).attr('src', 'files/images/icons/reply.png');
			height = $('#tab4').height();
			$('#tab4').height(height-200);
			$(this).attr('alt', 'Reply');
		}
		$('#tab4 #reply_'+id).toggle();
	});
	
	//delete question
	$("#tab4 img.delete").on( 'click', function () {
		id = $(this).attr('name');
		reset();
		alertify.confirm("Are you sure you want to delete this question?", function (e) {
			if(e){
				$.post('delete_qna', {qna_id:id}, function(data){
					alertify.success(data);
				});
			}
		});
		return false;
	});
	
	$('#tab4').on('click', 'input[name="reply_submit"]', function() {
		id = $(this).attr('alt');
		uid = $('#tab4 #reply_u'+id).val();
		question = $.trim($('#tab4 #question_'+id).val());
		unique = $('#hidden_product_unique').val();
		if(question == ''){return false;}
		$.post('add_qna_reply', {unique:unique, question:question, qna_id:id, uid:uid}, function(e){
			$('span#msg_qna_'+id).html(e);
            $('span#msg_qna_'+id).css('text-align', 'left');
            $('span#msg_qna_'+id).fadeIn();
            $('span#msg_qna_'+id).fadeOut(10000);
			$('#tab4 #question_'+id).val('');
		});
    });
	
	$.post('cart_cp',{}, function(data){
		$('#quick_links .login_cart .cart').html(data);
	});
	$.post('cart',{}, function(data){
		$('#right-indiv #cucart').html(data);
	});
	
	//add to cart
	$('#left-question #add_cart').click(function(e) {
		$("#right-indiv #cucart").html('<img src="files/images/ajax-loader.gif" />')
		
		id = $('#left-question #hidden_product_unique').val();
		price = $('#left-question .price').html();
		sprce = $('#left-question #sprice').val();
		discacr = $('#left-question #disc').val();
		
        $.post('add_cart', {unique:id, price:price, disc:discacr, sprice:sprce}, function(data){
			if(data){
				$.post('cart',{}, function(data){
					$('#right-indiv #cucart').html(data);
				});
				$.post('cart_cp',{}, function(data){
					$('#quick_links .login_cart .cart').html(data);
				});
				//$('#right-indiv #cucart').load('cart');
				//$('#quick_links .login_cart .cart').load('cart_cp');
			}
		});
    });
	
	// Clear cart
	$("#left-product").on( 'click', '#clear_cart', function () {
		reset();
		alertify.confirm("Are you sure you want to clear the cart?", function (e) {
			if(e){
				$("#left-product #right-indiv #cucart").html('<img src="files/images/ajax-loader.gif" />')
				$.post('clear_cart', {}, function(){
					$.post('cart',{}, function(data){$('#right-indiv #cucart').html(data);});
					$.post('cart_cp',{}, function(data){$('#quick_links .login_cart .cart').html(data);});
				});
			}
		});
		return false;
	});
	
	$('#right-indiv').on('click', '#place_order', function() {
		window.location = 'place_order';
	});
	
	// Awesome tables with dynamically generated pagination
	$('table.pagination').each(function() {
		var perPage = $(this).attr("rel"); // Number of items per page
	
		$(this).children("tbody").children().hide(); // Hide all table-entries
		var childrenCount = $(this).children("tbody").children().size(); // Get total count of entries
		var pageCount = Math.ceil(childrenCount / perPage);
		if(pageCount <= 1){$("tfoot").hide();}
			$(this).find("tfoot tr td").append('<div class="pagination" />');
			$(this).find(".pagination").append('<a href="#first" rel="first">First</a>');
			
			for(var i = 0; i < pageCount; i++) {
				$(this).find(".pagination").append('<a href="#" class="graybutton pagelink" rel="' + (i + 1) + '">' + (i + 1) + '</a>');
			}
			
			$(this).find(".pagination").append('<a href="#last" rel="last">Last</a>');
			
			for(var i = 0; i < perPage; i++) { // Loop through entries
				$(this).children("tbody").children("tr:nth-child(" + (i + 1) + ")").show(); // Show requested entry
				$(this).find("tfoot .pagination a:nth-child(2)").addClass("active");
			}
			
			$(this).find('tfoot .pagination a[rel="first"]').click(function() {
				$(this).siblings().removeClass("active");
				$(this).siblings('a:nth-child(2)').addClass("active");
				
				$(this).parent().parent().parent().parent().parent().children("tbody").children().hide();
				
				for(var i = 0; i < perPage; i++) {
					$(this).parent().parent().parent().parent().parent().children("tbody").children("tr:nth-child(" + (i + 1) + ")").show();
				}
				return false;
			});
			
			$(this).find('tfoot .pagination a[rel="last"]').click(function() {
				$(this).siblings().removeClass("active");
				$(this).siblings('a:nth-child(' + (pageCount + 1) + ')').addClass("active");
				
				$(this).parent().parent().parent().parent().parent().children("tbody").children().hide();
				
				for(var i = 0; i < perPage; i++) {
					$(this).parent().parent().parent().parent().parent().children("tbody").children("tr:nth-child(" + (i + (pageCount - 1) * perPage + 1) + ")").show();
				}
				return false;
			});
			
			$(this).find('tfoot .pagination a.pagelink').click(function() {
				$(this).siblings().removeClass("active"); // Remove all .active classes
				$(this).addClass("active"); // Add class .active
				
				var offset = perPage * ($(this).attr("rel") - 1); // Define offset
			
				$(this).parent().parent().parent().parent().parent().children("tbody").children().hide(); // Hide all other entries
			
				for(var i = 0; i < perPage; i++) { // Loop required entries
					$(this).parent().parent().parent().parent().parent().children("tbody").children("tr:nth-child(" + (i + 1 + offset) + ")").show(); // Show requested entry
				}
				return false;
			});
		});
		
	$("div.close, .mask_post").click(function() {
		disablePopup();  // function to close pop-up forms
	});
});

	function popup(path) {
		$('.preview_image').attr('src', 'files/'+path);
		$(".post_product").fadeIn(300);
		$(".post_product").css('top', '25%');
		$(".mask_post").css("opacity", "0.7");
		$(".mask_post").fadeIn(300);
	}
	function disablePopup() {
		$(".post_product").fadeOut("normal");
		$(".mask_post").fadeOut("normal");
	}