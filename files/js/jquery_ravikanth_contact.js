$(function() {
	
	$('#refresh').click(function(e) {
        $.post('captcha',{},function(data){
			$('#captchaimg').html(data);
			$('#captchaimg img').css('width', '230px');
			$('#captchaimg img').css('height', '40px');
			$.post('captcha_keyword',{},function(e){
				$('#precap').val(e);
				
			});
		})
    });
	
	$("#contact-page #contact_form").submit(function() {
		$("#contact-page .error").hide();
		$("#contact-page .cont_mess").html('<img src="files/images/ajax-loader.gif" />');
		$("#contact-page .cont_mess").show();
		
		name = $.trim($("#contact_form #name").val());
		email = $.trim($("#contact_form #email").val());
		phone = $.trim($("#contact_form #phone").val());
		company = $.trim($("#contact_form #company").val());
		comment_type = $("#contact_form #comment_type").val();
		comment = $.trim($("#contact_form #comment").val());
		captcha = $("#contact_form #captcha").val();
		precap = $("#contact_form #precap").val();
		
		if(name == '' && email == '' && phone == '' && comment_type == '' && comment == ''){
			$("#contact_form div").html('');$("#contact_form div").hide();
			$("#contact-page .cont_mess").css('color','#f30');
			$("#contact-page .cont_mess").html("* marked fields are mandatory");
			return false;
		}
		if(name == ''){
			$("#contact_form div").html('');$("#contact_form div").hide();
			$("#contact-page .cont_mess").html('<img src="files/images/ajax-loader.gif" />');
			$("#contact-page .cont_mess").hide();
			$("#contact_form .name_mess").css('color','#f30');
			$("#contact_form .name_mess").html("The Name field is required");
			$("#contact_form .name_mess").show();
			return false;
		}
		if(email == ''){
			$("#contact_form div").html('');$("#contact_form div").hide();
			$("#contact-page .cont_mess").html('<img src="files/images/ajax-loader.gif" />');
			$("#contact-page .cont_mess").hide();
			$("#contact_form .email_mess").css('color','#f30');
			$("#contact_form .email_mess").html("The Email field is required");
			$("#contact_form .email_mess").show();
			return false;
		}
		if(phone == ''){
			$("#contact_form div").html('');$("#contact_form div").hide();
			$("#contact-page .cont_mess").html('<img src="files/images/ajax-loader.gif" />');
			$("#contact-page .cont_mess").hide();
			$("#contact_form .phone_mess").css('color','#f30');
			$("#contact_form .phone_mess").html("The Contact field is required");
			$("#contact_form .phone_mess").show();
			return false;
		}
		if(comment_type == ''){
			$("#contact_form div").html('');$("#contact_form div").hide();
			$("#contact-page .cont_mess").html('<img src="files/images/ajax-loader.gif" />');
			$("#contact-page .cont_mess").hide();
			$("#contact_form .comments_mess").css('color','#f30');
			$("#contact_form .comments_mess").html("The Comment Type field is required");
			$("#contact_form .comments_mess").show();
			return false;
		}
		if(comment == ''){
			$("#contact_form div").html('');$("#contact_form div").hide();
			$("#contact-page .cont_mess").html('<img src="files/images/ajax-loader.gif" />');
			$("#contact-page .cont_mess").hide();
			$("#contact_form .comment_mess").css('color','#f30');
			$("#contact_form .comment_mess").html("The Comment field is required");
			$("#contact_form .comment_mess").show();
			return false;
		}
		if(captcha == ''){
			$("#contact_form div").html('');$("#contact_form div").hide();
			$("#contact-page .cont_mess").html('<img src="files/images/ajax-loader.gif" />');
			$("#contact-page .cont_mess").hide();
			$("#contact_form .precap_mess").css('color','#f30');
			$("#contact_form .precap_mess").html("The Security Check is required");
			$("#contact_form .precap_mess").show();
			return false;
		}
		
		if(!IsEmail(email)){
			$("#contact_form div").html('');$("#contact_form div").hide();
			$("#contact-page .cont_mess").html('<img src="files/images/ajax-loader.gif" />');
			$("#contact-page .cont_mess").hide();
			$("#contact_form .email_mess").css('color','#f30');
			$("#contact_form .email_mess").html("Invalid email address");
			$("#contact_form .email_mess").show();
			return false;
		}
		
		if(precap != captcha){
    		$("#contact_form div").html('');$("#contact_form div").hide();
			$("#contact-page .cont_mess").html('<img src="files/images/ajax-loader.gif" />');
			$("#contact-page .cont_mess").hide();
			$("#contact_form .precap_mess").css('color','#f30');
			$("#contact_form .precap_mess").html("The security code is incorrect. Please try again.");
			$("#contact_form .precap_mess").show();
			return false;
		}
		
		$("#contact-page .cont_mess").html('<img src="files/images/ajax-loader.gif" />');
    });
});

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}