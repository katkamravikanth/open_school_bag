$(document).ready(function() {
	
	$("fieldset#signin_menu").hide();
	$("fieldset#payment").hide();
	
	//email validate and ajax checking
	$("#profile #email").keyup(function(){
		$("#profile .mess").show();
		
		if($(this).val() != ''){
			$.post('email',{email:$(this).val(), id:$("#profile #uid").val()}, function (data){
				if(data){
					$("#profile .mess").css('color', '#f30');
					$("#profile .mess").html("Email already exists");
					return false;
				}else{
					$("#profile .mess").hide();
					$("#profile .mess").html('<img src="files/images/ajax-loader.gif" />');
				}
			});
		}
	});
	
	//profile change link
	$('#change_pic').click(function() {
        $('#profile_pic').toggle();
		$('#change_pic').toggle();
    });
	
	$('#profile_pic #profile, #profile_pic #publish').click(function(e) {
        $("#profile_pic .mess").show();
    });
	
	// add preview product 
	var _picFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];
	$('#profile_pics').change(function(e) {
		var sFileName = $(this).val();
		if (sFileName.length > 0) {
			var blnValid = false;
			for (var j = 0; j < _picFileExtensions.length; j++) {
				var sCurExtension = _picFileExtensions[j];
				if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
					blnValid = true;
					break;
				}
			}
			if (!blnValid) {
				$("#profile_pics").val();
				$('#profile_pic .mess').html("The file format you have uploaded is not valid. Please check and try again.");
				$('#profile_pic .mess').css('color', '#f30');
				$('#profile_pic .mess').show();
				
				$("#profile").hide();
				$("#publish").hide();
				return false;
			}else{
				$('#profile_pic .mess').hide();
			}
		}
		$('#profile').removeAttr('disabled');
		$("#profile").show();
		
		$('#publish').removeAttr('disabled');
		$("#publish").show();
		return true;
	});
	
	//profile update
	$("#signin_menu #update").click(function() {
		$("#signin_menu .mess").html('<img src="files/images/ajax-loader.gif" />');
		$("#signin_menu .mess").show();
		
		uid = $("#uid").val();
        fullname = $.trim($("#signin_menu #full_name").val());
        email = $.trim($("#signin_menu #email").val());
        phone = $.trim($("#signin_menu #telephone").val());
		
		if(fullname == '' || fullname == '' || email == '' || phone == ''){
			$("#signin_menu .mess").css('color', '#f30');
			$("#signin_menu .mess").html("* marked fields are mandatory");
			return false;
		}
		
		if(email == '' || !IsEmail(email)){
			$("#signin_menu .mess").css('color', '#f30');
			$("#signin_menu .mess").html("Invalid email address");
			return false;
		}
		
		$.post("profile/update",{uid:uid, fname:fullname, email:email, phone:phone, type:'b'}, function(data){
			
			if(!data){
				$("#signin_menu .mess").css('color', '#f30');
				$("#signin_menu .mess").html("!Failed Please try later");
			}else{
				$("#pro_fullname").html(fullname);
				$("#pro_email").html(email);
				$("#pro_phone").html(phone);
				$("#signin_menu #full_name").val(fullname);
				$("#signin_menu #email").val(email)
				$("#signin_menu #telephone").val(phone)
				$("#signin_menu .mess").css('color', '#090');
				$("#signin_menu .mess").html('Successfully updated');
			}
		});
    });
	
	//bank update
	$("#payment_menu #bank_name").change(function(e) {
        if($(this).val() == 'Others'){
			$("#payment_menu .bank_other").slideDown();
		}else{
			$("#payment_menu .bank_other").slideUp();
		}
    });
	
	$("#payment_menu #payment_bank_save").click(function() {
		$("#payment_menu .mess").html('<img src="files/images/ajax-loader.gif" />');
		$("#payment_menu .mess").show();
		
		uidd = $("#uid").val();
		bank_id = $("#payment_menu #bank_id").val();
		bank_name = $.trim($("#payment_menu #bank_name").val());
		bank_other = $.trim($("#payment_menu #bank_other").val());
		bank_code = $.trim($("#payment_menu #bank_code").val());
		branch_code = $.trim($("#payment_menu #bank_branch_code").val());
		account_type = $.trim($("#payment_menu #bank_account_type").val());
		account_number = $.trim($("#payment_menu #bank_account_number").val());
		
		if(bank_name == '' || bank_code == '' || branch_code == '' || account_type == '' || account_number == ''){
			$("#payment_menu .mess").css('color', '#f30');
			$("#payment_menu .mess").html("Please fill the banks details");
			return false;
		}
		
		if(bank_name == 'Other' && bank_name == ''){
			$("#payment_menu .mess").css('color', '#f30');
			$("#payment_menu .mess").html("Please enter bank name");
			return false;
		}
		
		$.post("profile/update",{uid:uidd, bank_id:bank_id, bank_name:bank_name, bank_other:bank_other, bank_code:bank_code, branch_code:branch_code, account_type:account_type, account_number:account_number, type:'bank'}, function(data){			
			if(data == ''){
				$("#payment_menu .mess").css('color', '#f30');
				$("#payment_menu .mess").html("!Failed Please try later");
			}else{
				if(bank_name != 'Others'){
					$(".bank_name").html(bank_name);
				}else{
					$(".bank_name").html(bank_other);
				}
				$(".bank_code").html(bank_code);
				$(".branch_code").html(branch_code);
				$(".account_type").html(account_type);
				$(".account_number").html(account_number);
				$("#payment_menu .mess").css('color', '#090');
				$("#payment_menu .mess").html('Successfully updated');
			}
		});
    });
	
	
	//publish profile
	$("#profile #update").click(function() {
		$("#profile .mess").html('<img src="files/images/ajax-loader.gif" />');
		$("#profile .mess").show();
		
		uid = $("#uid").val();
		
        qualific = $.trim($("#profile #qualific").val());
        total_exp = $.trim($("#profile #total_exp").val());
        special = $.trim($("#profile #special").val());
		
		$.post("profile/update",{uid:uid, qualific:qualific, total_exp:total_exp, special:special, type:'q'}, function(data){
			
			if(!data){
				$("#profile .mess").css('color', '#f30');
				$("#profile .mess").html("!Failed Please try later");
			}else{
				$("#profile .mess").css('color', '#090');
				$("#profile .mess").html('Successfully updated');
			}
		});
    });
	
	//change password
	$("#profile #cpass").click(function() {
		$("#profile .mess").html('<img src="files/images/ajax-loader.gif" />');
		$("#profile .mess").show();
		
		cpass = $("#profile #current").val();
		newpass = $("#profile #new").val();
		renewpass = $("#profile #re_new").val();
		
		if(cpass == ''){
			$("#profile .mess").css('color', '#f30');
			$("#profile .mess").html("Please enter your current password");
			return false;
		}
		if(newpass == ''){
			$("#profile .mess").css('color', '#f30');
			$("#profile .mess").html("Please enter your new password");
			return false;
		}
		if(renewpass == ''){
			$("#profile .mess").css('color', '#f30');
			$("#profile .mess").html("Please enter your re-new password");
			return false;
		}
		if(cpass == newpass){
			$("#profile .mess").css('color', '#f30');
			$("#profile .mess").html("Password should not match with current password");
			return false;
		}
		if(newpass != renewpass){
			$("#profile .mess").css('color', '#f30');
			$("#profile .mess").html("Re-New Password does not match with new password");
			return false;
		}
		$.post("cpword",{cpass:cpass, newpass:newpass}, function(data){
			$("#profile #current").val('');
			$("#profile #new").val('');
			$("#profile #re_new").val('');
			if(!data){
				$("#profile .mess").css('color', '#f30');
				$("#profile .mess").html("!Failed Please try later");
			}else{
				$("#profile .mess").css('color', '#090');
				$("#profile .mess").html('Successfully updated');
			}
		});
	});
	
	
	//make me seller
	$("#make_seller #bank_name").change(function(e) {
        if($(this).val() == 'Others'){
			$("#make_seller .bank_other").slideDown();
		}else{
			$("#make_seller .bank_other").slideUp();
		}
    });
	
	$("#make_seller #bank_save").click(function() {
		$(".table .mess").html('<img src="files/images/ajax-loader.gif" />');
		$(".table .mess").show();
		
		uidd = $("#make_seller #uid").val();
		bank_name = $.trim($("#make_seller #bank_name").val());
		bank_other = $.trim($("#make_seller #bank_other").val());
		bank_code = $.trim($("#make_seller #bank_code").val());
		branch_code = $.trim($("#make_seller #bank_branch_code").val());
		account_type = $.trim($("#make_seller #account_type").val());
		account_number = $.trim($("#make_seller #account_no").val());
		
		if(bank_name == '' || bank_code == '' || branch_code == '' || account_type == '' || account_number == ''){
			$(".table .mess").css('color','#f30');
			$(".table .mess").html("Please fill the banks details");
			return false;
		}
		if(bank_name == 'Others' && bank_other == ''){
			$(".table .mess").css('color','#f30');
			$(".table .mess").html("Please enter the banks name");
			return false;
		}
		
		$.post("profile/update",{uid:uidd, bank_name:bank_name, bank_other:bank_other, bank_code:bank_code, branch_code:branch_code, account_type:account_type, account_number:account_number, type:'bank'}, function(data){	
		console.log(data);		
			if(!data){
				$(".table .mess").css('color','#f30');
				$(".table .mess").html("!Failed Please try later");
			}else{
				$(".table .mess").css('color','#090');
				$(".table .mess").html('Update successful. You can upload materials for sale at the next login.');
			}
		});
    });
	
	//onclick edit for profile and bank
	$(".signin").click(function(e) {          
		e.preventDefault();
		$("fieldset#signin_menu").toggle();
		$(".signin").toggleClass("menu-open");
	});
	
	$(".payment").click(function(e) {          
		e.preventDefault();
		$("fieldset#payment_menu").toggle();
		$(".payment").toggleClass("menu-open");
	});
	
	$("fieldset#signin_menu").mouseup(function() {
		return false
	});
	
	$("fieldset#payment_menu").mouseup(function() {
		return false
	});
	
	$(document).mouseup(function(e) {
		if($(e.target).parent("a.signin").length==0) {
			$(".signin").removeClass("menu-open");
			$("fieldset#signin_menu").hide();
		}
		if($(e.target).parent("a.payment").length==0) {
			$(".payment").removeClass("menu-open");
			$("fieldset#payment_menu").hide();
		}
	});
		
	$('#forgot_username_link').tipsy({gravity: 'w'});
	
});

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}