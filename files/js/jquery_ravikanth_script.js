$(document).ready(function() {

	$("#sign-in-tab, #login_form, #login_form1, #login_checkout").click(function() {
		loadSignIn();  // function to display the sin in form
	});

	$("#register-tab, #register_form, #register_form1, #register_checkout").click(function() {
		loadRegister(); // function to display the register form
	});
	
	$("#forgot_form").click(function() {
		loadForgot();  // function to display the forgot form
	});
	
	//subject drop down menu
	$('#s_level').change(function(e) {
        level = $(this).val();
		$.post('level_subjects', {level:level}, function(data){
			$('#search_subject').html(data);
		});
    });

	$("div.close").click(function() {
		disablePopup();  // function to close pop-up forms
		if($("#act").val() == 'activated' || $("#act").val() == 'not-activated'){
			window.location.href='http://openschoolbag.com.sg/';
		}
	});

	$("#background-on-popup").click(function() {
		disablePopup();  // function to close pop-up forms
		if($("#act").val() == 'activated' || $("#act").val() == 'not-activated'){
			window.location.href='http://openschoolbag.com.sg/';
		}
	});

	$(this).keyup(function(event) {
		if (event.which == 27) { // 27 is the code of the ESC key
			disablePopup();
			if($("#act").val() == 'activated' || $("#act").val() == 'not-activated'){
				history.go(-1);
			}
		}
	});
	
	function loadSignIn() {
		$("li.mess").html('<img src="files/images/ajax-loader.gif" />').hide();
		$("input[type='text'], input[type='email'], input[type='password']").val('');
		
		$("#sign-in-form").fadeIn(300);
		$("#forgot-form").fadeOut("normal");
		$("#register-form").fadeOut("normal");
		$("#success").fadeOut("normal");
		$("#background-on-popup").css("opacity", "0.7");
		$("#background-on-popup").fadeIn(300);
	}

	function loadRegister() {
		$("input[type='text'], input[type='tel'], input[type='email'], input[type='password'], #bank_name").val('');
		
		$("input[id='user_seller']").removeAttr('checked');
		$("input[id='user_buyer']").attr('checked', 'checked');
		
		$("#payment").slideUp(300);
		
		$("li.user_mess").html('<img src="files/images/ajax-loader.gif" />').hide();
		$("li.reg_mess").html('<img src="files/images/ajax-loader.gif" />').hide();
		$("li.pay_mess").html('<img src="files/images/ajax-loader.gif" />').hide();
		
		$("#register-form").fadeIn(300);
		$("#sign-in-form").fadeOut("normal");
		$("#forgot-form").fadeOut("normal");
		$("#success").fadeOut("normal");
		$("#background-on-popup").css("opacity", "0.7");
		$("#background-on-popup").fadeIn(300);
	}
	
	function loadForgot() {
		$("input[type='text'], input[type='email'], input[type='password']").val('');
		$("li.forg_mess").html('<img src="files/images/ajax-loader.gif" />').hide();
		
		$("#forgot-form").fadeIn(300);
		$("#sign-in-form").fadeOut("normal");
		$("#register-form").fadeOut("normal");
		$("#success").fadeOut("normal");
		$("#background-on-popup").css("opacity", "0.7");
		$("#background-on-popup").fadeIn(300);
	}
	
	function loadSuccess() {
		$("#success").fadeIn(300);
		$("#sign-in-form").fadeOut("normal");
		$("#register-form").fadeOut("normal");
		$("#forgot-form").fadeOut("normal");
		$("#background-on-popup").css("opacity", "0.7");
		$("#background-on-popup").fadeIn(300);
	}

	function disablePopup() {
		$("#sign-in-form").fadeOut("normal");
		$("#register-form").fadeOut("normal");
		$("#forgot-form").fadeOut("normal");
		$("#success").fadeOut("normal");
		$("#background-on-popup").fadeOut("normal");
	}
	
	$("#checkbox .unchecked-state").click( // checkbox select event
		function(event) {
			$(this).parent().addClass("selected");
			$(this).parent().find("checkbox").attr("checked","checked");
		}
	);
	
	$("#checkbox .checked-state").click( // checkbox deselect event
		function(event) {
			$(this).parent().removeClass("selected");
			$(this).parent().find("checkbox").removeAttr("checked");
		}
	);
	
	$('#quick_links .login_cart .cart').click(function() {
        if($(this).html() != '0 item(s) - $0.00'){
			window.location.href = "place_order";
		}
    });
	
	$("#sign-in-submit").click(function() {
		$("li.mess").html('<img src="files/images/ajax-loader.gif" />')
		$("li.mess").show();
		
        username = $.trim($("#log_username").val());
        password = $("#log_password").val();
		
		if(username == '' || password == ''){
			$("li.mess").html("Please fill in username and password.");
			return false;
		}
		
		$.post("login",{username:username, password:password}, function(data){
			if(data == 1){
				$("li.mess").html("Invalid username and password or your account has not been activated. Please register as new user or try again.");
			}else{
				window.location.reload();
			}
		})
    });
	
	$('#sign-in-form').keypress(function(e) {
		// Enter pressed?
		if(e.which == 10 || e.which == 13) {
			$("li.mess").html('<img src="files/images/ajax-loader.gif" />')
			$("li.mess").show();
			
			username = $.trim($("#log_username").val());
			password = $("#log_password").val();
			
			if(username == '' || password == ''){
				$("li.mess").html("Please fill username and password");
				return false;
			}
			
			$.post("login",{username:username, password:password}, function(data){
				if(data == 1){
					$("li.mess").html("Invalid username and password or your account has not been activated. Please register as new user or try again.");
				}else{
					window.location.reload();
				}
			});
		}
    });
	
	$("#email").blur(function(){
		if($(this).val() != ''){
			$("li.reg_mess").show();
			$.post('email',{email:$(this).val()}, function (data){
				if(data){
					$("li.reg_mess").html("Email already exists");
					return false;
				}else{
					$("li.reg_mess").hide();
					$("li.reg_mess").html('<img src="files/images/ajax-loader.gif" />');
				}
			});
		}
	});
	
	$("#register-form #telephone").blur(function(){
		if($(this).val() != ''){
			if($(this).val().length < 8 || $(this).val().length > 8){
				$("li.reg_mess").html("Telephone number should be 8 numbers only");
				$("li.reg_mess").show();
				return false;
			}else{
				$("li.reg_mess").hide();
				$("li.reg_mess").html('<img src="files/images/ajax-loader.gif" />');
			}
		}
	});
	
	$("#reg_username").blur(function(){
		if($(this).val() != ''){
			$("li.user_mess").show();
			
			if($(this).val().length <= 5){
				$("li.user_mess").html("Username must be minimum 6 characters");
				return false;
			}
			$.post('username',{username:$(this).val()}, function (data){
				if(data){
					$("li.user_mess").html("Username already exists");
					return false;
				}else{
					$("li.user_mess").hide();
					$("li.user_mess").html('<img src="files/images/ajax-loader.gif" />')
				}
			});
		}
	});
	
	$("input[name='user_seller']").click(function() {
		if (!$('input[id=user_buyer]:checked').val() ) {
			$("#payment").slideDown(300);
		}else if (!$('input[id=user_seller]:checked').val() ) {
			$("#payment").slideUp(300);
		}else{
			$("#payment").slideUp(300);
		}
    });
	
	$("#bank_name").change(function(e) {
        if($(this).val() == 'Others'){
			$(".bank_other").slideDown();
		}else{
			$(".bank_other").slideUp();
		}
    });
	
	$("#create-account-submit").click(function() {
		$("li.reg_mess").html('<img src="files/images/ajax-loader.gif" />');
		$("li.reg_mess").show();
		
		fname = $.trim($("#fname").val());
		lname = $.trim($("#lname").val());
        fullname = fname+' '+lname;
        email = $.trim($("#email").val());
        phone = $.trim($("#telephone").val());
        username = $.trim($("#reg_username").val());
        password = $("#reg_password").val();
        re_password = $("#re_password").val();
		var user_seller = '';
		
		var bank_name = '';
		var bank_other = '';
		var bank_code = '';
		var branch_code = '';
		var account_type = '';
		var account_number = '';
		
		if(fname == '' || lname == '' || email == '' || phone == '' || username == '' || password == '' || re_password == ''){
			$("html, body").animate({ scrollTop: "150px" });
			$("li.reg_mess").html("* marked fields are mandatory");
			return false;
		}
		
		if(phone != ''){
			if(phone.length < 8 || phone.length > 8){
				$("html, body").animate({ scrollTop: "150px" });
				$("li.reg_mess").html("Telephone number should be 8 numbers only");
				return false;
			}
		}
		
		if(!IsEmail(email)){
			$("html, body").animate({ scrollTop: "150px" });
			$("li.reg_mess").html("Invalid email address");
			return false;
		}
		
		if(email != ''){
			$("li.reg_mess").show();
			$.post('email',{email:email}, function (data){
				if(data){
					$("html, body").animate({ scrollTop: "150px" });
					$("li.reg_mess").html("Email already exists");
					$("li.reg_mess").show();
					return false;
				}
			});
		}
		
		if(username != ''){
			if(username.length <= 5){
				$("li.reg_mess").hide();
				$("li.user_mess").html("Username must be minimum 6 characters");
				$("li.user_mess").show();
				return false;
			}
			$.post('username',{username:username}, function (data){
				if(data){
					$("li.user_mess").html("Username already exists");
					$("li.user_mess").show();
					$("li.reg_mess").hide();
					return false;
				}
			});
		}
		
		if(password == username){
			$("li.reg_mess").hide();
			$("li.user_mess").html("Password should not match username");
			$("li.user_mess").show();
			return false;
		}
		if(password.length <= 5){
			$("li.reg_mess").hide();
			$("li.user_mess").html("Password must be minimum 6 characters");
			$("li.user_mess").show();
			return false;
		}
		if(password != re_password){
			$("li.reg_mess").hide();
			$("li.user_mess").html("Password does not match");
			$("li.user_mess").show();
			return false;
		}
		
		if (!$('input[id=user_buyer]:checked').val() ) {
			user_seller = 'Yes';
		}else if (!$('input[id=user_seller]:checked').val() ) {
			user_seller = 'No';
		}
		
		if($("input:radio[id='user_seller']").is(":checked")){
			$("li.pay_mess").show();
			bank_name = $.trim($("#bank_name").val());
			bank_other = $.trim($("#bank_other").val());
			bank_code = $.trim($("#bank_code").val());
			branch_code = $.trim($("#branch_code").val());
			account_type = $.trim($("#account_type").val());
			account_number = $.trim($("#account_number").val());
			
			if(bank_name == '' || bank_code == '' || branch_code == '' || account_type == '' || account_number == ''){
				$("li.pay_mess").html("Please fill the banks details");
				$("li.reg_mess").hide();
				return false;
			}
			if(bank_name == 'Other' && bank_other == ''){
				$("li.pay_mess").html("Please fill the bank name");
				$("li.reg_mess").hide();
				return false;
			}
		}
		$("li.pay_mess").hide();
		$("li.pay_mess").html('<img src="files/images/ajax-loader.gif" />');
		
		if($('#checkbox li').hasClass('selected')){
			$("li.reg_mess").html('<img src="files/images/ajax-loader.gif" />');
			$("li.reg_mess").show();
			
			$.post("register",{fname:fullname, email:email, phone:phone, username:username, password:password, user_seller:user_seller, bank_name:bank_name, bank_other:bank_other, bank_code:bank_code, branch_code:branch_code, account_type:account_type, account_number:account_number}, function(data){
				//console.log(data);
				$("li.user_mess").hide();
				if(!data){
					$("html, body").animate({ scrollTop: "150px" });
					$("li.reg_mess").show();
					$("li.reg_mess").html("Registration failed. Please try again later. Or <a href='contact'>contact us</a>.");
					return false;
				}else{
					$("html, body").animate({ scrollTop: "150px" });
					$("li.reg_mess").hide();
					$("li.reg_mess").html('<img src="files/images/ajax-loader.gif" />');
					$("#success ul").html('<p><span class="register-numbering">&radic;</span><span class="register-numbering-text">Success</span></p><li><label><span>Thank you for your registration. We have sent you an email to activate your account. Please check your mailbox</span></label></li>');
					loadSuccess()
				}
			});
		}else{
			$("html, body").animate({ scrollTop: "150px" });
			$("li.reg_mess").show();
			$("li.reg_mess").html("Please accept the terms and conditions to complete registration");
		}
    });
	$('#register-form').keypress(function(e) {
		// Enter pressed?
		if(e.which == 10 || e.which == 13) {
			$("li.reg_mess").html('<img src="files/images/ajax-loader.gif" />');
			$("li.reg_mess").show();
			
			fname = $.trim($("#fname").val());
			lname = $.trim($("#lname").val());
			fullname = fname+' '+lname;
			email = $.trim($("#email").val());
			phone = $.trim($("#telephone").val());
			username = $.trim($("#reg_username").val());
			password = $("#reg_password").val();
			re_password = $("#re_password").val();
			var user_seller = '';
			
			var bank_name = '';
			var bank_other = '';
			var bank_code = '';
			var branch_code = '';
			var account_type = '';
			var account_number = '';
			
			if(fname == '' || lname == '' || email == '' || phone == '' || username == '' || password == '' || re_password == ''){
				$("html, body").animate({ scrollTop: "150px" });
				$("li.reg_mess").html("* marked fields are mandatory");
				return false;
			}
			
			if(!IsEmail(email)){
				$("html, body").animate({ scrollTop: "150px" });
				$("li.reg_mess").html("Invalid email address");
				return false;
			}
			
			if(email != ''){
				$("li.reg_mess").show();
				$.post('email',{email:email}, function (data){
					if(data){
						$("html, body").animate({ scrollTop: "150px" });
						$("li.reg_mess").html("Email already exists");
						$("li.reg_mess").show();
						return false;
					}
				});
			}
			
			if(username != ''){
				if(username.length <= 5){
					$("li.reg_mess").hide();
					$("li.user_mess").show();
					$("li.user_mess").html("Username must be minimum 6 characters");
					return false;
				}
				$.post('username',{username:username}, function (data){
					if(data){
						$("li.user_mess").html("Username already exists");
						$("li.user_mess").show();
						$("li.reg_mess").hide();
						return false;
					}
				});
			}
			
			if(password == username){
				$("li.reg_mess").hide();
				$("li.user_mess").show();
				$("li.user_mess").html("Password should not match username");
				return false;
			}
			if(password.length <= 5){
				$("li.reg_mess").hide();
				$("li.user_mess").show();
				$("li.user_mess").html("Password must be minimum 6 characters");
				return false;
			}
			if(password != re_password){
				$("li.reg_mess").hide();
				$("li.user_mess").show();
				$("li.user_mess").html("Password does not match");
				return false;
			}
			
			if (!$('input[id=user_buyer]:checked').val() ) {
				user_seller = 'Yes';
			}else if (!$('input[id=user_seller]:checked').val() ) {
				user_seller = 'No';
			}
			
			if(user_seller == 'Yes'){
				$("li.pay_mess").show();
				bank_name = $.trim($("#bank_name").val());
				bank_other = $.trim($("#bank_other").val());
				bank_code = $.trim($("#bank_code").val());
				branch_code = $.trim($("#branch_code").val());
				account_type = $.trim($("#account_type").val());
				account_number = $.trim($("#account_number").val());
				
				if(bank_name == '' || bank_code == '' || branch_code == '' || account_type == '' || account_number == ''){
					$("li.pay_mess").html("Please fill the banks details");
					$("li.reg_mess").hide();
					return false;
				}
				if(bank_name == 'Other' && bank_other == ''){
					$("li.pay_mess").html("Please fill the bank name");
					$("li.reg_mess").hide();
					return false;
				}
			}
			$("li.pay_mess").hide();
			$("li.pay_mess").html('<img src="files/images/ajax-loader.gif" />');
			
			if($('#checkbox li').hasClass('selected')){
				$("li.reg_mess").html('<img src="files/images/ajax-loader.gif" />');
				$("li.reg_mess").show();
				
				$.post("register",{fname:fullname, email:email, phone:phone, username:username, password:password, user_seller:user_seller, bank_name:bank_name, bank_other:bank_other, bank_code:bank_code, branch_code:branch_code, account_type:account_type, account_number:account_number}, function(data){
					//console.log(data);
					$("li.user_mess").hide();
					if(!data){
						$("html, body").animate({ scrollTop: "150px" });
						$("li.reg_mess").show();
						$("li.reg_mess").html("Registration failed. Please try again later. Or <a href='contact'>contact us</a>.");
						return false;
					}else{
						$("html, body").animate({ scrollTop: "150px" });
						$("li.reg_mess").hide();
						$("li.reg_mess").html('<img src="files/images/ajax-loader.gif" />');
						$("#success ul").html('<p><span class="register-numbering">&radic;</span><span class="register-numbering-text">Success</span></p><li><label><span>Thank you for your registration. We have sent you an email to activate your account. Please check your mailbox</span></label></li>');
						loadSuccess()
					}
				});
			}else{
				$("html, body").animate({ scrollTop: "150px" });
				$("li.reg_mess").show();
				$("li.reg_mess").html("Please accept the terms and conditions to complete registration");
			}
		}
	});
	
	$("#forgot-submit").click(function() {
		$("li.forg_mess").html('<img src="files/images/ajax-loader.gif" />');
		$("li.forg_mess").show();
        email = $.trim($("#forgot_email").val());
		
		if(email == '' || !IsEmail(email)){
			$("li.forg_mess").html("Please enter a valid email address");
			return false;
		}
		$("li.forg_mess").html('<img src="files/images/ajax-loader.gif" />');
		
		$.post("forgot",{email:email}, function(data){
			if(data == 1){
				$("li.forg_mess").html("Invalid email address");
			}else{
				$("li.forg_mess").html('<img src="files/images/ajax-loader.gif" />');
				$("#success ul").html('<p><span class="register-numbering">&radic;</span><span class="register-numbering-text">Success</span></p><li><label><span>A temporary password has been sent to your mailbox. Please login with the temporary password and change a new password</span></label></li>');
				loadSuccess();
			}
		})
    });
	
	$("#reactivate-account").click(function() {
		$("li.act_mess").html('<img src="files/images/ajax-loader.gif" />');
        email = $.trim($("#activation_email").val());
		
		if(email == '' || !IsEmail(email)){
			$("li.act_mess").html("Please valid email address");
			return false;
		}$("li.act_mess").html('<img src="files/images/ajax-loader.gif" />');
		
		$.post("re_acrivate",{email:email}, function(data){
			if(data == 1){
				$("li.act_mess").html("!Invalid email address");
			}else{
				$("li.act_mess").html('<img src="files/images/ajax-loader.gif" />')
				$("#success ul").html('<p><span class="register-numbering">&radic;</span><span class="register-numbering-text">Success</span></p><li><label><span>We have sent you an email to activate your account. Please check your mailbox</span></label></li>');
				loadSuccess();
			}
		})
    });
	$('#search_box').each(function() {
        $('#search_box input, #search_box select, #search_box .styled-select').keypress(function(e) {
            // Enter pressed?
            if(e.which == 10 || e.which == 13) {
                this.form.submit();
            }
        });
    });
});

$(window).load(function() {
	$('#slider').nivoSlider({effect: 'slideInLeft', directionNav: false});
});

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}