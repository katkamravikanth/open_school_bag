-- phpMyAdmin SQL Dump
-- version 4.0.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 19, 2013 at 02:20 PM
-- Server version: 5.5.32-cll
-- PHP Version: 5.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `openscho_OpenBag`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE IF NOT EXISTS `bank` (
  `bank_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `bank_name` varchar(200) NOT NULL,
  `bank_other` varchar(200) DEFAULT NULL,
  `bank_code` varchar(50) NOT NULL,
  `bank_branch_code` varchar(50) NOT NULL,
  `bank_account_type` varchar(50) NOT NULL,
  `bank_account_number` varchar(50) NOT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`bank_id`, `user_id`, `bank_name`, `bank_other`, `bank_code`, `bank_branch_code`, `bank_account_type`, `bank_account_number`) VALUES
(1, 2, 'HSBC', '', 'HSB123456', 'HSBB987654321', 'Savings Account', '45612378912345'),
(3, 28, 'Citibank', '', '57890', '89403', 'saving', '23567890126'),
(4, 32, 'POSB', '', '7171', '081', 'savings', '9999999'),
(5, 3, 'Bank of China', '', 'adsfasdf', 'adsfasdf', 'asdfasdf', 'asdfasdfa'),
(6, 9, 'DBS Bank', '', '1212', '1212', 'savings', '121212'),
(7, 34, 'OCBC', '', '1212', '1212', 'savings', '12121212'),
(8, 31, 'DBS Bank', '', '7171', '8181', 'savings', '1234567890'),
(9, 36, 'Others', 'UCO', '12345', '12345', 'Savings', '123456789'),
(10, 37, 'Others', 'UCO', 'djfkaj', 'we', 'savings', '123456'),
(11, 41, 'POSB', '', '7171', '081', 'Savings account', '209360381'),
(12, 42, 'HSBC', '', 'MIDLGB2170R', '404680', 'CURRENT', '31139851'),
(13, 44, 'UOB', '', '7375', '373', 'Current', '3733035542'),
(16, 48, 'Others', 'CIMB', '7986', '001', 'Current', '2000003152'),
(17, 49, 'UOB', '', '7375', '057', 'Current', '3743057099'),
(18, 40, 'Bank of China', '', '52471', '1111', 'saving', '587423695874'),
(19, 50, 'HSBC', '', 'HSBC000125', 'HSB1231321321', 'Sevings Account', '12345678912345678'),
(20, 53, 'POSB', '', '1', '81', 'Saving', '192-19291-0'),
(21, 59, 'POSB', '', '123', '123', '123', '123'),
(22, 60, 'POSB', '', '7171', '091', 'POSB Savings Account', '137-64370-7'),
(23, 61, 'DBS Bank', '', '023', '300', 'saving', '023440300');

-- --------------------------------------------------------

--
-- Table structure for table `cat_levels`
--

CREATE TABLE IF NOT EXISTS `cat_levels` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(100) NOT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `cat_levels`
--

INSERT INTO `cat_levels` (`level_id`, `level`) VALUES
(1, 'Pre-School'),
(2, 'Primary 1'),
(3, 'Primary 2'),
(4, 'Primary 3'),
(5, 'Primary 4'),
(6, 'Primary 5'),
(7, 'Primary 6'),
(8, 'Secondary 1'),
(9, 'Secondary 2'),
(10, 'Secondary 3'),
(11, 'Secondary 4/5'),
(12, 'Junior College'),
(13, 'Tertiary'),
(22, 'Primary 1 and 2'),
(23, 'Primary 3 and 4'),
(24, 'Primary 5 and 6'),
(25, 'Lower Primary'),
(26, 'Upper Primary');

-- --------------------------------------------------------

--
-- Table structure for table `cat_resources`
--

CREATE TABLE IF NOT EXISTS `cat_resources` (
  `resource_id` int(11) NOT NULL AUTO_INCREMENT,
  `resources` varchar(100) NOT NULL,
  `icon` varchar(50) NOT NULL,
  PRIMARY KEY (`resource_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `cat_resources`
--

INSERT INTO `cat_resources` (`resource_id`, `resources`, `icon`) VALUES
(1, 'Educational Kits and Games', 'icons/story-books2.png'),
(2, 'Audio', 'icons/audio.png'),
(3, 'Video', 'icons/video.png'),
(5, 'Ten Year Series', 'icons/10-year-series.png'),
(6, 'Assessment Book', 'icons/19506c0f05aa4b0ceb0553d5ad060086.png'),
(7, 'Guidebooks', 'icons/guide-books.png'),
(8, 'Storybooks', 'icons/story-books1.png'),
(10, 'Enrichment', 'icons/7c3738a64c81382bcb29cec8e68f4a1a.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `cat_subjects`
--

CREATE TABLE IF NOT EXISTS `cat_subjects` (
  `subject_id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) NOT NULL,
  `subjects` varchar(100) NOT NULL,
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=135 ;

--
-- Dumping data for table `cat_subjects`
--

INSERT INTO `cat_subjects` (`subject_id`, `level_id`, `subjects`) VALUES
(1, 1, 'Chinese'),
(2, 1, 'English'),
(3, 1, 'Mathematics'),
(4, 2, 'Chinese'),
(5, 2, 'Malay'),
(6, 2, 'Tamil'),
(7, 2, 'English'),
(8, 2, 'Mathematics'),
(9, 2, 'Science'),
(10, 3, 'Chinese'),
(11, 3, 'Malay'),
(12, 3, 'Tamil'),
(13, 3, 'English'),
(14, 3, 'Mathematics'),
(15, 3, 'Science'),
(16, 4, 'Chinese'),
(17, 4, 'Malay'),
(18, 4, 'Tamil'),
(19, 4, 'English'),
(20, 4, 'Mathematics'),
(21, 4, 'Science'),
(22, 5, 'Chinese'),
(23, 5, 'Malay'),
(24, 5, 'Tamil'),
(25, 5, 'English'),
(26, 5, 'Mathematics'),
(27, 5, 'Science'),
(28, 6, 'Chinese'),
(29, 6, 'Malay'),
(30, 6, 'Tamil'),
(31, 6, 'English'),
(32, 6, 'Mathematics'),
(33, 6, 'Science'),
(34, 7, 'Chinese'),
(35, 7, 'Malay'),
(36, 7, 'Tamil'),
(37, 7, 'English'),
(38, 7, 'Mathematics'),
(39, 7, 'Science'),
(40, 8, 'Chinese'),
(41, 8, 'Malay'),
(42, 8, 'Tamil'),
(43, 8, 'English'),
(44, 8, 'Mathematics'),
(45, 8, 'Chemistry'),
(46, 8, 'Biology'),
(47, 8, 'Physics'),
(48, 8, 'History'),
(49, 8, 'Literature'),
(50, 8, 'Geography'),
(51, 8, 'Economics'),
(52, 9, 'Chinese'),
(53, 9, 'Malay'),
(54, 9, 'Tamil'),
(55, 9, 'English'),
(56, 9, 'Mathematics'),
(57, 9, 'Chemistry'),
(58, 9, 'Biology'),
(59, 9, 'Physics'),
(60, 9, 'History'),
(61, 9, 'Literature'),
(62, 9, 'Geography'),
(63, 9, 'Economics'),
(64, 10, 'Chinese'),
(65, 10, 'Malay'),
(66, 10, 'Tamil'),
(67, 10, 'English'),
(68, 10, 'Mathematics'),
(69, 10, 'Chemistry'),
(70, 10, 'Biology'),
(71, 10, 'Physics'),
(72, 10, 'History'),
(73, 10, 'Literature'),
(74, 10, 'Geography'),
(75, 10, 'Economics'),
(76, 11, 'Chinese'),
(77, 11, 'Malay'),
(78, 11, 'Tamil'),
(79, 11, 'English'),
(80, 11, 'Mathematics'),
(81, 11, 'Chemistry'),
(82, 11, 'Biology'),
(83, 11, 'Physics'),
(84, 11, 'History'),
(85, 11, 'Literature'),
(86, 11, 'Geography'),
(87, 11, 'Economics'),
(88, 12, 'Chinese'),
(89, 12, 'Malay'),
(90, 12, 'Tamil'),
(91, 12, 'English'),
(92, 12, 'General Paper'),
(93, 12, 'Mathematics'),
(94, 12, 'Chemistry'),
(95, 12, 'Biology'),
(96, 12, 'Physics'),
(97, 12, 'History'),
(98, 12, 'Literature'),
(99, 12, 'Geography'),
(100, 12, 'Economics'),
(101, 22, 'Mathematics'),
(102, 22, 'English'),
(103, 22, 'Chinese'),
(104, 22, 'Malay'),
(105, 0, 'Tamil'),
(106, 22, 'Science'),
(107, 23, 'English'),
(108, 23, 'Chinese'),
(109, 0, 'Mathematics'),
(110, 23, 'Malay'),
(111, 0, 'Tamil'),
(112, 24, 'English'),
(113, 24, 'Chinese'),
(114, 0, 'Malay'),
(115, 24, 'Mathematics'),
(116, 24, 'Science'),
(117, 24, 'Tamil'),
(118, 22, 'Tamil'),
(119, 23, 'Mathematics'),
(120, 23, 'Science'),
(121, 23, 'Tamil'),
(122, 24, 'Malay'),
(123, 25, 'English'),
(124, 25, 'Chinese'),
(125, 25, 'Mathematics'),
(126, 25, 'Science'),
(127, 25, 'Malay'),
(128, 25, 'Tamil'),
(129, 26, 'English'),
(130, 26, 'Chinese'),
(131, 26, 'Mathematics'),
(132, 26, 'Malay'),
(133, 26, 'Tamil'),
(134, 26, 'Science');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('0b03e419a67a1c0f4fad7edd08d9d1c1', '203.117.37.234', 'Mozilla/5.0 (Linux; Android 4.1.1; PadFone 2 Build/JRO03L) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.59 Mo', 1387423117, 'a:2:{s:9:"user_data";s:0:"";s:8:"prev_url";s:33:"https://www.openschoolbag.com.sg/";}'),
('13d76de25bae2c720c421dc9982d0367', '222.165.21.90', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36', 1387433142, 'a:2:{s:9:"user_data";s:0:"";s:8:"prev_url";s:33:"https://www.openschoolbag.com.sg/";}'),
('1d4364fbeae115d0b5a82848bb016114', '202.156.70.252', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36', 1387429893, 'a:2:{s:9:"user_data";s:0:"";s:8:"prev_url";s:33:"https://www.openschoolbag.com.sg/";}'),
('2957a9c1c38afdc5bc8437dd6aa95a16', '222.165.21.90', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36', 1387433142, 'a:2:{s:9:"user_data";s:0:"";s:8:"prev_url";s:33:"https://www.openschoolbag.com.sg/";}'),
('31a5de233450f2688051445a012b10dd', '66.249.77.51', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1387428353, 'a:2:{s:9:"user_data";s:0:"";s:8:"prev_url";s:33:"https://www.openschoolbag.com.sg/";}'),
('3f80de958125a012ce6a24ff7b553775', '203.117.37.234', 'Mozilla/5.0 (Linux; Android 4.1.1; PadFone 2 Build/JRO03L) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.59 Mo', 1387423117, 'a:4:{s:9:"user_data";s:0:"";s:8:"prev_url";s:61:"https://www.openschoolbag.com.sg/product/z3jef138577624599up6";s:12:"captcha_time";d:1387423184.384540081024169921875;s:4:"word";s:8:"AiMJCuND";}'),
('4bc176cbd6ea1523da9d02a18e506efa', '203.117.37.234', 'Mozilla/5.0 (Linux; Android 4.1.1; PadFone 2 Build/JRO03L) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.59 Mo', 1387423117, 'a:2:{s:9:"user_data";s:0:"";s:8:"prev_url";s:33:"https://www.openschoolbag.com.sg/";}'),
('52d166183d50f7b236b11453e01545e5', '66.249.77.51', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1387428354, 'a:2:{s:9:"user_data";s:0:"";s:8:"prev_url";s:33:"https://www.openschoolbag.com.sg/";}'),
('a0aad3c2e72817ee8fc0900ed37601eb', '180.74.133.63', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)', 1387426891, 'a:2:{s:9:"user_data";s:0:"";s:8:"prev_url";s:33:"https://www.openschoolbag.com.sg/";}'),
('beac284fb7a6c07f5caee1403a2e4def', '203.116.251.236', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B144 S', 1387426643, 'a:2:{s:9:"user_data";s:0:"";s:8:"prev_url";s:33:"https://www.openschoolbag.com.sg/";}'),
('de4106d58f22eb3cb58fa41ba7935f11', '202.156.70.252', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36', 1387429893, 'a:2:{s:9:"user_data";s:0:"";s:8:"prev_url";s:33:"https://www.openschoolbag.com.sg/";}');

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE IF NOT EXISTS `cms_pages` (
  `cms_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `order` int(11) unsigned NOT NULL DEFAULT '0',
  `body` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`cms_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `images_banners`
--

CREATE TABLE IF NOT EXISTS `images_banners` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `photo` varchar(100) NOT NULL,
  `path` varchar(100) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `images_banners`
--

INSERT INTO `images_banners` (`image_id`, `photo`, `path`, `title`, `order`, `created`, `modified`) VALUES
(11, 'banner/d0c8e015fa2b32db2c4c194fe9086297.jpg', 'who-we-care', NULL, 2, '2013-10-01 07:50:43', '2013-10-09 12:54:07'),
(12, 'banner/ae4a8c197c0c794426fdffbcb00ce898.jpg', 'buyer', NULL, 5, '2013-10-01 08:07:51', '2013-10-08 20:05:34'),
(13, 'banner/de65feba78a910f2a0dd8ad0d687e264.jpg', 'seller', NULL, 1, '2013-10-01 08:07:59', '2013-10-01 08:07:59'),
(23, 'banner/d64b5de596db8f32aa464ecc12566101.jpg', 'who-we-care', '', 1, '2013-10-07 22:25:54', '2013-10-22 15:10:39'),
(37, 'banner/75411cdeeb3367761747e9f3e4b494c5.jpg', 'home', '', 4, '2013-11-01 12:38:50', '2013-12-08 10:32:31'),
(38, 'banner/594456d671cd7910d6a0c37f2bc6f183.jpg', 'about-slider', '', 1, '2013-11-01 12:38:56', '2013-11-01 12:38:56'),
(42, 'banner/76412b71f74f924bb269783c7d6a7c79.jpg', 'home', '', 3, '2013-11-08 19:23:31', '2013-12-08 10:32:28'),
(43, 'banner/33798d332f247501a10e912970000642.jpg', 'promo-bg', '', 1, '2013-11-08 19:49:56', '2013-11-08 19:49:56'),
(44, 'banner/fda9299ba721a31367023bc5c762e4e7.jpg', 'promo-bg', '', 2, '2013-11-08 20:26:52', '2013-11-08 20:27:26'),
(46, 'icons/cbe71c6ba3018b3b2fbc9adaccc2e7a1.jpg', 'product-icon', 'AUDIO', 5, '2013-11-08 20:41:06', '2013-11-08 20:48:44'),
(49, 'icons/73542ca5524e7b8d4a9e2551a504ee7d.jpg', 'product-icon', 'GUIDEBOOKS', 1, '2013-11-08 20:42:21', '2013-11-08 20:46:20'),
(50, 'icons/f802e45a77723002490929b872336931.jpg', 'product-icon', 'ASSESSMENT', 1, '2013-11-08 20:44:37', '2013-11-08 20:46:56'),
(51, 'icons/d4b345c4f500b6f48bc9e4b92e99582d.jpg', 'product-icon', 'ENRICHMENT', 3, '2013-11-08 20:44:57', '2013-11-08 20:44:57'),
(52, 'icons/a9524e0065edefbf0ee7dddfe7fe33ee.jpg', 'product-icon', 'GAMES/TOOLS', 4, '2013-11-08 20:45:25', '2013-11-08 20:45:25'),
(54, 'icons/73d883cb79d722f78bae7a985a18557c.jpg', 'product-icon', 'TYS', 2, '2013-11-08 20:47:46', '2013-11-08 20:47:46'),
(55, 'icons/ca9b76517ae3f36e338f56022c9540ce.jpg', 'product-icon', 'VIDEO', 5, '2013-11-08 20:48:17', '2013-11-08 20:48:17'),
(62, 'banner/e2e9d53acbebb2b1135aa64146282f3a.jpg', 'home', '', 2, '2013-11-27 13:03:00', '2013-12-08 10:32:28'),
(64, 'banner/f256b6a9ef4c3a785bbc36673fcd6814.jpg', 'home', '', 1, '2013-12-08 10:30:50', '2013-12-08 10:32:25');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `note_message` text NOT NULL,
  `note_link` varchar(200) DEFAULT NULL,
  `note_linkname` varchar(30) DEFAULT NULL,
  `note_status` varchar(10) NOT NULL DEFAULT 'Unread',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=84 ;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`note_id`, `user_id`, `to_user_id`, `note_message`, `note_link`, `note_linkname`, `note_status`, `created`, `modified`) VALUES
(1, 2, 2, 'Your have posted new product, Product name :<strong>Product Digital Test</strong>', 'https://www.openschoolbag.com.sg/product/2oz1x13866746678aao7', 'Go to Product', 'Read', '2013-12-10 19:24:27', '2013-12-10 19:24:27'),
(2, 2, 1, 'New product posted by :<strong>ravikanth</strong>, Product name :<strong>Product Digital Test</strong>', 'https://www.openschoolbag.com.sg/product/2oz1x13866746678aao7', 'Go to Product', 'Read', '2013-12-10 19:24:27', '2013-12-10 19:24:27'),
(3, 2, 2, 'Your have posted new product, Product name :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/product/10l2i1386674742kravr', 'Go to Product', 'Read', '2013-12-10 19:25:42', '2013-12-10 19:25:42'),
(4, 2, 1, 'New product posted by :<strong>ravikanth</strong>, Product name :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/product/10l2i1386674742kravr', 'Go to Product', 'Read', '2013-12-10 19:25:42', '2013-12-10 19:25:42'),
(5, 55, 1, 'New user ( <strong>Ravikanth K</strong> ) registered', NULL, NULL, 'Read', '2013-12-10 19:27:23', '2013-12-10 19:27:23'),
(6, 2, 55, 'You have purchased product :<strong>Product Digital Test</strong>', 'https://www.openschoolbag.com.sg/product/2oz1x13866746678aao7', 'Go to Product', 'Read', '2013-12-10 19:29:51', '2013-12-10 19:29:51'),
(7, 55, 2, 'You have a new order for the product :<strong>Product Digital Test</strong>', 'https://www.openschoolbag.com.sg/mysale/1386675945', 'Go to Order', 'Read', '2013-12-10 19:29:51', '2013-12-10 19:29:51'),
(8, 55, 1, 'New order for the product :<strong>Product Digital Test</strong>', 'https://www.openschoolbag.com.sg/admin/order/1386675945', 'Go to Order', 'Read', '2013-12-10 19:29:51', '2013-12-10 19:29:51'),
(9, 2, 55, 'You have purchased product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/product/10l2i1386674742kravr', 'Go to Product', 'Read', '2013-12-10 19:32:44', '2013-12-10 19:32:44'),
(10, 55, 2, 'You have a new order for the product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/mysale/1386676076', 'Go to Order', 'Read', '2013-12-10 19:32:44', '2013-12-10 19:32:44'),
(11, 55, 1, 'New order for the product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/admin/order/1386676076', 'Go to Order', 'Read', '2013-12-10 19:32:44', '2013-12-10 19:32:44'),
(12, 55, 2, 'You have a new order for shipment of product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/mysale/1386676076', 'Go to Order', 'Read', '2013-12-10 19:32:44', '2013-12-10 19:32:44'),
(13, 55, 1, 'New order for shipment of product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/admin/order/1386676076', 'Go to Order', 'Read', '2013-12-10 19:32:44', '2013-12-10 19:32:44'),
(14, 2, 55, 'You have purchased product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/product/10l2i1386674742kravr', 'Go to Product', 'Read', '2013-12-10 19:36:09', '2013-12-10 19:36:09'),
(15, 55, 2, 'You have a new order for the product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/mysale/1386676262', 'Go to Order', 'Read', '2013-12-10 19:36:09', '2013-12-10 19:36:09'),
(16, 55, 1, 'New order for the product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/admin/order/1386676262', 'Go to Order', 'Read', '2013-12-10 19:36:09', '2013-12-10 19:36:09'),
(17, 55, 2, 'You have a new order for shipment of product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/mysale/1386676262', 'Go to Order', 'Read', '2013-12-10 19:36:09', '2013-12-10 19:36:09'),
(18, 55, 1, 'New order for shipment of product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/admin/order/1386676262', 'Go to Order', 'Read', '2013-12-10 19:36:09', '2013-12-10 19:36:09'),
(19, 2, 55, 'You have purchased product :<strong>Product Digital Test</strong>', 'https://www.openschoolbag.com.sg/product/2oz1x13866746678aao7', 'Go to Product', 'Read', '2013-12-10 19:36:11', '2013-12-10 19:36:11'),
(20, 55, 2, 'You have a new order for the product :<strong>Product Digital Test</strong>', 'https://www.openschoolbag.com.sg/mysale/1386676262', 'Go to Order', 'Read', '2013-12-10 19:36:11', '2013-12-10 19:36:11'),
(21, 55, 1, 'New order for the product :<strong>Product Digital Test</strong>', 'https://www.openschoolbag.com.sg/admin/order/1386676262', 'Go to Order', 'Read', '2013-12-10 19:36:11', '2013-12-10 19:36:11'),
(22, 2, 55, 'You have used a promo code <strong>code10</strong> on total amount of <strong>S$ 5.00</strong>', '', '', 'Read', '2013-12-10 19:36:11', '2013-12-10 19:36:11'),
(23, 55, 1, '<strong>ravikanth_k22</strong> used a promo code <strong>code10</strong> on total amount of <strong>S$ 5.00</strong>', '', '', 'Read', '2013-12-10 19:36:11', '2013-12-10 19:36:11'),
(24, 2, 55, 'Your order status changed to <strong>Shipped</strong> for the product of <strong>Product Physical Test</strong>', NULL, NULL, 'Read', '2013-12-10 19:44:22', '2013-12-10 19:44:22'),
(25, 2, 55, 'Your order status changed to <strong>Shipped</strong> for the product of <strong>Product Physical Test</strong>', NULL, NULL, 'Read', '2013-12-10 19:45:08', '2013-12-10 19:45:08'),
(26, 2, 40, 'You have purchased product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/product/10l2i1386674742kravr', 'Go to Product', 'Read', '2013-12-10 19:58:16', '2013-12-10 19:58:16'),
(27, 40, 2, 'You have a new order for the product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/mysale/1386677383', 'Go to Order', 'Read', '2013-12-10 19:58:16', '2013-12-10 19:58:16'),
(28, 40, 1, 'New order for the product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/admin/order/1386677383', 'Go to Order', 'Read', '2013-12-10 19:58:16', '2013-12-10 19:58:16'),
(29, 40, 2, '<strong>parikshat</strong> got discount of <strong>S$ 0.45</strong> on <strong>Product Physical Test</strong>', '', '', 'Read', '2013-12-10 19:58:16', '2013-12-10 19:58:16'),
(30, 2, 40, 'You got discount of <strong>S$ 0.45</strong> on <strong>Product Physical Test</strong>', '', '', 'Read', '2013-12-10 19:58:16', '2013-12-10 19:58:16'),
(31, 40, 1, '<strong>parikshat</strong> got discount of <strong>S$ 0.45</strong> on <strong>Product Physical Test</strong>', '', '', 'Read', '2013-12-10 19:58:16', '2013-12-10 19:58:16'),
(32, 40, 2, 'You have a new order for shipment of product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/mysale/1386677383', 'Go to Order', 'Read', '2013-12-10 19:58:16', '2013-12-10 19:58:16'),
(33, 40, 1, 'New order for shipment of product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/admin/order/1386677383', 'Go to Order', 'Read', '2013-12-10 19:58:16', '2013-12-10 19:58:16'),
(34, 2, 40, 'You have purchased product :<strong>Product Digital Test</strong>', 'https://www.openschoolbag.com.sg/product/2oz1x13866746678aao7', 'Go to Product', 'Read', '2013-12-10 19:58:18', '2013-12-10 19:58:18'),
(35, 40, 2, 'You have a new order for the product :<strong>Product Digital Test</strong>', 'https://www.openschoolbag.com.sg/mysale/1386677383', 'Go to Order', 'Read', '2013-12-10 19:58:18', '2013-12-10 19:58:18'),
(36, 40, 1, 'New order for the product :<strong>Product Digital Test</strong>', 'https://www.openschoolbag.com.sg/admin/order/1386677383', 'Go to Order', 'Read', '2013-12-10 19:58:18', '2013-12-10 19:58:18'),
(37, 2, 40, 'Your order status changed to <strong>Shipped</strong> for the product of <strong>Product Physical Test</strong>', NULL, NULL, 'Unread', '2013-12-10 19:59:59', '2013-12-10 19:59:59'),
(38, 2, 40, 'You have purchased product :<strong>Product Digital Test</strong>', 'https://www.openschoolbag.com.sg/product/2oz1x13866746678aao7', 'Go to Product', 'Unread', '2013-12-10 20:31:49', '2013-12-10 20:31:49'),
(39, 40, 2, 'You have a new order for the product :<strong>Product Digital Test</strong>', 'https://www.openschoolbag.com.sg/mysale/1386679623', 'Go to Order', 'Read', '2013-12-10 20:31:49', '2013-12-10 20:31:49'),
(40, 40, 1, 'New order for the product :<strong>Product Digital Test</strong>', 'https://www.openschoolbag.com.sg/admin/order/1386679623', 'Go to Order', 'Read', '2013-12-10 20:31:49', '2013-12-10 20:31:49'),
(41, 2, 40, 'You have purchased product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/product/10l2i1386674742kravr', 'Go to Product', 'Unread', '2013-12-10 20:31:49', '2013-12-10 20:31:49'),
(42, 40, 2, 'You have a new order for the product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/mysale/1386679623', 'Go to Order', 'Read', '2013-12-10 20:31:49', '2013-12-10 20:31:49'),
(43, 40, 1, 'New order for the product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/admin/order/1386679623', 'Go to Order', 'Read', '2013-12-10 20:31:49', '2013-12-10 20:31:49'),
(44, 40, 2, '<strong>parikshat</strong> got discount of <strong>S$ 0.45</strong> on <strong>Product Physical Test</strong>', '', '', 'Read', '2013-12-10 20:31:49', '2013-12-10 20:31:49'),
(45, 2, 40, 'You got discount of <strong>S$ 0.45</strong> on <strong>Product Physical Test</strong>', '', '', 'Unread', '2013-12-10 20:31:49', '2013-12-10 20:31:49'),
(46, 40, 1, '<strong>parikshat</strong> got discount of <strong>S$ 0.45</strong> on <strong>Product Physical Test</strong>', '', '', 'Read', '2013-12-10 20:31:49', '2013-12-10 20:31:49'),
(47, 40, 2, 'You have a new order for shipment of product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/mysale/1386679623', 'Go to Order', 'Read', '2013-12-10 20:31:49', '2013-12-10 20:31:49'),
(48, 40, 1, 'New order for shipment of product :<strong>Product Physical Test</strong>', 'https://www.openschoolbag.com.sg/admin/order/1386679623', 'Go to Order', 'Read', '2013-12-10 20:31:49', '2013-12-10 20:31:49'),
(49, 56, 1, 'New user ( <strong>Eryn Wu</strong> ) registered', NULL, NULL, 'Read', '2013-12-11 12:23:59', '2013-12-11 12:23:59'),
(50, 2, 40, 'Your order status changed to <strong>Delivered</strong> for the product of <strong>Product Physical Test</strong>', NULL, NULL, 'Unread', '2013-12-11 12:32:15', '2013-12-11 12:32:15'),
(51, 2, 40, 'Your order status changed to <strong>Delivered</strong> for the product of <strong>Product Physical Test</strong>', NULL, NULL, 'Unread', '2013-12-11 12:32:26', '2013-12-11 12:32:26'),
(52, 2, 40, 'Your order status changed to <strong>Delivered</strong> for the product of <strong>Product Physical Test</strong>', NULL, NULL, 'Unread', '2013-12-11 12:32:27', '2013-12-11 12:32:27'),
(53, 2, 55, 'Your order status changed to <strong>Delivered</strong> for the product of <strong>Product Physical Test</strong>', NULL, NULL, 'Read', '2013-12-11 12:32:37', '2013-12-11 12:32:37'),
(54, 2, 55, 'Your order status changed to <strong>Delivered</strong> for the product of <strong>Product Physical Test</strong>', NULL, NULL, 'Read', '2013-12-11 12:32:50', '2013-12-11 12:32:50'),
(55, 57, 1, 'New user ( <strong>Edison Koo</strong> ) registered', NULL, NULL, 'Read', '2013-12-12 02:36:30', '2013-12-12 02:36:30'),
(56, 37, 37, 'Your have posted new product, Product name :<strong>Physical 1</strong>', 'https://www.openschoolbag.com.sg/product/2u0lk13869040025yirk', 'Go to Product', 'Read', '2013-12-13 11:06:42', '2013-12-13 11:06:42'),
(57, 37, 1, 'New product posted by :<strong>huifen</strong>, Product name :<strong>Physical 1</strong>', 'https://www.openschoolbag.com.sg/product/2u0lk13869040025yirk', 'Go to Product', 'Read', '2013-12-13 11:06:42', '2013-12-13 11:06:42'),
(58, 36, 36, 'Your have posted new product, Product name :<strong>Digital 1</strong>', 'https://www.openschoolbag.com.sg/product/4qr3s138690410766ufv', 'Go to Product', 'Read', '2013-12-13 11:08:27', '2013-12-13 11:08:27'),
(59, 36, 1, 'New product posted by :<strong>huifenong</strong>, Product name :<strong>Digital 1</strong>', 'https://www.openschoolbag.com.sg/product/4qr3s138690410766ufv', 'Go to Product', 'Read', '2013-12-13 11:08:27', '2013-12-13 11:08:27'),
(60, 58, 1, 'New user ( <strong>Huifen Ong</strong> ) registered', NULL, NULL, 'Read', '2013-12-13 11:12:34', '2013-12-13 11:12:34'),
(61, 36, 58, 'You have purchased product :<strong>Digital 1</strong>', 'https://www.openschoolbag.com.sg/product/4qr3s138690410766ufv', 'Go to Product', 'Read', '2013-12-13 11:16:24', '2013-12-13 11:16:24'),
(62, 58, 36, 'You have a new order for the product :<strong>Digital 1</strong>', 'https://www.openschoolbag.com.sg/mysale/1386905426', 'Go to Order', 'Read', '2013-12-13 11:16:24', '2013-12-13 11:16:24'),
(63, 58, 1, 'New order for the product :<strong>Digital 1</strong>', 'https://www.openschoolbag.com.sg/admin/order/1386905426', 'Go to Order', 'Read', '2013-12-13 11:16:24', '2013-12-13 11:16:24'),
(64, 37, 58, 'You have purchased product :<strong>Physical 1</strong>', 'https://www.openschoolbag.com.sg/product/2u0lk13869040025yirk', 'Go to Product', 'Read', '2013-12-13 11:16:24', '2013-12-13 11:16:24'),
(65, 58, 37, 'You have a new order for the product :<strong>Physical 1</strong>', 'https://www.openschoolbag.com.sg/mysale/1386905426', 'Go to Order', 'Read', '2013-12-13 11:16:24', '2013-12-13 11:16:24'),
(66, 58, 1, 'New order for the product :<strong>Physical 1</strong>', 'https://www.openschoolbag.com.sg/admin/order/1386905426', 'Go to Order', 'Read', '2013-12-13 11:16:24', '2013-12-13 11:16:24'),
(67, 58, 37, 'You have a new order for shipment of product :<strong>Physical 1</strong>', 'https://www.openschoolbag.com.sg/mysale/1386905426', 'Go to Order', 'Read', '2013-12-13 11:16:24', '2013-12-13 11:16:24'),
(68, 58, 1, 'New order for shipment of product :<strong>Physical 1</strong>', 'https://www.openschoolbag.com.sg/admin/order/1386905426', 'Go to Order', 'Read', '2013-12-13 11:16:24', '2013-12-13 11:16:24'),
(69, 37, 58, 'You have used a promo code <strong>testpromo</strong> on total amount of <strong>S$ 0.20</strong>', '', '', 'Read', '2013-12-13 11:16:27', '2013-12-13 11:16:27'),
(70, 58, 1, '<strong>huifentest</strong> used a promo code <strong>testpromo</strong> on total amount of <strong>S$ 0.20</strong>', '', '', 'Read', '2013-12-13 11:16:27', '2013-12-13 11:16:27'),
(71, 37, 58, 'Your order status changed to <strong>Shipped</strong> for the product of <strong>Physical 1</strong>', NULL, NULL, 'Read', '2013-12-13 11:19:56', '2013-12-13 11:19:56'),
(72, 59, 1, 'New user ( <strong>reny reny</strong> ) registered', NULL, NULL, 'Read', '2013-12-13 14:00:04', '2013-12-13 14:00:04'),
(73, 2, 40, 'Your order status changed to <strong>Pending</strong> for the product of <strong>Product Physical Test</strong>', NULL, NULL, 'Unread', '2013-12-13 14:22:11', '2013-12-13 14:22:11'),
(74, 2, 40, 'Your order status changed to <strong>Shipped</strong> for the product of <strong>Product Physical Test</strong>', NULL, NULL, 'Unread', '2013-12-13 14:22:13', '2013-12-13 14:22:13'),
(75, 37, 58, 'Your order status changed to <strong>Shipped</strong> for the product of <strong>Physical 1</strong>', NULL, NULL, 'Read', '2013-12-14 18:02:08', '2013-12-14 18:02:08'),
(76, 60, 1, 'New user ( <strong>Candy Luo</strong> ) registered', NULL, NULL, 'Read', '2013-12-16 10:28:01', '2013-12-16 10:28:01'),
(77, 60, 60, 'Your have posted new product, Product name :<strong>0101 Shape Bean Bag</strong>', 'https://www.openschoolbag.com.sg/product/1p12x1387176001vknwh', 'Go to Product', 'Read', '2013-12-16 14:40:01', '2013-12-16 14:40:01'),
(78, 60, 1, 'New product posted by :<strong>Gifts of Montessori</strong>, Product name :<strong>0101 Shape Bean Bag</strong>', 'https://www.openschoolbag.com.sg/product/1p12x1387176001vknwh', 'Go to Product', 'Read', '2013-12-16 14:40:01', '2013-12-16 14:40:01'),
(79, 60, 60, 'Your have posted new product, Product name :<strong>0101 Shape Bean Bag</strong>', 'https://www.openschoolbag.com.sg/product/ziyys1387176404a87it', 'Go to Product', 'Read', '2013-12-16 14:46:44', '2013-12-16 14:46:44'),
(80, 60, 1, 'New product posted by :<strong>Gifts of Montessori</strong>, Product name :<strong>0101 Shape Bean Bag</strong>', 'https://www.openschoolbag.com.sg/product/ziyys1387176404a87it', 'Go to Product', 'Read', '2013-12-16 14:46:44', '2013-12-16 14:46:44'),
(81, 60, 60, 'Your have posted new product, Product name :<strong>0102 Bean Bag - Learn my Name</strong>', 'https://www.openschoolbag.com.sg/product/6ycaz1387178648b71rq', 'Go to Product', 'Unread', '2013-12-16 15:24:08', '2013-12-16 15:24:08'),
(82, 60, 1, 'New product posted by :<strong>Gifts of Montessori</strong>, Product name :<strong>0102 Bean Bag - Learn my Name</strong>', 'https://www.openschoolbag.com.sg/product/6ycaz1387178648b71rq', 'Go to Product', 'Read', '2013-12-16 15:24:08', '2013-12-16 15:24:08'),
(83, 61, 1, 'New user ( <strong>TingTing Xiao</strong> ) registered', NULL, NULL, 'Read', '2013-12-18 09:14:57', '2013-12-18 09:14:57');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(20) NOT NULL,
  `trans_fee` varchar(20) NOT NULL,
  `total_amount` varchar(30) NOT NULL,
  `final_amount` varchar(30) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_no`, `trans_fee`, `total_amount`, `final_amount`, `created`, `modified`) VALUES
(1, '1386675945', '0.50', '2', '2', '2013-12-10 19:29:51', '2013-12-10 19:29:51'),
(2, '1386676076', '0.50', '3', '3', '2013-12-10 19:32:45', '2013-12-10 19:32:45'),
(3, '1386676262', '0.50', '5', '4.5', '2013-12-10 19:36:11', '2013-12-10 19:36:11'),
(4, '1386677383', '0.50', '4.55', '4.55', '2013-12-10 19:58:18', '2013-12-10 19:58:18'),
(5, '1386679623', '0.50', '4.55', '4.55', '2013-12-10 20:31:50', '2013-12-10 20:31:50'),
(6, '1386905426', '0.50', '0.2', '0.1', '2013-12-13 11:16:27', '2013-12-13 11:16:27');

-- --------------------------------------------------------

--
-- Table structure for table `orders_items`
--

CREATE TABLE IF NOT EXISTS `orders_items` (
  `order_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(200) NOT NULL,
  `puser_id` varchar(200) NOT NULL,
  `order_pname` varchar(200) NOT NULL,
  `order_pqty` int(11) NOT NULL,
  `order_pprice` varchar(20) NOT NULL,
  `order_punique` varchar(50) NOT NULL,
  `order_no` varchar(20) NOT NULL,
  `order_ptype` varchar(20) NOT NULL,
  `order_cat` varchar(200) NOT NULL,
  `order_discount` varchar(20) NOT NULL DEFAULT '0',
  `order_promo` varchar(20) DEFAULT NULL,
  `order_d_expir` varchar(20) DEFAULT NULL,
  `order_status` varchar(20) NOT NULL DEFAULT 'Done',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`order_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `orders_items`
--

INSERT INTO `orders_items` (`order_item_id`, `user_id`, `puser_id`, `order_pname`, `order_pqty`, `order_pprice`, `order_punique`, `order_no`, `order_ptype`, `order_cat`, `order_discount`, `order_promo`, `order_d_expir`, `order_status`, `created`, `modified`) VALUES
(1, 'ravikanth_k22', 'ravikanth', 'Product Digital Test', 1, '2', '2oz1x13866746678aao7', '1386675945', 'Digital', 'Pre-School:English:Assessment Book', '0', NULL, '2013-12-12 19:24:27', 'Done', '2013-12-10 19:29:51', '2013-12-10 19:29:51'),
(2, 'ravikanth_k22', 'ravikanth', 'Product Physical Test', 1, '3', '10l2i1386674742kravr', '1386676076', 'Physical', 'Junior College:English:Enrichment', '0', NULL, NULL, 'Shipped', '2013-12-10 19:32:44', '2013-12-11 12:32:50'),
(3, 'ravikanth_k22', 'ravikanth', 'Product Physical Test', 1, '3', '10l2i1386674742kravr', '1386676262', 'Physical', 'Junior College:English:Enrichment', '0.30', 'code10', NULL, 'Shipped', '2013-12-10 19:36:09', '2013-12-11 12:32:37'),
(4, 'ravikanth_k22', 'ravikanth', 'Product Digital Test', 1, '2', '2oz1x13866746678aao7', '1386676262', 'Digital', 'Pre-School:English:Assessment Book', '0.20', 'code10', '2013-12-12 19:24:27', 'Done', '2013-12-10 19:36:11', '2013-12-10 19:36:11'),
(5, 'parikshat', 'ravikanth', 'Product Physical Test', 1, '3', '10l2i1386674742kravr', '1386677383', 'Physical', 'Junior College:English:Enrichment', '0.45', NULL, NULL, 'Shipped', '2013-12-10 19:58:16', '2013-12-11 12:32:27'),
(6, 'parikshat', 'ravikanth', 'Product Digital Test', 1, '2', '2oz1x13866746678aao7', '1386677383', 'Digital', 'Pre-School:English:Assessment Book', '0', NULL, '2013-12-12 19:24:27', 'Done', '2013-12-10 19:58:18', '2013-12-10 19:58:18'),
(7, 'parikshat', 'ravikanth', 'Product Digital Test', 1, '2', '2oz1x13866746678aao7', '1386679623', 'Digital', 'Pre-School:English:Assessment Book', '0', NULL, '2013-12-12 19:24:27', 'Done', '2013-12-10 20:31:49', '2013-12-10 20:31:49'),
(8, 'parikshat', 'ravikanth', 'Product Physical Test', 1, '3', '10l2i1386674742kravr', '1386679623', 'Physical', 'Junior College:English:Enrichment', '0.45', NULL, NULL, 'Shipped', '2013-12-10 20:31:49', '2013-12-13 14:22:13'),
(9, 'huifentest', 'huifenong', 'Digital 1', 1, '0.1', '4qr3s138690410766ufv', '1386905426', 'Digital', 'Upper Primary:Chinese:Assessment Book', '0.05', 'testpromo', '2013-12-14 11:08:27', 'Done', '2013-12-13 11:16:24', '2013-12-13 11:16:24'),
(10, 'huifentest', 'huifen', 'Physical 1', 1, '0.1', '2u0lk13869040025yirk', '1386905426', 'Physical', 'Lower Primary:Malay:Assessment Book', '0.05', 'testpromo', NULL, 'Shipped', '2013-12-13 11:16:24', '2013-12-14 18:02:08');

-- --------------------------------------------------------

--
-- Table structure for table `order_shipping`
--

CREATE TABLE IF NOT EXISTS `order_shipping` (
  `shipping_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(20) NOT NULL,
  `shipping_fname` varchar(200) NOT NULL,
  `shipping_pincode` varchar(10) NOT NULL,
  `shipping_address` varchar(255) NOT NULL,
  `shipping_city` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`shipping_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `order_shipping`
--

INSERT INTO `order_shipping` (`shipping_id`, `order_no`, `shipping_fname`, `shipping_pincode`, `shipping_address`, `shipping_city`, `created`, `modified`) VALUES
(1, '1386676076', 'Ravikanth', '12345', 'test address', 'test city', '2013-12-10 19:31:37', '2013-12-10 19:31:37'),
(2, '1386676262', 'Ravikanth', '12345', 'test address', 'test city', '2013-12-10 19:35:23', '2013-12-10 19:35:23'),
(3, '1386677383', 'Parikshat Anand', '411020', 'Aundh', 'Michigan', '2013-12-10 19:55:09', '2013-12-10 19:55:09'),
(4, '1386678808', 'Parikshat Anand', '411020', 'Aundh', 'Michigan', '2013-12-10 20:19:25', '2013-12-10 20:19:25'),
(5, '1386679623', 'parikshat', '411020', 'aundh', 'Pune', '2013-12-10 20:30:57', '2013-12-10 20:30:57'),
(6, '1386787564', 'Edison Koo', '506942', 'Ferraria Park Condominium 10 Flora Drive #08-08', 'Singapore', '2013-12-12 02:42:08', '2013-12-12 02:42:08'),
(7, '1386905426', 'Huifen Ong', '123456', 'test address', 'Singapore', '2013-12-13 11:14:47', '2013-12-13 11:14:47');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `page_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`page_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `title`, `slug`, `body`) VALUES
(1, 'About Us', 'about', '<p>We believe that Singapore has an established education system with its constant improvements in general education, the professional development of the teachers and the availability of learning resources. We see a&nbsp;wide range of learning resources&nbsp;from academic resources like assessment books and guidebooks,enrichment books like encyclopedias and flash cards to education aids like memory retention games and mathematical building blocks, to name a few!</p>\n<p>We are proud that education is taken with much importance here by parents, students, community and the nation! We want to do our part to enhance the education space by making available these resources at greater convenience and increasing ease of access.</p>\n<p>We hope to increase the reach of these resources to support educators, parents, and learners to build a more dynamic and vibrant education landscape. This is how&nbsp;<strong>OpenSchoolbag</strong>&nbsp;came about!</p>'),
(2, 'About Us', 'about1', '<p>&nbsp;</p>\n<h3>OpenSchoolbag wants you to learn beyond what you have in your schoolbag! Education materials should not be only available on shelves and racks of bookstores. It should be on your tablet, PC, laptop, anywhere, anytime you want it!</h3>'),
(3, 'Partners', 'partners', '<p>We believe in a multi-faceted education landscape comprising content experts, vendors, resource providers, etc. as such, we are always on the lookout for partnerships to better complement and enhance the education landscape. If there are any areas we can potentially work together, we want to hear from you! Drop us a note at <strong>info@OpenSchoolbag.com.sg!</strong></p>'),
(4, 'Privacy Policy', 'privacy_policy', '<h2 class="MsoListParagraphCxSpFirst" style="margin-left: 18pt; text-indent: -18pt; text-align: left;"><!--[if !supportLists]-->1.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><!--[endif]-->Use of Site</h2>\n<p class="MsoListParagraph" style="margin-left: 39.6pt; mso-add-space: auto; text-align: justify; text-justify: inter-ideograph; text-indent: -21.6pt; mso-list: l0 level2 lfo1;"><!--[if !supportLists]-->1.1.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span><!--[endif]-->By accessing and/or using our Website and the related sites, applications, services, good and/or registering an account with us, you consent to all the terms and conditions including this privacy policy.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-align: left;">&nbsp;</p>\n<h2 class="MsoListParagraphCxSpMiddle" style="margin-left: 18pt; text-indent: -18pt; text-align: left;"><!--[if !supportLists]-->2.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><!--[endif]-->User Information</h2>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]-->2.1.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span><!--[endif]-->OpenSchoolbag.com.sg respects and cares for your privacy and we do not sell, rent or release your information to third parties, except when required, &nbsp;to deliver services on our website such as, but not limited to, permissive user information, product information, and reviews on products.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]-->2.2.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span><!--[endif]-->User information includes all information provided by Users upon account creation or any additional information provided by Users with needs related to payment or delivery.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]-->2.3.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;</span>Our payment vendors may collect your credit information, only for the purpose of collecting payments from Buyers or transferring payments to Sellers on the site. We are not exposed to the information provided to the payment vendors.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]-->2.4.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span><!--[endif]-->We may combine information you provided willingly and information we collect automatically.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]-->2.5.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span><!--[endif]-->Information that you choose to publish will no longer be considered private. This published information may include, but not restricted to, product information, pictures, text, reviews.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]-->2.6.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span><!--[endif]--><span lang="EN">We process information on our servers in different jurisdictions world-wide so we may process your personal information on a server located outside the country where you live.</span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18pt; text-align: left;">&nbsp;</p>\n<h2 class="MsoListParagraphCxSpMiddle" style="margin-left: 18pt; text-indent: -18pt; text-align: left;"><!--[if !supportLists]-->3.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><!--[endif]-->Use of information</h2>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]-->3.1.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span><!--[endif]--><span lang="EN">We use collected information only as appropriate to provide you with quality service and security. </span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]-->3.2.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span><!--[endif]--><span lang="EN">We may use the information collected to verify your identity, establish and set up your account, verify or re-issue a password, log your activity and contact you from time to time. The information helps us improve our services to you, customize your browsing experience and inform you about additional products, services or promotions.</span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]-->3.3. &nbsp;<span lang="EN">By providing your contact information, you are deemed to have given OpenSchoolbag.com.sg the permission to send you including, but not restricted to, marketing promotions, newsletters, etc.</span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]-->3.4.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span><!--[endif]--><span lang="EN">We may use this information, even to third parties,only to track any fraudulent activities and other inappropriate activities and monitor content integrity. </span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]-->3.5.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;</span>We may use your information to resolve disputes, collect fees and troubleshoot problems.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]--><span lang="EN">3.6.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span><!--[endif]--><span lang="EN">We will ask for your consent before using information for a purpose other than those set out in this privacy policy. </span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]--><span lang="EN">3.7.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span><!--[endif]--><span lang="EN">We retain information as long as it is necessary and relevant for our operations. So we may retain information from closed accounts to comply with the law, prevent fraud, collect any fees owed, resolve disputes, troubleshoot problems, assist with any investigation, enforce our Website terms and take other actions permitted by law.</span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;">3.8.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span>In the event of business transition, merger, acquisition, bankruptcy or sale of business, your information will be considered as assets transferred.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-align: left;">&nbsp;</p>\n<h2 class="MsoListParagraphCxSpMiddle" style="margin-left: 18pt; text-indent: -18pt; text-align: left;"><!--[if !supportLists]-->4.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><!--[endif]-->Use of information beyond Site</h2>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]-->&nbsp; &nbsp; &nbsp; &nbsp; OpenSchoolbag.com.sg does not allow Users to use information of other Users beyond the Website. This includes, but not limited to, sending advertising emails to Buyers, sharing information on selling posts on other sites.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-align: left;">&nbsp;</p>\n<h2 class="MsoListParagraphCxSpMiddle" style="margin-left: 18pt; text-indent: -18pt; text-align: left;"><!--[if !supportLists]-->5.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><!--[endif]-->Security</h2>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]-->5.1.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span><!--[endif]-->We will take effort in maintaining security of the Website and your information by preventing unauthorised access to it through industry standard technologies and internal procedures. However we cannot guarantee the unauthorised access can never happen.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]-->5.2.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span><!--[endif]-->Registered Users are responsible for their own passwords. We do not take responsibility to unauthorised access due to Users&rsquo; oversight such as disclosure to others, logging in from shared computers and so on.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-align: left;">&nbsp;</p>\n<h2 class="MsoListParagraphCxSpMiddle" style="margin-left: 18pt; text-indent: -18pt; text-align: left;"><!--[if !supportLists]-->6.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><!--[endif]-->Third Party Sites</h2>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]-->6.1.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;</span>Our payment vendors may collect your credit information, only for the purpose of collecting payments from Buyers or transferring payments to Sellers on the Website. We are not exposed to the information provide to the payment vendors, this information is subject to the privacy policy applicable to the payment vendor.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-top: 3.75pt; margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]--><span lang="EN">6.2.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span><!--[endif]--><span lang="EN">There are links to third party websites. We are not responsible for these sites and have no responsibility or liability whatsoever with regard to privacy matters or any other legal matter with respect to such sites. We encourage you to carefully read the privacy policies and the terms of use or service of such websites. Our terms of use and our privacy policy apply only to information collected by us in accordance with this policy.</span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-top: 3.75pt; margin-left: 39.6pt; text-align: left;"><strong><span lang="EN">&nbsp;</span></strong></p>\n<h2 class="MsoListParagraphCxSpMiddle" style="margin-top: 3.75pt; margin-left: 18pt; text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><span lang="EN">7.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span lang="EN">&nbsp;</span>Cookies &amp; IP Addresses</h2>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]--><span lang="EN">7.1.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span><!--[endif]--><span lang="EN">We collect additional information while you access, browse, and view or otherwise use the Website. In other words, when you access the Website we are aware of your usage of the Website, and may gather, collect and record the information relating to such usage, including, geo-location information, IP address, device and connection information, browser information and web-log information, and all communications recorded by Users through the Website. We use that information to, not limited to, enhance user experience, personalize your browsing experience as well as monitor the Site for fraud and inappropriate content or behavior.</span></p>\n<p style="text-align: left;">&nbsp;</p>\n<p class="MsoListParagraphCxSpLast" style="margin-left: 39.6pt; text-indent: -21.6pt; text-align: left;"><!--[if !supportLists]--><span lang="EN">7.2.<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span><!--[endif]--><span lang="EN">When you visit the </span><span lang="EN">Website</span><span lang="EN">, we may use industry-wide technologies such as "cookies" (or similar technologies), which store certain information on your computer and which will allow us, among other things, to enable automatic sign-in to the </span><span lang="EN">Website</span><span lang="EN">, make your browsing much more convenient and effortless and allow us to test user experience and offer you personalized browsing or promotions</span><span lang="EN">. If you do not block the cookies, we will assume you have given us the permission to collect information. However, some of our services may not function properly if the cookies are disabled.</span></p>'),
(5, 'Terms and Conditions', 'tnc', '<p class="MsoNormal" style="text-align: left;">&ldquo;We&rdquo; refers to the management team of OpenSchoolbag.com.sg. &ldquo;Users&rdquo; refers to all &ldquo;Sellers&rdquo; and &ldquo;Buyers&rdquo; of the Website. &ldquo;Sellers&rdquo; refers to individuals or companies posting and selling products on the site. &ldquo;Buyers&rdquo; refers to individuals or companies shopping, buying and making payment for products on sale on the site. &ldquo;Website&rdquo; refers to www.OpenSchoolbag.com.sg.</p>\n<h2 style="margin-left: 36pt; text-indent: -18pt; text-align: left;"><!--[if !supportLists]-->1.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp; </span><!--[endif]-->Terms of Use</h2>\n<p class="MsoListParagraphCxSpFirst" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>1.1<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;</span></strong>OpenSchoolbag.com.sg is an online marketplace to buy and sell digital or physical educational materials directly from and to each other. The products may include, but not limited to, original digital or physical materials created by the Sellers. OpenSchoolbag.com.sg provides the service of a marketplace but at no time owns any products for or on behalf of any Sellers.</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>1.2<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;</span></strong><!--[endif]-->The Website offers the services including, but not limited to, posting, selling, finding and buying of educational materials, including reviews and discussions. We have no control over and do not guarantee the copyrights, quality, safety and legitimacy of products advertised, accuracy of Users&rsquo; contents or listings and ability of Sellers.</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>1.3<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;</span></strong>We are responsible for operating and managing of our Website and make reasonable effort in order to maintain efficient service.</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>1.4<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;</span></strong>OpenSchoolbag.com.sg&rsquo;s logos and trademarks are not allowed to be displayed or used in any manner without any prior written consent of OpenSchoolbag.com.sg. You are not allowed to alter, distort or modify our logos in confusingly or misleading similar mark that falsely imply a relationship or affiliation to OpenSchoolbag.com.sg or its employees.</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>1.5<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;</span></strong>We are not responsible for third parties&rsquo; links to our Website. We do not examine nor evaluate, hence are not liable for any product, actions, statement or content related to these links. For the same, OpenSchoolbag.com.sg is not responsible for product, actions, statement or content related to third parties&rsquo; links on our Website. You should review their privacy and conditions of use.</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>1.6<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;</span></strong>We reserve the rights to make changes to the terms and conditions accordingly, of which the Users will be notified a fair period, determined by us, before the effective date. Users will be notified by Email or&nbsp; other reasonable means. Your use after effective dates of these changes constitutes your consent to the change of terms. If you do not agree, you must stop using the service immediately.</p>\n<p class="MsoListParagraphCxSpLast" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>1.7<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;</span></strong>By registering as a User, we deem that you have read, understood and agreed to all the terms in this agreement.</p>\n<p class="MsoListParagraphCxSpLast" style="text-indent: -18pt; text-align: left;">&nbsp;</p>\n<h2 style="margin-left: 36pt; text-indent: -18pt; text-align: left;"><!--[if !supportLists]-->2.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp; </span><!--[endif]-->Sellers</h2>\n<p class="MsoListParagraphCxSpFirst" style="text-indent: -18pt; text-align: left;"><strong style="font-size: 10.909090995788574px;">2.1<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;</span></strong><strong style="font-size: 10.909090995788574px;">Registration</strong></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.1.1<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp;&nbsp;</span>You are considered to be a &lsquo;Seller&rsquo; on OpenSchoolbag.com.sg once you have completed registration with us.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.1.2<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>Upon account registration, you agree to OpenSchoolbag.com.sg&rsquo;s service fees and charges for completed sales transactions for allowing you to post and sell your products.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.1.3<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>We will acknowledge that you have provided accurate and truthful information in the account registration form upon sign up. We reserve the right to refuse or terminate any account, access or use of the Website if in doubt.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.1.4<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>You are allowed to post and sell your education materials once you are a registered Seller.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-align: left;">&nbsp;</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>2.2<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></strong><!--[endif]--><strong>Product Information</strong></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.2.1<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>Sellers are responsible for managing and providing information of their products on sale. This includes, but not restricted to- Summary of resource, price, subject, level, type of resource and previews of the products.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.2.2<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>OpenSchoolbag.com.sg will &nbsp;deem the product information provided as accurate and authentic.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.2.3<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>We reserve the right to remove any product listing if in doubt.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.2.4<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span><!--[endif]-->All changes to product listing are subjected to our approval.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-align: left;">&nbsp;</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>2.3<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></strong><!--[endif]--><strong>Pricing and Payment</strong></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.3.1<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Sellers are responsible for &nbsp;determining the prices of your products. The prices set on your products should include service fees, delivery charges, if any, and other costs that may arise in supplying the products.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.3.2<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>For physical products, OpenSchoolbag.com.sg will notify you by email or any other form of communication as we deem fit once there is a successful purchase with verified payment of your products.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><span style="text-indent: -36pt;">2.3.3</span><span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span><span style="text-indent: -36pt;">Service fees charged by OpenSchoolbag.com.sg will be automatically deducted from total sales transactions during stipulated periods. The settlement amount will then be paid out to Sellers on the payment date determined by us.</span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.3.4<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>Sellers will receive a sales transactions report at the end of every transaction period defined by us.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.3.5<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span><!--[endif]-->We would disburse the settlement amount&nbsp; to Sellers to the bank account indicated at the point of your account registration.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.3.6<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>The minimum settlement amount to be paid out is $10. If your settlement amount is less than $10, it will be rolled over to the next period and paid out when it exceeds the minimum amount.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.3.7<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp;&nbsp;</span>There will be a withdrawal charge for each request of withdrawal other than the standard payment date. &nbsp;The settlement amount will be paid to Sellers in 3 working days after withdrawal request is acknowledged and accepted by us. The withdrawal charge is 10% on settlement amount.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.3.8<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>OpenSchoolbag.com.sg reserves the rights to adjust the fees and charges accordingly, of which the Sellers will be notified a fair period, determined by OpenSchoolbag.com.sg, before the effective date.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.3.9<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>OpenSchoolbag.com.sg will not be responsible for technical fault arising from payment system but will work with the system provider to minimise the downtime.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-align: left;">&nbsp;</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>2.4<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></strong><!--[endif]--><strong>Copyrights, Publications &amp; Delivery</strong></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.4.1<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>By posting your product for sale on OpenSchoolbag.com.sg, you have assured that you are the original creator or the lawful owner of it. We take no responsibility or part in disputes arising from copyrights and publications issues.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.4.2<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>For purchase of physical products, orders must be processed within 10 working days.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.4.3<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span><!--[endif]-->Refund or exchange must be done within reasonable conditions and the Sellers&rsquo; rights should not be unreasonably withheld.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-align: left;">&nbsp;</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>2.5<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></strong><!--[endif]--><strong>Ownership of Marketing</strong></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.5.1<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>Sellers agree that OpenSchoolbag.com.sg may, at its discretion, engage in promotional activities for and on behalf of Sellers to encourage transactions between Buyers and Sellers including reducing or discounting the prices set by the Sellers from time to time.</p>\n<p class="MsoListParagraphCxSpLast" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->2.5.2<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>Sellers also agree that OpenSchoolbag.com.sg has the rights to use the products on sale for promotions and marketing purposes, where the Sellers will be given the rightful credits.</p>\n<p class="MsoListParagraphCxSpLast" style="margin-left: 54pt; text-indent: -36pt; text-align: left;">&nbsp;</p>\n<h2 style="margin-left: 36pt; text-indent: -18pt; text-align: left;"><!--[if !supportLists]-->3.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp; </span><!--[endif]-->Buyers</h2>\n<p class="MsoListParagraphCxSpFirst" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>3.1<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></strong><!--[endif]--><strong>Registration</strong></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->3.1.1<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>We will acknowledge that you have provided accurate and truthful information in the account registration form upon sign up. We reserve the right to refuse or terminate any account, access or use of the Website if in doubt.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->3.1.2<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Upon registration, you understood and agreed to transaction fees in addition to your purchases.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->3.1.3<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>You are only allowed to make purchases and payments after you have registered an account with us.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18pt; text-align: left;"><strong>&nbsp;</strong></p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>3.2<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></strong><!--[endif]--><strong>Resource Information</strong></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->3.2.1<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>Sellers are responsible for managing and providing products. This includes, but not restricted to- Summary of resource, price, subject, level, type of resource and previews of the products.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->3.2.2<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>OpenSchoolbag.com.sg will deem the information provided as accurate and true information of the product. We will not be responsible for any differences in the information provided and actual product delivered.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-align: left;">&nbsp;</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>3.3<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></strong><!--[endif]--><strong>Payment</strong></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->3.3.1<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp;&nbsp;</span><!--[endif]-->By making a payment for a purchase, you are deemed to have understood and agreed to all information about the &nbsp;product you are purchasing.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->3.3.2<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; </span>Buyers are to bear the transaction fees for payment made over the Website. &nbsp;These fees are charged by our payment vendor. Hence, we will update and change the transaction fees if our payment vendor change their fees.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->3.3.3<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>OpenSchoolbag.com.sg will not be responsible for technical fault arising from payment vendor but will work with the system provider to minimise the downtime.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->3.3.4<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp;&nbsp;</span><!--[endif]-->Payment made over the website is strictly non-refundable.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-align: left;">&nbsp;</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>3.4<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></strong><!--[endif]--><strong>Delivery</strong></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->3.4.1<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp;&nbsp;</span><!--[endif]-->When the payment is made, the delivery will be as per follows:</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 61.2pt; text-indent: -25.2pt; text-align: left;"><!--[if !supportLists]--><span style="font-family: ''Courier New''; mso-fareast-font-family: ''Courier New'';">o<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span></span><!--[endif]-->Digital products: A downloadable link will be available after you have purchased a product. The link will be available for a <span style="text-decoration: underline;">number of days fixed by Sellers</span>, with effect from the purchase date, after which the link will expire. You may download the purchased material for unlimited number of times.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 61.2pt; text-indent: -25.2pt; text-align: left;"><!--[if !supportLists]--><span style="font-family: ''Courier New''; mso-fareast-font-family: ''Courier New'';">o<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span></span>Physical products: The product will be sent to you directly by the Seller at the address you have indicated at the User registration. Your order will be processed within 10 working days.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->3.4.2<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span><!--[endif]-->Once a purchase is made, our service is rendered and will not be responsible for post-delivery consequences such as use of products and accuracy of actual content against information provided.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-align: left;">&nbsp;</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>3.5<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></strong><!--[endif]--><strong>Disputes</strong></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->3.5.1<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp; &nbsp; &nbsp; &nbsp;</span>OpenSchoolbag.com.sg will not be responsible for disputes between Sellers and Buyers on products listed, purchased and delivered through our Website.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-indent: -36pt; text-align: left;"><!--[if !supportLists]-->3.5.2<span style="font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>In the event of unforeseen circumstances beyond the terms and conditions mentioned herewith, we encourage you to contact us.</p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 54pt; text-align: left;">&nbsp;</p>\n<p class="MsoListParagraphCxSpLast" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>3.6<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></strong><!--[endif]--><strong>Distribution<br /> </strong>OpenSchoolbag.com.sg or the Sellers, also the original creators of the products, reserve the right to press charges in the event of illegitimate distribution or misuse of products purchased through our Website.</p>\n<p class="MsoListParagraphCxSpLast" style="text-indent: -18pt; text-align: left;">&nbsp;</p>\n<h2 style="margin-left: 36pt; text-indent: -18pt; text-align: left;"><!--[if !supportLists]-->4.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp; </span><!--[endif]-->Technical</h2>\n<p class="MsoListParagraph" style="text-align: justify; text-justify: inter-ideograph; text-indent: -18.0pt; mso-list: l0 level2 lfo1;"><!--[if !supportLists]--><strong>4.1<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></strong><!--[endif]-->OpenSchoolbag.com.sg makes no representation regarding the services provided by our payment vendor, and is not responsible for delays in payment caused by them.</p>\n<p class="MsoNormal" style="text-align: left;">&nbsp;</p>\n<h2 style="margin-left: 36pt; text-indent: -18pt; text-align: left;"><!--[if !supportLists]-->5.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp; </span><!--[endif]-->Payment</h2>\n<p class="MsoListParagraphCxSpFirst" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>5.1<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;</span></strong>We make no representation of our payment service provider and will not be responsible if there are delays or technical faults in the payment process on our Website encountered by our Users.</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>5.2<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;</span></strong>Buyers will bear the transaction fees charged by the payment vendor for purchases over the Website.&nbsp; These fees are charged by our payment service provider and hence we will adjust our fees accordingly if our payment service provider change their fees.</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>5.3<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;</span></strong>Sellers will pay OpenSchoolbag.com.sg service fees for all sales done over the site for allowing you to post and sell your products.</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><!--[if !supportLists]--><strong>5.4<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;</span></strong>Sellers will pay a withdrawal charge for each request of withdrawal outside the standard payment date determined by us</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><strong>5.5<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;</span></strong>OpenSchoolbag.com.sg reserves the rights to adjust the fees and charges accordingly, of which the Sellers will be notified a fair period, determined by OpenSchoolbag.com.sg, before the effective date.</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;">&nbsp;</p>\n<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt; text-align: left;"><strong><span style="text-indent: 0px;">Updated as at 26 Nov 13</span></strong></p>'),
(6, 'Why OpenSchoolBag', 'why_osb', '<h2>Why OpenSchoolbag?</h2>\n<p>&nbsp;</p>\n<p><strong>''The Ultimate Education Marketplace''</strong></p>\n<p>We aspire to be the complete platform for exchange of education resources online. Looking for enrichment materials, assessment and topical guides, academic books, educational tools and aids and the list goes on. As long as it is synonymous with education, you name it, you find it&hellip; all on <strong>OpenSchoolbag.com.sg</strong>!</p>');
INSERT INTO `pages` (`page_id`, `title`, `slug`, `body`) VALUES
(7, 'FAQ''s', 'faqs', '<h1 class="MsoNormal"><span lang="EN-US">General </span></h1>\n<p class="MsoListParagraphCxSpFirst" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><em><span lang="EN-US">1.<span style="font-style: normal; font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></em></strong><!--[endif]--><strong><span lang="EN-US">I forgot my password, what should I do? <br /> </span></strong><em><span lang="EN-US">Go to Login, click on Forgot Password, key in your email address and a new temporary password will be sent to you within minutes.<br /> <!--[if !supportLineBreakNewLine]--><br /> <!--[endif]--></span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><em><span lang="EN-US">2.<span style="font-style: normal; font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></em></strong><!--[endif]--><strong><span lang="EN-US">My particulars have changed since I registered my account. How can I change them?<br /> </span></strong><em><span lang="EN-US">You can go to your Dashboard &gt; General&gt; Update Profile<br /> <!--[if !supportLineBreakNewLine]--><br /> <!--[endif]--></span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><em><span lang="EN-US">3.<span style="font-style: normal; font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></em></strong><strong><span lang="EN-US">Can I change my username after my account is created?</span></strong></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US">To avoid confusion, we do <span style="text-decoration: underline;">not</span> allow change of Username.</span></em></p>\n<p class="MsoListParagraphCxSpLast" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US">&nbsp;</span></em></p>\n<h1 class="MsoNormal"><span lang="EN-US">For Buyer</span></h1>\n<h2 class="MsoNormal"><span style="text-decoration: underline;"><span lang="EN-US">Payment/Refund</span></span></h2>\n<p class="MsoListParagraphCxSpFirst" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">4.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">What are the modes of payment?<br /></span></strong><em><span lang="EN-US">We allow only Paypal at the moment.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US"> <!--[if !supportLineBreakNewLine]--><br /> <!--[endif]--></span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><em><span lang="EN-US">5.<span style="font-style: normal; font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></em></strong><!--[endif]--><strong><span lang="EN-US">Where can I view my past purchases?<br /></span></strong><em><span lang="EN-US">You may see under Dashboard&gt; Buying&gt; View Purchases</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">6.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">Can I ask for a refund for my purchase?<br /> </span></strong><em><span lang="EN-US">There are only refunds for faulty physical products and no refunds for digital products as they are immediately delivered to you.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><em><span lang="EN-US">7.<span style="font-style: normal; font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></em></strong><!--[endif]--><strong><span lang="EN-US">How does the refund process work?<br /> </span></strong><em><span lang="EN-US">Please notify us within <span style="text-decoration: underline;">seven</span> working days from delivery of physical product and we will have the seller to contact you on the refund or other process.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;">&nbsp;</p>\n<h2 class="MsoNormal"><span style="text-decoration: underline;"><span lang="EN-US">Product/Download/Delivery</span></span></h2>\n<p class="MsoListParagraphCxSpFirst" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">8.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">How do I search for products?<br /></span></strong><em><span lang="EN-US">You may use the search engine in the banner on every page. Search by Level, Subject, Resource type and keywords. You should provide more information so the search engine can return with the products you want.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US"> <!--[if !supportLineBreakNewLine]--><br /> <!--[endif]--></span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US">For example, I am looking for a topical paper for Sec 2,&nbsp; Mathematics , Algebra:</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Level:&nbsp; Secondary 2</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Subject: Mathematics</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Resource Type: Assessment Book</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Keywords: Algebra</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">9.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">Is the product information accurate?</span></strong><span lang="EN-US"><br /> <em>All product information is provided by the sellers. If you wish to know more, you may raise questions under Q&amp;A section. Click on the Product and go to Ask a Question.</em></span></p>\n<p class="MsoListParagraphCxSpMiddle"><span lang="EN-US">&nbsp;</span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">10.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">The product I received is different from the product information. What can I do?</span></strong><span lang="EN-US"><br /> <em>By making payment for a product, we will assume that you have understood and agreed to all the product information provided. There are strictly no refunds for digital products. However, you may leave a review on the product to feedback to the seller.<br /> <!--[if !supportLineBreakNewLine]--><br /> <!--[endif]--></em></span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">11.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">What if I did not manage to download my purchase in time?</span></strong><span lang="EN-US"><br /> <em>The download period is set by the seller so please pay attention to the download date and time when you are making payment.</em></span></p>\n<p class="MsoListParagraphCxSpMiddle"><span lang="EN-US">&nbsp;</span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">12.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">I cannot download my purchase anymore. Why is that so?</span></strong><span lang="EN-US"><br /> <em>You could have missed the download period allowed. We cannot extend the download period so please pay attention to the download date and time when you are making payment.<br /> <!--[if !supportLineBreakNewLine]--><br /> <!--[endif]--></em></span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">13.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">My order did not arrive. Who should I notify?<br /></span></strong><em><span lang="EN-US">All digital products will be available for download under Buying&gt; View Purchases. If you do not see it after purchase, please contact us immediately.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US">All physical products will be sent to you directly from seller. They will be delivered to you within 10 business days. Please check the delivery status by going to Buying&gt; View Purchases. If the status is delivered but you have not received it, please contact us.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">14.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">Can I let another person use my purchased digital product?<br /></span></strong><em><span lang="EN-US">No. Upon purchase, you agreed not to circulate the digital product you have purchased. You do not want to carry the risk of having the copyrights owner taking legal action against you.</span></em></p>\n<p class="MsoListParagraphCxSpLast" style="margin-left: 18.0pt; mso-add-space: auto;"><span lang="EN-US">&nbsp;</span></p>\n<h2 class="MsoNormal"><span style="text-decoration: underline;"><span lang="EN-US">Questions/ Reviews/ Ratings</span></span></h2>\n<p class="MsoNormal"><strong style="font-size: 11px;"><span lang="EN-US">15.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><strong style="font-size: 11px;"><span lang="EN-US">Can I ask questions if I do not have an account?<br /></span></strong><em><span lang="EN-US">&nbsp; &nbsp; &nbsp; &nbsp; No. It is recommended that you register an account with us so that you can be notified when you &nbsp; &nbsp; &nbsp; &nbsp; question has been answered.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">16.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">If I have question about a product, where and who do I raise the question?<br /></span></strong><em><span lang="EN-US">Every product has a Q&amp;A section. We advise you to look through the list of questions and answers before raising any questions as it may have been raised by other buyers previously.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US">All questions for a product will be answered by its seller. <br /> <br /> You will be notified once your question has been answered.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="text-align: center;" align="center"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">17.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">What if I do not get an answer to my question?<br /></span></strong><em><span lang="EN-US">The seller may have missed your question. Contact us if you do not get an answer and we will contact the seller.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">18.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">What is the difference between &ldquo;Product Rating&rdquo; and &ldquo;User Rating&rdquo;?<br /></span></strong><em style="font-size: 11px;"><span lang="EN-US">You are entitled to leave a rating on the products you have purchased. This is a &ldquo;Product Rating&rdquo;.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US">The ratings to all products uploaded by a seller will be averaged to give the seller a &ldquo;User Rating&rdquo;.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">19.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">Who has the right to give reviews and ratings of products?<br /></span></strong><em><span lang="EN-US">Anyone who has purchased the product has the right to give review and rating to the product.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">20.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">Can I give a review or rating?<br /></span></strong><em><span lang="EN-US">Yes, you can give review or rating to products you have purchased. But you can only do it once for each product and no amendments are allowed.</span></em></p>\n<p class="MsoListParagraphCxSpLast" style="margin-left: 18.0pt; mso-add-space: auto;"><span lang="EN-US">&nbsp;</span></p>\n<h1 class="MsoNormal"><span lang="EN-US">For Seller </span></h1>\n<h2 class="MsoNormal"><span style="text-decoration: underline;"><span lang="EN-US">Payment/Refund</span></span></h2>\n<p class="MsoListParagraphCxSpFirst" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">21.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">Can I change the bank account that you credit my proceeds to?<br /></span></strong><em><span lang="EN-US">You can go to your Dashboard </span></em>&gt;<em><span lang="EN-US"> General&gt; &nbsp;Update Profile</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-align: center;" align="center"><span lang="EN-US">&nbsp;</span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">22.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">When will I receive my sales proceeds?<br /></span></strong><em><span lang="EN-US">For sales during the period 1-15<sup>th</sup>, you will receive the proceeds on the 22<sup>nd</sup> of the same month and for the period 16-31<sup>st</sup>, on the 7<sup>th</sup> of the following month.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">23.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">Can I make non-scheduled withdrawals of my sales proceeds?<br /></span></strong><em><span lang="EN-US">Yes, you can. Please write in to us and give us <span style="text-decoration: underline;">three</span> working days to process. You will be subjected to a 10% withdrawal charge.</span></em></p>\n<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>\n<h2 class="MsoNormal"><span style="text-decoration: underline;"><span lang="EN-US">Account/ Products</span></span></h2>\n<p class="MsoListParagraphCxSpFirst" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">24.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">What is the difference between Partner and Member?<br /></span></strong><em><span lang="EN-US">As a Member, you will upload and manage your own products while being a Partner seller, we will upload and manage your products on your behalf. Please drop us an enquiry for details.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><span lang="EN-US">&nbsp;</span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">25.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">What is Seller&rsquo;s Profile page?</span></strong><span lang="EN-US"><br /> <em>Seller&rsquo;s Profile page allows you to share your qualifications and expertise. This can help you in the sales of your products.<br /> Please see Dashboard&gt; Selling&gt; Publish Profile<br /> <!--[if !supportLineBreakNewLine]--><br /> <!--[endif]--></em></span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">26.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">Can I update my Seller&rsquo;s profile page?<br /></span></strong><em><span lang="EN-US">Yes, you can do it anytime from your Dashboard&gt; Selling&gt; Publish Profile</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">27.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">Where do I upload my products for sale?<br /> </span></strong><em><span lang="EN-US">You can upload your products from Dashboard&gt; Selling&gt; Post a Product</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">28.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">Can I change my product information?</span></strong><span lang="EN-US"><br /> <em>You may change the product information but subjected to our approval. This is to protect the buyers&rsquo; interests.<br /> <br /> Please go to Dashboard&gt; Selling&gt; Manage my Products<br /> <!--[if !supportLineBreakNewLine]--><br /> <!--[endif]--></em></span></p>\n<p class="MsoListParagraphCxSpLast" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">29.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">What are the formats of files I can put up for sale?<br /> </span></strong><em><span lang="EN-US">All file types, except executable file formats.</span></em></p>\n<p class="MsoNormal" style="text-indent: 18.0pt;"><span lang="EN-US">&nbsp;</span></p>\n<h2 class="MsoNormal"><span style="text-decoration: underline;"><span lang="EN-US">Sales/ Notifications/ Delivery</span></span></h2>\n<p class="MsoListParagraphCxSpFirst" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">30.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">Can I view the total purchases/downloads of my products? <br /> </span></strong><em><span lang="EN-US">Yes, you can see them in Dashboard&gt; Selling&gt; View Sales</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">31.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">Does my dashboard reflect my instant sales history?<br /> </span></strong><em><span lang="EN-US">Yes, the sales history is real time, updated instantly.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">32.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">Will I be notified if someone purchases my products?<br /></span></strong><em><span lang="EN-US">Yes, you will be notified immediately when your physical product is purchased so that you can prepare delivery.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US"> <br /> For both digital and physical products, you will receive a bi-weekly sales report. Alternatively, you can go into Dashboard&gt; Selling&gt; View Sales for all sales records.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">33.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">How long must I have the physical product delivered to buyer?<br /></span></strong><em><span lang="EN-US">You must have the physical product delivered to the buyer within 10 business days from the date of purchase.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US"> <br /> Please update the status of delivery from Dashboard&gt; Selling&gt; View Sales.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">34.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">Who will bear the shipping costs?<br /></span></strong><em><span lang="EN-US">Shipping costs should be included in the selling price of your product. Buyer will not pay any fees in additional to the price posted for the product.</span></em></p>\n<p class="MsoListParagraphCxSpLast"><span lang="EN-US">&nbsp;</span></p>\n<h2 class="MsoNormal"><span style="text-decoration: underline;"><span lang="EN-US">Questions/ Reviews/ Ratings</span></span></h2>\n<p class="MsoListParagraphCxSpFirst" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">35.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">I received notification that a buyer has sent a question. How do I answer it?<br /> </span></strong><em><span lang="EN-US">Please go to Dashboard&gt; Notifications.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">36.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">What if I disagree with any reviews or ratings given to me?<br /></span></strong><em><span lang="EN-US">Buyers are entitled to leave any reviews or ratings and we do not intervene. However if offensive language is used, we will intervene to remove the review.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">37.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">What is the difference between &ldquo;Product Rating&rdquo; and &ldquo;User Rating&rdquo;?<br /></span></strong><em style="font-size: 11px;"><span lang="EN-US">Every buyer is entitled to leave a rating on the product they have purchased. This is a &ldquo;Product Rating&rdquo;.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US">&nbsp;</span></em></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto;"><em><span lang="EN-US">The ratings to all products you have uploaded will be averaged to give you a &ldquo;User Rating&rdquo;.</span></em></p>\n<p class="MsoListParagraphCxSpMiddle"><span lang="EN-US">&nbsp;</span></p>\n<p class="MsoListParagraphCxSpMiddle" style="margin-left: 18.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!--[if !supportLists]--><strong><span lang="EN-US">38.<span style="font-weight: normal; font-size: 7pt; font-family: ''Times New Roman'';">&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-US">How does the system work out my &ldquo;User Rating&rdquo;?<br /></span></strong><em><span lang="EN-US">The ratings for your products are averaged out to work out your User rating.</span></em></p>');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE IF NOT EXISTS `partners` (
  `partner_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `partner_title` varchar(200) NOT NULL,
  `partner_slogan` varchar(200) DEFAULT NULL,
  `partner_logo` varchar(100) NOT NULL,
  `partner_body` text NOT NULL,
  `partner_fb` varchar(200) DEFAULT NULL,
  `partner_tw` varchar(200) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`partner_id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`partner_id`, `user_id`, `partner_title`, `partner_slogan`, `partner_logo`, `partner_body`, `partner_fb`, `partner_tw`, `created`, `modified`) VALUES
(1, 4, 'Ramesh', 'Ramesh 123', 'partners/3555f3e13266350afedfadf3432a59b4.jpg', 'Thank you for your registration. We have sent you an email to activate your account. Please check your mailbox', 'ramesh123', 'ramesh123', '2013-10-18 15:25:15', '2013-10-18 15:25:15'),
(4, 48, 'SAP', 'Singapore Asia Publishers Pte Ltd', '', '-', 'Singapore-Asia-Publishers', '-', '2013-11-29 18:54:55', '2013-12-02 23:13:03'),
(5, 49, 'Young Scientists Reader', 'Promote the Learning of Science Among School Children Using Exciting and Engaging Media', 'partners/17abad2ec5a9b9a2130315dd22505f16.jpg', '-', '-', '-', '2013-11-29 19:15:09', '2013-12-02 23:12:22'),
(6, 50, 'Ravikanth is here', 'nothing less nothing more', 'partners/9db1c614f92420d48e94ea43a4a2a2d6.jpg', 'Technically skilled and accomplished IT Professional PHP Programmer. - View Ravikanth Katkam''s profile', 'http://facebook.com/katkamravikanth', 'https://twitter.com/katkamravikanth', '2013-12-02 17:05:20', '2013-12-02 17:05:20');

-- --------------------------------------------------------

--
-- Table structure for table `paypal_log`
--

CREATE TABLE IF NOT EXISTS `paypal_log` (
  `paypal_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `paypal_log_data` text,
  `paypal_log_created` datetime DEFAULT NULL,
  PRIMARY KEY (`paypal_log_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `paypal_log`
--

INSERT INTO `paypal_log` (`paypal_log_id`, `paypal_log_data`, `paypal_log_created`) VALUES
(1, 'a:1:{s:4:"text";s:1011:"[12/10/2013 7:29 PM] - SUCCESS!\nIPN POST Vars from Paypal:\nmc_gross=2.50\n protection_eligibility=Ineligible\n address_status=confirmed\n payer_id=9JLPT3RMBW2F6\n tax=0.00\n address_street=1 Main St\n payment_date=03:29:36 Dec 10, 2013 PST\n payment_status=Pending\n charset=windows-1252\n address_zip=95131\n first_name=raja\n address_country_code=US\n address_name=raja gonda''s Test Store\n notify_version=3.7\n custom=\n payer_status=verified\n address_country=United States\n address_city=San Jose\n quantity=1\n verify_sign=AFcWxV21C7fd0v3bYYYRCpSSRl31AcgMwP-eEI41XcTadE4QY91RLOcb\n payer_email=dot@dotspiders.com\n txn_id=0B6226642F7327225\n payment_type=instant\n payer_business_name=raja gonda''s Test Store\n last_name=gonda\n address_state=CA\n receiver_email=info@openschoolbag.com.sg\n pending_reason=unilateral\n txn_type=web_accept\n item_name=OpenSchoolBag\n mc_currency=SGD\n item_number=\n residence_country=US\n test_ipn=1\n handling_amount=0.00\n transaction_subject=\n payment_gross=\n shipping=0.00\n ipn_track_id=5e8759fcaa7e5\n ";}', '2013-12-10 19:29:41'),
(2, 'a:1:{s:4:"text";s:1011:"[12/10/2013 7:32 PM] - SUCCESS!\nIPN POST Vars from Paypal:\nmc_gross=3.50\n protection_eligibility=Ineligible\n address_status=confirmed\n payer_id=9JLPT3RMBW2F6\n tax=0.00\n address_street=1 Main St\n payment_date=03:32:07 Dec 10, 2013 PST\n payment_status=Pending\n charset=windows-1252\n address_zip=95131\n first_name=raja\n address_country_code=US\n address_name=raja gonda''s Test Store\n notify_version=3.7\n custom=\n payer_status=verified\n address_country=United States\n address_city=San Jose\n quantity=1\n verify_sign=AmjsN3Quw4rPz1OWYImTuyPNu-PRAE1IbSWty42wZIm1K9UDbAxQpBYD\n payer_email=dot@dotspiders.com\n txn_id=4L125738SJ346060E\n payment_type=instant\n payer_business_name=raja gonda''s Test Store\n last_name=gonda\n address_state=CA\n receiver_email=info@openschoolbag.com.sg\n pending_reason=unilateral\n txn_type=web_accept\n item_name=OpenSchoolBag\n mc_currency=SGD\n item_number=\n residence_country=US\n test_ipn=1\n handling_amount=0.00\n transaction_subject=\n payment_gross=\n shipping=0.00\n ipn_track_id=4025495b21289\n ";}', '2013-12-10 19:32:15'),
(3, 'a:1:{s:4:"text";s:1011:"[12/10/2013 7:35 PM] - SUCCESS!\nIPN POST Vars from Paypal:\nmc_gross=5.00\n protection_eligibility=Ineligible\n address_status=confirmed\n payer_id=9JLPT3RMBW2F6\n tax=0.00\n address_street=1 Main St\n payment_date=03:35:49 Dec 10, 2013 PST\n payment_status=Pending\n charset=windows-1252\n address_zip=95131\n first_name=raja\n address_country_code=US\n address_name=raja gonda''s Test Store\n notify_version=3.7\n custom=\n payer_status=verified\n address_country=United States\n address_city=San Jose\n quantity=1\n verify_sign=A--8MSCLabuvN8L.-MHjxC9uypBtA-T6Z5X-Mp0j1NkEXxP5acCyhVTo\n payer_email=dot@dotspiders.com\n txn_id=7JR38164C6062761D\n payment_type=instant\n payer_business_name=raja gonda''s Test Store\n last_name=gonda\n address_state=CA\n receiver_email=info@openschoolbag.com.sg\n pending_reason=unilateral\n txn_type=web_accept\n item_name=OpenSchoolBag\n mc_currency=SGD\n item_number=\n residence_country=US\n test_ipn=1\n handling_amount=0.00\n transaction_subject=\n payment_gross=\n shipping=0.00\n ipn_track_id=ebffadeda562b\n ";}', '2013-12-10 19:35:54'),
(4, 'a:1:{s:4:"text";s:1011:"[12/10/2013 7:58 PM] - SUCCESS!\nIPN POST Vars from Paypal:\nmc_gross=5.05\n protection_eligibility=Ineligible\n address_status=confirmed\n payer_id=9JLPT3RMBW2F6\n tax=0.00\n address_street=1 Main St\n payment_date=03:58:02 Dec 10, 2013 PST\n payment_status=Pending\n charset=windows-1252\n address_zip=95131\n first_name=raja\n address_country_code=US\n address_name=raja gonda''s Test Store\n notify_version=3.7\n custom=\n payer_status=verified\n address_country=United States\n address_city=San Jose\n quantity=1\n verify_sign=AFcWxV21C7fd0v3bYYYRCpSSRl31AtWtG5y9vr3EXiKJ2XkSNDUps6rF\n payer_email=dot@dotspiders.com\n txn_id=1K197859HE1023214\n payment_type=instant\n payer_business_name=raja gonda''s Test Store\n last_name=gonda\n address_state=CA\n receiver_email=info@openschoolbag.com.sg\n pending_reason=unilateral\n txn_type=web_accept\n item_name=OpenSchoolBag\n mc_currency=SGD\n item_number=\n residence_country=US\n test_ipn=1\n handling_amount=0.00\n transaction_subject=\n payment_gross=\n shipping=0.00\n ipn_track_id=2c93c2c56fed1\n ";}', '2013-12-10 19:58:07'),
(5, 'a:1:{s:4:"text";s:1011:"[12/10/2013 8:31 PM] - SUCCESS!\nIPN POST Vars from Paypal:\nmc_gross=5.05\n protection_eligibility=Ineligible\n address_status=confirmed\n payer_id=9JLPT3RMBW2F6\n tax=0.00\n address_street=1 Main St\n payment_date=04:31:33 Dec 10, 2013 PST\n payment_status=Pending\n charset=windows-1252\n address_zip=95131\n first_name=raja\n address_country_code=US\n address_name=raja gonda''s Test Store\n notify_version=3.7\n custom=\n payer_status=verified\n address_country=United States\n address_city=San Jose\n quantity=1\n verify_sign=AEnFCJ1l4e.T..39YaTNlfI1hPWlA7yGUwGEfsa7f0vwtTmS4MqO7uT3\n payer_email=dot@dotspiders.com\n txn_id=5834706301906491S\n payment_type=instant\n payer_business_name=raja gonda''s Test Store\n last_name=gonda\n address_state=CA\n receiver_email=info@openschoolbag.com.sg\n pending_reason=unilateral\n txn_type=web_accept\n item_name=OpenSchoolBag\n mc_currency=SGD\n item_number=\n residence_country=US\n test_ipn=1\n handling_amount=0.00\n transaction_subject=\n payment_gross=\n shipping=0.00\n ipn_track_id=f0f5d6bbc71f2\n ";}', '2013-12-10 20:31:38'),
(6, 'a:1:{s:4:"text";s:1046:"[12/12/2013 2:44 AM] - SUCCESS!\nIPN POST Vars from Paypal:\nmc_gross=53.50\n protection_eligibility=Ineligible\n address_status=unconfirmed\n payer_id=C55ZKRY77YCQW\n tax=0.00\n address_street=Blk 209 #12-342 Pasir Ris Street 21\n payment_date=10:44:40 Dec 11, 2013 PST\n payment_status=Completed\n charset=gb2312\n address_zip=510209\n first_name=Koo\n mc_fee=2.59\n address_country_code=SG\n address_name=Jakoo Koo\n notify_version=3.7\n custom=\n payer_status=unverified\n business=info@openschoolbag.com.sg\n address_country=Singapore\n address_city=\n quantity=1\n verify_sign=AAh-gjn1ENnDuooduWNAFaW4Pdn0AFxZazmY0dSCP2illvI9oo0nt4Qu\n payer_email=Jakoo@txc.com.sg\n txn_id=0UB98229VY735100C\n payment_type=instant\n last_name=Jakoo\n address_state=\n receiver_email=info@openschoolbag.com.sg\n payment_fee=\n receiver_id=6F6JD4TF3DNS2\n txn_type=web_accept\n item_name=OpenSchoolBag\n mc_currency=SGD\n item_number=\n residence_country=SG\n receipt_id=0551-0824-0352-9911\n handling_amount=0.00\n transaction_subject=\n payment_gross=\n shipping=0.00\n ipn_track_id=ac725e6333c99\n ";}', '2013-12-12 02:44:52'),
(7, 'a:1:{s:4:"text";s:1050:"[12/13/2013 11:16 AM] - SUCCESS!\nIPN POST Vars from Paypal:\nmc_gross=0.60\n protection_eligibility=Ineligible\n address_status=unconfirmed\n payer_id=W9UY7SXMN566C\n tax=0.00\n address_street=Blk 675 Hougang Avenue 8 #09-601\n payment_date=19:16:12 Dec 12, 2013 PST\n payment_status=Completed\n charset=gb2312\n address_zip=530675\n first_name=Huifen\n mc_fee=0.52\n address_country_code=SG\n address_name=Ong Huifen\n notify_version=3.7\n custom=\n payer_status=unverified\n business=info@openschoolbag.com.sg\n address_country=Singapore\n address_city=\n quantity=1\n verify_sign=AxY4PZnLwHn-wCsVYatnHp9kp4-PAobQm.09.osGTY0Wx4HEoOju-Fed\n payer_email=huifenong00@gmail.com\n txn_id=89E08921J86713338\n payment_type=instant\n last_name=Ong\n address_state=\n receiver_email=info@openschoolbag.com.sg\n payment_fee=\n receiver_id=6F6JD4TF3DNS2\n txn_type=web_accept\n item_name=OpenSchoolBag\n mc_currency=SGD\n item_number=\n residence_country=SG\n receipt_id=2850-3871-6083-6001\n handling_amount=0.00\n transaction_subject=\n payment_gross=\n shipping=0.00\n ipn_track_id=6b07e1fbbdd17\n ";}', '2013-12-13 11:16:22');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_type` varchar(20) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_level` varchar(50) NOT NULL,
  `product_subject` varchar(50) NOT NULL,
  `product_resource` varchar(50) NOT NULL,
  `product_desc` text NOT NULL,
  `product_price` varchar(20) NOT NULL,
  `product_down_days` varchar(10) DEFAULT NULL,
  `product_pages` varchar(10) DEFAULT NULL,
  `product_weight` varchar(10) DEFAULT NULL,
  `product_pic` varchar(200) DEFAULT NULL,
  `product_preview` text,
  `product_file` varchar(200) DEFAULT NULL,
  `product_desclaimer` varchar(50) NOT NULL,
  `product_shipping` varchar(20) NOT NULL,
  `product_unique` varchar(30) NOT NULL,
  `product_status` varchar(20) NOT NULL,
  `down_count` int(11) NOT NULL DEFAULT '0',
  `order_count` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `user_id`, `product_type`, `product_name`, `product_level`, `product_subject`, `product_resource`, `product_desc`, `product_price`, `product_down_days`, `product_pages`, `product_weight`, `product_pic`, `product_preview`, `product_file`, `product_desclaimer`, `product_shipping`, `product_unique`, `product_status`, `down_count`, `order_count`, `created`, `modified`) VALUES
(1, 2, 'Digital', 'AJAX Toolkit Developer\\''s Guide', 'Secondary 1', 'Biology,Chemistry,Chinese,Economics,English', 'Guidebooks', 'Because information is delivered via a browser, AJAX works best with relatively small amounts of data (up to 200 records,\\napproximately six fields with 50 characters of data each). The larger the data set returned, the more time it will take to construct\\nand deconstruct a SOAP message, and as the size of an individual record gets larger, the impact on performance becomes\\ngreater. Also, as more HTML nodes are created from the data, the potential for poor performance increases. Because browsers\\nare not efficient, careful consideration needs to be given to browser memory management if you intend to display a large\\namount of data.', '210', '7', '54', '', 'product_pic/69f95207235fae35bfdf2982653ebe6a.jpg', 'product_pic/2dc3b97f750be093f00f196b7094423e.JPG,product_pic/1116d8dfcc00d1d7e4071e03a6c3302e.jpg,product_pic/5e625d8e56dd194864beda9c0dc0a831.jpg,product_pic/d01e9db88b0a5b04c322d72d8fb8358b.jpg,product_pic/2ef185546f5ba6580897bd2220669de0.jpg', 'product_file/0ed840e0af853e9f94c7b72a6ffde6cf.pdf', 'Yes', '', 'vcctr1382081735zynn2', 'Inactive', 1, 0, '2013-10-18 15:35:35', '2013-10-22 15:02:07'),
(4, 32, 'Digital', 'testpaper', 'Primary 5', 'English,Mathematics', 'Assessment Book', 'test description', '0.08', '5', '2', '', 'product_pic/82f7ece67b543541da098a9be6b2f475.jpg', 'product_pic/475b6889c5d40a1acec39e155efbb4ce.png', 'product_file/3586ce3913240380970c0a37acd9bf6a.pptx', 'Yes', '', 'r9nxb1382456199meofo', 'Inactive', 3, 4, '2013-10-22 23:36:39', '2013-11-10 16:20:54'),
(5, 32, 'Physical', 'test toy 1', 'Pre-School', 'Chinese,English,Mathematics', 'Educational Kits and Games', 'toy product', '0.10', '', '', '500', 'product_pic/2e00b31c1422f79f3d75892d4edad238.jpg', '', '', 'Yes', '', '8k7tt1382456335srfaw', 'Inactive', 0, 9, '2013-10-22 23:38:55', '2013-11-30 21:16:38'),
(6, 34, 'Digital', 'test', 'Junior College', 'Chemistry', 'Assessment Book', 'test', '.8', '4', '8', '', 'product_pic/28212e450330257cb870dd878e22ff1a.JPG', '', 'product_file/435b349f38e4ce9e40d29de30fabb709.JPG', 'Yes', '', 'hw5141383060321zm5g8', 'Inactive', 3, 2, '2013-10-29 23:25:21', '2013-11-13 21:36:15'),
(7, 34, 'Digital', 'resource', 'Primary 4', 'Chinese', 'Ten Year Series', 'resource test test', '0.3', '3', '15', '', 'product_pic/966dd1ea0184542501cc56d467244533.jpg', 'product_pic/541ed88104250474a365bc1825e9cf1d.jpg', 'product_file/6011fb4f7de4a3773ce3a1c8c852c260.pptx', 'Yes', '0', 'jyvxm1383729313nnsvh', 'Inactive', 5, 13, '2013-11-06 17:15:13', '2013-12-01 15:09:03'),
(8, 37, 'Digital', 'testtest', 'Pre-School', 'Chinese,English,Mathematics', 'Audio', 'ZZzz', '8+++', '2', '2', '', '', '', 'product_file/82d2a6f5676293f05fb5dae6c7f30282.xlsx', 'Yes', '', 'gvopj138435188581z15', 'Inactive', 0, 0, '2013-11-13 22:11:25', '2013-11-13 22:11:25'),
(9, 37, 'Digital', 'ZZZ', 'Secondary 3', 'Chemistry', 'Assessment Book', 'ZZZzz', '****', '2', '2', '', '', '', 'product_file/9433300e6668286aa5c7704ed6a84b0d.xlsx', 'Yes', '', 'urbvp1384351942s99zz', 'Inactive', 0, 0, '2013-11-13 22:12:22', '2013-11-13 22:12:22'),
(10, 42, 'Digital', 'ALGEBRA EXAM PAPERS', 'Secondary 4/5', 'Mathematics,Physics', 'Assessment Book', 'This book contains 15 Algebra Exam Papers plus solutions to hundreds of exam questions. Each test should take 30 minutes to complete and serve as ideal preparation for any secondary level math exam. This is a unique opportunity to purchase hundreds of exam style questions complete with professional mark schemes.', '5.00', '7', '156', '', 'product_pic/26b2154a4617cc2bf8c2e980d8abc341.jpg', 'product_pic/3b22862e3cc6a1c23356daedf66d6523.png,product_pic/c8fe64ca959b92e730c6d13deb7ac2d3.png,product_pic/744680bdf88288f8fc4aa69a8623ef52.png,product_pic/42573f4949552d8d025509bbdeb0c523.png,product_pic/3f1210b93b03b99f75d0d3173aa90083.png', 'product_file/692ea0b46fdbc098607dd1dfab4b33b9.pdf', 'Yes', '', '4vrhx1384533552sc2fy', 'Active', 1, 0, '2013-11-16 00:39:12', '2013-11-16 11:16:35'),
(11, 42, 'Digital', 'TRIGONOMETRY AND GEOMETRY', 'Secondary 4/5', 'Mathematics', 'Assessment Book', 'This book contains 15 exam papers that are compromised purely of trigonometry and geometry questions. Each paper should take 30 minutes to complete and should serve as an excellent preparation tool for a mathematics exam.', '5.00', '7', '139', '', 'product_pic/82db8a6b7717b7c07038a81fc3e6df0b.jpg', 'product_pic/bd7b81c9c9d1ecd42fb1469659e5df17.png,product_pic/e1a5dc45dea4b64dd6d5e937b7a9da18.png,product_pic/ef8df1629071b45121d95755f62f3aee.png,product_pic/e2d90c0bad5d771d3e11ffa1d7b3a9fe.png,product_pic/9024efec3e1ac9386503ea5eabd4fd57.png', 'product_file/32ecd493e7fbc575860c84df8f2791f9.pdf', 'Yes', '', 'zkglr1384535917qjv85', 'Active', 2, 0, '2013-11-16 01:18:37', '2013-11-30 15:07:15'),
(13, 34, 'Digital', 'Pre-School Education Kits', 'Pre-School', 'Others', 'Educational Kits and Games', 'Pre-School Education Kits', '1.20', '2', '5', '', 'product_pic/7e4bb4f11e2dc2294de44b5cdc2f7251.jpg', 'product_pic/b935f01455ced4abd256ed7465b33ffe.jpg', 'product_file/fb66b781fea2500429161cee151e0978.pdf', 'Yes', '', 'idzl513853014335tuq4', 'Inactive', 0, 3, '2013-11-24 21:57:13', '2013-11-28 09:35:40'),
(14, 34, 'Physical', 'Pri 1 Chinese', 'Primary 1', 'English', 'Video', 'Pri 1 Chinese', '1.80', '', '', '2000', 'product_pic/c0116217faf4c54d148b495a91be5f4a.jpg', '', '', 'Yes', '0', 'deiat1385301549kemgq', 'Active', 0, 11, '2013-11-24 21:59:09', '2013-12-02 19:54:01'),
(15, 41, 'Digital', 'TNC Case Studies', 'Junior College', 'Geography', 'Guidebooks', 'A list of 10 TNC case studies (Adidas, Apple, British Petroleum, General Motors, Kraft Foods, Nestle, Nike, Royal Dutch Shell, Samsung Electronics, Toyota) and their global structure, as well as economic, social and environmental impacts.', '5.0', '2', '37', '', 'product_pic/07e5edea157a23c27775f53018e76d85.jpg', 'product_pic/3e198c6edf395a06e8feeb1714c48091.jpg,product_pic/7025840c1a064629422bc642e0555841.jpg', 'product_file/3b92f5bbe151366bd29e6079b76d3f32.pdf', 'Yes', '', 'd9rza1385365295kzs67', 'Inactive', 0, 0, '2013-11-25 15:41:35', '2013-11-25 15:43:19'),
(16, 41, 'Digital', 'Volcanic hazards case studies', 'Junior College', 'Geography', 'Guidebooks', 'A list of 8 volcanic hazards case studies that features effective and ineffective management of eruptions in developed (Mount Eyjafjallajokul, Mount St Helens, Mount Unzen, Mount Vesuvius) and less developed countries (Mount Merapi, Mount Pinatubo, Mount Tambora, Nevado Del Ruiz). Each case study includes the primary, secondary and tertiary effects of the eruption, as well as prediction, mitigation and response measures in place if applicable.', '5.00', '2', '16', '', 'product_pic/05bc43a3527b855d62d2eebb56160b02.jpg', 'product_pic/3ecccc0c01d895a8452dfbc0a1da9974.jpg,product_pic/bf24705425d8a310f0d507fc82038aba.jpg', 'product_file/1fe8b55f045ab200ee51d1a17bb22725.pdf', 'Yes', '', '8njlj138536551495tdt', 'Inactive', 0, 0, '2013-11-25 15:45:14', '2013-11-25 15:45:14'),
(17, 41, 'Digital', 'Earthquake hazards case studies', 'Junior College', 'Geography', 'Guidebooks', 'A list of 8 earthquake hazards case studies that features effective and ineffective management of earthquakes in developed (2010 Chile, 2004 Parkfield in US, 1999 Chi-Chi in Taiwan, and 2011 Tohoku in Japan) and less developed countries (2012 Guerrero-Oaxaca in Mexico, 2010 Haiti, 2004 Indian Ocean in Indonesia, and 1985 Mexico City in Mexico). Each case study includes the tectonic setting of the earthquake, primary, secondary and tertiary effects of the earthquake, as well as prediction, mitigation and response measures in place if applicable.', '5.00', '2', '15', '', 'product_pic/ff18a5db3d0a113385caa3f383c9668d.jpg', 'product_pic/5a04b2004bd5a31e9a629e2395558360.jpg,product_pic/edbbe2994e3aa28b48e028d90607dfa0.jpg', 'product_file/064804c71795bb99a6ba4391669ff9b9.pdf', 'Yes', '', '7jc1d1385365611jk3py', 'Inactive', 1, 0, '2013-11-25 15:46:51', '2013-11-29 11:13:19'),
(18, 44, 'Physical', 'Sevi - Fantasy Colourful Blocks', 'Pre-School', 'Others', 'Educational Kits and Games', 'Blocks', '29.90', '', '', '1.5 kg', 'product_pic/fede0aa92eb628080381f2de3ef8433d.jpg', 'product_pic/11a54c32e4eed7a4ed53250ad0d80a5e.jpg', '', 'Yes', '', 'iiwmi1385369646cb7x2', 'Active', 0, 0, '2013-11-25 16:54:06', '2013-11-25 16:54:06'),
(19, 44, 'Physical', 'Montessori - Parts of a Leaf Puzzle', 'Pre-School', 'Others', 'Educational Kits and Games', 'Montessori Parts of a Leaf Puzzle to learn the different parts of a Leaf:\\nAlso available : Parts of a Flower\\nParts of a Tree\\nParts of a seed', '15.90', '', '', '600g', '', '', '', 'Yes', '', 'r4dxq1385370052evir4', 'Active', 0, 0, '2013-11-25 17:00:52', '2013-11-25 17:00:52'),
(20, 44, 'Physical', 'Montessori Large Moveable Alphabet (LMA)', 'Pre-School', 'English', 'Educational Kits and Games', 'Moveable Alphabet \\nmade of wood \\n5 pcs each of consonants and 10 pcs each of vowels', '125.00', '', '', '1 kg', 'product_pic/3cd541f353199618dfe5653768a1c247.JPG', 'product_pic/0920a8767fe3a16771c80259cd65083e.jpg', '', 'Yes', '', 'pdkc41385370226j9mmr', 'Active', 0, 0, '2013-11-25 17:03:46', '2013-11-25 17:03:46'),
(21, 2, 'Digital', 'A Collection of Nursery Rhymes', 'Pre-School', 'English', 'Assessment Book', 'A Collection of Nursery Rhymes\\n\\nMARY, MARY, QUITE CONTRARY \\nMary, Mary quite contrary, \\nHow does your garden grow? \\nWith silver bells and cockle shells, \\nAnd pretty maids all in a row.', '2', '5', '9', '', 'product_pic/e7de1394a16a6d2eeecc8629a2164446.jpg', 'product_pic/05b094d33ac5a556dd36be3f00cac3ab.jpg', 'product_file/12e5aa0fe28f5f52fd4dc9b0e48cc897.pdf', 'Yes', '', 'zt5xm1385458298cwytf', 'Inactive', 0, 0, '2013-11-26 17:31:38', '2013-11-26 17:31:38'),
(22, 2, 'Digital', 'NURSERY RHYME BOOK', 'Pre-School', 'English', 'Assessment Book', 'Jack Sprat – p.4. \\nMary’s Lamb – Sarah Josepha Hale (1788-1879). pp.5-7. \\nRoses Are Red – p.8. \\nBow-Wow-Wow – p.9. \\nStar Light, Star Bright – p.10. \\nMistress Mary – p.11. \\nDeedle, Deedle, Dumpling – p.12. \\nPussycat, Pussycat – p.13. \\nBaa Baa Black Sheep – p.14. \\nA Cat Came Fiddling Out Of A Barn – p.15. \\nJack And Jill – pp.16-17. \\nHickory Dickory Dock – p.18. \\nLittle Jack Horner – p.19. \\nJack Be Nimble – p.20. \\nCobbler, Cobbler – p.21. \\nThere Was A Crooked Man – p.22. \\nThe King Of France – p.23. \\nHickety Pickety My Black Hen – p.24. \\nSix Little Mice Sat Down To Spin – p.25.', '3', '7', '55', '', 'product_pic/3271863064b7260ba24d28059560cfa4.jpg', 'product_pic/42facbd1252aa1ec0204b18e6da45707.jpg', 'product_file/7e090a3c80a6835b68b3ed063077babe.pdf', 'Yes', '', 'nmhmk1385458864v98ns', 'Inactive', 0, 0, '2013-11-26 17:41:04', '2013-11-26 17:41:04'),
(23, 2, 'Physical', 'COMPUTER P4', 'Pre-School', 'English', 'Educational Kits and Games', 'With its multicultural nature, Singapore has a full calendar of colourful festivals throughout the year, including the Festival of Lights (Deepavali), Chinese New Year celebrations and the Buddhist celebration of Vesak Day.', '20', '', '', '10', 'product_pic/86eb4756efa1fe171fd60a2aeabb52a2.jpg', 'product_pic/b8ebc8afdb9bd2e391b2cc1c04492145.jpg', '', 'Yes', '', 'et7tq1385459508cj1z9', 'Inactive', 0, 0, '2013-11-26 17:51:48', '2013-11-26 17:57:18'),
(25, 2, 'Physical', 'Shaun White Youth Helmet and Pad Combo Set - Size Small/Medium', 'Pre-School', 'English', 'Educational Kits and Games', 'Product Description\\nShaun White Youth Helmet and Pad Combo Set has been built to Shaun\\''s preferred comfort and performance specifications. The protection is CPSC approved and you can\\''t beat the looks.\\nFeatures\\nCPSA Approved/ABS Outer Shell\\nEPS Inner Foam /Comfortable inner pads/Adjustable straps\\nEVA inner foam/Hard plastic cap/Durable nylon covering\\nDurable nylon covering\\nAdditional Info\\n“R”Web#:060827\\nSKU:CF1873E3\\nUPC/EAN/ISBN:864244014861\\nManufacturer #: SHA07048SM\\nProduct Weight:2 pounds\\nProduct Dimensions (in inches):16.0 x 12.0 x 8.0\\nHow to Get It\\nShipping Info:\\nThis item can be shipped to all 50 United States including Alaska & Hawaii\\nThis item is NOT eligible for shipping to APO/FPO addresses, U.S. Territories, Puerto Rico or P.O. Boxes\\nDue to shipping restrictions, Ship Charges will be separate from other items in your order\\nThis item may ship by itself\\n\\nShipping Methods:\\nThis item ships via Standard Shipping only\\n\\nStore Pickup(learn more):\\n\\nThis item is sold online only (not sold in stores)\\nOrders placed for Store Pickup will receive online pricing and promotions\\nIn-stock status is approximate and may not reflect recent sales\\nNot all items are carried at all stores. Please click the \\"Select a store\\" link to check product availability', '39.99', '', '', '2000', 'product_pic/fdffd1ac0e52b324584d2116f8e62d5c.jpg', '', '', 'Yes', '0', 'ucb2e13856998398xw69', 'Inactive', 0, 0, '2013-11-29 12:37:19', '2013-11-29 12:37:19'),
(26, 2, 'Digital', 'Bicycle, Skate and Skateboard Safety Fact Sheet (2013)', 'Pre-School', 'English', 'Guidebooks', 'Fatalities \\n? 112 children ages 19 and under died while riding a bike in 2010.1\\n \\n? 48% of children who died while bike?riding in 2010 were 15?19 years old.1\\n  \\n? 79% of children who died while bike?riding were boys.1\\n  \\n? This is the smallest number of deaths since 1999, and is a 56% reduction  in the number of \\ndeaths since that year and a 59% decrease in the death rate.1', '5', '10', '2', '', 'product_pic/a1cffb5a233c9a61ca036403fc0b06b2.jpg', '', 'product_file/cbea80bb2a7d97b88515789365aa2e7c.pdf', 'Yes', '0', '9nmsj1385699995fkb9v', 'Inactive', 0, 0, '2013-11-29 12:39:55', '2013-11-29 12:39:55'),
(27, 45, 'Physical', 'test', 'Primary 2', 'Science', 'Educational Kits and Games', 'test', '53', '', '', '500', 'product_pic/3f3dcd48df1fb53e746217d3aa12087a.PNG', '', '', 'Yes', '0', 'ej39m1385700703ajl2q', 'Inactive', 0, 0, '2013-11-29 12:51:43', '2013-11-29 12:51:43'),
(28, 45, 'Physical', 'The Young Scientists 2014 (Level 1)', 'Primary 2', 'Science', 'Educational Kits and Games', 'The Young Scientists (Level 1) endeavours to open up the world of Science to curious minds to prepare them for a fascinating journey into the studies of various Sciences in the years to come. The stories are creatively crafted and illustrated with animation of the highest standard in the comic industry.\\n\\nThe Young Scientists magazine is written with reference to the Singapore Primary Level Science Syllabus. The magazine also comes with PSLE format test questions for student to challenge themselves.\\n\\n(i)Price listed is for 10 copies of magazines delivered monthly; issue starts from Jan 2014 (#133) and ends in October 2014 (#142) (ii)Price listed is inclusive of delivery in Singapore only.', '53.00', '', '', '500', 'product_pic/7c4b0b35b36816fca978019624f1cf2a.PNG', '', '', 'Yes', '0', 'qmkhy1385706020vras7', 'Inactive', 0, 0, '2013-11-29 14:20:20', '2013-11-29 14:20:20'),
(29, 34, 'Digital', '??', 'Primary 4', 'Chinese', 'Assessment Book', 'test', '1.00', '1', '200', '', 'product_pic/09fff2da038f70edebb29979afe15cb9.jpg', '', 'product_file/b7dd8b43f5e58bcde803d30cf87c2bef.pdf', 'Yes', '0', '7mafg1385708967mxpwe', 'Inactive', 0, 0, '2013-11-29 15:09:27', '2013-11-29 15:10:11'),
(30, 49, 'Physical', 'The Young Scientists 2014 (Level 1)', 'Primary 1 and 2', 'Science,Others', 'Enrichment', 'The Young Scientists (Level 1) endeavours to open up the world of Science to curious minds to prepare them for a fascinating journey into the studies of various Sciences in the years to come. The stories are creatively crafted and illustrated with animation of the highest standard in the comic industry.\\n\\nThe Young Scientists magazine is written with reference to the Singapore Primary Level Science Syllabus. The magazine also comes with PSLE format test questions for student to challenge themselves.\\n\\n(i)Price listed is for 10 copies of magazines delivered monthly; issue starts from Jan 2014 (#133) and ends in October 2014 (#142) (ii)Price listed is inclusive of delivery in Singapore only.', '53.00', '', '', '500', 'product_pic/4bd8bf27dd494e159fe10b19f4686c89.PNG', '', '', 'Yes', '0', 'r2n9x1385740925whw45', 'Active', 0, 0, '2013-11-30 00:02:05', '2013-11-30 00:02:05'),
(31, 49, 'Physical', 'The Young Scientists 2014 (Level 2)', 'Primary 3 and 4', 'Science,Others', 'Enrichment', 'The Young Scientists (Level 2) strives to reveal the wonder of Science by creating stories in the comic format to assist pupils to understand better the science concepts and contents encompassed in the Lower Block themes of the latest Primary Science syllabus, e.g. Diversity, Cycles, Systems, Energy and Interactions.\\nStories and content for every magazine are designed with relevant graphics and colourful illustrations to help reader relate intelligently to the Science concepts taught in the classroom.\\n\\nThe Young Scientists magazine is written with reference to the Singapore Primary Level Science Syllabus. The magazine also comes with PSLE format test questions for student to challenge themselves.\\n\\n(i)Price listed is for 10 copies of magazines delivered monthly; issue starts from Jan 2014 (#133) and ends in October 2014 (#142) (ii)Price listed is inclusive of delivery in Singapore only.', '53.00', '', '', '500', 'product_pic/09036746530c4b77fe44398fd916ce5d.PNG', '', '', 'Yes', '0', '5ksv21385741254bnqsr', 'Active', 0, 0, '2013-11-30 00:07:34', '2013-11-30 00:07:34'),
(32, 49, 'Physical', 'The Young Scientists 2014 (Level 3)', 'Primary 5 and 6', 'Science,Others', 'Enrichment', 'The Young Scientists (Level 3) explore further the intricacy and mysteries of Science with fascinating articles, challenging questions to enhance development of Science concepts, meaningful experiments to build confidence and interesting Science stories in comic format.\\nAll the above Science contents are designed in compliance with the Upper Block themes of the latest Primary Science Syllabus, e.g. Diversity, Cycles, Systems, Energy and Interactions. Each magazine is designed to supplement the facts and content of the readers’ learning in the classroom.\\n\\nThe Young Scientists magazine is written with reference to the Singapore Primary Level Science Syllabus. The magazine also comes with PSLE format test questions for student to challenge themselves.\\n\\n(i)Price listed is for 10 copies of magazines delivered monthly; issue starts from Jan 2014 (#133) and ends in October 2014 (#142) (ii)Price listed is inclusive of delivery in Singapore only.', '53.00', '', '', '500', 'product_pic/b38fb61a246fa25b49e67409f9ba7789.PNG', '', '', 'Yes', '0', '9g0j21385741443japtj', 'Active', 0, 0, '2013-11-30 00:10:43', '2013-11-30 00:10:43'),
(33, 49, 'Physical', 'The Young Scientists 2014 (Level 4)', 'Primary 6', 'Science,Others', 'Enrichment', 'The Young Scientists Level 4 is specially designed and written for current and graduating Primary 6 students. The topics of the magazine aim to expose readers to more in-depth understanding of the themes of Diversity, Models, Systems and Interactions while maintaining the fun and exciting comic style which is the trademark of The Young Scientists magazine.\\n\\nThe Young Scientists magazine is written with reference to the Singapore Primary Level Science Syllabus. The magazine also comes with PSLE format test questions for student to challenge themselves.\\n\\n(i)Price listed is for 10 copies of magazines delivered monthly; issue starts from Jan 2014 (#01) and ends in October 2014 (#10) (ii)Price listed is inclusive of delivery in Singapore only.', '65.00', '', '', '500', 'product_pic/b38a35458c28cf851f0d4638f2afadf9.png', '', '', 'Yes', '0', '1xije138574159564lkv', 'Active', 0, 0, '2013-11-30 00:13:15', '2013-11-30 00:13:15'),
(34, 49, 'Physical', 'The Young Scientists 2014 (Level 1 and 2 Bundle Pack)', 'Lower Primary', 'Science,Others', 'Enrichment', 'The Young Scientists (Level 1) endeavours to open up the world of Science to curious minds to prepare them for a fascinating journey into the studies of various Sciences in the years to come. The stories are creatively crafted and illustrated with animation of the highest standard in the comic industry.\\n\\nThe Young Scientists (Level 2) strives to reveal the wonder of Science by creating stories in the comic format to assist pupils to understand better the science concepts and contents encompassed in the Lower Block themes of the latest Primary Science syllabus, e.g. Diversity, Cycles, Systems, Energy and Interactions.\\nStories and content for every magazine are designed with relevant graphics and colourful illustrations to help reader relate intelligently to the Science concepts taught in the classroom.\\n\\nThe Young Scientists magazine is written with reference to the Singapore Primary Level Science Syllabus. The magazine also comes with PSLE format test questions for student to challenge themselves.\\n\\n(i)Price listed is for 10 copies of magazines delivered monthly; issue starts from Jan 2014 (#133) and ends in October 2014 (#142) (ii)Price listed is inclusive of delivery in Singapore only. \\n\\nBundle deal!', '101.00', '', '', '500', 'product_pic/96e338f1be861c7e0da2db23cdb897a9.png', '', '', 'Yes', '0', 're4231385773883l7sj3', 'Active', 0, 0, '2013-11-30 09:11:23', '2013-11-30 09:11:23'),
(35, 49, 'Physical', 'The Young Scientists 2014 (Level 2 and 3 Bundle Pack)', 'Upper Primary', 'Science,Others', 'Enrichment', 'The Young Scientists (Level 2) strives to reveal the wonder of Science by creating stories in the comic format to assist pupils to understand better the science concepts and contents encompassed in the Lower Block themes of the latest Primary Science syllabus, e.g. Diversity, Cycles, Systems, Energy and Interactions.\\nStories and content for every magazine are designed with relevant graphics and colourful illustrations to help reader relate intelligently to the Science concepts taught in the classroom.\\n\\nThe Young Scientists (Level 3) explore further the intricacy and mysteries of Science with fascinating articles, challenging questions to enhance development of Science concepts, meaningful experiments to build confidence and interesting Science stories in comic format.\\nAll the above Science contents are designed in compliance with the Upper Block themes of the latest Primary Science Syllabus, e.g. Diversity, Cycles, Systems, Energy and Interactions. Each magazine is designed to supplement the facts and content of the readers’ learning in the classroom.\\n\\nThe Young Scientists magazine is written with reference to the Singapore Primary Level Science Syllabus. The magazine also comes with PSLE format test questions for student to challenge themselves.\\n\\n(i)Price listed is for 10 copies of magazines delivered monthly; issue starts from Jan 2014 (#133) and ends in October 2014 (#142) (ii)Price listed is inclusive of delivery in Singapore only.', '101.00', '', '', '500', 'product_pic/f10aa82d2e9d2a108d94f799ef415b14.png', '', '', 'Yes', '0', '8lrf21385774280ob9e4', 'Active', 0, 0, '2013-11-30 09:18:00', '2013-11-30 09:18:00'),
(36, 49, 'Physical', 'Young Scientists Collection Set 2013 (Level 1)', 'Primary 1 and 2', 'Science', 'Enrichment', 'The Level 1 Collection Set aims to open up the world of Science to curious little minds and prepare both P1 and P2 students for a fascinating journey into the studies of various Science subjects in the Lower Block and Upper Block syllabus in P3 and beyond.nnThe stories and characters are creatively crafted to keep the young ones engaged and interested in the magazines.\\n\\nThe 2013 Collection set comprises 10 magazines from the 2013 edition; Issues#123 to Issue#132. The Collection Set makes a wonderful gift for young curious readers.\\n\\n(i) Collection Set comprises 10 magazines from Issue #123 to Issue #132. (ii) Free shipping to addresses within Singapore only.', '50.00', '', '', '500', 'product_pic/c2ac8c6b9b6659c03a209817c4a89cb9.png', '', '', 'Yes', '0', 'qlk46138577478405s9s', 'Active', 0, 0, '2013-11-30 09:26:24', '2013-11-30 09:28:57'),
(37, 49, 'Physical', 'Young Scientists Collection Set 2013 (Level 2)', 'Primary 3 and 4', 'Science,Others', 'Enrichment', 'The Level 2 Collection Set reveals the wonders of Upper Block Science Syllabus by creating stories in comic format to assist pupils in understanding the Science concepts better. Its compilation brings together all the wonderful stories and characters in 2012. Students will be able to relate to favourite characters such as the Evil One in many of the stories.\\n\\n(i) Collection Set comprises 10 magazines from Issue #123 to Issue #132. (ii) Free shipping to addresses within Singapore only.', '50.00', '', '', '500', 'product_pic/b272ae5144409df4d4d407002714629d.png', '', '', 'Yes', '0', 't2s9e1385775588qi9zk', 'Active', 0, 0, '2013-11-30 09:39:48', '2013-11-30 09:39:48'),
(38, 49, 'Physical', 'Young Scientists Collection Set 2013 (Level 3)', 'Primary 5 and 6', 'Science,Others', 'Enrichment', 'The 2013 Level 3 Collection Set explores further the intricacies and mysteries of Upper Block Science Syllabus with fascinating articles and challenging PSLE format type questions. True to a Young Scientists magazine, the characters and stories are interestingly written to help students grasps more advanced Science concepts.\\n\\n(i) Collection Set comprises 10 magazines from Issue #123 to Issue #132. (ii) Free shipping to addresses within Singapore only.', '50.00', '', '', '500', 'product_pic/0475f8a6d476bb54075db85806a773b2.png', '', '', 'Yes', '0', 'z3jef138577624599up6', 'Active', 0, 0, '2013-11-30 09:50:45', '2013-11-30 09:50:45'),
(39, 49, 'Physical', 'Young Scientists Collection Set 2012 (Level 1)', 'Primary 1 and 2', 'Science,Others', 'Enrichment', 'The Level 1 Collection Set aims to open up the world of Science to curious little minds and prepare both P1 and P2 students for a fascinating journey into the studies of various Science subjects in the Lower Block and Upper Block syllabus in P3 and beyond. The stories and characters are creatively crafted to keep the young ones engaged and interested in the magazines.\\n\\n(i) Collection Set comprises 10 magazines from Issue #113 to Issue #122. (ii) Free shipping to addresses within Singapore only.', '45.00', '', '', '500', 'product_pic/49e32d020ce46f17453c57bca04524d1.jpg', '', '', 'Yes', '0', 'i14681385776451l6pw5', 'Active', 0, 0, '2013-11-30 09:54:11', '2013-11-30 09:54:57'),
(40, 49, 'Physical', 'Young Scientists Collection Set 2012 (Level 2)', 'Primary 3 and 4', 'Science,Others', 'Enrichment', 'The Level 2 Collection Set reveals the wonders of Upper Block Science Syllabus by creating stories in comic format to assist pupils in understanding the Science concepts better. This compilation brings together all the wonderful stories and characters in 2012. Students will be able to relate to favourite characters such as the Evil One in many of the stories.\\n\\n(i) Collection Set comprises 10 magazines from Issue #113 to Issue #122. (ii) Free shipping to addresses within Singapore only.', '45.00', '', '', '500', 'product_pic/ac371971b34b72aec56606be52104759.jpg', '', '', 'Yes', '0', '8hj2c13857766360k2e6', 'Active', 0, 0, '2013-11-30 09:57:16', '2013-11-30 09:57:16'),
(41, 49, 'Physical', 'Young Scientists Collection Set 2012 (Level 3)', 'Primary 5 and 6', 'Science', 'Enrichment', 'The 2012 Level 3 Collection Set explores further the intricacies and mysteries of Upper Block Science Syllabus with fascinating articles and challenging PLSE format type questions. True to a Young Scientists magazine, the characters and stories are interestingly written to help students grasps more advanced Science concepts.nn(i) Collection Set comprises 10 magazines from Issue #113 to Issue #122. (ii) Free shipping to addresses within Singapore only.', '45.00', '', '', '500', 'product_pic/7662b3b37bd77a73b523dea465788f5f.jpg', '', '', 'Yes', '0', 'olw621385776774m2cpw', 'Active', 0, 0, '2013-11-30 09:59:34', '2013-11-30 13:50:39'),
(42, 41, 'Digital', 'TNC Case Studies', 'Junior College', 'Geography', 'Guidebooks', 'A list of 10 TNC case studies (Adidas, Apple, British Petroleum, General Motors, Kraft Foods, Nestle, Nike, Royal Dutch Shell, Samsung Electronics, Toyota) and their global structure, as well as economic, social and environmental impacts.', '5.00', '2', '37', '', 'product_pic/cae9830caee0ecc95ac79d407154d0bc.jpg', 'product_pic/da10064e2ea44ce1b03c7a6a826ae8f5.jpg,product_pic/ea5c1a7faec0a4efbb582715121338e7.jpg', 'product_file/2ad6a9b9003d1d2c0feb1c4dc22fe825.pdf', 'Yes', '0', 's5zkp1385828284k5l2k', 'Active', 0, 0, '2013-12-01 00:18:04', '2013-12-01 00:18:04'),
(43, 41, 'Digital', 'Volcanic hazards case studies', 'Junior College', 'Geography', 'Guidebooks', 'A list of 8 volcanic hazards case studies that features effective and ineffective management of eruptions in developed (Mount Eyjafjallajokul, Mount St Helens, Mount Unzen, Mount Vesuvius) and less developed countries (Mount Merapi, Mount Pinatubo, Mount Tambora, Nevado Del Ruiz). Each case study includes the primary, secondary and tertiary effects of the eruption, as well as prediction, mitigation and response measures in place if applicable.', '5.00', '2', '16', '', 'product_pic/65a5f4daf1e2bc05d33b20e609054617.jpg', 'product_pic/e6a52f3648f73edcad0c22625e6e688f.jpg,product_pic/1f38ee2f5c89bb0f3d4a888ebadd541a.jpg', 'product_file/e8680c19434a4de5460d8c6252d34fa3.pdf', 'Yes', '0', 'i0hnj1385828390fh3lb', 'Active', 0, 0, '2013-12-01 00:19:50', '2013-12-01 00:19:50'),
(44, 41, 'Digital', 'Earthquake hazards case studies.', 'Junior College', 'Geography', 'Guidebooks', 'A list of 8 earthquake hazards case studies that features effective and ineffective management of earthquakes in developed (2010 Chile, 2004 Parkfield in US, 1999 Chi-Chi in Taiwan, and 2011 Tohoku in Japan) and less developed countries (2012 Guerrero-Oaxaca in Mexico, 2010 Haiti, 2004 Indian Ocean in Indonesia, and 1985 Mexico City in Mexico). Each case study includes the tectonic setting of the earthquake, primary, secondary and tertiary effects of the earthquake, as well as prediction, mitigation and response measures in place if applicable.', '5.00', '2', '15', '', 'product_pic/677bc28197f86f59e97df7773638c16a.jpg', 'product_pic/5ff8eb053f1c611ee84d2c7f98ffa202.jpg,product_pic/c12d452c33d215080bcf539a294ba92f.jpg', '', 'Yes', '0', '2zlix138582846718f8b', 'Active', 3, 0, '2013-12-01 00:21:07', '2013-12-02 16:50:21'),
(45, 50, 'Digital', 'sadfasd', 'Lower Primary', 'English', 'Educational Kits and Games', 'asdfsdf', '2', '2', '2', '', '', '', '', 'Yes', '0', 'irkac1385976096n81xd', 'Inactive', 0, 0, '2013-12-02 17:21:36', '2013-12-02 17:21:36'),
(46, 50, 'Digital', 'dfasfasdfas', 'Lower Primary', 'English', 'Educational Kits and Games', 'sdfasdf', '34', '2', '2', '', '', '', '', 'Yes', '0', '7g2yk1385976759n327b', 'Inactive', 0, 0, '2013-12-02 17:32:39', '2013-12-02 17:32:39'),
(47, 50, 'Digital', 'asdfadsf', 'Pre-School', 'English', 'Educational Kits and Games', 'asdf asdf asdf', '1', '2', '2', '', '', '', '', 'Yes', '0', 'xybhe1385977010i1m5x', 'Inactive', 0, 0, '2013-12-02 17:36:50', '2013-12-02 17:36:50'),
(48, 50, 'Physical', 'asdfadsf', 'Pre-School', 'English', 'Guidebooks', 'asdf asdf asdf asdf', '2', '', '', '12', '', '', '', 'Yes', '0', 'tgqrb138597750913gbl', 'Inactive', 0, 0, '2013-12-02 17:45:09', '2013-12-02 17:45:09'),
(49, 50, 'Digital', 'afa sfasd fasd', 'Lower Primary', 'English', 'Enrichment', 'as fasdf asdf', '2', '2', '2', '', '', '', 'product_file/6b1cf4728787092b3006c44b17061cf9.jpg', 'Yes', '0', 'mzgm61385977717e0aik', 'Inactive', 1, 0, '2013-12-02 17:48:37', '2013-12-02 17:48:45'),
(50, 53, 'Digital', 'Chinese Assesment Book', 'Primary 1', 'Chinese', 'Assessment Book', 'Description', '10', '7', '100', '', '', '', 'product_file/6c452aac1482021a9348593fbc72d2e3.pdf', 'Yes', '0', 'f10fc1386145948s7vyu', 'Inactive', 0, 0, '2013-12-04 16:32:28', '2013-12-04 16:32:28'),
(51, 2, 'Digital', 'Product Digital Test', 'Pre-School', 'English', 'Assessment Book', 'Test product digital', '2', '2', '2', '', 'product_pic/4e3cfac6cf1cf21a375865f8297b0b5a.jpg', '', 'product_file/4ad3d73b16506e85f97bbd2125c62006.jpg', 'Yes', '0', '2oz1x13866746678aao7', 'Inactive', 1, 4, '2013-12-10 19:24:27', '2013-12-10 20:31:49'),
(52, 2, 'Physical', 'Product Physical Test', 'Junior College', 'English', 'Enrichment', 'Product Physical Test', '3', '', '', '200', 'product_pic/f06af0bca4e4c34deadce9690c64dc11.jpg', '', '', 'Yes', '0', '10l2i1386674742kravr', 'Inactive', 0, 9, '2013-12-10 19:25:42', '2013-12-11 12:32:50'),
(53, 37, 'Physical', 'Physical 1', 'Lower Primary', 'Malay', 'Assessment Book', 'Quick Test', '0.10', '', '', '100', 'product_pic/aaca126c0f1472a0f2a1d3577f2a6670.jpg', '', '', 'Yes', '0', '2u0lk13869040025yirk', 'Inactive', 0, 1, '2013-12-13 11:06:42', '2013-12-13 11:16:24'),
(54, 36, 'Digital', 'Digital 1', 'Upper Primary', 'Chinese', 'Assessment Book', 'Quick Test', '0.10', '1', '1', '', 'product_pic/d00acc5825830a8073a01c86b9481d9f.jpg', '', 'product_file/18cf71e375b651f2664501788d06c1f7.docx', 'Yes', '0', '4qr3s138690410766ufv', 'Inactive', 1, 1, '2013-12-13 11:08:27', '2013-12-13 11:16:46'),
(55, 60, 'Physical', '0101 Shape Bean Bag', 'Pre-School', 'Others', 'Educational Kits and Games', 'Learning Benefits:\\n\\nStay Active\\nGross motor skills are enhanced by learning to reach, throw, catch and balance the bean bags.\\n\\nFeel it\\nLearn shapes and colors with this manipulative learning resource.\\n\\nFeel and count the sides\\n\\n•Material: Felt\\n•Stitch: Blanket stitch\\n\\n•Price: $20 for 4 shapes\\n\\n•Customise shapes (eg hexagon, pentagon) are available\\n•Random colour will be given.\\n•FREE Bean Bag Activities Parent Instruction\\n\\nInclusive of free normal mail. We will not be liable for damage or lost parcel. Top up of $2.40 is highly recommended for registered mail.', '20', '', '', '-', 'product_pic/07764b73b1ff46caa4cff3273bdbfd47.png', 'product_pic/02d7e933f8ef31b05c5c74b256fe004f.png', '', 'Yes', '0', '1p12x1387176001vknwh', 'Active', 0, 0, '2013-12-16 14:40:01', '2013-12-16 14:46:44'),
(56, 60, 'Physical', '0102 Bean Bag - Learn my Name', 'Pre-School', 'Others', 'Educational Kits and Games', 'Bean bag activities develops gross motor skills and hand eye coordination. Bean bag throw is a fun play and teach your child to control her arm strength.\\n\\nIn addition, your child can learn his/her name with GoM Bean bag - Learn my name.\\n\\n Felt letters are machine sewn to provided sensory tactile learning of letters.\\n\\n Inner sides are double-lined sewn for durability.\\n\\nA great novelty gifts for children!\\n\\nParent instruction and suggestion on bean bag activities is included.\\n\\nPrice: $10 per letter bean bag\\n\\nEach bean bag is 4.5 x 4.5 inch.', '10', '', '', '-', 'product_pic/b86abc12b349d3dd6838781e3f9b29c6.jpg', 'product_pic/19a3ce75df0e104e7a4b9333aced5277.jpg', '', 'Yes', '0', '6ycaz1387178648b71rq', 'Active', 0, 0, '2013-12-16 15:24:08', '2013-12-16 15:24:08');

-- --------------------------------------------------------

--
-- Table structure for table `promos`
--

CREATE TABLE IF NOT EXISTS `promos` (
  `promo_id` int(11) NOT NULL AUTO_INCREMENT,
  `promo_type` varchar(20) NOT NULL,
  `promo_min_amount` varchar(10) NOT NULL DEFAULT '0',
  `promo_code` varchar(50) DEFAULT NULL,
  `promo_start` date NOT NULL,
  `promo_end` date NOT NULL,
  `promo_percent` varchar(10) NOT NULL,
  `promo_percent_type` varchar(5) NOT NULL,
  `product_title` varchar(200) DEFAULT NULL,
  `promo_category` varchar(50) DEFAULT NULL,
  `promo_users` int(11) NOT NULL DEFAULT '0',
  `promo_used_count` int(11) NOT NULL DEFAULT '0',
  `promo_status` varchar(20) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`promo_id`),
  UNIQUE KEY `promo_code` (`promo_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `promos`
--

INSERT INTO `promos` (`promo_id`, `promo_type`, `promo_min_amount`, `promo_code`, `promo_start`, `promo_end`, `promo_percent`, `promo_percent_type`, `product_title`, `promo_category`, `promo_users`, `promo_used_count`, `promo_status`, `created`, `modified`) VALUES
(1, 'On Cart Amount', '1', 'code10', '2013-12-09', '2013-12-11', '10', '%', NULL, NULL, 2, 1, 'Expired', '2013-12-10 19:34:12', '2013-12-10 19:36:11'),
(2, 'On Product Amount', '0', NULL, '2013-12-09', '2013-12-11', '15', '%', 'Product Physical Test', '', 3, 2, 'Expired', '2013-12-10 19:47:25', '2013-12-10 20:31:49'),
(3, 'On Cart Amount', '5.50', '5OFFCHECKOUT', '2013-12-13', '2014-01-31', '5', 'S$', '', NULL, 100, 0, 'Active', '2013-12-13 10:54:08', '2013-12-17 18:07:05');

-- --------------------------------------------------------

--
-- Table structure for table `promo_used`
--

CREATE TABLE IF NOT EXISTS `promo_used` (
  `puid` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `discount_values` varchar(10) NOT NULL,
  `promo_code` varchar(20) DEFAULT NULL,
  `promo_for` varchar(100) NOT NULL,
  `username` varchar(200) NOT NULL,
  `order_no` varchar(30) NOT NULL,
  `date_used` date NOT NULL,
  `original_price` varchar(20) NOT NULL DEFAULT '0',
  `purchased_amount` varchar(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`puid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `promo_used`
--

INSERT INTO `promo_used` (`puid`, `start_date`, `end_date`, `discount_values`, `promo_code`, `promo_for`, `username`, `order_no`, `date_used`, `original_price`, `purchased_amount`) VALUES
(1, '2013-12-09', '2013-12-11', '% 10', 'code10', 'On Cart Amount', 'ravikanth_k22', '1386676262', '2013-12-10', '5.00', '4.50'),
(2, '2013-12-09', '2013-12-11', '% 15', NULL, 'Product Physical Test', 'parikshat', '1386677383', '2013-12-10', '2.55', '2.55'),
(3, '2013-12-09', '2013-12-11', '% 15', NULL, 'Product Physical Test', 'parikshat', '1386679623', '2013-12-10', '4.55', '4.55'),
(4, '2013-12-13', '2013-12-14', 'S$ 0.10', 'testpromo', 'On Cart Amount', 'huifentest', '1386905426', '2013-12-13', '0.2', '0.1');

-- --------------------------------------------------------

--
-- Table structure for table `qna`
--

CREATE TABLE IF NOT EXISTS `qna` (
  `qna_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_unique` varchar(20) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `qna_body` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`qna_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `qna`
--

INSERT INTO `qna` (`qna_id`, `user_id`, `product_unique`, `parent_id`, `qna_body`, `created`, `modified`) VALUES
(1, 31, 'r9nxb1382456199meofo', NULL, 'test question', '2013-10-22 23:49:48', '2013-10-22 23:49:48'),
(2, 32, 'r9nxb1382456199meofo', 1, 'reply.', '2013-10-22 23:59:06', '2013-10-22 23:59:06'),
(3, 34, 'hw5141383060321zm5g8', NULL, 'test test', '2013-10-30 18:10:19', '2013-10-30 18:10:19'),
(4, 34, 'hw5141383060321zm5g8', 3, 'test test', '2013-10-30 18:11:11', '2013-10-30 18:11:11'),
(5, 40, '2zlix138582846718f8b', NULL, 'Please provide some more details of this product.', '2013-12-02 17:13:31', '2013-12-02 17:13:31');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `product_unique` varchar(20) NOT NULL,
  `review` text NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`review_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`review_id`, `user_id`, `to_user_id`, `product_unique`, `review`, `rate`, `created`, `modified`) VALUES
(2, 36, 34, 'jyvxm1383729313nnsvh', 'Test test', 3, '2013-12-01 15:12:56', '2013-12-01 15:12:56');

-- --------------------------------------------------------

--
-- Table structure for table `site`
--

CREATE TABLE IF NOT EXISTS `site` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `margin` varchar(20) NOT NULL,
  `trans_fee` varchar(20) NOT NULL,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `site`
--

INSERT INTO `site` (`site_id`, `email`, `phone`, `margin`, `trans_fee`) VALUES
(1, 'contact@openschoolbag.com.sg', '+65 8264-4750', '15', '0.50');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_fname` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_phone` varchar(15) DEFAULT NULL,
  `user_username` varchar(200) NOT NULL,
  `user_password` text NOT NULL,
  `user_seller` varchar(5) DEFAULT NULL,
  `user_level` int(11) NOT NULL,
  `user_status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_email` (`user_email`),
  UNIQUE KEY `user_username` (`user_username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_fname`, `user_email`, `user_phone`, `user_username`, `user_password`, `user_seller`, `user_level`, `user_status`, `created`, `modified`) VALUES
(1, 'Admin admin', 'contact@openschoolbag.com.sg', '82644750', 'admin', 'c484731969b767820934f6fb74a803f05b382e4b5b10383f6819a5374fcda0fb8d0d7e14f9cf6ef8f76d406669c081821fc6f32d068e98b3f831b163c8a0f84b', 'Yes', 9, 1, '2013-10-18 15:16:48', '2013-11-06 18:21:15'),
(2, 'Ravikanth Katkam', 'ravikanth@dotspiders.com', '12345678', 'ravikanth', 'f4e66775cdf277787fa2efd10a39dc6b9326eeaa71d04e2b9e769c6ea58a2593b9260f1ac74e6cc430b71643ae945132bd4d605a6f54681130e448380386f79e', 'Yes', 2, 1, '2013-10-18 15:19:00', '2013-10-21 23:00:34'),
(3, 'Rajini Kanth', 'ravikanth2@dotspiders.com', '98765432', 'rajinikanth', '5d3dbb2f6a6ad8fb0def5bc8ad9283854ddf158d23350c4a29a368e0e11e1ecebb7145fc48a30fdb4391b331db12572a910f6092a498428843e0b8a154cbb606', 'Yes', 2, 1, '2013-10-18 15:22:53', '2013-10-23 12:33:38'),
(6, 'Chandra G', 'gpmouli@gmail.com', '91054019', 'dotspiders205', '7b0d080d5ff538e03ebe469ef1ce6986d3f09fde9d2276af5c5a1a1bb1d4518af9a58466eb320bc26dc3bf07408062f2c783b5f476caf111250cc91809f08130', 'No', 1, 0, '2013-10-21 22:59:28', '2013-10-21 22:59:28'),
(7, 'Chandra G', 'chandra@dotspiders.com', '65095001', 'dotspiders206', '364bca1743b0c774f92e7d80d8108bcb1288d52123a386f17f37573d63894693c20d467be5ed3d76816f9978f8bd6fe8fd4c72428d5fe34501b7b0b04efd69ff', 'No', 1, 1, '2013-10-21 23:03:07', '2013-10-22 13:00:55'),
(9, 'gillian ong', 'gillian_ong@ymail.com', '96672273', 'gillohp', 'd0cd06eb3612da094d22f74917b78c0547b54338a2466dff1809e5147bef90080efa7b00344e07e8e6f3b29a934d627992b4559b7489f3a814ea1d989c10ccc8', 'Yes', 2, 1, '2013-10-21 23:07:54', '2013-10-31 15:41:19'),
(10, 'Kanth Ravi', 'katkamravikanth2@gmail.com', '98765432', 'katkam', '5d3dbb2f6a6ad8fb0def5bc8ad9283854ddf158d23350c4a29a368e0e11e1ecebb7145fc48a30fdb4391b331db12572a910f6092a498428843e0b8a154cbb606', 'No', 1, 0, '2013-10-22 12:17:05', '2013-10-22 12:17:05'),
(22, 'raj kumar', 'ashish@dotspiders.com', '98765432', 'rajkumar', '0dd3e512642c97ca3f747f9a76e374fbda73f9292823c0313be9d78add7cdd8f72235af0c553dd26797e78e1854edee0ae002f8aba074b066dfce1af114e32f8', 'No', 1, 1, '2013-10-22 13:30:37', '2013-10-22 13:30:37'),
(25, 'ashish chandra', 'ashraj_it@yahoo.com', '876543200', 'ashish', '0dd3e512642c97ca3f747f9a76e374fbda73f9292823c0313be9d78add7cdd8f72235af0c553dd26797e78e1854edee0ae002f8aba074b066dfce1af114e32f8', 'No', 1, 1, '2013-10-22 13:41:33', '2013-10-24 20:22:31'),
(28, 'ashish1 ch', 'ravikanth_22@yahoo.co.in', '89456792', 'ashish1', '0dd3e512642c97ca3f747f9a76e374fbda73f9292823c0313be9d78add7cdd8f72235af0c553dd26797e78e1854edee0ae002f8aba074b066dfce1af114e32f8', 'Yes', 2, 1, '2013-10-22 14:08:55', '2013-10-24 18:19:09'),
(31, 'Huijuan ong', 'huijuan@openschoolbag.com.sg', '12345679', 'onghuijuan', '0f97010f4c8456addb3e57e8c34af061f3d7dd49ae335f01246f00fded7e10ea029ba474eab8fd37d0b224398a66c50096f8765e50b6feaabac7bb99a6866781', 'Yes', 2, 1, '2013-10-22 23:02:08', '2013-10-29 17:08:38'),
(32, 'hui juan hotmail', 'ar_bo_208@hotmail.com', '12345678', 'huijuan646', '0f97010f4c8456addb3e57e8c34af061f3d7dd49ae335f01246f00fded7e10ea029ba474eab8fd37d0b224398a66c50096f8765e50b6feaabac7bb99a6866781', 'Yes', 2, 1, '2013-10-22 23:32:15', '2013-10-22 23:32:39'),
(34, 'gillian ong', 'gillian@openschoolbag.com.sg', '91234567', 'gillian', 'd0cd06eb3612da094d22f74917b78c0547b54338a2466dff1809e5147bef90080efa7b00344e07e8e6f3b29a934d627992b4559b7489f3a814ea1d989c10ccc8', 'Yes', 2, 1, '2013-10-24 21:33:41', '2013-10-24 21:35:00'),
(36, 'Huifen Ong', 'huifen@openschoolbag.com.sg', '97805945', 'huifenong', '1179850afa4cccaa179abadb9792cd4020717c629a3c3ccf15de8634b812ff474423ac902b3263fa94f57807bcac35c3685dd8d276ac8cc952ab2688437f3029', 'Yes', 2, 1, '2013-10-29 23:42:02', '2013-10-29 23:43:32'),
(37, 'Huifen Ong', 'huifenong@hotmail.com', '97805945', 'huifen', '1179850afa4cccaa179abadb9792cd4020717c629a3c3ccf15de8634b812ff474423ac902b3263fa94f57807bcac35c3685dd8d276ac8cc952ab2688437f3029', 'Yes', 2, 1, '2013-10-30 21:40:10', '2013-10-31 00:03:46'),
(38, 'Hon Sing Lee', 'honsing2@gmail.com', '81216326', 'Honsing', '47e3b3cc7f2a88368d5518db183b479073424a7d6700c6e0d63aa49987fa5f3c296f877bdf00b906bedd1f03fe95dd8861e54f89e635b8f4b37a3b1c046b9aa4', 'No', 1, 1, '2013-11-08 07:40:42', '2013-11-08 07:41:06'),
(40, 'Parikshat Anand', 'parikshat@dotspiders.com', '73852360', 'parikshat', 'd9cc75ea3c205a1ce2c5a25434b1a167a54e465ad1461952f7e30f499512fc6716b8a700f19d8da9562dd48506a5a029a6a50dfd01cb98336dc1dffd25ac98ed', 'Yes', 2, 1, '2013-11-13 12:45:35', '2013-12-10 19:52:54'),
(41, 'Stacy Wong', 'wongxinying@yahoo.com.sg', '91520481', 'caughtingeog', '56579e1cc260d29b16411e69aa96b3fe70de03ef169b7834fb936f413892989f194e64dc99b92a9a6619e24588c5bbc43044e245f989c417529471ff2c8b164d', 'Yes', 2, 1, '2013-11-13 15:44:18', '2013-11-13 15:44:43'),
(42, 'IRFAN KHAN', 'irfan.izk@gmail.com', '75870294', 'MATH PROBLEMS SOLVED', '12221b73efe55211824826cb613bdac52ab6cbee42fb271526003e82f0d9953e9ce9031124845e5fa7795a7f026d15cf0941544849d1117e53e115124fbbe5e0', 'Yes', 2, 1, '2013-11-15 00:31:13', '2013-12-04 22:55:21'),
(43, 'Julie Teo', 'Youknowitall@gmail.com', '81839477', 'JulieTeo', 'a7f6110c7be9f96a4538c4c3740c56e6ba4cc18e9a725243742091836af658b61ef236b26d866d77011bd1ae254904f67dae512ac770ac2ef3aac4d0bfe9eb6e', 'No', 1, 1, '2013-11-25 12:08:05', '2013-11-29 10:57:28'),
(44, 'Nafisa from Enrighten', 'info@enrighten.com', '97591452', 'Right to Learn', '619361811b9a03aa0ff9db8cc00c8cf1e4b054ec6286c452cdbf642f764860651ba58027406679fc7034e41be10aa0ac06627d612b2007f6a4951ee6777b92a1', 'Yes', 2, 1, '2013-11-25 16:12:36', '2013-11-25 16:19:37'),
(48, 'Michelle from SAP', 'michelleyoo@sapgrp.com', '62768280', 'Singapore Asia Publishers Pte Ltd', '8c395da8bb809c2f4a9809ea0ac92cf53ac25d6d43cec055d8e7b8ad4e71b2cb3cd9143728124ca8dbdedaf5ee0e27129edbdda73a35b401628f86cc441d3166', 'Yes', 3, 1, '2013-11-29 18:54:55', '2013-11-29 19:53:57'),
(49, 'Chung Weng', 'yucw@youngscientistsreader.com.sg', '91074190', 'Young Scientists Reader', '9bba40b5f1221251e7ddc643a78928e3abf76f72f02491969304a96db76d0e908dce02e2b57d50305c2faa7a1cad227465be6a6ade55afc02415035383c6daab', 'Yes', 3, 1, '2013-11-29 19:15:09', '2013-11-29 19:49:57'),
(50, 'Ravikanth', 'ravikanth_k22@yahoo.coin', NULL, 'ravikanthk22', 'aef4e0b4dec7945499471d56b4e8e2d590491e4902abd099d689f3d1acb7464e4dcbc4ac6bd9e5181f0c516be796d69b06ee7e1465040e264cab2bbabbc5dfbe', 'Yes', 3, 1, '2013-12-02 17:05:20', '2013-12-02 17:09:36'),
(51, 'Eunice Loo', 'looeunice@yahoo.com.sg', '98346452', 'looeunice', 'db85920be9d282d1c15188516d6e3dd3ad71e268f7bf156ee6e2a8a8bf74fbb0cfa29c3561825f0b13bac7784d09d151de827b9a52afb31f1584001e6fea32c1', 'No', 1, 1, '2013-12-03 21:36:41', '2013-12-03 21:37:16'),
(53, 'Ethan Cheong', 'ethan.cheong@webcada.com', '96653235', 'ethancheong', '27833c11ec4a4610058d1ac588eb604f4b06de4ecb9e09fc77ac5fffd140516d8270b0292837d6afd615d2b53edcf9996a2b7d1a8f1950ad253e2ce08282fbe7', 'Yes', 2, 1, '2013-12-04 16:24:58', '2013-12-04 16:25:34'),
(54, 'Swee Eng Ong', 'osweeeng@gmail.com', '97460676', 'osweeeng', '7ff89908133f1aa0698b062821b077d1f670d0c2e15c6909e19c11b5a33dc89f72260b7f2fe66048843fc4c3aeedd70bd9b09c1cda33540ece1187990753eb9d', 'No', 1, 1, '2013-12-08 10:00:31', '2013-12-08 10:00:46'),
(55, 'Ravikanth K', 'ravikanth_k22@yahoo.co.in', '12345678', 'ravikanth_k22', '5d3dbb2f6a6ad8fb0def5bc8ad9283854ddf158d23350c4a29a368e0e11e1ecebb7145fc48a30fdb4391b331db12572a910f6092a498428843e0b8a154cbb606', 'No', 1, 1, '2013-12-10 19:27:21', '2013-12-10 19:27:51'),
(56, 'Eryn Wu', 'wu.eryn@gmail.com', '91116050', 'w_eryn', '5df9d18c2be6ebc4e84801b71b065fcc63b721c097370ce0dc8ca5f716b36956266d06913514274b8bc9ff4d88ce29112f82802d838c0293b910944a25752dd4', 'No', 1, 1, '2013-12-11 12:23:57', '2013-12-11 12:25:02'),
(57, 'Edison Koo', 'Edison@txc.com.sg', '97398800', 'Edison', 'efe8f2d1270be9ce41163adeb92e79f66c4387fbf323541fbc5d763c15f059b24766b9cd6604446a1d8f0858960fb01632c13891e237922a56232f7db54371bd', 'No', 1, 1, '2013-12-12 02:36:27', '2013-12-12 02:37:38'),
(58, 'Huifen Ong', 'huifenong00@gmail.com', '97805945', 'huifentest', '1179850afa4cccaa179abadb9792cd4020717c629a3c3ccf15de8634b812ff474423ac902b3263fa94f57807bcac35c3685dd8d276ac8cc952ab2688437f3029', 'No', 1, 1, '2013-12-13 11:12:31', '2013-12-13 11:13:17'),
(59, 'reny reny', 'js.tham.2010@gmail.com', '12345678', 'renyreny', '631a1c3786ac6a1098d53d9a651c42004a36f8d6f02e2569fc43e1ab34e0d97d4fe72f10b5344640afdbc173ad938a9ca1b3a7da58c0804cfb067c3315c12ef3', 'Yes', 2, 1, '2013-12-13 14:00:02', '2013-12-13 14:00:17'),
(60, 'Candy Luo', 'giftsofmontessori@gmail.com', '97805428', 'Gifts of Montessori', '9607d6ce755432c45c9d2d149f9a2129247d4d6e92bfc2db1fca760868774a51620a9265f66eb397dfed7dd3ad255e1b705e30c16b9b07f04dbc5d1fdee25415', 'Yes', 2, 1, '2013-12-16 10:27:58', '2013-12-16 10:28:40'),
(61, 'TingTing Xiao', 'xiaott2168@gmail.com', 'scy6128v', 'xiaott2168', '2b6c7917e6514a91ceef0fbad4d4e90cc5008fda916782c5df450db309c532591ab65c2bcbc5dd32ae132dccc3d4276a3e0346af0d32277314a6b2771d1bf51b', 'Yes', 2, 1, '2013-12-18 09:14:53', '2013-12-18 09:15:14');

-- --------------------------------------------------------

--
-- Table structure for table `user_activation`
--

CREATE TABLE IF NOT EXISTS `user_activation` (
  `active_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `active_code` varchar(250) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`active_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `user_activation`
--

INSERT INTO `user_activation` (`active_id`, `user_id`, `active_code`, `created`, `modified`) VALUES
(1, 1, '7109980520192023a7bbd73250516f069df18b5001891080113', '2013-10-18 15:16:48', '2013-10-18 15:16:48'),
(2, 2, '955240019a309aeb455936fb533b0e7763b69d00e61091939', '2013-10-18 15:19:00', '2013-10-18 15:19:00'),
(3, 3, '1289592554af3de8a92893e00c5c48199422868df6733307696', '2013-10-18 15:22:53', '2013-10-18 15:22:53'),
(4, 5, '180125594095df79e2448d245fdcbf4668b9489c1748609850', '2013-10-19 10:47:08', '2013-10-19 10:47:08'),
(5, 6, '854729782c1877e3fb8f44aa9454cecf1a2f9b4f1563471537', '2013-10-21 22:59:28', '2013-10-21 22:59:28'),
(6, 7, '1080634476dbb896bdc1f6ecaca7d583ecc906cb8816934732', '2013-10-21 23:03:07', '2013-10-21 23:03:07'),
(7, 8, '1921926110aadff0a8bd04d68fb6a7d54505515f81341890176', '2013-10-21 23:03:52', '2013-10-21 23:03:52'),
(8, 9, '1188724247cbe5701c813ae0df821f5a7c4285829f1854642569', '2013-10-21 23:07:54', '2013-10-21 23:07:54'),
(9, 10, '231196092c5df6d39c3e8972397e5faf853efa1c61182255252', '2013-10-22 12:17:05', '2013-10-22 12:17:05'),
(10, 11, '91181153832225ff118a7404df5d55e5ed9292cc81958137649', '2013-10-22 12:35:43', '2013-10-22 12:35:43'),
(11, 13, '17576814447ebc2c8aa51f075ccc653a0f8e86fbb42131833950', '2013-10-22 12:36:13', '2013-10-22 12:36:13'),
(12, 21, '9718891487b69ad8a8999d4ca7c42b8a729fb0ffd1507101031', '2013-10-22 12:39:27', '2013-10-22 12:39:27'),
(13, 22, '204356862c223199626bf0875cbc4e5859c93040c1787978516', '2013-10-22 13:30:37', '2013-10-22 13:30:37'),
(14, 25, '1560596537b69ad8a8999d4ca7c42b8a729fb0ffd1866811786', '2013-10-22 13:41:33', '2013-10-22 13:41:33'),
(15, 28, '728367167137399abd34fa30e9eec25a3602ce4bf1746404155', '2013-10-22 14:08:55', '2013-10-22 14:08:55'),
(16, 29, '42528814805ad02b13192aa1d6f072088acf366a9823661275', '2013-10-22 16:48:34', '2013-10-22 16:48:34'),
(17, 31, '1879342291b2e81fca446a166a2c69d67b9a64e480930768088', '2013-10-22 23:02:08', '2013-10-22 23:02:08'),
(18, 32, '6415878588d0dc576ddc6296b64ca306a75ff8022675794202', '2013-10-22 23:32:15', '2013-10-22 23:32:15'),
(19, 34, '347096355932eba7c32d30568294a283fd11403021801153228', '2013-10-24 21:33:41', '2013-10-24 21:33:41'),
(20, 36, '20947037394095df79e2448d245fdcbf4668b9489c941364182', '2013-10-29 23:42:02', '2013-10-29 23:42:02'),
(21, 37, '654722553d11aa4fd62f2a74522980e9392f605761622858971', '2013-10-30 21:40:10', '2013-10-30 21:40:10'),
(22, 38, '404931522e99efe9c6feb1d1e01652b8046e2a6e025808897', '2013-11-08 07:40:42', '2013-11-08 07:40:42'),
(23, 40, '170411234601c7eaded9a9619b0e355072116d05f4548868279', '2013-11-13 12:45:35', '2013-11-13 12:45:35'),
(24, 41, '1518481269d05e7e85fae93816c09368083c5ed57f106969214', '2013-11-13 15:44:18', '2013-11-13 15:44:18'),
(25, 42, '7429123809e1fc252604e2bf83c80013cef6c5f4a1190273229', '2013-11-15 00:31:13', '2013-11-15 00:31:13'),
(26, 43, '616983163c2c398a955ce88ac76bb110c977cc109368565360', '2013-11-25 12:08:05', '2013-11-25 12:08:05'),
(27, 44, '1120745835637fe5256d242747cd60b21cd3f18a301768324221', '2013-11-25 16:12:36', '2013-11-25 16:12:36'),
(28, 45, '1218780245c1be3e1c243e7beac0edc6d3cfcd12c71226641996', '2013-11-25 17:42:54', '2013-11-25 17:42:54'),
(29, 46, '16671465159e7a00704c395176467e88f1e52346381520298165', '2013-11-29 16:25:35', '2013-11-29 16:25:35'),
(30, 51, '1533306272593d78dae35cfcce81411b80f35c22cf117566414', '2013-12-03 21:36:41', '2013-12-03 21:36:41'),
(31, 53, '431770480695a77542ac9bd2369dc0b1561469b6d882012574', '2013-12-04 16:24:58', '2013-12-04 16:24:58'),
(32, 54, '16387586724682f0496aef3774f96dcff450827160779684233', '2013-12-08 10:00:31', '2013-12-08 10:00:31'),
(33, 55, '9589779370aadff0a8bd04d68fb6a7d54505515f81027239225', '2013-12-10 19:27:21', '2013-12-10 19:27:21'),
(34, 56, '19558663466851b285144a5cadccb1e16dedef2ea6612227916', '2013-12-11 12:23:57', '2013-12-11 12:23:57'),
(35, 57, '497977858a849517dbce9aef06e07a4fb997b578e484775680', '2013-12-12 02:36:27', '2013-12-12 02:36:27'),
(36, 58, '970951894bf48ba4ac1691bb0ba52af71b92e02f11498292063', '2013-12-13 11:12:31', '2013-12-13 11:12:31'),
(37, 59, '1659757828a103a16786439cd24ffa58955a5518f8235272756', '2013-12-13 14:00:02', '2013-12-13 14:00:02'),
(38, 60, '63210383359adf8d69d4a3ffccffe4783ccddc6f1210252603', '2013-12-16 10:27:58', '2013-12-16 10:27:58'),
(39, 61, '1457376342f75b3a32c3499b5aa586f6c1edc56a2937340299', '2013-12-18 09:14:53', '2013-12-18 09:14:53');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE IF NOT EXISTS `user_profile` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `profile_pic` varchar(100) DEFAULT NULL,
  `profile_qualifications` text,
  `profile_total_exp` text,
  `profile_special` text,
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`profile_id`, `user_id`, `profile_pic`, `profile_qualifications`, `profile_total_exp`, `profile_special`) VALUES
(1, 32, 'profile/77e1beff3437e03e0d25c3738ab237b7.jpg', '<p>Diploma in Education, NIE</p>\n<p>Honours in Science, NUS</p>', '<ul>\n<li>6 years in MOE school</li>\n<li>15 years in curriculum planning in tuition centre</li>\n</ul>', '<ul>\n<li>Design practice papers close to syllabus</li>\n<li>Regular tests on concept understanding</li>\n</ul>'),
(2, 25, 'profile/7dff247f432ed27648384a52d50ae88d.jpg', NULL, NULL, NULL),
(3, 41, 'profile/882789d082f646f998933b965f7a716a.gif', '<p>Bachelor of Arts in Environmental Studies (Magna cum laude) from New York University</p>\n<p>Masters of Science in Population and Development from London School of Economics and Political Science</p>', '<p>4 years part-time tutoring experience</p>\n<p>Founder of Caughtingeog, an online sharing platform for A Level Geography materials: http://caughtingeog.wordpress.com/&nbsp;</p>', '<p>A Level Geography</p>'),
(4, 42, 'profile/9f4bc8b938306af1c88da2afb3ea514f.png', '<p>MATHEMATICS FIRST CLASS HONOURS - University of Manchester</p>\n<p>Certificate of Advance Mathematics - University of Cambridge</p>\n<p>&nbsp;</p>', '<p>I have been teaching Advance level Mathematics and Calculus for 11 years at a leading school in the UK. The materials i am offering will give you the best possible guidance and preparation for a Secondary Mathematics exam.</p>', '<p>I will be offering materials in:</p>\n<p>ALGEBRA</p>\n<p>GEOMETRY AND TRIGONOMETRY</p>\n<p>PROBABILITY AND STATISTICS</p>\n<p>NUMBER</p>\n<p>Including videos and solutions to hundreds of math problems with examiners'' mark schemes and comments. &nbsp;</p>'),
(5, 44, 'profile/9546a80be430ce93c1d877deab124a47.png', NULL, NULL, NULL),
(6, 40, 'profile/5168dc6d248dde4680ab84f62e7470cf.jpg', '<p>BE</p>', '<p>1 Year</p>', '<p>ABC</p>'),
(8, 49, 'profile/99fc4f9239830a6fefc9fc660ba7eb29.jpg', '<p><strong><span style="text-decoration: underline;"><span style="font-size: 8.5pt; font-family: ''Verdana'',''sans-serif''; mso-bidi-font-family: Arial;">Mrs Linda Anne Gwyneth Gan.</span></span></strong><span class="apple-converted-space"><span style="font-size: 8.5pt; font-family: ''Verdana'',''sans-serif''; mso-bidi-font-family: Arial;">&nbsp;</span></span></p>\n<p><span style="font-size: 8.5pt; font-family: ''Verdana'',''sans-serif''; mso-bidi-font-family: Arial;">Mrs Linda Gan was born and educated in the U.K. She holds a Master''s degree in Child Development from the University of London and was an Assistant Professor in the Early Childhood and Special Needs Academic Group at the National Institute of Education for thirty years, training pre-school, primary and secondary school teachers.</span></p>\n<p><span style="font-size: 8.5pt; font-family: ''Verdana'',''sans-serif''; mso-bidi-font-family: Arial;">During that time she has been engaged as an English language consultant for developing and coordinating&nbsp; language arts curricula for the Ministry of Education - the Monolingual LEAP curriculum in 1985, the PAP Kindergarten syllabus in 1989, the MOE Kindergarten Framework in 2003 and most recently, in 2012, the new Social Studies Primary 4 materials, which are being introduced in schools in 2013.</span></p>\n<p>&nbsp;</p>\n<p><span style="font-size: 8.5pt; font-family: ''Verdana'',''sans-serif''; mso-bidi-font-family: Arial;">She has published a number of research articles on bilingual education, literacy and phonoligcial awareness in young children and written a wide range of story and vocabulary enrichment books which promote children''s&nbsp; English language skills.</span></p>', '<p>&nbsp;</p>\n<p><span style="font-size: 8.5pt; font-family: ''Verdana'',''sans-serif''; mso-bidi-font-family: Arial;">Young Scientists Reader Pte Ltd is established as an associate firm to the publishing firm Young Scientists Pte Ltd which produces the much-loved Young Scientists monthly magazine.<br /> <br /> Young Scientists Reader aims to promote the learning of Science among school children using exciting and engaging methods because it is getting ever more challenging to attract the focus of the younger generation of children in order for them to learn and retain the necessary information to excel in their Science studies.<br /> <br /> An innovative medium to deliver syllabus-based Science information that captivates the attention of school children is through the use of a comic-based magazine with stories and topics centred on a Lower Block or Upper Block Science subject. It is through the guiding principles of this medium that the Young Scientists magazine is borne.<br /> <br /> Young Scientists Reader strives to continuously work towards improving and maintaining the quality of the content so that your child will enjoy and benefit from the Young Scientists magazine.</span></p>', '<p class="MsoNormal"><span style="background-position: initial initial; background-repeat: initial initial;">Enrichment in Science content for Primary levels.&nbsp;</span></p>'),
(9, 48, NULL, '<p><span style="margin: 0px; padding: 0px; line-height: 18.8125px; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: small;">Singapore Asia Publishers Pte Ltd (SAP), founded in 1977, is a leading publishing group specializing in academic materials such as supplementary workbooks, guidebooks, textbooks, reference books, dictionaries and other educational materials.<br style="margin: 0px; padding: 0px;" /><br style="margin: 0px; padding: 0px;" /></span><span style="margin: 0px; padding: 0px; line-height: 18.8125px; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: small;">Under the impressionable imprints of SAP education, SAP Kids and teachers@work, we have, to date, published over 3000 titles, catering to students of all levels, as well as adults. Our books are distributed worldwide, covering the regions of Southeast Asia, Middle East, Europe and the United States of America.</span></p>\n<p>&nbsp;</p>', '<p><span style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: small; line-height: 18.8125px;">Our core operations are in Singapore, with overseas offices in China, Hong Kong, Malaysia and the Philippines. Singapore Asia Publishers Group continues to expand her presence globally. Our current distribution network spans across the globe which includes Australia, Brunei, Botswana, Cambodia, China, Eygpt, Ghana, Greece, Hong Kong, India, Indonesia, Korea, Malaysia, Maldives, Mauritius, Myanmar, New Zealand, Nigeria, Pakistan, Papua New Guinea, Philippines, Saudi Arabia, South Africa, Sri Lanka, Taiwan, Thailand, Trinidad and Tobago, United Arab Emirates, United Kingdom, United States of America and Vietnam.</span></p>', '<p><span style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: small; line-height: 18.8125px;">Singapore Asia Publishers Group is committed to producing educational publications worldwide and strives to achieve her vision to be the world&rsquo;s educational publisher.</span></p>'),
(10, 50, 'profile/170ccbe36ee2ac9ac452c02214439587.jpg', '<p><span style="color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">Technically skilled and accomplished IT Professional PHP Programmer. - View&nbsp;</span><span style="font-weight: bold; color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">Ravikanth Katkam''s</span><span style="color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">&nbsp;profile. Viadeo helps professionals like</span><span style="color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">Technically skilled and accomplished IT Professional PHP Programmer. - View&nbsp;</span><span style="font-weight: bold; color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">Ravikanth Katkam''s</span><span style="color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">&nbsp;profile. Viadeo helps professionals like</span></p>', '<p><span style="color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">Technically skilled and accomplished IT Professional PHP Programmer. - View&nbsp;</span><span style="font-weight: bold; color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">Ravikanth Katkam''s</span><span style="color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">&nbsp;profile. Viadeo helps professionals like</span><span style="color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">Technically skilled and accomplished IT Professional PHP Programmer. - View&nbsp;</span><span style="font-weight: bold; color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">Ravikanth Katkam''s</span><span style="color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">&nbsp;profile. Viadeo helps professionals like</span><span style="color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">Technically skilled and accomplished IT Professional PHP Programmer. - View&nbsp;</span><span style="font-weight: bold; color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">Ravikanth Katkam''s</span><span style="color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">&nbsp;profile. Viadeo helps professionals like</span></p>', '<p><span style="color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">Technically skilled and accomplished IT Professional PHP Programmer. - View&nbsp;</span><span style="font-weight: bold; color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">Ravikanth Katkam''s</span><span style="color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">&nbsp;profile. Viadeo helps professionals like</span><span style="color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">Technically skilled and accomplished IT Professional PHP Programmer. - View&nbsp;</span><span style="font-weight: bold; color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">Ravikanth Katkam''s</span><span style="color: #444444; font-family: arial, sans-serif; font-size: small; line-height: 16px;">&nbsp;profile. Viadeo helps professionals like</span></p>'),
(11, 60, 'profile/81800f5fe6d01c309c2bd0ab3d16891e.png', '<p>Certificate in Montessori Teaching&nbsp;</p>', '<p>More than 10 years of tutoring experience (Primary / Secondary)</p>\n<p>2 years of Phonics Teaching (Preschoolers)</p>', '<p>Montessori Method</p>');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
